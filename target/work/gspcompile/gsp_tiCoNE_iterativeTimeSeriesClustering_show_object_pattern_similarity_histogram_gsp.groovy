import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_iterativeTimeSeriesClustering_show_object_pattern_similarity_histogram_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/iterativeTimeSeriesClustering/_show_object_pattern_similarity_histogram.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
if(true && (pattern)) {
printHtmlPart(1)
expressionOut.print(request.contextPath)
printHtmlPart(2)
expressionOut.print(clustering.sessionId)
printHtmlPart(3)
expressionOut.print(iteration)
printHtmlPart(3)
expressionOut.print(bins)
printHtmlPart(3)
expressionOut.print(type)
printHtmlPart(4)
expressionOut.print(pattern.id)
printHtmlPart(5)
}
else {
printHtmlPart(1)
expressionOut.print(request.contextPath)
printHtmlPart(2)
expressionOut.print(clustering.sessionId)
printHtmlPart(3)
expressionOut.print(iteration)
printHtmlPart(3)
expressionOut.print(bins)
printHtmlPart(3)
expressionOut.print(type)
printHtmlPart(5)
}
printHtmlPart(6)
if(true && (type == "rss")) {
printHtmlPart(7)
}
else if(true && (type == "pearson")) {
printHtmlPart(8)
}
printHtmlPart(9)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1457014059000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
