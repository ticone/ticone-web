import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_timeSeriesDataSetfilter_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/timeSeriesDataSet/filter.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
if(true && (params.popup)) {
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main_popup")],-1)
printHtmlPart(1)
}
else {
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',9,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
}
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',11,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',15,['tag':("sidebarTitle")],2)
printHtmlPart(2)
createClosureForHtmlPart(4, 2)
invokeTag('content','g',18,['tag':("title")],2)
printHtmlPart(2)
if(true && (!params.popup)) {
printHtmlPart(5)
createClosureForHtmlPart(6, 3)
invokeTag('content','g',20,['tag':("sidebarClass")],3)
printHtmlPart(5)
createClosureForHtmlPart(7, 3)
invokeTag('content','g',21,['tag':("mainClass")],3)
printHtmlPart(2)
}
printHtmlPart(2)
createClosureForHtmlPart(8, 2)
invokeTag('content','g',25,['tag':("sidebar")],2)
printHtmlPart(9)
invokeTag('image','asset',34,['src':("ajax-loader.gif")],-1)
printHtmlPart(10)
createTagBody(2, {->
printHtmlPart(5)
if(true && (params.popup)) {
printHtmlPart(11)
invokeTag('hiddenField','g',42,['name':("popup"),'value':("1")],-1)
printHtmlPart(5)
}
printHtmlPart(12)
invokeTag('select','g',50,['from':(dataSets),'optionKey':("id"),'optionValue':("name"),'value':(params.id),'class':("form-control"),'name':("tsDataSet")],-1)
printHtmlPart(13)
invokeTag('field','g',62,['type':("text"),'id':("name"),'name':("name"),'class':("form-control")],-1)
printHtmlPart(14)
invokeTag('select','g',68,['name':("dataFilter"),'from':(dataFilters),'optionKey':("id"),'optionValue':("name"),'class':("form-control")],-1)
printHtmlPart(15)
invokeTag('select','g',80,['name':("clusteringMethod"),'from':(clusteringMethods),'optionKey':("id"),'optionValue':("name"),'class':("form-control")],-1)
printHtmlPart(16)
invokeTag('field','g',87,['type':("number"),'min':("1"),'max':("100"),'value':("10"),'id':("steps"),'name':("steps"),'class':("form-control")],-1)
printHtmlPart(17)
invokeTag('select','g',93,['name':("simFunction"),'from':(simFunctions),'optionKey':("id"),'optionValue':("name"),'class':("form-control")],-1)
printHtmlPart(18)
invokeTag('select','g',99,['name':("patternRefinementFunction"),'from':(refFunctions),'optionKey':("id"),'optionValue':("name"),'class':("form-control")],-1)
printHtmlPart(19)
invokeTag('field','g',105,['type':("number"),'value':("1"),'id':("randomSeed"),'name':("randomSeed"),'class':("form-control")],-1)
printHtmlPart(20)
invokeTag('field','g',111,['type':("number"),'value':("1000"),'id':("permutations"),'name':("permutations"),'class':("form-control")],-1)
printHtmlPart(21)
invokeTag('submitButton','g',116,['name':("next")],-1)
printHtmlPart(2)
})
invokeTag('uploadForm','g',117,['action':("filter"),'params':([step: 2]),'class':("form-horizontal")],2)
printHtmlPart(1)
})
invokeTag('captureBody','sitemesh',118,[:],1)
printHtmlPart(22)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1458215546000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
