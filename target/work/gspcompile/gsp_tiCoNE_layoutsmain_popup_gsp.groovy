import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_layoutsmain_popup_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/layouts/main_popup.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',8,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("Content-Type"),'content':("text/html; charset=UTF-8")],-1)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',9,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("X-UA-Compatible"),'content':("IE=edge,chrome=1")],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('layoutTitle','g',10,['default':("${grails.util.Holders.config.grails.project.groupId} - Time Course Network Enricher")],-1)
})
invokeTag('captureTitle','sitemesh',10,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',10,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',11,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("viewport"),'content':("width=device-width, initial-scale=1.0")],-1)
printHtmlPart(2)
invokeTag('stylesheet','asset',12,['src':("application.css")],-1)
printHtmlPart(1)
invokeTag('javascript','asset',13,['src':("application.js")],-1)
printHtmlPart(3)
invokeTag('layoutHead','g',42,[:],-1)
printHtmlPart(1)
invokeTag('layoutResources','r',43,[:],-1)
printHtmlPart(4)
})
invokeTag('captureHead','sitemesh',44,[:],1)
printHtmlPart(4)
createTagBody(1, {->
printHtmlPart(5)
invokeTag('pageProperty','g',51,['name':("page.sidebarTitle")],-1)
printHtmlPart(6)
invokeTag('pageProperty','g',53,['name':("page.sidebar")],-1)
printHtmlPart(7)
invokeTag('pageProperty','g',59,['name':("page.title")],-1)
printHtmlPart(8)
invokeTag('layoutBody','g',61,[:],-1)
printHtmlPart(9)
invokeTag('layoutResources','r',62,[:],-1)
printHtmlPart(10)
})
invokeTag('captureBody','sitemesh',67,[:],1)
printHtmlPart(11)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1455878957000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
