import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_layoutsmain_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/layouts/main.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',8,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("Content-Type"),'content':("text/html; charset=UTF-8")],-1)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',9,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("X-UA-Compatible"),'content':("IE=edge,chrome=1")],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('layoutTitle','g',10,['default':("${grails.util.Holders.config.grails.project.groupId} - Time Course Network Enricher")],-1)
})
invokeTag('captureTitle','sitemesh',10,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',10,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',11,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("viewport"),'content':("width=device-width, initial-scale=1.0")],-1)
printHtmlPart(2)
invokeTag('assetPath','g',12,['src':("ticone_logo_icon.png")],-1)
printHtmlPart(3)
invokeTag('stylesheet','asset',13,['src':("application.css")],-1)
printHtmlPart(1)
invokeTag('javascript','asset',14,['src':("application.js")],-1)
printHtmlPart(4)
invokeTag('image','asset',53,['src':("ajax-loader.gif")],-1)
printHtmlPart(5)
invokeTag('layoutHead','g',174,[:],-1)
printHtmlPart(6)
})
invokeTag('captureHead','sitemesh',175,[:],1)
printHtmlPart(6)
createTagBody(1, {->
printHtmlPart(7)
expressionOut.print(createLink(controller: 'welcome', action: 'index'))
printHtmlPart(8)
expressionOut.print(createLink(controller: 'welcome', action: 'index'))
printHtmlPart(9)
invokeTag('img','g',191,['height':("36px"),'alt':(grails.util.Holders.config.grails.project.groupId),'file':("ticone_logo_square.png")],-1)
printHtmlPart(10)
invokeTag('img','g',192,['height':("36px"),'alt':(grails.util.Holders.config.grails.project.groupId),'file':("ticone_logo_3.png")],-1)
printHtmlPart(11)
createClosureForHtmlPart(12, 2)
invokeTag('link','g',200,['controller':("download"),'action':("index")],2)
printHtmlPart(13)
expressionOut.print(createLink(controller: 'download', action: 'index', fragment: 'cyto'))
printHtmlPart(14)
invokeTag('image','asset',202,['src':("cytoscape_logo_large_cropped.png"),'width':("16")],-1)
printHtmlPart(15)
expressionOut.print(createLink(controller: 'download', action: 'index', fragment: 'stand'))
printHtmlPart(16)
expressionOut.print(createLink(controller: 'iterativeTimeSeriesClustering', action: 'create'))
printHtmlPart(17)
expressionOut.print(createLink(controller: 'iterativeTimeSeriesClustering', action: 'create'))
printHtmlPart(18)
expressionOut.print(createLink(controller: 'timeSeriesDataSet', action: 'index'))
printHtmlPart(19)
expressionOut.print(createLink(controller: 'graph', action: 'index'))
printHtmlPart(20)
createClosureForHtmlPart(21, 2)
invokeTag('link','g',220,['controller':("tutorial"),'action':("index")],2)
printHtmlPart(22)
createClosureForHtmlPart(23, 2)
invokeTag('link','g',221,['controller':("citation"),'action':("index")],2)
printHtmlPart(24)
expressionOut.print(createLink(controller: 'about', action: 'index'))
printHtmlPart(25)
createTagBody(2, {->
printHtmlPart(26)
expressionOut.print(createLink(controller: 'signup', action: 'index'))
printHtmlPart(27)
createTagBody(3, {->
printHtmlPart(28)
invokeTag('checkBox','g',239,['name':("rememberMe")],-1)
printHtmlPart(29)
invokeTag('hiddenField','g',241,['name':("targetUri"),'value':(request.forwardURI - request.contextPath)],-1)
printHtmlPart(30)
})
invokeTag('form','g',243,['class':("form"),'controller':("auth"),'action':("signIn")],3)
printHtmlPart(31)
})
invokeTag('isNotLoggedIn','shiro',247,[:],2)
printHtmlPart(32)
createTagBody(2, {->
printHtmlPart(33)
invokeTag('principal','shiro',250,[:],-1)
printHtmlPart(34)
expressionOut.print(createLink(controller: 'user', action: 'show'))
printHtmlPart(35)
expressionOut.print(createLink(controller: 'iterativeTimeSeriesClustering', action: 'your'))
printHtmlPart(36)
expressionOut.print(createLink(controller: 'auth', action: 'signOut', params: [targetUri: request.forwardURI - request.contextPath]))
printHtmlPart(37)
})
invokeTag('isLoggedIn','shiro',258,[:],2)
printHtmlPart(38)
invokeTag('pageProperty','g',266,['name':("page.topRow")],-1)
printHtmlPart(39)
if(true && (!pageProperty(name:'page.sidebarHide').equals( 'true' ))) {
printHtmlPart(40)
if(true && (pageProperty(name:'page.hideGraph').equals("true"))) {
printHtmlPart(41)
}
else if(true && (pageProperty(name:'page.sidebarClass') != "")) {
printHtmlPart(42)
expressionOut.print(pageProperty(name:'page.sidebarClass'))
printHtmlPart(43)
}
else {
printHtmlPart(44)
}
printHtmlPart(45)
if(true && (!pageProperty(name:'page.hideGraph').equals("true") && !pageProperty(name: 'page.preventLayoutResize').equals("true"))) {
printHtmlPart(46)
}
printHtmlPart(45)
if(true && (pageProperty(name:'page.sidebarSimple').equals( 'true' ))) {
printHtmlPart(47)
invokeTag('pageProperty','g',289,['name':("page.sidebar")],-1)
printHtmlPart(32)
}
else {
printHtmlPart(48)
if(true && (!pageProperty(name:'page.sidebarPanelHide').equals( 'true' ))) {
printHtmlPart(49)
invokeTag('pageProperty','g',294,['name':("page.sidebarTitle")],-1)
printHtmlPart(50)
invokeTag('pageProperty','g',296,['name':("page.sidebar")],-1)
printHtmlPart(51)
}
printHtmlPart(32)
}
printHtmlPart(52)
if(true && (!pageProperty(name:'page.hideGraph').equals("true"))) {
printHtmlPart(53)
if(true && (pageProperty(name:'page.mainClass') != "")) {
printHtmlPart(54)
expressionOut.print(pageProperty(name:'page.mainClass'))
printHtmlPart(55)
}
else {
printHtmlPart(56)
}
printHtmlPart(57)
}
printHtmlPart(58)
}
else {
printHtmlPart(40)
if(true && (pageProperty(name:'page.mainClass') != "")) {
printHtmlPart(59)
expressionOut.print(pageProperty(name:'page.mainClass'))
printHtmlPart(43)
}
else {
printHtmlPart(60)
}
printHtmlPart(58)
}
printHtmlPart(61)
if(true && (!pageProperty(name:'page.header').equals( '' ))) {
printHtmlPart(62)
invokeTag('pageProperty','g',321,['name':("page.header")],-1)
printHtmlPart(63)
}
else if(true && (!pageProperty(name:'page.title').equals( '' ))) {
printHtmlPart(64)
invokeTag('pageProperty','g',325,['name':("page.title")],-1)
printHtmlPart(65)
}
printHtmlPart(1)
invokeTag('layoutBody','g',328,[:],-1)
printHtmlPart(1)
invokeTag('layoutResources','r',329,[:],-1)
printHtmlPart(66)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(67)
createTagBody(2, {->
invokeTag('meta','g',339,['name':("app.version")],-1)
})
invokeTag('link','g',339,['controller':("changelog"),'action':("index"),'fragment':(grails.util.Holders.grailsApplication.metadata['app.version'].replaceAll('\\.',''))],2)
printHtmlPart(68)
invokeTag('meta','g',339,['name':("app.grails.version")],-1)
printHtmlPart(69)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(70)
invokeTag('meta','g',340,['name':("app.ticone_lib.version")],-1)
printHtmlPart(71)
})
invokeTag('captureBody','sitemesh',343,[:],1)
printHtmlPart(72)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1488283951000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
