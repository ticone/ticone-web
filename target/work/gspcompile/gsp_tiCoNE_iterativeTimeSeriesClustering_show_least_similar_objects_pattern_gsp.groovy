import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_iterativeTimeSeriesClustering_show_least_similar_objects_pattern_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/iterativeTimeSeriesClustering/_show_least_similar_objects_pattern.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(request.contextPath)
printHtmlPart(1)
expressionOut.print(clustering.sessionId)
printHtmlPart(2)
expressionOut.print(iteration)
printHtmlPart(2)
expressionOut.print(pattern)
printHtmlPart(2)
expressionOut.print(percentage)
printHtmlPart(2)
expressionOut.print(type)
printHtmlPart(3)
if(true && (type == "percentage")) {
printHtmlPart(4)
expressionOut.print(params.percentage)
printHtmlPart(5)
expressionOut.print(p.name)
printHtmlPart(6)
}
else if(true && (type == "rss")) {
printHtmlPart(7)
expressionOut.print(params.percentage)
printHtmlPart(8)
expressionOut.print(p.name)
printHtmlPart(6)
}
else if(true && (type == "pearson")) {
printHtmlPart(9)
expressionOut.print(params.percentage)
printHtmlPart(8)
expressionOut.print(p.name)
printHtmlPart(6)
}
printHtmlPart(10)
expressionOut.print(params.pattern)
printHtmlPart(11)
expressionOut.print(params.percentage)
printHtmlPart(12)
expressionOut.print(params.pattern)
printHtmlPart(13)
expressionOut.print(request.contextPath)
printHtmlPart(14)
expressionOut.print(params.id)
printHtmlPart(2)
expressionOut.print(params.iteration)
printHtmlPart(2)
expressionOut.print(params.pattern)
printHtmlPart(2)
expressionOut.print(params.percentage)
printHtmlPart(2)
expressionOut.print(type)
printHtmlPart(15)
invokeTag('submitButton','g',96,['name':("deletePatternLeastSimilarObjects"),'value':("Delete"),'onclick':("deletePatternLeastSimilarObjects();")],-1)
printHtmlPart(16)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1456512449000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
