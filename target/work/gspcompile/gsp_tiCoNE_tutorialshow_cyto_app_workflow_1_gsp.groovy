import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_tutorialshow_cyto_app_workflow_1_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/tutorial/show_cyto_app_workflow_1.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("tutorial")],-1)
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',9,['tag':("title")],2)
printHtmlPart(4)
createTagBody(2, {->
printHtmlPart(5)
invokeTag('image','asset',29,['src':("control_panel.png")],-1)
printHtmlPart(6)
expressionOut.print(assetPath(src: "cytoscape_toy_network.txt"))
printHtmlPart(7)
invokeTag('image','asset',51,['src':("load_network.png")],-1)
printHtmlPart(8)
invokeTag('image','asset',61,['src':("network.png")],-1)
printHtmlPart(9)
expressionOut.print(assetPath(src: "cytoscape_toy_dataset.txt"))
printHtmlPart(10)
invokeTag('image','asset',81,['src':("load_table.png")],-1)
printHtmlPart(11)
invokeTag('image','asset',89,['src':("table_selector.png")],-1)
printHtmlPart(12)
invokeTag('image','asset',104,['src':("settings.png")],-1)
printHtmlPart(13)
invokeTag('image','asset',155,['src':("graphs.png")],-1)
printHtmlPart(14)
invokeTag('image','asset',208,['src':("delete_pattern_options.png")],-1)
printHtmlPart(15)
invokeTag('image','asset',216,['src':("pattern_least_fitting_objects.png")],-1)
printHtmlPart(16)
invokeTag('image','asset',240,['src':("splitpattern_least_similar.png")],-1)
printHtmlPart(17)
invokeTag('image','asset',254,['src':("split_pattern_accept.png")],-1)
printHtmlPart(18)
invokeTag('image','asset',265,['src':("splitted_pattern_action.png")],-1)
printHtmlPart(19)
invokeTag('image','asset',283,['src':("least_fitting_objects.png")],-1)
printHtmlPart(20)
invokeTag('image','asset',294,['src':("suggested_patterns.png")],-1)
printHtmlPart(21)
invokeTag('image','asset',314,['src':("pattern_history.png")],-1)
printHtmlPart(22)
invokeTag('image','asset',336,['src':("lucky_iterations.png")],-1)
printHtmlPart(23)
invokeTag('image','asset',355,['src':("visualized_network.png")],-1)
printHtmlPart(24)
invokeTag('image','asset',369,['src':("kpm_result_table.png")],-1)
printHtmlPart(25)
invokeTag('image','asset',388,['src':("kpm_selection.png")],-1)
printHtmlPart(26)
})
invokeTag('content','g',394,['tag':("tutorialContent")],2)
printHtmlPart(1)
})
invokeTag('captureBody','sitemesh',395,[:],1)
printHtmlPart(27)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1473788716000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
