import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_tutorialshow_cyto_app_workflow_cluster_connectivity_2_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/tutorial/show_cyto_app_workflow_cluster_connectivity_2.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("tutorial")],-1)
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',9,['tag':("title")],2)
printHtmlPart(4)
createTagBody(2, {->
printHtmlPart(5)
expressionOut.print(assetPath(src: "qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5-seed42-samePatternEdgeProb0.4.txt"))
printHtmlPart(6)
invokeTag('image','asset',26,['src':("load_network.png")],-1)
printHtmlPart(7)
invokeTag('image','asset',36,['src':("network.png")],-1)
printHtmlPart(8)
expressionOut.print(assetPath(src: "qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5.txt"))
printHtmlPart(9)
invokeTag('image','asset',56,['src':("load_table.png")],-1)
printHtmlPart(10)
invokeTag('image','asset',64,['src':("table_selector.png")],-1)
printHtmlPart(11)
invokeTag('image','asset',79,['src':("workflow4_1.3.77_input.png")],-1)
printHtmlPart(12)
invokeTag('image','asset',97,['src':("workflow4_1.3.77_iteration1_clusters.png")],-1)
printHtmlPart(13)
invokeTag('image','asset',106,['src':("workflow4_1.3.77_iteration5_clusters.png")],-1)
printHtmlPart(14)
invokeTag('image','asset',115,['src':("workflow4_1.3.77_iteration11_clusters.png")],-1)
printHtmlPart(15)
invokeTag('image','asset',131,['src':("calculate_cluster_pvalues_1.3.77.png")],-1)
printHtmlPart(16)
invokeTag('image','asset',139,['src':("workflow4_cluster_pvalues.png")],-1)
printHtmlPart(17)
invokeTag('image','asset',154,['src':("filterclusters_1.3.77.png")],-1)
printHtmlPart(18)
invokeTag('image','asset',162,['src':("workflow4_clusters_filtered.png")],-1)
printHtmlPart(19)
invokeTag('image','asset',182,['src':("workflow4_click_connectivity_tab.png")],-1)
printHtmlPart(20)
invokeTag('image','asset',190,['src':("workflow4_1.3.77_connectivity_setup.png")],-1)
printHtmlPart(21)
invokeTag('image','asset',237,['src':("workflow4_1.3.77_connectivity_result.png")],-1)
printHtmlPart(22)
})
invokeTag('content','g',282,['tag':("tutorialContent")],2)
printHtmlPart(1)
})
invokeTag('captureBody','sitemesh',283,[:],1)
printHtmlPart(23)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1496156946000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
