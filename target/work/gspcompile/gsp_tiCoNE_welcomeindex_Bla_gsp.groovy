import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_welcomeindex_Bla_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/welcome/index_Bla.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',8,['tag':("sidebarTitle")],2)
printHtmlPart(2)
createClosureForHtmlPart(4, 2)
invokeTag('content','g',13,['tag':("sidebar")],2)
printHtmlPart(2)
createTagBody(2, {->
printHtmlPart(5)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(6)
})
invokeTag('content','g',14,['tag':("title")],2)
printHtmlPart(7)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(8)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(9)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(10)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(11)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(12)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(13)
expressionOut.print(createLink(controller: 'iterativeTimeSeriesClustering', action: 'create'))
printHtmlPart(14)
})
invokeTag('captureBody','sitemesh',67,[:],1)
printHtmlPart(15)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1453819338000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
