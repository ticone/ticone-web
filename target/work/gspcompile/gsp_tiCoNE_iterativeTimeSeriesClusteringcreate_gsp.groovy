import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_iterativeTimeSeriesClusteringcreate_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/iterativeTimeSeriesClustering/create.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
expressionOut.print(createLink(controller: 'timeSeriesDataSet', action: 'filter', id: '%s', params: [popup: 1]))
printHtmlPart(3)
expressionOut.print(createLink(controller: 'iterativeTimeSeriesClustering', action: 'dataSetsAsJSON', params: [inGroups: true]))
printHtmlPart(4)
expressionOut.print(createLink(controller: 'iterativeTimeSeriesClustering', action: 'networksAsJSON', params: [inGroups: true]))
printHtmlPart(5)
})
invokeTag('captureHead','sitemesh',124,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(6)
createClosureForHtmlPart(7, 2)
invokeTag('content','g',128,['tag':("sidebarTitle")],2)
printHtmlPart(6)
createClosureForHtmlPart(8, 2)
invokeTag('content','g',131,['tag':("title")],2)
printHtmlPart(6)
createClosureForHtmlPart(9, 2)
invokeTag('content','g',132,['tag':("sidebarClass")],2)
printHtmlPart(6)
createClosureForHtmlPart(10, 2)
invokeTag('content','g',133,['tag':("mainClass")],2)
printHtmlPart(6)
createClosureForHtmlPart(11, 2)
invokeTag('content','g',141,['tag':("sidebar")],2)
printHtmlPart(12)
invokeTag('image','asset',150,['src':("ajax-loader.gif")],-1)
printHtmlPart(13)
if(true && (flash.error)) {
printHtmlPart(14)
expressionOut.print(flash.error)
printHtmlPart(15)
}
printHtmlPart(6)
createTagBody(2, {->
printHtmlPart(16)
expressionOut.print(createLink(controller: 'timeSeriesDataSet', action: 'create', params: [popup: 1]))
printHtmlPart(17)
expressionOut.print(createLink(controller: 'graph', action: 'create', params: [popup: 1]))
printHtmlPart(18)
invokeTag('field','g',216,['type':("number"),'min':("2"),'max':("1000"),'value':("10"),'id':("numberPatterns"),'name':("numberPatterns"),'class':("form-control")],-1)
printHtmlPart(19)
invokeTag('select','g',228,['name':("clusteringMethod"),'from':(clusteringMethods),'optionKey':("id"),'optionValue':("name"),'class':("form-control")],-1)
printHtmlPart(20)
invokeTag('field','g',235,['type':("number"),'min':("1"),'max':("100"),'value':("10"),'id':("steps"),'name':("steps"),'class':("form-control")],-1)
printHtmlPart(21)
invokeTag('select','g',241,['name':("simFunction"),'from':(simFunctions),'optionKey':("id"),'optionValue':("name"),'class':("form-control")],-1)
printHtmlPart(22)
invokeTag('field','g',247,['type':("checkbox"),'id':("objectIntersect"),'name':("objectIntersect"),'class':("form-control"),'checked':("true")],-1)
printHtmlPart(23)
invokeTag('select','g',253,['name':("patternRefinementFunction"),'from':(refFunctions),'optionKey':("id"),'optionValue':("name"),'class':("form-control")],-1)
printHtmlPart(24)
invokeTag('field','g',259,['type':("number"),'value':(System.currentTimeMillis()),'id':("randomSeed"),'name':("randomSeed"),'class':("form-control")],-1)
printHtmlPart(25)
invokeTag('field','g',265,['type':("number"),'value':("0"),'id':("permutations"),'name':("permutations"),'class':("form-control")],-1)
printHtmlPart(26)
invokeTag('field','g',271,['type':("number"),'value':("100"),'id':("refinementPermutations"),'name':("refinementPermutations"),'class':("form-control")],-1)
printHtmlPart(27)
invokeTag('submitButton','g',276,['name':("submit")],-1)
printHtmlPart(6)
})
invokeTag('uploadForm','g',277,['action':("async_upload"),'class':("form-horizontal")],2)
printHtmlPart(1)
})
invokeTag('captureBody','sitemesh',278,[:],1)
printHtmlPart(28)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1460035906000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
