import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_timeSeriesDataSetfilter_least_conserved_object_sets_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/timeSeriesDataSet/filter_least_conserved_object_sets.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',14,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(3)
createClosureForHtmlPart(4, 2)
invokeTag('content','g',18,['tag':("sidebarTitle")],2)
printHtmlPart(3)
createClosureForHtmlPart(5, 2)
invokeTag('content','g',21,['tag':("title")],2)
printHtmlPart(3)
createClosureForHtmlPart(6, 2)
invokeTag('content','g',22,['tag':("sidebarClass")],2)
printHtmlPart(3)
createClosureForHtmlPart(7, 2)
invokeTag('content','g',23,['tag':("mainClass")],2)
printHtmlPart(3)
createClosureForHtmlPart(4, 2)
invokeTag('content','g',26,['tag':("sidebar")],2)
printHtmlPart(8)
invokeTag('image','asset',35,['src':("ajax-loader.gif")],-1)
printHtmlPart(9)
createTagBody(2, {->
printHtmlPart(10)
if(true && (params.popup)) {
printHtmlPart(11)
invokeTag('hiddenField','g',43,['name':("popup"),'value':("1")],-1)
printHtmlPart(10)
}
printHtmlPart(12)
invokeTag('field','g',52,['type':("number"),'min':("1"),'max':("100"),'value':("10"),'id':("percent"),'name':("percent"),'class':("form-control")],-1)
printHtmlPart(13)
invokeTag('select','g',58,['name':("simFunction"),'from':(simFunctions),'optionKey':("id"),'optionValue':("name"),'class':("form-control")],-1)
printHtmlPart(14)
invokeTag('hiddenField','g',59,['name':("name"),'value':(params.name)],-1)
printHtmlPart(14)
invokeTag('hiddenField','g',60,['name':("dataFilter"),'value':(params.dataFilter)],-1)
printHtmlPart(14)
invokeTag('hiddenField','g',61,['name':("tsDataSet"),'value':(params.tsDataSet)],-1)
printHtmlPart(15)
invokeTag('submitButton','g',66,['name':("Submit")],-1)
printHtmlPart(3)
})
invokeTag('uploadForm','g',67,['action':("do_filter"),'class':("form-horizontal")],2)
printHtmlPart(1)
})
invokeTag('captureBody','sitemesh',68,[:],1)
printHtmlPart(16)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1458215653000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
