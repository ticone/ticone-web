import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_citationindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/citation/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',8,['tag':("preventLayoutResize")],2)
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',9,['tag':("sidebarPanelHide")],2)
printHtmlPart(2)
createClosureForHtmlPart(4, 2)
invokeTag('content','g',10,['tag':("sidebarClass")],2)
printHtmlPart(2)
createClosureForHtmlPart(5, 2)
invokeTag('content','g',11,['tag':("mainClass")],2)
printHtmlPart(2)
createClosureForHtmlPart(6, 2)
invokeTag('content','g',12,['tag':("title")],2)
printHtmlPart(7)
expressionOut.print(assetPath(src: "paper1_synthetic_ts_data.tsv"))
printHtmlPart(8)
expressionOut.print(assetPath(src: "paper1_synthetic_network.tsv"))
printHtmlPart(9)
expressionOut.print(assetPath(src: "paper1_cy_session.cys"))
printHtmlPart(10)
})
invokeTag('captureBody','sitemesh',28,[:],1)
printHtmlPart(11)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1488200157000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
