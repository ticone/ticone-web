import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_timeSeriesClusteringVisualizationshow_vivagraph_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/timeSeriesClusteringVisualization/show_vivagraph.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',4,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',4,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(3)
for( color in (vis.patternColors) ) {
printHtmlPart(4)
expressionOut.print(color)
printHtmlPart(5)
}
printHtmlPart(6)
for( p in (patterns) ) {
printHtmlPart(4)
expressionOut.print(p.name)
printHtmlPart(5)
}
printHtmlPart(7)
for( node in (vis.graph.nodes) ) {
if(true && (node.coefficients.size() > 0)) {
printHtmlPart(4)
expressionOut.print(node.id)
printHtmlPart(5)
}
}
printHtmlPart(8)
for( edge in (vis.graph.edges) ) {
printHtmlPart(9)
expressionOut.print(edge.n1.id)
printHtmlPart(10)
expressionOut.print(edge.n2.id)
printHtmlPart(11)
expressionOut.print(edge.n1.id)
printHtmlPart(12)
expressionOut.print(edge.n2.id)
printHtmlPart(13)
expressionOut.print(edge.n1.id)
printHtmlPart(12)
expressionOut.print(edge.n2.id)
printHtmlPart(14)
}
printHtmlPart(15)
expressionOut.print(createLink(controller: 'timeSeriesClusteringVisualization', action: 'save_layout', id: vis.id))
printHtmlPart(16)
for( node in (vis.graph.nodes) ) {
printHtmlPart(17)
if(true && (true)) {
printHtmlPart(18)
expressionOut.print(node.coefficients)
printHtmlPart(19)
expressionOut.print(node.name)
printHtmlPart(20)
for( pattern in (node.coefficients) ) {
printHtmlPart(4)
expressionOut.print(pattern.key)
printHtmlPart(21)
expressionOut.print((int)((Double.valueOf(pattern.value)-vis.graph.minCoeff)/(vis.graph.maxCoeff-vis.graph.minCoeff)*100))
printHtmlPart(22)
}
printHtmlPart(23)
expressionOut.print(node.id)
printHtmlPart(24)
}
printHtmlPart(25)
}
printHtmlPart(26)
for( edge in (vis.graph.edges) ) {
printHtmlPart(27)
expressionOut.print(edge.n1.id)
printHtmlPart(28)
expressionOut.print(edge.n2.id)
printHtmlPart(29)
expressionOut.print(edge.n1.id)
printHtmlPart(12)
expressionOut.print(edge.n2.id)
printHtmlPart(30)
}
printHtmlPart(31)
for( nodePos in (layout.locations) ) {
printHtmlPart(32)
expressionOut.print(nodePos.node.id)
printHtmlPart(33)
expressionOut.print(nodePos.x)
printHtmlPart(33)
expressionOut.print(nodePos.y)
printHtmlPart(34)
}
printHtmlPart(35)
expressionOut.print(layout.transformMatrix)
printHtmlPart(36)
})
invokeTag('captureHead','sitemesh',259,[:],1)
printHtmlPart(37)
createTagBody(1, {->
printHtmlPart(38)
createTagBody(2, {->
printHtmlPart(38)
if(true && (selectedPattern == i)) {
printHtmlPart(39)
invokeTag('radio','g',263,['name':("myGroup"),'value':("-1"),'checked':("true"),'onclick':("SubmitFrm();")],-1)
printHtmlPart(40)
}
else {
printHtmlPart(39)
invokeTag('radio','g',266,['name':("myGroup"),'value':("-1"),'onclick':("SubmitFrm();")],-1)
printHtmlPart(40)
}
printHtmlPart(38)

i=0

printHtmlPart(41)
for( _it417816833 in (patterns) ) {
changeItVariable(_it417816833)
printHtmlPart(42)
if(true && (selectedPattern == i)) {
printHtmlPart(25)
invokeTag('radio','g',271,['name':("myGroup"),'value':(i),'onclick':("SubmitFrm();"),'checked':("true")],-1)
expressionOut.print(patterns[i].name)
printHtmlPart(43)
expressionOut.print(i)
printHtmlPart(44)
}
else {
printHtmlPart(25)
invokeTag('radio','g',274,['name':("myGroup"),'value':(i),'onclick':("SubmitFrm();")],-1)
expressionOut.print(patterns[i].name)
printHtmlPart(43)
expressionOut.print(i)
printHtmlPart(44)
}
printHtmlPart(42)
createTagBody(4, {->
printHtmlPart(45)
expressionOut.print(i)
printHtmlPart(46)

j=0

printHtmlPart(47)
for( _it2033490785 in (patterns[i].timeSeriesPattern) ) {
changeItVariable(_it2033490785)
printHtmlPart(48)
expressionOut.print(i)
printHtmlPart(49)
expressionOut.print(j)
printHtmlPart(33)
expressionOut.print(it)
printHtmlPart(50)

j=j+1

printHtmlPart(47)
}
printHtmlPart(51)
expressionOut.print(i)
printHtmlPart(52)
expressionOut.print(i)
printHtmlPart(53)
expressionOut.print(i)
printHtmlPart(54)
})
invokeTag('javascript','g',291,[:],4)
printHtmlPart(42)

i=i+1

printHtmlPart(41)
}
printHtmlPart(55)
invokeTag('textField','g',295,['name':("neighborhood"),'value':("1")],-1)
printHtmlPart(41)
invokeTag('submitButton','g',296,['name':("neighborhoodBtn"),'value':("Update Neighborhood"),'onclick':("SubmitFrm();")],-1)
printHtmlPart(56)
invokeTag('submitButton','g',298,['name':("toggleRender"),'value':("Toggle Layouting"),'onclick':("ToggleRender();")],-1)
printHtmlPart(56)
invokeTag('submitButton','g',300,['name':("saveLayout"),'value':("Save Graph Layout"),'onclick':("saveLayout();")],-1)
printHtmlPart(41)
})
invokeTag('content','g',301,['tag':("sidebar")],2)
printHtmlPart(57)
})
invokeTag('captureBody','sitemesh',303,[:],1)
printHtmlPart(58)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1455449109000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
