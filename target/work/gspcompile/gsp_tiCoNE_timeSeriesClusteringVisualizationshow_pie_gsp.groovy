import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_timeSeriesClusteringVisualizationshow_pie_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/timeSeriesClusteringVisualization/show_pie.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',3,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',3,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(3)

i = 0

printHtmlPart(4)
for( _it1943764464 in (patterns) ) {
changeItVariable(_it1943764464)
printHtmlPart(5)
expressionOut.print(i+1)
printHtmlPart(6)
expressionOut.print(vis.patternColors[i])
printHtmlPart(7)
expressionOut.print(i+1)
printHtmlPart(8)
expressionOut.print(it.name)
printHtmlPart(9)

i = i + 1

printHtmlPart(10)
}
printHtmlPart(11)
expressionOut.print(vis.graph.id)
printHtmlPart(12)
})
invokeTag('captureHead','sitemesh',150,[:],1)
printHtmlPart(13)
createClosureForHtmlPart(14, 1)
invokeTag('captureBody','sitemesh',153,[:],1)
printHtmlPart(15)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1424784886000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
