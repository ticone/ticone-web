import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_tutorialshow_cyto_app_workflow_clustering_comparison_2_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/tutorial/show_cyto_app_workflow_clustering_comparison_2.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("tutorial")],-1)
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',9,['tag':("title")],2)
printHtmlPart(4)
createTagBody(2, {->
printHtmlPart(5)
expressionOut.print(assetPath(src: "qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5.txt"))
printHtmlPart(6)
expressionOut.print(assetPath(src: "qual-patterns3-objects80-seed43-bg120-samples2-conftrue-tp5.txt"))
printHtmlPart(7)
invokeTag('image','asset',29,['src':("load_table.png")],-1)
printHtmlPart(8)
invokeTag('image','asset',45,['src':("workflow4_input.png")],-1)
printHtmlPart(9)
invokeTag('image','asset',55,['src':("workflow4_iteration1_clusters.png")],-1)
printHtmlPart(10)
invokeTag('image','asset',64,['src':("workflow4_iteration9_clusters.png")],-1)
printHtmlPart(11)
invokeTag('image','asset',73,['src':("workflow4_iteration12_clusters.png")],-1)
printHtmlPart(12)
invokeTag('image','asset',90,['src':("workflow5_input_ds2.png")],-1)
printHtmlPart(9)
invokeTag('image','asset',100,['src':("workflow5_iteration1_clusters.png")],-1)
printHtmlPart(13)
invokeTag('image','asset',109,['src':("workflow5_iteration4_clusters.png")],-1)
printHtmlPart(14)
invokeTag('image','asset',118,['src':("workflow5_iteration7_clusters.png")],-1)
printHtmlPart(15)
invokeTag('image','asset',136,['src':("workflow5_open_clustering_comparison.png")],-1)
printHtmlPart(16)
invokeTag('image','asset',144,['src':("workflow5_comparison_setup.png")],-1)
printHtmlPart(17)
invokeTag('image','asset',204,['src':("workflow5_comparison_result.png")],-1)
printHtmlPart(18)
invokeTag('image','asset',250,['src':("workflow5_comparison_result_sorted.png")],-1)
printHtmlPart(19)
})
invokeTag('content','g',256,['tag':("tutorialContent")],2)
printHtmlPart(1)
})
invokeTag('captureBody','sitemesh',257,[:],1)
printHtmlPart(20)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1475165811000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
