import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_iterativeTimeSeriesClusteringyour_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/iterativeTimeSeriesClustering/your.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',10,['tag':("sidebarTitle")],2)
printHtmlPart(2)
createClosureForHtmlPart(4, 2)
invokeTag('content','g',13,['tag':("title")],2)
printHtmlPart(2)
createClosureForHtmlPart(5, 2)
invokeTag('content','g',14,['tag':("sidebarClass")],2)
printHtmlPart(2)
createClosureForHtmlPart(6, 2)
invokeTag('content','g',15,['tag':("mainClass")],2)
printHtmlPart(2)
createClosureForHtmlPart(7, 2)
invokeTag('content','g',18,['tag':("sidebar")],2)
printHtmlPart(8)
for( clustering in (clusterings) ) {
printHtmlPart(9)
createTagBody(3, {->
expressionOut.print(clustering.sessionId)
})
invokeTag('link','g',35,['action':("show_cytoscape"),'id':(clustering.sessionId)],3)
printHtmlPart(10)
expressionOut.print(clustering.dataSet.name)
printHtmlPart(10)
expressionOut.print(clustering.graph != null ? clustering.graph.name : "---")
printHtmlPart(10)
expressionOut.print(clustering.clusteringMethod.name)
printHtmlPart(10)
expressionOut.print(clustering.similarityFunction.name)
printHtmlPart(10)
expressionOut.print(clustering.patternRefinementFunction.name)
printHtmlPart(10)
expressionOut.print(clustering.discretizationSteps)
printHtmlPart(10)
invokeTag('formatDate','g',42,['date':(clustering.dateCreated)],-1)
printHtmlPart(11)
}
printHtmlPart(12)
})
invokeTag('captureBody','sitemesh',46,[:],1)
printHtmlPart(13)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1455367570000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
