import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_timeSeriesDataSetcreate_popup_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/timeSeriesDataSet/create_popup.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main_popup")],-1)
printHtmlPart(2)
if(true && (user == null)) {
printHtmlPart(3)
}
printHtmlPart(4)
})
invokeTag('captureHead','sitemesh',17,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(5)
createClosureForHtmlPart(6, 2)
invokeTag('content','g',21,['tag':("title")],2)
printHtmlPart(5)
createClosureForHtmlPart(7, 2)
invokeTag('content','g',24,['tag':("sidebarTitle")],2)
printHtmlPart(5)
createClosureForHtmlPart(7, 2)
invokeTag('content','g',27,['tag':("sidebar")],2)
printHtmlPart(8)
createTagBody(2, {->
printHtmlPart(9)
invokeTag('field','g',45,['type':("text"),'id':("name"),'name':("name"),'class':("form-control")],-1)
printHtmlPart(10)
invokeTag('field','g',51,['type':("file"),'id':("tsFile"),'name':("tsFile"),'class':("form-control")],-1)
printHtmlPart(11)
invokeTag('checkBox','g',57,['id':("published"),'name':("published"),'value':("on"),'class':("form-control")],-1)
printHtmlPart(12)
invokeTag('submitButton','g',60,['name':("submit"),'class':("btn btn-primary")],-1)
printHtmlPart(5)
})
invokeTag('uploadForm','g',61,['action':("upload"),'params':([popup:1]),'class':("form-horizontal")],2)
printHtmlPart(1)
})
invokeTag('captureBody','sitemesh',62,[:],1)
printHtmlPart(13)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1459852462000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
