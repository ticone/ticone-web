import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_timeSeriesClusteringVisualizationshow_vivagraph_webgl_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/timeSeriesClusteringVisualization/show_vivagraph_webgl.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',4,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',4,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(3)
for( color in (vis.patternColors) ) {
printHtmlPart(4)
expressionOut.print(color)
printHtmlPart(5)
}
printHtmlPart(6)
for( p in (patterns) ) {
printHtmlPart(4)
expressionOut.print(p.name)
printHtmlPart(5)
}
printHtmlPart(7)
for( node in (vis.graph.nodes) ) {
printHtmlPart(8)
expressionOut.print(node.id)
printHtmlPart(9)
expressionOut.print(node.name)
printHtmlPart(10)
for( pattern in (node.coefficients) ) {
printHtmlPart(4)
expressionOut.print(pattern.key)
printHtmlPart(11)
expressionOut.print((int)((Double.valueOf(pattern.value)-vis.graph.minCoeff)/(vis.graph.maxCoeff-vis.graph.minCoeff)*100))
printHtmlPart(12)
}
printHtmlPart(13)
}
printHtmlPart(14)
for( edge in (vis.graph.edges) ) {
printHtmlPart(15)
expressionOut.print(edge.n1.id)
printHtmlPart(16)
expressionOut.print(edge.n2.id)
printHtmlPart(17)
expressionOut.print(edge.n1.id)
printHtmlPart(18)
expressionOut.print(edge.n2.id)
printHtmlPart(19)
}
printHtmlPart(20)
})
invokeTag('captureHead','sitemesh',84,[:],1)
printHtmlPart(21)
createTagBody(1, {->
printHtmlPart(22)

i=0

printHtmlPart(23)
for( _it1061115215 in (patterns) ) {
changeItVariable(_it1061115215)
printHtmlPart(24)
if(true && (selectedPattern == i)) {
printHtmlPart(25)
invokeTag('radio','g',89,['name':("myGroup"),'value':(i),'onclick':("SubmitFrm();"),'checked':("true")],-1)
expressionOut.print(patterns[i].name)
printHtmlPart(26)
}
else {
printHtmlPart(25)
invokeTag('radio','g',92,['name':("myGroup"),'value':(i),'onclick':("SubmitFrm();")],-1)
expressionOut.print(patterns[i].name)
printHtmlPart(26)
}
printHtmlPart(24)

i=i+1

printHtmlPart(23)
}
printHtmlPart(23)
invokeTag('submitButton','g',96,['name':("toggleRender"),'onclick':("ToggleRender();")],-1)
printHtmlPart(27)
})
invokeTag('captureBody','sitemesh',98,[:],1)
printHtmlPart(28)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1425920793000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
