import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_layoutswelcomemain_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/layouts/welcomemain.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',8,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("Content-Type"),'content':("text/html; charset=UTF-8")],-1)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',9,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("X-UA-Compatible"),'content':("IE=edge,chrome=1")],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('layoutTitle','g',10,['default':("${grails.util.Holders.config.grails.project.groupId} - Time Course Network Enricher")],-1)
})
invokeTag('captureTitle','sitemesh',10,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',10,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',11,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("viewport"),'content':("width=device-width, initial-scale=1.0")],-1)
printHtmlPart(2)
invokeTag('assetPath','g',12,['src':("ticone_logo_icon.png")],-1)
printHtmlPart(3)
invokeTag('stylesheet','asset',13,['src':("application.css")],-1)
printHtmlPart(1)
invokeTag('javascript','asset',14,['src':("application.js")],-1)
printHtmlPart(4)
invokeTag('layoutHead','g',48,[:],-1)
printHtmlPart(1)
invokeTag('layoutResources','r',49,[:],-1)
printHtmlPart(5)
})
invokeTag('captureHead','sitemesh',50,[:],1)
printHtmlPart(5)
createTagBody(1, {->
printHtmlPart(6)
invokeTag('layoutBody','g',54,[:],-1)
printHtmlPart(7)
invokeTag('layoutResources','r',55,[:],-1)
printHtmlPart(8)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(9)
invokeTag('meta','g',68,['name':("app.version")],-1)
printHtmlPart(10)
invokeTag('meta','g',68,['name':("app.grails.version")],-1)
printHtmlPart(11)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(12)
invokeTag('meta','g',68,['name':("app.ticone_lib.version")],-1)
printHtmlPart(13)
})
invokeTag('captureBody','sitemesh',71,[:],1)
printHtmlPart(14)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1473691554000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
