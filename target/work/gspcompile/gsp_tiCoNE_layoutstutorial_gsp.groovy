import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_layoutstutorial_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/layouts/tutorial.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
createTagBody(1, {->
printHtmlPart(0)
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(0)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(0)
})
invokeTag('captureHead','sitemesh',7,[:],2)
printHtmlPart(0)
createTagBody(2, {->
printHtmlPart(2)
createTagBody(3, {->
printHtmlPart(3)
invokeTag('pageProperty','g',12,['name':("page.title")],-1)
printHtmlPart(4)
if(true && (isLatest)) {
printHtmlPart(5)
}
printHtmlPart(6)
expressionOut.print(params["ver"])
printHtmlPart(7)
expressionOut.print(product)
printHtmlPart(8)
if(true && (summarizedVersionsList.size() > 1)) {
printHtmlPart(9)
}
printHtmlPart(10)
loop:{
int x = 0
for( it in (summarizedVersionsList) ) {
printHtmlPart(11)
expressionOut.print(it)
if(true && (x < summarizedVersionsList.size()-1)) {
printHtmlPart(12)
}
printHtmlPart(13)
x++
}
}
printHtmlPart(14)
})
invokeTag('content','g',20,['tag':("title")],3)
printHtmlPart(15)
createClosureForHtmlPart(16, 3)
invokeTag('content','g',21,['tag':("sidebarClass")],3)
printHtmlPart(15)
createClosureForHtmlPart(17, 3)
invokeTag('content','g',22,['tag':("mainClass")],3)
printHtmlPart(15)
createClosureForHtmlPart(18, 3)
invokeTag('content','g',23,['tag':("sidebarTitle")],3)
printHtmlPart(15)
createTagBody(3, {->
printHtmlPart(19)
if(true && (lastVer)) {
printHtmlPart(11)
createTagBody(5, {->
printHtmlPart(20)
expressionOut.print(lastVer)
printHtmlPart(21)
})
invokeTag('link','g',27,['action':("show_workflow"),'id':(params["id"]),'params':([ver: lastVer])],5)
printHtmlPart(13)
}
printHtmlPart(22)
if(true && (nextVer)) {
printHtmlPart(11)
createTagBody(5, {->
printHtmlPart(23)
expressionOut.print(nextVer)
printHtmlPart(24)
})
invokeTag('link','g',32,['action':("show_workflow"),'id':(params["id"]),'params':([ver: nextVer])],5)
printHtmlPart(13)
}
printHtmlPart(25)
})
invokeTag('content','g',38,['tag':("sidebar")],3)
printHtmlPart(26)
expressionOut.print(raw(pageProperty(name:'page.tutorialContent')))
printHtmlPart(27)
})
invokeTag('captureBody','sitemesh',65,[:],2)
printHtmlPart(28)
})
invokeTag('applyLayout','g',1,['name':("main")],1)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1476782764000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
