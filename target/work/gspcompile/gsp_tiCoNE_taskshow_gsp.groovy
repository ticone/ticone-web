import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_taskshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/task/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'http-equiv':("Refresh"),'content':("3")],-1)
printHtmlPart(2)
if(true && (t.finished && t.redirect_url)) {
printHtmlPart(3)
expressionOut.print(t.redirect_url)
printHtmlPart(4)
}
printHtmlPart(5)
})
invokeTag('captureHead','sitemesh',16,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(6)
createClosureForHtmlPart(7, 2)
invokeTag('content','g',18,['tag':("sidebarHide")],2)
printHtmlPart(6)
createTagBody(2, {->
printHtmlPart(8)
expressionOut.print(t.title)
printHtmlPart(9)
})
invokeTag('content','g',21,['tag':("title")],2)
printHtmlPart(6)
if(true && (t.finished)) {
printHtmlPart(10)
if(true && (t.statusMessage)) {
printHtmlPart(11)
expressionOut.print(t.statusMessage)
printHtmlPart(12)
}
else {
printHtmlPart(13)
expressionOut.print(t.redirect_url)
printHtmlPart(14)
}
printHtmlPart(6)
}
else {
printHtmlPart(15)
if(true && (t.user && t.user.email)) {
printHtmlPart(16)
if(true && (t.sendMail)) {
printHtmlPart(17)
expressionOut.print(t.user.email)
printHtmlPart(18)
}
else {
printHtmlPart(19)
createTagBody(5, {->
printHtmlPart(20)
invokeTag('checkBox','g',43,['name':("sendMail"),'onclick':("submitMailForm();")],-1)
printHtmlPart(21)
})
invokeTag('form','g',43,['name':("sendMailForm"),'action':("update"),'params':([id: t.taskId ])],5)
printHtmlPart(16)
}
printHtmlPart(22)
}
printHtmlPart(22)
if(true && (t.canCancel)) {
printHtmlPart(22)
createTagBody(4, {->
printHtmlPart(23)
invokeTag('submitButton','g',47,['name':("cancelTask"),'value':("Ask for Stop"),'onclick':("cancelTask();")],-1)
printHtmlPart(21)
})
invokeTag('form','g',47,['name':("cancelTaskForm"),'action':("cancel"),'params':([id: t.taskId ])],4)
printHtmlPart(22)
}
printHtmlPart(24)
}
printHtmlPart(25)
})
invokeTag('captureBody','sitemesh',51,[:],1)
printHtmlPart(26)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1456481000000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
