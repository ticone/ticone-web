import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_timeSeriesDataSetcreate_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/timeSeriesDataSet/create.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
if(true && (params.popup)) {
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main_popup")],-1)
printHtmlPart(1)
}
else {
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',8,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
}
printHtmlPart(2)
if(true && (user == null)) {
printHtmlPart(3)
}
printHtmlPart(4)
})
invokeTag('captureHead','sitemesh',21,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(5)
createClosureForHtmlPart(6, 2)
invokeTag('content','g',25,['tag':("title")],2)
printHtmlPart(5)
createClosureForHtmlPart(7, 2)
invokeTag('content','g',28,['tag':("sidebarTitle")],2)
printHtmlPart(5)
createClosureForHtmlPart(8, 2)
invokeTag('content','g',48,['tag':("sidebar")],2)
printHtmlPart(5)
if(true && (!params.popup)) {
printHtmlPart(5)
createClosureForHtmlPart(9, 3)
invokeTag('content','g',50,['tag':("sidebarClass")],3)
printHtmlPart(5)
}
printHtmlPart(10)
if(true && (flash.error)) {
printHtmlPart(11)
expressionOut.print(flash.error)
printHtmlPart(12)
}
printHtmlPart(5)
createTagBody(2, {->
printHtmlPart(13)
if(true && (params.popup)) {
printHtmlPart(14)
invokeTag('hiddenField','g',70,['name':("popup"),'value':("1")],-1)
printHtmlPart(13)
}
printHtmlPart(15)
invokeTag('field','g',75,['type':("text"),'id':("name"),'name':("name"),'value':(name),'class':("form-control")],-1)
printHtmlPart(16)
invokeTag('field','g',81,['type':("file"),'id':("tsFile"),'name':("tsFile"),'class':("form-control")],-1)
printHtmlPart(17)
invokeTag('checkBox','g',87,['id':("published"),'value':(published),'name':("published"),'class':("form-control")],-1)
printHtmlPart(18)
invokeTag('submitButton','g',90,['name':("submit"),'class':("btn btn-primary")],-1)
printHtmlPart(5)
})
invokeTag('uploadForm','g',91,['action':("async_upload"),'class':("form-horizontal")],2)
printHtmlPart(1)
})
invokeTag('captureBody','sitemesh',92,[:],1)
printHtmlPart(19)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1456324641000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
