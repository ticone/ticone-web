import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_welcomeindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/welcome/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',8,['tag':("sidebarTitle")],2)
printHtmlPart(2)
createClosureForHtmlPart(4, 2)
invokeTag('content','g',13,['tag':("sidebar")],2)
printHtmlPart(2)
createClosureForHtmlPart(5, 2)
invokeTag('content','g',14,['tag':("sidebarClass")],2)
printHtmlPart(2)
createClosureForHtmlPart(6, 2)
invokeTag('content','g',15,['tag':("title")],2)
printHtmlPart(7)
createTagBody(2, {->
printHtmlPart(8)
invokeTag('img','g',29,['height':("48px"),'alt':(grails.util.Holders.config.grails.project.groupId),'file':("ticone_logo_3.png"),'style':("vertical-align:bottom;")],-1)
printHtmlPart(9)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(10)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(11)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(12)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(13)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(14)
expressionOut.print(createLink(controller: 'tutorial', action: 'index'))
printHtmlPart(15)
expressionOut.print(createLink(controller: 'citation', action: 'index'))
printHtmlPart(16)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(17)
invokeTag('image','asset',95,['src':("cytoscape_logo_large_cropped.png"),'width':("24px"),'height':("24px")],-1)
printHtmlPart(18)
expressionOut.print(createLink(controller: 'iterativeTimeSeriesClustering', action: 'create'))
printHtmlPart(19)
expressionOut.print(grails.util.Holders.config.grails.project.groupId)
printHtmlPart(20)
expressionOut.print(createLink(controller: 'download', action: 'index'))
printHtmlPart(21)
})
invokeTag('isNotLoggedIn','shiro',107,[:],2)
printHtmlPart(22)
createClosureForHtmlPart(23, 2)
invokeTag('link','g',109,['controller':("changelog")],2)
printHtmlPart(24)
createClosureForHtmlPart(25, 2)
invokeTag('link','g',119,['controller':("tutorial")],2)
printHtmlPart(26)
})
invokeTag('captureBody','sitemesh',120,[:],1)
printHtmlPart(27)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1496921685000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
