import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_signupindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/signup/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',5,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',7,['tag':("sidebarHide")],2)
printHtmlPart(2)
invokeTag('content','g',8,['tag':("sidebarTitle")],-1)
printHtmlPart(2)
invokeTag('content','g',9,['tag':("sidebar")],-1)
printHtmlPart(2)
createClosureForHtmlPart(4, 2)
invokeTag('content','g',10,['tag':("title")],2)
printHtmlPart(5)
createTagBody(2, {->
printHtmlPart(6)
if(true && (flash.message)) {
printHtmlPart(7)
expressionOut.print(flash.message)
printHtmlPart(8)
}
printHtmlPart(9)
createTagBody(3, {->
printHtmlPart(10)
invokeTag('renderErrors','g',22,['bean':(user),'as':("list")],-1)
printHtmlPart(11)
})
invokeTag('hasErrors','g',24,['bean':(user)],3)
printHtmlPart(12)
invokeTag('textField','g',30,['name':("username"),'value':(""),'class':("form-control")],-1)
printHtmlPart(13)
invokeTag('passwordField','g',36,['name':("password"),'value':(""),'class':("form-control")],-1)
printHtmlPart(14)
invokeTag('passwordField','g',42,['name':("password2"),'value':(""),'class':("form-control")],-1)
printHtmlPart(15)
invokeTag('textField','g',48,['name':("email"),'value':(""),'class':("form-control")],-1)
printHtmlPart(16)
invokeTag('submitButton','g',55,['name':("submit"),'class':("btn btn-primary")],-1)
printHtmlPart(17)
})
invokeTag('form','g',58,['action':("register"),'class':("form-horizontal")],2)
printHtmlPart(1)
})
invokeTag('captureBody','sitemesh',59,[:],1)
printHtmlPart(18)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1455655966000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
