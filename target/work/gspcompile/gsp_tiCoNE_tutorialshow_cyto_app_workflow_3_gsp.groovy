import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_tutorialshow_cyto_app_workflow_3_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/tutorial/show_cyto_app_workflow_3.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("tutorial")],-1)
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',9,['tag':("title")],2)
printHtmlPart(4)
createTagBody(2, {->
printHtmlPart(5)
invokeTag('image','asset',29,['src':("control_panel_1.3.77.png")],-1)
printHtmlPart(6)
expressionOut.print(assetPath(src: "cytoscape_toy_network.txt"))
printHtmlPart(7)
invokeTag('image','asset',51,['src':("load_network_cytoscape_3.4.png")],-1)
printHtmlPart(8)
invokeTag('image','asset',58,['src':("load_network_cytoscape_3.4_advanced.png")],-1)
printHtmlPart(9)
invokeTag('image','asset',68,['src':("network_cytoscape_3.5.png")],-1)
printHtmlPart(10)
expressionOut.print(assetPath(src: "cytoscape_toy_dataset.txt"))
printHtmlPart(11)
invokeTag('image','asset',88,['src':("load_table_cytoscape_3.5.png")],-1)
printHtmlPart(12)
invokeTag('image','asset',96,['src':("load_table_cytoscape_3.5_advanced.png")],-1)
printHtmlPart(13)
invokeTag('image','asset',104,['src':("table_selector_1.3.77.png")],-1)
printHtmlPart(14)
invokeTag('image','asset',119,['src':("settings_1.3.77.png")],-1)
printHtmlPart(15)
invokeTag('image','asset',152,['src':("graphs_1.3.77.png")],-1)
printHtmlPart(16)
invokeTag('image','asset',200,['src':("perform_iterations_1.3.77.png")],-1)
printHtmlPart(17)
invokeTag('image','asset',208,['src':("lucky_iterations_1.3.77.png")],-1)
printHtmlPart(18)
invokeTag('image','asset',223,['src':("calculate_cluster_pvalues_1.3.77.png")],-1)
printHtmlPart(19)
invokeTag('image','asset',231,['src':("lucky_iterations_1.3.77_with_pvals.png")],-1)
printHtmlPart(20)
invokeTag('image','asset',249,['src':("cluster_delete_1.3.77.png")],-1)
printHtmlPart(21)
invokeTag('image','asset',257,['src':("delete_pattern_options.png")],-1)
printHtmlPart(22)
invokeTag('image','asset',265,['src':("cluster_delete_leastfitting_1.3.77.png")],-1)
printHtmlPart(23)
invokeTag('image','asset',289,['src':("cluster_split_1.3.77.png")],-1)
printHtmlPart(24)
invokeTag('image','asset',297,['src':("splitpattern_least_similar.png")],-1)
printHtmlPart(25)
invokeTag('image','asset',309,['src':("cluster_split_1.3.77_dialog.png")],-1)
printHtmlPart(26)
invokeTag('image','asset',320,['src':("cluster_split_1.3.77_accept.png")],-1)
printHtmlPart(27)
invokeTag('image','asset',338,['src':("filterobjects_1.3.77.png")],-1)
printHtmlPart(28)
invokeTag('image','asset',349,['src':("filterobjects_1.3.77_preview.png")],-1)
printHtmlPart(29)
invokeTag('image','asset',366,['src':("cluster_history_1.3.77.png")],-1)
printHtmlPart(30)
invokeTag('image','asset',390,['src':("filterclusters_1.3.77.png")],-1)
printHtmlPart(31)
invokeTag('image','asset',398,['src':("filterclusters_1.3.77_clustertable.png")],-1)
printHtmlPart(32)
invokeTag('image','asset',422,['src':("network_enrichment_1.3.77.png")],-1)
printHtmlPart(33)
invokeTag('image','asset',436,['src':("kpm_control_panel_1.2.53.png")],-1)
printHtmlPart(34)
invokeTag('image','asset',444,['src':("kpm_1.3.77.png")],-1)
printHtmlPart(35)
invokeTag('image','asset',457,['src':("kpm_visualize_on_network.png")],-1)
printHtmlPart(36)
invokeTag('image','asset',465,['src':("kpm_subnetwork_1.3.77.png")],-1)
printHtmlPart(37)
})
invokeTag('content','g',471,['tag':("tutorialContent")],2)
printHtmlPart(1)
})
invokeTag('captureBody','sitemesh',472,[:],1)
printHtmlPart(38)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1496152980000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
