import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_iterativeTimeSeriesClustering_show_least_similar_objects_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/iterativeTimeSeriesClustering/_show_least_similar_objects.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(request.contextPath)
printHtmlPart(1)
expressionOut.print(clustering.sessionId)
printHtmlPart(2)
expressionOut.print(iteration)
printHtmlPart(2)
expressionOut.print(percentage)
printHtmlPart(2)
expressionOut.print(type)
printHtmlPart(3)
if(true && (type == "percentage")) {
printHtmlPart(4)
expressionOut.print(params.percentage)
printHtmlPart(5)
}
else if(true && (type == "rss")) {
printHtmlPart(6)
expressionOut.print(params.percentage)
printHtmlPart(7)
}
else if(true && (type == "pearson")) {
printHtmlPart(8)
expressionOut.print(params.percentage)
printHtmlPart(7)
}
printHtmlPart(9)
if(true && (type == "percentage")) {
printHtmlPart(10)
expressionOut.print(params.percentage)
printHtmlPart(11)
}
printHtmlPart(12)
if(true && (type == "rss")) {
printHtmlPart(13)
expressionOut.print(params.percentage)
printHtmlPart(14)
}
printHtmlPart(12)
if(true && (type == "pearson")) {
printHtmlPart(15)
expressionOut.print(params.percentage)
printHtmlPart(14)
}
printHtmlPart(16)
expressionOut.print(request.contextPath)
printHtmlPart(17)
expressionOut.print(params.id)
printHtmlPart(2)
expressionOut.print(params.iteration)
printHtmlPart(2)
expressionOut.print(params.percentage)
printHtmlPart(2)
expressionOut.print(type)
printHtmlPart(18)
invokeTag('submitButton','g',105,['name':("deleteLeastSimilarObjects"),'value':("Delete"),'onclick':("deleteLeastSimilarObjects();")],-1)
printHtmlPart(19)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1456506970000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
