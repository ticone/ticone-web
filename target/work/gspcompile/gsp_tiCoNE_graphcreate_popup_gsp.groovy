import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_graphcreate_popup_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/graph/create_popup.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main_popup")],-1)
printHtmlPart(2)
if(true && (user == null)) {
printHtmlPart(3)
}
printHtmlPart(4)
})
invokeTag('captureHead','sitemesh',26,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(5)
createClosureForHtmlPart(6, 2)
invokeTag('content','g',30,['tag':("title")],2)
printHtmlPart(5)
createClosureForHtmlPart(7, 2)
invokeTag('content','g',33,['tag':("sidebarTitle")],2)
printHtmlPart(5)
createClosureForHtmlPart(8, 2)
invokeTag('content','g',36,['tag':("sidebar")],2)
printHtmlPart(9)
createTagBody(2, {->
printHtmlPart(10)
invokeTag('field','g',56,['type':("text"),'id':("name"),'name':("name"),'class':("form-control")],-1)
printHtmlPart(11)
invokeTag('field','g',62,['type':("file"),'id':("graphFile"),'name':("graphFile"),'class':("form-control")],-1)
printHtmlPart(12)
invokeTag('checkBox','g',68,['id':("published"),'name':("published"),'value':("on"),'class':("form-control")],-1)
printHtmlPart(13)
invokeTag('submitButton','g',71,['name':("submit"),'class':("btn btn-primary")],-1)
printHtmlPart(5)
})
invokeTag('uploadForm','g',72,['action':("upload"),'params':([popup:1]),'class':("form-horizontal")],2)
printHtmlPart(1)
})
invokeTag('captureBody','sitemesh',73,[:],1)
printHtmlPart(14)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1457015095000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
