import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_timeSeriesDataSetindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/timeSeriesDataSet/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',10,['tag':("sidebarTitle")],2)
printHtmlPart(2)
createClosureForHtmlPart(4, 2)
invokeTag('content','g',13,['tag':("title")],2)
printHtmlPart(2)
createClosureForHtmlPart(5, 2)
invokeTag('content','g',14,['tag':("sidebarClass")],2)
printHtmlPart(2)
createClosureForHtmlPart(6, 2)
invokeTag('content','g',15,['tag':("mainClass")],2)
printHtmlPart(2)
createClosureForHtmlPart(7, 2)
invokeTag('content','g',18,['tag':("sidebar")],2)
printHtmlPart(8)
createClosureForHtmlPart(9, 2)
invokeTag('link','g',20,['action':("create")],2)
printHtmlPart(10)
for( ds in (dataSets) ) {
printHtmlPart(11)
expressionOut.print(ds.name)
printHtmlPart(12)
expressionOut.print(ds.objects.size())
printHtmlPart(12)
expressionOut.print(ds.objects?.find {true}.samples.size())
printHtmlPart(12)
expressionOut.print(ds.objects?.find {true}.samples?.find { true }.timepoints.size())
printHtmlPart(12)
invokeTag('formatDate','g',40,['date':(ds.dateCreated)],-1)
printHtmlPart(12)
createClosureForHtmlPart(13, 3)
invokeTag('link','g',41,['action':("filter"),'id':(ds.id)],3)
printHtmlPart(14)
}
printHtmlPart(15)
})
invokeTag('captureBody','sitemesh',45,[:],1)
printHtmlPart(16)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1456993550000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
