import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_iterativeTimeSeriesClusteringshow_vivagraph_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/iterativeTimeSeriesClustering/show_vivagraph.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',4,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',4,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(3)
for( color in (vis.patternColors) ) {
printHtmlPart(4)
expressionOut.print(color)
printHtmlPart(5)
}
printHtmlPart(6)
for( p in (patterns) ) {
printHtmlPart(4)
expressionOut.print(p.name)
printHtmlPart(5)
}
printHtmlPart(7)
for( node in (patternNodes) ) {
printHtmlPart(4)
expressionOut.print(node.id)
printHtmlPart(5)
}
printHtmlPart(8)
if(true && (vis.graph)) {
printHtmlPart(9)
for( edge in (vis.graph.edges) ) {
printHtmlPart(10)
expressionOut.print(edge.n1.id)
printHtmlPart(11)
expressionOut.print(edge.n2.id)
printHtmlPart(12)
expressionOut.print(edge.n1.id)
printHtmlPart(13)
expressionOut.print(edge.n2.id)
printHtmlPart(14)
expressionOut.print(edge.n1.id)
printHtmlPart(13)
expressionOut.print(edge.n2.id)
printHtmlPart(15)
}
printHtmlPart(9)
}
printHtmlPart(16)
expressionOut.print(createLink(controller: 'timeSeriesClusteringVisualization', action: 'save_layout', id: vis.id))
printHtmlPart(17)
if(true && (vis.graph)) {
printHtmlPart(18)
for( node in (vis.graph.nodes) ) {
printHtmlPart(19)
expressionOut.print(node.name)
printHtmlPart(20)
for( node_pattern in (vis.clustering.objectPatternCoefficients.findAll {it.object.name == node.name}) ) {
printHtmlPart(4)
expressionOut.print(node_pattern.pattern.name)
printHtmlPart(21)
expressionOut.print((int)((Double.valueOf(node_pattern.coefficient)-vis.clustering.minCoeff)/(vis.clustering.maxCoeff-vis.clustering.minCoeff)*100))
printHtmlPart(22)
}
printHtmlPart(23)
if(true && (node.id in patternNodes.collect {it.id})) {
printHtmlPart(24)
}
else {
printHtmlPart(25)
}
printHtmlPart(26)
expressionOut.print(node.id)
printHtmlPart(27)
}
printHtmlPart(28)
for( edge in (vis.graph.edges) ) {
printHtmlPart(29)
expressionOut.print(edge.n1.id)
printHtmlPart(30)
expressionOut.print(edge.n2.id)
printHtmlPart(31)
expressionOut.print(edge.n1.id)
printHtmlPart(13)
expressionOut.print(edge.n2.id)
printHtmlPart(32)
}
printHtmlPart(33)
}
printHtmlPart(34)
if(true && (layout)) {
printHtmlPart(35)
for( nodePos in (layout.locations) ) {
printHtmlPart(36)
expressionOut.print(nodePos.node.id)
printHtmlPart(37)
expressionOut.print(nodePos.x)
printHtmlPart(37)
expressionOut.print(nodePos.y)
printHtmlPart(38)
}
printHtmlPart(33)
}
printHtmlPart(39)
if(true && (layout)) {
printHtmlPart(40)
expressionOut.print(layout.transformMatrix)
printHtmlPart(41)
}
printHtmlPart(42)
})
invokeTag('captureHead','sitemesh',265,[:],1)
printHtmlPart(43)
createTagBody(1, {->
printHtmlPart(44)
createTagBody(2, {->
printHtmlPart(44)
if(true && (selectedPattern == i)) {
printHtmlPart(45)
invokeTag('radio','g',269,['name':("myGroup"),'value':("-1"),'checked':("true"),'onclick':("SubmitFrm();")],-1)
printHtmlPart(46)
}
else {
printHtmlPart(45)
invokeTag('radio','g',272,['name':("myGroup"),'value':("-1"),'onclick':("SubmitFrm();")],-1)
printHtmlPart(46)
}
printHtmlPart(44)

i=0

printHtmlPart(47)
for( _it530942536 in (patterns) ) {
changeItVariable(_it530942536)
printHtmlPart(48)
if(true && (selectedPattern == i)) {
printHtmlPart(9)
invokeTag('radio','g',277,['name':("myGroup"),'value':(i),'onclick':("SubmitFrm();"),'checked':("true")],-1)
expressionOut.print(patterns[i].name)
printHtmlPart(49)
expressionOut.print(i)
printHtmlPart(50)
}
else {
printHtmlPart(9)
invokeTag('radio','g',280,['name':("myGroup"),'value':(i),'onclick':("SubmitFrm();")],-1)
expressionOut.print(patterns[i].name)
printHtmlPart(49)
expressionOut.print(i)
printHtmlPart(50)
}
printHtmlPart(48)
createTagBody(4, {->
printHtmlPart(51)
expressionOut.print(i)
printHtmlPart(52)

j=0

printHtmlPart(53)
for( _it958523759 in (patterns[i].timeSeriesPattern) ) {
changeItVariable(_it958523759)
printHtmlPart(54)
expressionOut.print(i)
printHtmlPart(55)
expressionOut.print(j)
printHtmlPart(37)
expressionOut.print(it)
printHtmlPart(56)

j=j+1

printHtmlPart(53)
}
printHtmlPart(57)
expressionOut.print(i)
printHtmlPart(58)
expressionOut.print(i)
printHtmlPart(59)
expressionOut.print(i)
printHtmlPart(60)
})
invokeTag('javascript','g',297,[:],4)
printHtmlPart(48)

i=i+1

printHtmlPart(47)
}
printHtmlPart(61)
invokeTag('textField','g',301,['name':("neighborhood"),'value':("1")],-1)
printHtmlPart(47)
invokeTag('submitButton','g',302,['name':("neighborhoodBtn"),'value':("Update Neighborhood"),'onclick':("SubmitFrm();")],-1)
printHtmlPart(62)
invokeTag('submitButton','g',304,['name':("toggleRender"),'value':("Toggle Layouting"),'onclick':("ToggleRender();")],-1)
printHtmlPart(62)
invokeTag('submitButton','g',306,['name':("saveLayout"),'value':("Save Graph Layout"),'onclick':("saveLayout();")],-1)
printHtmlPart(47)
})
invokeTag('content','g',307,['tag':("sidebar")],2)
printHtmlPart(63)
})
invokeTag('captureBody','sitemesh',309,[:],1)
printHtmlPart(64)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1455449100000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
