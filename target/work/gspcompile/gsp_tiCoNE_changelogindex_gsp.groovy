import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_changelogindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/changelog/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
})
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('content','g',8,['tag':("sidebarClass")],2)
printHtmlPart(2)
createClosureForHtmlPart(4, 2)
invokeTag('content','g',9,['tag':("mainClass")],2)
printHtmlPart(2)
createClosureForHtmlPart(5, 2)
invokeTag('content','g',10,['tag':("sidebarTitle")],2)
printHtmlPart(2)
createClosureForHtmlPart(6, 2)
invokeTag('content','g',19,['tag':("sidebar")],2)
printHtmlPart(2)
createClosureForHtmlPart(7, 2)
invokeTag('content','g',20,['tag':("preventLayoutResize")],2)
printHtmlPart(2)
createClosureForHtmlPart(8, 2)
invokeTag('content','g',23,['tag':("title")],2)
printHtmlPart(9)
createClosureForHtmlPart(10, 2)
invokeTag('renderHtml','markdown',28,[:],2)
printHtmlPart(11)
createClosureForHtmlPart(12, 2)
invokeTag('renderHtml','markdown',35,[:],2)
printHtmlPart(13)
createClosureForHtmlPart(14, 2)
invokeTag('renderHtml','markdown',45,[:],2)
printHtmlPart(15)
createClosureForHtmlPart(16, 2)
invokeTag('renderHtml','markdown',48,[:],2)
printHtmlPart(17)
})
invokeTag('captureBody','sitemesh',49,[:],1)
printHtmlPart(18)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1496921301000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
