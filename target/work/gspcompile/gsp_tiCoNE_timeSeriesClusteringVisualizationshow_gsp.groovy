import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_timeSeriesClusteringVisualizationshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/timeSeriesClusteringVisualization/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',4,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',4,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(3)
expressionOut.print(params.id)
printHtmlPart(4)
expressionOut.print(patterns[selectedPattern].name)
printHtmlPart(5)
expressionOut.print(String.format("%.2f",vis.graph.minCoeff))
printHtmlPart(5)
expressionOut.print(String.format("%.2f",vis.graph.maxCoeff))
printHtmlPart(6)
expressionOut.print(vis.patternColors[selectedPattern])
printHtmlPart(7)
expressionOut.print(vis.graph.id)
printHtmlPart(8)
})
invokeTag('script','r',248,['type':("text/javascript")],2)
printHtmlPart(9)
})
invokeTag('captureHead','sitemesh',249,[:],1)
printHtmlPart(9)
createTagBody(1, {->
printHtmlPart(10)

i=0

printHtmlPart(11)
for( _it1622460244 in (patterns) ) {
changeItVariable(_it1622460244)
printHtmlPart(12)
if(true && (selectedPattern == i)) {
printHtmlPart(13)
invokeTag('radio','g',254,['name':("myGroup"),'value':(i),'onclick':("SubmitFrm();"),'checked':("true")],-1)
expressionOut.print(patterns[i].name)
printHtmlPart(14)
}
else {
printHtmlPart(13)
invokeTag('radio','g',257,['name':("myGroup"),'value':(i),'onclick':("SubmitFrm();")],-1)
expressionOut.print(patterns[i].name)
printHtmlPart(14)
}
printHtmlPart(12)

i=i+1

printHtmlPart(11)
}
printHtmlPart(15)
})
invokeTag('captureBody','sitemesh',262,[:],1)
printHtmlPart(16)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1426500898000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
