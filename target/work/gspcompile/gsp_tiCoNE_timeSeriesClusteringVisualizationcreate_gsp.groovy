import org.springframework.web.servlet.tags.form.SelectedValueComparator
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_tiCoNE_timeSeriesClusteringVisualizationcreate_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/timeSeriesClusteringVisualization/create.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createTagBody(2, {->
createClosureForHtmlPart(2, 3)
invokeTag('captureTitle','sitemesh',4,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',4,[:],2)
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',6,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
createTagBody(2, {->
printHtmlPart(5)
invokeTag('field','g',10,['type':("file"),'id':("patternFile"),'name':("patternFile")],-1)
printHtmlPart(6)
invokeTag('field','g',11,['type':("file"),'id':("tsFile"),'name':("tsFile")],-1)
printHtmlPart(7)
invokeTag('field','g',12,['type':("file"),'id':("graphFile"),'name':("graphFile")],-1)
printHtmlPart(8)
invokeTag('field','g',13,['type':("number"),'min':("1"),'max':("100"),'id':("threshold"),'name':("threshold")],-1)
printHtmlPart(9)
invokeTag('submitButton','g',15,['name':("submit")],-1)
printHtmlPart(10)
})
invokeTag('uploadForm','g',16,['action':("upload"),'params':([patternFile: patternFile, tsFile: tsFile, threshold: threshold, graphFile: graphFile])],2)
printHtmlPart(3)
})
invokeTag('captureBody','sitemesh',17,[:],1)
printHtmlPart(11)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1434026126000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
