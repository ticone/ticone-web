package tsviz_grails;

import grails.util.Holders;
import groovy.util.ConfigObject;

import java.util.Base64;

import org.scribe.builder.api.DefaultApi20;
import org.scribe.extractors.AccessTokenExtractor;
import org.scribe.extractors.JsonTokenExtractor;
import org.scribe.model.OAuthConfig;
import org.scribe.model.OAuthConstants;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuth20ServiceImpl;
import org.scribe.oauth.OAuthService;


public class DimanaApi20 extends DefaultApi20 {

	@Override
	public String getAuthorizationUrl(OAuthConfig config) {
		ConfigObject appConf = Holders.getConfig();
		
		Object auth_url = appConf.get("authorizationUri");
		
		return String.format(auth_url.toString(), config.getApiKey(),
				config.getCallback());
	}

	@Override
	public String getAccessTokenEndpoint() {
		ConfigObject appConf = Holders.getConfig();
		
		Object access_token = appConf.get("tokenAccessPoint");
		
		return access_token.toString();
	}

	@Override
	public Verb getAccessTokenVerb() {
		return Verb.POST;
	}

	@Override
	public AccessTokenExtractor getAccessTokenExtractor() {
		return new JsonTokenExtractor();
	}

	@Override
	public OAuthService createService(OAuthConfig config) {
		return new DiMAnaOAuth2Service(this, config);
	}
}

class DiMAnaOAuth2Service extends OAuth20ServiceImpl {

	private static final String GRANT_TYPE_AUTHORIZATION_CODE = "authorization_code";
	private static final String GRANT_TYPE = "grant_type";
	private DefaultApi20 api;	
	private OAuthConfig config;

	public DiMAnaOAuth2Service(DefaultApi20 api, OAuthConfig config) {
		super(api, config);
		this.api = api;
		this.config = config;
	}

	@Override
	public Token getAccessToken(Token requestToken, Verifier verifier) {
		OAuthRequest request = new OAuthRequest(api.getAccessTokenVerb(),
				api.getAccessTokenEndpoint());
		switch (api.getAccessTokenVerb()) {
		case POST:
			request.addHeader(
					"Authorization",
					"Basic "
							+ Base64.getEncoder().encodeToString(
									String.format("%s:%s", config.getApiKey(),
											config.getApiSecret()).getBytes()));
			// request.addBodyParameter(OAuthConstants.CLIENT_ID,
			// config.getApiKey());
			// request.addBodyParameter(OAuthConstants.CLIENT_SECRET,
			// config.getApiSecret());
			request.addBodyParameter(OAuthConstants.CODE, verifier.getValue());
			request.addBodyParameter(OAuthConstants.REDIRECT_URI,
					config.getCallback());
			request.addBodyParameter(GRANT_TYPE, GRANT_TYPE_AUTHORIZATION_CODE);
			break;
		case GET:
		default:
			request.addQuerystringParameter(OAuthConstants.CLIENT_ID,
					config.getApiKey());
			request.addQuerystringParameter(OAuthConstants.CLIENT_SECRET,
					config.getApiSecret());
			request.addQuerystringParameter(OAuthConstants.CODE,
					verifier.getValue());
			request.addQuerystringParameter(OAuthConstants.REDIRECT_URI,
					config.getCallback());
			if (config.hasScope())
				request.addQuerystringParameter(OAuthConstants.SCOPE,
						config.getScope());
		}
		Response response = request.send();
		return api.getAccessTokenExtractor().extract(response.getBody());
	}
}