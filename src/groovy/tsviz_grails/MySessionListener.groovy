package tsviz_grails

import javax.servlet.http.HttpSessionEvent
import javax.servlet.http.HttpSessionListener

public class MySessionListener implements HttpSessionListener {

	public void sessionCreated(HttpSessionEvent event) {
		println "session has been created"
	}

	public void sessionDestroyed(HttpSessionEvent event) {
		String id = event.getSession().getId();
		
		println "session has been destroyed"
		IterativeTimeSeriesClustering.list().each { ds ->
			if (ds.userSessionId == id) {
				println "deleting " + ds
				ds.iterations.each { it ->
					def vis = it.visualization
					def clust = vis.clustering
//					it.delete(flush: true)
					//vis.delete(flush: true)
					clust.objects.clear()
				}
				ds.delete(flush: true);
			}
		}
		Graph.where {sessionId == id}.list().each { ds ->
			println "deleting " + ds
			
			GraphFile f = ds.file
			
			ds.delete(flush: true);
			f.delete(flush: true);
		}
		println Graph.list()
		TimeSeriesDataSet.where {sessionId == id}.list().each { ds ->
			TimeSeriesFile f = ds.file
			println "deleting " + ds
			ds.delete(flush: true)
			
			f.delete(flush: true);
		}
	}

}
