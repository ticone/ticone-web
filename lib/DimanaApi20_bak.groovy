package tsviz_grails;

import java.util.Base64;

import org.scribe.builder.api.DefaultApi20;
import org.scribe.extractors.AccessTokenExtractor;
import org.scribe.extractors.JsonTokenExtractor;
import org.scribe.model.OAuthConfig;
import org.scribe.model.OAuthConstants;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuth20ServiceImpl;
import org.scribe.oauth.OAuthService;
import grails.util.Holders


public class DimanaApi20_bak extends DefaultApi20 {

	//def grailsApplication
	
	//private static final String AUTHORIZATION_URL = "http://localhost:8000/o/authorize/?client_id=%s&response_type=code";

	@Override
	public String getAuthorizationUrl(OAuthConfig config) {
		def appConf = Holders.config
		
		String authorization_url = appConf.authorizationUri;
		println authorization_url
		return String.format(authorization_url, config.getApiKey());
	}

	@Override
	public String getAccessTokenEndpoint() {
		def appConf = Holders.config
		
		String tokenAccessPoint = appConf.tokenAccessPoint
		println tokenAccessPoint
		return tokenAccessPoint;
	}

	@Override
	public Verb getAccessTokenVerb() {
		return Verb.POST;
	}

	@Override
	public AccessTokenExtractor getAccessTokenExtractor() {
		return new JsonTokenExtractor();
	}

	@Override
	public OAuthService createService(OAuthConfig config) {
		return new DiMAnaOAuth2Service(this, config);
	}
}

class DiMAnaOAuth2Service extends OAuth20ServiceImpl {

	private static final String GRANT_TYPE_AUTHORIZATION_CODE = "authorization_code";
	private static final String GRANT_TYPE = "grant_type";
	private DefaultApi20 api;	
	private OAuthConfig config;

	public DiMAnaOAuth2Service(DefaultApi20 api, OAuthConfig config) {
		super(api, config);
		this.api = api;
		this.config = config;
	}

	@Override
	public Token getAccessToken(Token requestToken, Verifier verifier) {
		OAuthRequest request = new OAuthRequest(api.getAccessTokenVerb(),
				api.getAccessTokenEndpoint());
		switch (api.getAccessTokenVerb()) {
		case POST:
			request.addHeader(
					"Authorization",
					"Basic "
							+ Base64.getEncoder().encodeToString(
									String.format("%s:%s", config.getApiKey(),
											config.getApiSecret()).getBytes()));
			request.addBodyParameter(OAuthConstants.CODE, verifier.getValue());
			request.addBodyParameter(OAuthConstants.REDIRECT_URI,
					config.getCallback());
			request.addBodyParameter(GRANT_TYPE, GRANT_TYPE_AUTHORIZATION_CODE);
			break;
		case GET:
		default:
			request.addHeader(
					"Authorization",
					"Basic "
							+ Base64.getEncoder().encodeToString(
									String.format("%s:%s", config.getApiKey(),
											config.getApiSecret()).getBytes()));
			request.addBodyParameter(OAuthConstants.CODE, verifier.getValue());
			request.addBodyParameter(OAuthConstants.REDIRECT_URI,
					config.getCallback());
			request.addBodyParameter(GRANT_TYPE, GRANT_TYPE_AUTHORIZATION_CODE);
		}
		Response response = request.send();
		println response
		println response.getBody()
		return api.getAccessTokenExtractor().extract(response.getBody());
	}
}