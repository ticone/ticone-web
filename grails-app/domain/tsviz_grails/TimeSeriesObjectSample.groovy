package tsviz_grails

class TimeSeriesObjectSample {
	
	String name;
	
	tsviz_grails.TimeSeriesObject object;
	
	static hasMany = [timepoints: tsviz_grails.TimeSeriesObjectSampleHasTimePoint]

    static constraints = {
    }
	
	static belongsTo = tsviz_grails.TimeSeriesObject
	
	static mapping = {
		object column: 'object_id', index: 'Object_SampleName_Idx'
		name column: 'name', index: 'Object_SampleName_Idx'
		timepoints cascade: 'all-delete-orphan'
	}
}
