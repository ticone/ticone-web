package tsviz_grails

import groovy.sql.Sql
import dk.sdu.imada.ticone.api.Cluster
import dk.sdu.imada.ticone.api.PatternObjectMapping
import dk.sdu.imada.ticone.tsdata.TimeSeriesObject
import dk.sdu.imada.ticone.util.StatsCalculation
import dk.sdu.imada.ticone.util.TimePointWeighting

class TimeSeriesClustering {

	static def dataSourceUnproxied
	static def grailsApplication
	TimeSeriesDataSet dataSet;
	
	Date dateCreated;
	Date lastUpdated;

	static hasMany = [patterns: Pattern_g,
		objects: tsviz_grails.TimeSeriesObject,
		objectPatternCoefficients: ObjectBelongsToPattern]
	
	static belongsTo = [TimeSeriesClusteringVisualization visualization, TimeSeriesDataSet]

	double minCoeff;
	double maxCoeff;
	
	int nextPatternNumber;

	static constraints = {
		minCoeff blank: true, nullable: true
		maxCoeff blank: true, nullable: true
	}
	
	static mapping = {
		patterns cascade: 'all-delete-orphan'
		objectPatternCoefficients cascade: 'all-delete-orphan'
	}

	static def createFromMapping(TimeSeriesDataSet dataSet, TimePointWeighting tpWeighting, PatternObjectMapping mapping, int nextPatternNumber) {
		TimeSeriesClustering cl = new TimeSeriesClustering(
			dataSet: dataSet,
			nextPatternNumber: nextPatternNumber);
		cl.save();

		// make sure that we have RSS
		StatsCalculation
				.calculateAllClusterRSS(tpWeighting, mapping);

		double minCoeff = Double.POSITIVE_INFINITY;
		double maxCoeff = Double.NEGATIVE_INFINITY;
		
		List<Cluster> ps = new ArrayList<Cluster>(mapping.clusterSet());
		Collections.sort(ps, new Comparator<Cluster>() {
				@Override
				public int compare(Cluster o1, Cluster o2) {
					double[] s1 = o1.getPrototype();
					double[] s2 = o2.getPrototype();
					int ind = 0;
					while (s1[ind] == s2[ind] && ind < s1.length-1)
						ind++;
						
					if (ind < s1.length)
						return Double.compare(s1[ind], s2[ind]);
					else
						return 0;
				}
			});
		
		Map<double[], Pattern_g> patterns = new HashMap<double[], Pattern_g>();

		println "Storing patterns"
		for (Cluster tsPattern: ps) {
			int patternNo;
			Double rss = Double.isNan(tsPattern.residualSumOfSquares) ? null: tsPattern.residualSumOfSquares;
			Double pearson = Double.isNan(tsPattern.avgPearson) ? null: tsPattern.avgPearson;
			Pattern_g p = new Pattern_g(
				timeSeriesPattern: tsPattern.getCluster(), 
				number: tsPattern.patternNumber, 
				name: String.format("Cluster %d", tsPattern.patternNumber),
				rss: rss, rsspvalue: tsPattern.rssPValue, 
				pearson: pearson, pearsonpvalue: tsPattern.pearsonPValue,
				keep: tsPattern.keep);
			p.save();
			cl.addToPatterns(p);
			cl.save();
			patterns.put(tsPattern.getPrototype(), p);
		}
		cl.save(flush:true);
		//cl.nextPatternNumber = nextPatternNumber;
		println "Storing objects"
		for (String objId: mapping.objectSet()) {
			//TimeSeriesObject object = mapping.getTimeSeriesObject(objId);
			// TODO: slow for large data sets!
			//			tsviz_grails.TimeSeriesObject object = dataSet.objects.find { o ->
			//				o.name == objId
			//			}
			def objects = tsviz_grails.TimeSeriesObject.findAllByDataSetAndName(dataSet, objId);
			objects.each {object ->
				cl.addToObjects(object);
			}
		}
		cl.save(flush:true);

		println "Storing Pattern object similarities"
		for (tsviz_grails.TimeSeriesObject obj: cl.objects) {
			Map<Cluster, Double> inputCoeffs = mapping.getCoefficients(obj.name);

			List<Cluster> patternList = new ArrayList<Cluster>(inputCoeffs.keySet());
			Collections.sort(patternList, new PatternComparator());

			for (Cluster tsPattern: patternList) {
				Pattern_g p = patterns.get(tsPattern.getPrototype());

				if (inputCoeffs.get(tsPattern) > maxCoeff) {
					maxCoeff = inputCoeffs.get(tsPattern);
				}

				if (inputCoeffs.get(tsPattern) < minCoeff) {
					minCoeff = inputCoeffs.get(tsPattern);
				}

				ObjectBelongsToPattern obj_pattern = new ObjectBelongsToPattern(
						coefficient: inputCoeffs.get(tsPattern),
						object: obj,
						pattern: p,
						clustering: cl).save();
				cl.addToObjectPatternCoefficients(obj_pattern);
			}
		}
		cl.minCoeff = minCoeff;
		cl.maxCoeff = maxCoeff;
		cl.save(flush: true);

		return cl;
	}

	static def createFromMappingBatch(TimeSeriesDataSet dataSet, TimePointWeighting tpWeighting, PatternObjectMapping mapping, int nextPatternNumber) {
		
		def batchSize = 150;

		TimeSeriesClustering cl = new TimeSeriesClustering(
			dataSet: dataSet,
			nextPatternNumber: nextPatternNumber);
		cl.save();
	
		// make sure that we have RSS
		StatsCalculation
				.calculateAllClusterRSS(tpWeighting, mapping);

		double minCoeff = Double.POSITIVE_INFINITY;
		double maxCoeff = Double.NEGATIVE_INFINITY;
		
		List<Cluster> ps = new ArrayList<Cluster>(mapping.clusterSet());
		Collections.sort(ps, new Comparator<Cluster>() {
				@Override
				public int compare(Cluster o1, Cluster o2) {
					double[] s1 = o1.getPrototype();
					double[] s2 = o2.getPrototype();
					int ind = 0;
					while (s1[ind] == s2[ind] && ind < s1.length-1)
						ind++;
						
					if (ind < s1.length)
						return Double.compare(s1[ind], s2[ind]);
					else
						return 0;
				}
			});
		
		Map<double[], Pattern_g> patterns = new HashMap<double[], Pattern_g>();
		
		println "Storing patterns"
		for (Cluster tsPattern: ps) {
			int patternNo;
			Pattern_g p = new Pattern_g(
				timeSeriesPattern: tsPattern.getPrototype(),
				number: tsPattern.patternNumber,
				name: String.format("Cluster %d", tsPattern.patternNumber),
				rsspvalue: tsPattern.rssPValue, pearsonpvalue: tsPattern.pearsonPValue,
				keep: tsPattern.keep);
			if(!tsPattern.residualSumOfSquares.isNaN())
				p.rss = tsPattern.residualSumOfSquares;
			if(!tsPattern.avgPearson.isNaN())
				p.pearson = tsPattern.avgPearson;
			p.save();
			cl.addToPatterns(p);
			patterns.put(tsPattern.getPrototype(), p);
		}
		cl.save(flush:true);
		
		
		println "Storing objects"
		def sql = Sql.newInstance(dataSourceUnproxied);
		// insert time_series_clustering_time_series_object
		sql.withTransaction {
			try{
				sql.withBatch(batchSize, 'insert into time_series_clustering_time_series_object (time_series_clustering_objects_id, time_series_object_id) values (?, ?)') { stmt ->
					for (String objId: mapping.objectSet()) {
						def objects = tsviz_grails.TimeSeriesObject.findAllByDataSetAndName(dataSet, objId);
						objects.each {object ->
							stmt.addBatch(cl.id, object.id);
						}
					}
				}
			} catch(java.sql.BatchUpdateException bue){
				return "Could not add clustering objects: ${bue.getMessage()}"
			} finally{
				sql.commit();
			}
		}
		cl.refresh();

			
		println "Storing Pattern object similarities"
		def nextPatternObjectId = -1
		sql.withTransaction {
			// get maximal node id
			sql.eachRow('select max(id)+1 as id from object_belongs_to_pattern') { row ->
				nextPatternObjectId = row.id;
			}
			if (nextPatternObjectId == null)
				nextPatternObjectId = 1;
			try{
				sql.withBatch(batchSize, 'insert into object_belongs_to_pattern (id, version, clustering_id, coefficient, object_id, pattern_id) values (?, ?, ?, ?, ?, ?)') { stmt ->
					for (tsviz_grails.TimeSeriesObject obj: cl.objects) {
						Map<Cluster, Double> inputCoeffs = mapping.getCoefficients(obj.name);
				
						List<Cluster> patternList = new ArrayList<Cluster>(inputCoeffs.keySet());
						Collections.sort(patternList, new PatternComparator());
						
						for (Cluster tsPattern: patternList) {
							Pattern_g p = patterns.get(tsPattern.getPrototype());
				
							if (inputCoeffs.get(tsPattern) > maxCoeff) {
								maxCoeff = inputCoeffs.get(tsPattern);
							}
				
							if (inputCoeffs.get(tsPattern) < minCoeff) {
								minCoeff = inputCoeffs.get(tsPattern);
							}
				
//							ObjectBelongsToPattern obj_pattern = new ObjectBelongsToPattern(
//									coefficient: inputCoeffs.get(tsPattern),
//									object: obj,
//									pattern: p,
//									clustering: cl).save();
							//cl.addToObjectPatternCoefficients(obj_pattern);
							stmt.addBatch(nextPatternObjectId, 1, cl.id, inputCoeffs.get(tsPattern), obj.id, p.id);
							nextPatternObjectId++;
						}
					}
				}
			} catch(java.sql.BatchUpdateException bue){
				return "Could not add pattern object sims: ${bue.getMessage()}"
			} finally{
				sql.commit();
			}
		}
		cl.refresh();
		cl.minCoeff = minCoeff;
		cl.maxCoeff = maxCoeff;
		cl.save(flush: true);
	
		return cl;
	}

	def toPatternObjectMapping(List<TimeSeriesObject> datas) {
		// build up a new PatternObjectMapping from database;
		PatternObjectMapping mapping = new PatternObjectMapping();

		// restore Pattern objects from database;
		Map<Pattern_g, Cluster> patterns = new HashMap<Pattern_g, Cluster>();
		this.patterns.each { Pattern_g p ->
			Cluster newPattern = new Cluster(p.timeSeriesPattern);
			newPattern.patternNumber = p.number;
			newPattern.keep = p.keep
			patterns.put(p, newPattern);
			// make sure that the pattern is in the mapping; also if there are no objects belonging to it;
			mapping.addPattern(newPattern);
		}

		Map<tsviz_grails.TimeSeriesObject, TimeSeriesObject> objects = new HashMap<tsviz_grails.TimeSeriesObject, TimeSeriesObject>();

		this.objects.each { tsviz_grails.TimeSeriesObject object ->
			TimeSeriesObject data = datas.find {
				it.name == object.name
			};
			objects.put(object, data);
			// TODO: do we need this?
			//mapping.addTimeSeriesObject(data);
		}

		this.objectPatternCoefficients.each { ObjectBelongsToPattern objPattern ->
			mapping.addMapping(objects.get(objPattern.object), patterns.get(objPattern.pattern), objPattern.coefficient);
		}
		
		for (Cluster c: mapping.clusterSet()) {
			List<TimeSeriesObject> ds = mapping.getPatternsData(c);
			Collections.sort(ds, new Comparator<TimeSeriesObject>() {
				@Override
				public int compare(TimeSeriesObject o1, TimeSeriesObject o2) {
					return o1.getName().compareTo(o2.getName());
				}
			})
		}
		
		return mapping
	}

	def get_total_rss() {
		double total_rss = 0.0;

		patterns.each {Pattern_g p ->
			total_rss += p.rss
		}

		return total_rss;
	}
}
