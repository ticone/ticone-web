package tsviz_grails

import groovy.io.FileType
import net.lingala.zip4j.core.ZipFile
import net.lingala.zip4j.exception.ZipException

class Graph {
	
	GraphFile file;
	
	NetworkGroup group;
	
	String name;
	
	boolean published;
	
	User user;
	
	String sessionId;
	
	Date dateCreated;
	
	Date lastUpdated;
	
	static hasMany = [
		nodes: Node, 
		edges: Edge, 
		layouts: GraphLayout,
		visualizations: TimeSeriesClusteringVisualization]
	
    static constraints = {
        user nullable: true
		sessionId nullable: true
    }
	
	static mapping = {
		nodes cascade: 'all-delete-orphan'
		edges cascade: 'all-delete-orphan'
		layouts cascade: 'all-delete-orphan'
		visualizations cascade: 'all-delete-orphan'
		group defaultValue: 1
	}
	
	static def importBioGRIDFromZIPFile(String version, String zipFileURL) {
		// do we have this version of biogrid already in the DB?
		boolean alreadyLoaded = Graph.where {(group == NetworkGroup.findByName('BioGRID')) && name ==~ "%" + version + "%"};
		if (alreadyLoaded) {
			render "The newest BioGRID version is already loaded."
			return;
		}
		
		def existingBiogridZips = []
		File.createTempFile("bla","blubb").getParentFile().eachFile(FileType.FILES) { it ->
			if( it.getName().startsWith("biogridZipFile") && it.getName().endsWith(version)) {
				existingBiogridZips.add(it)
			}
		}
		
		// check if the file has already been downloaded
		File zipFile;
		if (existingBiogridZips.size() == 0) {
			zipFile = File.createTempFile("biogridZipFile",version);
			// download zip file
			def fileStream = zipFile.newOutputStream()
			fileStream << new URL(zipFileURL).openStream()
			fileStream.close()
		} else {
			println "Using existing file"
			zipFile = existingBiogridZips.get(0)
		}
		
		// check if the file has already been extracted
		def existingBiogridDirs = [];
		File.createTempFile("bla","blubb").getParentFile().eachFile(FileType.DIRECTORIES) { it ->
			if( it.getName().startsWith("biogrid") && it.getName().endsWith(version)) {
				existingBiogridDirs.add(it)
			}
		}
		File targetDir;
		if (existingBiogridDirs.size() == 0) {
			targetDir = File.createTempDir("biogrid", version);
			
			try {
				ZipFile zip = new ZipFile(zipFile.getAbsolutePath());
				zip.extractAll(targetDir.getAbsolutePath());
		   } catch (ZipException e) {
			   e.printStackTrace();
		   }
		} else {
			println "Using existing directory"
			targetDir = existingBiogridDirs.get(0)
		}
		// import the files into the DB
		targetDir.eachFile(FileType.FILES) { file ->
			println file
			
			StringBuilder sb = new StringBuilder();
			
			int lineNo = 0;
			String line;
			file.withReader { reader ->
				while ((line = reader.readLine()) != null) {
					if (lineNo > 0) {
						String[] split = line.split("\t");
						sb.append(split[1]);
						sb.append("\t");
						sb.append(split[2]);
						sb.append("\n");
					}
					lineNo++;
				}
			}
			
			Graph g = GraphController.loadGraphBatchFromFile(sb.toString().getBytes(), file.getName() + " (Entrez ID)");
			g.group = NetworkGroup.findByName('BioGRID');
			g.published = true
			g.save(flush: true)
		}
	}
}
