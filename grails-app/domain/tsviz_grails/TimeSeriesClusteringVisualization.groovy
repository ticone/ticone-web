package tsviz_grails

import java.awt.Dimension
import java.awt.geom.Point2D

import tsviz_Grails.IterationHasVisualization
import edu.uci.ics.jung.algorithms.layout.CircleLayout
import edu.uci.ics.jung.algorithms.layout.FRLayout2
import edu.uci.ics.jung.graph.UndirectedSparseGraph
import edu.uci.ics.jung.graph.util.Pair


class TimeSeriesClusteringVisualization {
	
	TimeSeriesClustering clustering;
	
	Graph graph
	GraphLayout graphLayout
	String[] patternColors
	
	static belongsTo = [IterationHasVisualization, Graph, GraphLayout]

	static constraints = {
        graph nullable: true
        graphLayout nullable: true
    }
	
	def doForceLayout(userId, userSessionId) {
		GraphLayout newLayout = new GraphLayout(graph: graph, name: 'Force', published: true, user: userId, sessionId: userSessionId);
		def iterCount = 0;
		UndirectedSparseGraph<Long, Long> jungGraph = new UndirectedSparseGraph<Long, Long>();
		graph.nodes.each { Node it ->
			jungGraph.addVertex(it.id);
		}
		graph.edges.each { Edge it ->
			jungGraph.addEdge(it.id, new Pair<String>(it.n1.id, it.n2.id));
		}

		FRLayout2 force = new FRLayout2(jungGraph);
		force.setAttractionMultiplier(1.5);
		force.setRepulsionMultiplier(0.3);
		force.setSize(new Dimension((int)(graph.nodes.size()),(int)(graph.nodes.size())));
		force.initialize();
		
		
		while (!force.done() && iterCount < 1000) {
			force.step();
			iterCount++;
		}
		newLayout.locations = []
		newLayout.save(flush: true)
		jungGraph.vertices.each { Long it ->
			Point2D loc = force.getCoordinates(it);
			newLayout.setLocation(Node.get(it), (loc.x*10).toFloat(), (loc.y*10).toFloat());
		}
		graphLayout = newLayout
		save(flush: true)
		println "Iteration Coutn:"
		println iterCount
		println "/Iteration Coutn"
	}
	
	def doCircleLayout(userId, userSessionId) {
		GraphLayout newLayout = new GraphLayout(graph: graph, name: 'Circle', published: true, user: userId, sessionId: userSessionId);
		UndirectedSparseGraph<Long, Long> jungGraph = new UndirectedSparseGraph<Long, Long>();
		graph.nodes.each { Node it ->
			jungGraph.addVertex(it.id);
		}
		graph.edges.each { Edge it ->
			jungGraph.addEdge(it.id, new Pair<String>(it.n1.id, it.n2.id));
		}
		println jungGraph

		CircleLayout layout = new CircleLayout(jungGraph);
		layout.setSize(new Dimension(500, 500));
		layout.setRadius(250);
		layout.initialize();
		
		newLayout.locations = []
		newLayout.save(flush: true)
		jungGraph.vertices.each { Long it ->
			Point2D loc = layout.getCoordinates(it);
			newLayout.setLocation(Node.get(it), (loc.x*10).toFloat(), (loc.y*10).toFloat());
		}
		graphLayout = newLayout
		save(flush: true)
	}
}
