package tsviz_grails

import java.util.Map;

class Node {
	String name;
	
	tsviz_grails.TimeSeriesObject object;

	static constraints = {
		object nullable: true
	}
}
