package tsviz_grails

class TimeSeriesFile {
	
	byte[] tmpFile;

    static constraints = { 
		tmpFile(maxSize: 150 * 1024 * 1024, nullable: false, blank: false)
	}
}
