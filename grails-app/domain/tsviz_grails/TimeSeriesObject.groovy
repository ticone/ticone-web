package tsviz_grails

class TimeSeriesObject {
	
	String name;
	
	//String sample;
	
	static hasMany = [
		samples: tsviz_grails.TimeSeriesObjectSample,
		//timepoints: tsviz_grails.TimeSeriesObjectHasTimePoint,
		objectPatterns: ObjectBelongsToPattern]
	
	TimeSeriesDataSet dataSet;
	
	static belongsTo = TimeSeriesDataSet
	
	static mapping = {
		dataSet column: 'dataset_id', index: 'DataSet_Name_Sample_Idx'
		name column: 'name', index: 'DataSet_Name_Sample_Idx'
		//sample column: 'sample', index: 'DataSet_Name_Sample_Idx'
		//timepoints cascade: 'all-delete-orphan'
		objectPatterns cascade: 'all-delete-orphan'
		samples cascade: 'all-delete-orphan'
	}

    static constraints = {
    }
}
