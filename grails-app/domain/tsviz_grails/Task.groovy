package tsviz_grails

import java.security.SecureRandom
import java.util.Date;

class Task {
	
	String taskId;
	
	String title;
	
	User user;
	
	boolean canCancel;
	boolean cancelled;
	
	boolean finished;
	
	String statusMessage;
	
	String redirect_url;
	
	boolean sendMail;
	
	Date dateCreated;
	
	Date lastUpdated;

    static constraints = {
		taskId(nullable: false, blank: false, unique: true)
		title(nullable: false, blank: false, unique: false)
		user(nullable: true)
		statusMessage(nullable: true, size: 0..5000)
		redirect_url(nullable: true)
    }
	
	// use text, to increase max length
	static mapping = {
		statusMessage type: "text"
	}
	
	static def create_task_id() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
	}
}
