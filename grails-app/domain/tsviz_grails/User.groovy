package tsviz_grails

class User {
    String username
    String passwordHash
	String email
    
    static hasMany = [ roles: Role, permissions: String ]

    static constraints = {
        username(nullable: false, blank: false, unique: true)
		email(email: true, nullable: false, blank: false, unique: true)
    }
}
