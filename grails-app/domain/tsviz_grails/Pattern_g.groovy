package tsviz_grails

class Pattern_g {
	String name;
	int number;
	double rss;
	double rsspvalue;
	double pearson;
	double pearsonpvalue;
	double[] timeSeriesPattern;
	boolean keep;

	static constraints = {
        //timeSeriesPattern maxSize: 10000
		rss nullable: true
		rsspvalue nullable: true
		pearson nullable: true
		pearsonpvalue nullable: true
	}
	
	static mapping = {
		timeSeriesPattern column: 'time_series_pattern', sqlType: 'VARBINARY(10000)'
	}

	def get_number_of_objects(TimeSeriesClustering cl) {
		//return ObjectBelongsToPattern.findAll { objPattern ->
		//	clustering == cl && pattern == this
		//}.groupBy{it.object.id}.size()
		def res = ObjectBelongsToPattern.executeQuery("select count(distinct object_id) from ObjectBelongsToPattern op where clustering = ? and pattern = ?", [cl, this])[0]
		return res
	}
}
