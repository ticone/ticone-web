package tsviz_grails

import java.nio.file.AccessDeniedException

import tsviz_Grails.IterationHasVisualization
import dk.sdu.imada.ticone.api.AbstractTimeSeriesPreprocessor
import dk.sdu.imada.ticone.api.IAggregateCluster
import dk.sdu.imada.ticone.api.IClustering
import dk.sdu.imada.ticone.api.IDiscretizePrototype
import dk.sdu.imada.ticone.api.ISimilarity
import dk.sdu.imada.ticone.api.ITimeSeriesClusteringWithOverrepresentedPatterns
import dk.sdu.imada.ticone.api.PatternObjectMapping
import dk.sdu.imada.ticone.clustering.BasicTimeSeriesClusteringWithOverrepresentedPatterns
import dk.sdu.imada.ticone.clustering.CLARAClustering
import dk.sdu.imada.ticone.clustering.PAMKClustering
import dk.sdu.imada.ticone.clustering.STEMClustering
import dk.sdu.imada.ticone.clustering.TransClustClustering
import dk.sdu.imada.ticone.clustering.refinement.AggregateClusterMean
import dk.sdu.imada.ticone.clustering.refinement.AggregateClusterMedian
import dk.sdu.imada.ticone.io.Parser
import dk.sdu.imada.ticone.permutation.PermutateDatasetRowwise
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePatternMinToMax
import dk.sdu.imada.ticone.similarity.NegativeEuclideanSimilarity
import dk.sdu.imada.ticone.similarity.PearsonCorrelation
import dk.sdu.imada.ticone.tsdata.TimeSeriesObject
import dk.sdu.imada.ticone.util.TimePointWeighting

class IterativeTimeSeriesClustering {
	
	TimeSeriesDataSet dataSet;
	
	Graph graph;
	
	ClusteringMethod clusteringMethod;
	
	SimilarityFunction similarityFunction;
	
	PatternRefinementFunction patternRefinementFunction;
	
	Integer discretizationSteps;
	
	long randomSeed;
	
	int permutations;
	
	int refinementPermutations;
	
	String sessionId;
	
	User user;
	
	String userSessionId;
	
	Date dateCreated;
	
	Date lastUpdated;
	
	static hasMany = [iterations: IterationHasVisualization]

	static constraints = {
        graph nullable: true
        user nullable: true
		userSessionId nullable: true
    }
	
	static mapping = {
		iterations cascade: 'all-delete-orphan'
	}
	
	static belongsTo = [TimeSeriesDataSet, Graph, ClusteringMethod, SimilarityFunction, PatternRefinementFunction, User]
	
	static def get_clustering_and_mapping(userId, userSessionId, clustering_session_id, iteration_id) throws AccessDeniedException {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(clustering_session_id)
		
		// if the clustering was created by a user, and the current one is a guest or not the right user, we throw an exception
		if (itCl.user != null) {
			//if (SecurityUtils.getSubject().isAuthenticated() || SecurityUtils.getSubject().isRemembered()) {
			if (userId != null) {
				//def curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
				if (userId.toInteger() != itCl.user.id)
					throw new AccessDeniedException("You do not have rights to modify this clustering.");
			} else {
				throw new AccessDeniedException("You do not have rights to modify this clustering.");
			}
		}
		else if (itCl.userSessionId != null) {
			if (userSessionId != null) {
				if (itCl.userSessionId != userSessionId)
					throw new AccessDeniedException("You do not have rights to modify this clustering.");
			} else {
				throw new AccessDeniedException("You do not have rights to modify this clustering.");
			}
		}
		
		TimeSeriesDataSet dataSet = itCl.dataSet;
		
		AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
		Scanner scanner = new Scanner(new ByteArrayInputStream(itCl.dataSet.file.tmpFile));
		List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
		scanner.close();
		calc.initializeTimeSeriesData(tsData);
		calc.calculatePatterns();

		IDiscretizePrototype discretizePattern = new DiscretizePatternMinToMax(calc.minValue, calc.maxValue, itCl.discretizationSteps);
		ISimilarity similarityFunction;
		if (itCl.similarityFunction.name == "Euclidian Distance")
			similarityFunction = new NegativeEuclideanSimilarity();
		else if (itCl.similarityFunction.name == "Pearson Correlation")
			similarityFunction = new PearsonCorrelation();

		IAggregateCluster refinePattern;
		if (itCl.patternRefinementFunction.name == 'Mean')
			refinePattern = new AggregateClusterMean();
		else if (itCl.patternRefinementFunction.name == 'Median')
			refinePattern = new AggregateClusterMedian();

		// the visualization of the current iteration
		TimeSeriesClusteringVisualization current_vis = itCl.iterations.find { it.id == iteration_id }.visualization

		int numberPatterns = current_vis.clustering.patterns.size();
		IClustering transClust;
		if (itCl.clusteringMethod.name == "PAMK")
			transClust = new PAMKClustering(similarityFunction, new Random(itCl.randomSeed), refinePattern, discretizePattern);
		else if (itCl.clusteringMethod.name == "Transitivity Clustering")
			transClust = new TransClustClustering(discretizePattern, similarityFunction, 0.9, refinePattern);
		else if (itCl.clusteringMethod.name == "CLARA")
			transClust = new CLARAClustering(similarityFunction, refinePattern, discretizePattern, new Random(itCl.randomSeed));
		else if (itCl.clusteringMethod.name == "STEM")
			transClust = new STEMClustering(similarityFunction, discretizePattern, tsData.get(0).getOriginalTimeSeries().length);

		ITimeSeriesClusteringWithOverrepresentedPatterns cl = new BasicTimeSeriesClusteringWithOverrepresentedPatterns(calc, similarityFunction, new PermutateDatasetRowwise(), itCl.permutations,numberPatterns, discretizePattern, transClust, itCl.refinementPermutations);
		println itCl.permutations
		println itCl.refinementPermutations
		
		TimePointWeighting tpWeighting = new TimePointWeighting(tsData.get(0).getOriginalTimeSeries().length);

		// restore PatternObjectMapping from database;
		PatternObjectMapping mapping = current_vis.clustering.toPatternObjectMapping(tsData);
		cl.reset(mapping);
		
		return [dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, transClust, tpWeighting]
	}
}
