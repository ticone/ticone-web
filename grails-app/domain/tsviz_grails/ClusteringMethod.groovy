package tsviz_grails

class ClusteringMethod {
	
	String name;
	boolean available;

    static constraints = {
    }
	
	static mapping = {
		available defaultValue: true
	}
}
