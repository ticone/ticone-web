package tsviz_grails

import grails.converters.JSON

import org.codehaus.groovy.grails.commons.DefaultGrailsDomainClass

class TimeSeriesDataSetFilter {
	
	String name;

    static constraints = {
    }
	
	static def get_as_json() {
		def dataFilters = [[id: 0, name: 'Remove Least Conserved Object Sets'],
			[id: 1, name: 'Remove Least Varying Object Sets']]
		return dataFilters
	}
}
