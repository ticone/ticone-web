package tsviz_grails

class NodeLocation {
	Node node;
	Float x;
	Float y;

	static constraints = {
	}
	
	static belongsTo = Node
}
