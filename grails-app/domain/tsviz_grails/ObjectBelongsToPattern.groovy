package tsviz_grails

class ObjectBelongsToPattern {
	
	TimeSeriesClustering clustering;
	tsviz_grails.TimeSeriesObject object;
	Pattern_g pattern;
	double coefficient;
	
	static mapping = {
		clustering column: 'clustering_id', index: 'Idx,Idx2'
		object column: 'object_id', index: 'Idx'
		pattern column: 'pattern_id', index: 'Idx2'
	}
	
	static belongsTo = [TimeSeriesClustering, tsviz_grails.TimeSeriesObject, Pattern_g]
	
    static constraints = {
    }
}
