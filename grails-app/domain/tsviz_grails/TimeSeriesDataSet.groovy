package tsviz_grails

import java.util.Date;

import org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty
import org.codehaus.groovy.grails.commons.GrailsClass

class TimeSeriesDataSet {
	
	TimeSeriesFile file;
	
	DataSetGroup group;
	
	String name;
	
	boolean published;
	
	User user;
	
	String sessionId;
	
	Date dateCreated;
	
	Date lastUpdated;
	
	static hasMany = [objects: tsviz_grails.TimeSeriesObject, clusterings: TimeSeriesClustering]

    static constraints = {
        user nullable: true
		sessionId nullable: true
    }
	
	static mapping = {
		objects cascade: 'all-delete-orphan'
		clusterings cascade: 'all-delete-orphan'
		group defaultValue: 1
	}
}
