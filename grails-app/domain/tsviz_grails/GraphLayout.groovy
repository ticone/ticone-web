package tsviz_grails


class GraphLayout {

	Graph graph;
	
	String name;
	
	User user;
	
	String sessionId;
	
	boolean published;

	static hasMany = [
		locations: NodeLocation,
		visualizations: TimeSeriesClusteringVisualization]

	String transformMatrix = "matrix(1,0,0,1,0,0)"

	static constraints = {
        user nullable: true
		sessionId nullable: true
	}
	
	static belongsTo = Graph
	
	static mapping = {
		locations cascade: 'all-delete-orphan'
		visualizations cascade: 'all-delete-orphan'
	}

	def setLocation(Node n, float x, float y) {
		NodeLocation loc = new NodeLocation(node: n, x: x, y: y);
		loc.save();
		addToLocations(loc);
	}
}
