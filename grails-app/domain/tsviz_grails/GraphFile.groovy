package tsviz_grails

import java.io.File;

class GraphFile {
	
	byte[] tmpFile;

    static constraints = { 
		tmpFile(maxSize: 150 * 1024 * 1024, nullable: false, blank: false) 
	} 
}
