package tsviz_grails

class TimeSeriesObjectSampleHasTimePoint {
	
	tsviz_grails.TimeSeriesObjectSample sample;
	
	int timepoint;
	
	double timeSeriesSignal;
	
	static belongsTo = tsviz_grails.TimeSeriesObjectSample

    static constraints = {
    }
}
