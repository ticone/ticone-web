package tsviz_Grails

import tsviz_grails.IterativeTimeSeriesClustering;
import tsviz_grails.TimeSeriesClusteringVisualization;

class IterationHasVisualization {
	
	int iteration;
	
	String description;
	
	TimeSeriesClusteringVisualization visualization;
	
	static belongsTo = IterativeTimeSeriesClustering

    static constraints = {
    }
}
