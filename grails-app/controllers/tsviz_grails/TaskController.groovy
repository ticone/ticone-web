package tsviz_grails

class TaskController {

    def index() { }
	
	def show() {
		Task t = Task.findByTaskId(params.id)
		render(view: 'show', model: [t:t])
	}
	
	def update() {
		Task t = Task.findByTaskId(params.id)
		if (params.sendMail) {
			t.sendMail = true;
		} else {
			t.sendMail = false;
		}
		t.save(flush: true);
		redirect(action: 'show', id: t.taskId)
	}
	
	def cancel() {
		Task t = Task.findByTaskId(params.id)
		if (t.canCancel) {
			t.cancelled = true;
			t.save(flush: true);
		}
		redirect(action: 'show', id: t.taskId)
	}
}
