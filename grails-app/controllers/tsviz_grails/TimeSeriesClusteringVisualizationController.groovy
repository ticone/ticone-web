package tsviz_grails

import static groovyx.net.http.Method.*
import grails.converters.JSON
import groovy.sql.Sql
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder

import java.security.SecureRandom
import java.text.SimpleDateFormat

import org.apache.shiro.SecurityUtils


class TimeSeriesClusteringVisualizationController {
	
	static def dataSourceUnproxied

	def index() {
		//		TimeSeriesClustering cl = new TimeSeriesClusteringStub();
		//		Map<String, Map<double[], Double>> result = cl.doTimeSeriesClusteringWithPredefinedPatterns(null, null, 0);
		//		def r = GraphController.createGraphFromClusteringResult(result);
		//		Pattern[] p = r[0]
		//		Graph g = r[1]
		//		render g
	}

	def show() {
		TimeSeriesClusteringVisualization vis = TimeSeriesClusteringVisualization.get(params.id)
		[vis: vis, patterns: vis.patterns, selectedPattern: params.int('pattern',0)]
	}

	def show_pie() {
		TimeSeriesClusteringVisualization vis = TimeSeriesClusteringVisualization.get(params.id)
		[vis: vis, patterns: vis.patterns]
	}

	def show_vivagraph() {
		TimeSeriesClusteringVisualization vis = TimeSeriesClusteringVisualization.get(params.id)
		[vis: vis,
			patterns: vis.patterns.sort { it.name},
			selectedPattern: params.int('pattern',0),
			layout: vis.graphLayout]
	}

	def show_vivagraph_webgl() {
		TimeSeriesClusteringVisualization vis = TimeSeriesClusteringVisualization.get(params.id)
		[vis: vis, patterns: vis.patterns, selectedPattern: params.int('pattern',0)]
	}

	def create() {
	}
	
	def get_user_and_session(userId, userSessionId) {
		if (userId == null) {
			if (SecurityUtils.getSubject().isAuthenticated() || SecurityUtils.getSubject().isRemembered()) {
				userId = User.findByUsername(SecurityUtils.getSubject().getPrincipal()).id;
			}
		}
		def user;
		//if (userId != null) {
//			user = User.get(userId)
		//}
		if (userId == null && userSessionId == null) {
			userSessionId = session.getId();
		}
		return [userId, userSessionId]
	}

	def save_layout() {
		println "Saving graph layout"
		TimeSeriesClusteringVisualization vis = TimeSeriesClusteringVisualization.get(params.id)
		String name = params.name
		//vis.graphLayout.locations.clear();
		
		def userId;
		def userSessionId;
		(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
		vis.graphLayout = new GraphLayout(graph: vis.graph, name: params.name, published: true, user: userId, sessionId: userSessionId);
		vis.graphLayout.save(flush: true)
		
		
		def batchSize = 150;
		def nextLocationId = -1;
		def locationIds = []
		
		def sql = Sql.newInstance(dataSourceUnproxied);
		// insert node_location
		sql.withTransaction {
			// get maximal location id
			sql.eachRow('select max(id)+1 as id from node_location') { row ->
				nextLocationId =  row.id;
			}
			if (nextLocationId == null)
				nextLocationId = 1;

			try{
				sql.withBatch(batchSize, 'insert into node_location (id, version, node_id, x, y) values (?, ?, ?, ?, ?)') { stmt ->
					
					
					for (Node n: vis.graph.nodes) {
						if (!(n.id + "" in request.JSON))
							continue;
						float[] loc = request.JSON.get(n.id + "");
						stmt.addBatch(nextLocationId, 1, n.id, loc[0], loc[1]);
						locationIds.add(nextLocationId)
						nextLocationId++;
					}
				}
			} catch(java.sql.BatchUpdateException bue){
				return "Could not add node locations: ${bue.getMessage()}"
			} finally{
				sql.commit();
			}
		}
		
		println locationIds
		
		// insert graph_layout_node_location
		sql.withTransaction {
			
			try{
				sql.withBatch(batchSize, 'insert into graph_layout_node_location (graph_layout_locations_id, node_location_id) values (?, ?)') { stmt ->
					
					for(int location_id: locationIds) {
						stmt.addBatch(vis.graphLayout.id, location_id);
					}
				}
			} catch(java.sql.BatchUpdateException bue){
				return "Could not add graph_layout_node_location: ${bue.getMessage()}"
			} finally{
				sql.commit();
			}
		}
		
		vis.graphLayout.refresh();
		
		// TODO: use batch
//		for (Node n: vis.graph.nodes) {
//			if (!(n.id + "" in request.JSON))
//				continue;
//			float[] loc = request.JSON.get(n.id + "");
//			vis.graphLayout.setLocation(n, loc[0], loc[1]);
//		}
		vis.graphLayout.transformMatrix = request.JSON.get('transform')
		//if (!vis.graphLayout.save(flush: true)) {
		//    b.errors.each {
		//        println it
		//    }
		//	render(status: 500)
		//}
		println "Graph Layout saved"
		render(status: 200)
	}
	
	def change_layout() {
		TimeSeriesClusteringVisualization vis = TimeSeriesClusteringVisualization.get(params.id)
		GraphLayout layout = GraphLayout.get(params.layoutId)
		
		vis.graphLayout = layout;
		vis.graphLayout.save(flush: true);
		
		println "Graph Layout changed"
		render(status: 200)
	}
	
	def submitClusteringToKPMWeb() {
		TimeSeriesClusteringVisualization vis = TimeSeriesClusteringVisualization.get(params.id);
		TimeSeriesClustering clustering = vis.clustering;
		int[] pattern_ids = params.pattern_id.split(',').collect {s -> s.toInteger()}
		
		Set<Pattern_g> patterns = new HashSet<Pattern_g>();
		pattern_ids.each {p_id ->
			patterns.add(clustering.patterns.find {it.number == p_id});
		}
		
		// get object IDs of patterns
		Set<String> object_ids = new HashSet<String>();

		clustering.objectPatternCoefficients.each { ObjectBelongsToPattern objPattern ->
			//println objPattern.pattern
			if (patterns.contains(objPattern.pattern))
				object_ids.add(objPattern.object.name);
		}
		
		String id_string = String.join("\n",object_ids.collect {id -> id + "\t1"});
		
		String datasetContent = id_string.bytes.encodeBase64().toString();
		
		// generate data set
		String datasetName = 'ticone_ds'
		
		// generate unique ID for KPM web to attach to job later
		SecureRandom random = new SecureRandom();
		String attachedToID = new BigInteger(130, random).toString(32);
		
		// name for KPM web run
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
		String name = String.format("TiCoNE WEB run %s", timeStamp); 
		
//		// retrieve available kpm_web networks
//		url = '%s%s' % (settings.KPM_HOST, settings.KPM_SUBMIT_ASYNC_URL)
//	
		
		println params
		int number_pathways = params.n.toInteger();
		int k = params.k.toInteger();
		int l = 0;
		boolean negativeList = params.nodesNotPresent.equals("1")
		
		def parameters =  [
			"name": name,
			"algorithm": "Greedy",
			"strategy": "INES",
			"removeBENs": "true",
			"unmapped_nodes": (negativeList ? "Add to negative list" : "Add to positive list"),
			"computed_pathways": number_pathways,
			"l_samePercentage": "false",
			"samePercentage_val": 0,
			"k_values": [
				"val": k,
				"val_step": "1",
				"val_max": "0",
				"use_range": "false",
				"isPercentage": "false"
			],
			"l_values": [
				"val": l,
				"val_step": "1",
				"val_max": "0",
				"use_range": "false",
				"isPercentage": "false",
				"datasetName": datasetName
			]
		]
		
		println parameters
	
		def kpmSettings = [
			"parameters": parameters,
			// TODO: do we want this?
			"withPerturbation": "false",
			"perturbation": [
				[
					"technique": "Node-swap",
					"startPercent": "5",
					"stepPercent": "1",
					"maxPercent": "15",
					"graphsPerStep": "1"
				]
			],
			"linkType": "OR",
			"attachedToID": attachedToID,
			"positiveNodes": "",
			"negativeNodes": ""
		]
	
		def datasets = [
			[
				"name": datasetName,
				"attachedToID": attachedToID,
				"contentBase64": datasetContent
			]
		]

		String graphName = String.format('TiCoNE_%s', timeStamp)
		Graph g = vis.graph;
		StringBuilder content = new StringBuilder("");
		g.edges.each { edge ->
			//content.append(edge.n1.name);
			//content.append("\t");
			//content.append(edge.n2.name);
			//content.append("\n");
			content.append(edge.n1.name);
			content.append("\tpp\t");
			content.append(edge.n2.name);
			content.append("\n");
		}
		
		String graphContent = content.toString().bytes.encodeBase64().toString();

		def graph = [
			"name":graphName,
			"attachedToID":attachedToID,
			"contentBase64":graphContent
		]
		parameters["graphName"] = graphName
		
		def data = [
			'kpmSettings': kpmSettings as JSON,
			'datasets': datasets as JSON,
			'graph': graph as JSON]
		
		
		//data = data as JSON

		def http = new HTTPBuilder(String.format("%s:%s",
						 grailsApplication.config.kpm.host,
						 grailsApplication.config.kpm.port))
		http.request( POST) { req ->
			uri.path = grailsApplication.config.kpm.submit_async_url
			// the order of these two is important; otherwise NullPointerException
			// this one has to be set to URLENC for KPM web; otherwise it won't find the kpmSettings parameter
			requestContentType = ContentType.URLENC
			body = data
			
			// read what we have written; debugging
			//ByteArrayOutputStream baos = new ByteArrayOutputStream();
			//req.getEntity().writeTo(baos);
			//baos.close();
			//byte[] bytes = baos.toByteArray();
			//println new String(bytes)
		
			 response.success = { resp, json ->
				 if (!json['success'])
				 	render json['comment']
				else  {
					 redirect(controller: 'timeSeriesClusteringVisualization', action: "waitForKPMRun", 
						 params: [
							 questID: json['questID'],
							 runID: json['runID']])
				}
				
			}
		
			 response.failure = { resp, json ->
				 // TODO: doesnt work
				 println resp
				 println json
				 render "Connection failed"
			}
		}
	}
	
	def waitForKPMRun() {
		def http = new HTTPBuilder(String.format("%s:%s",
						 grailsApplication.config.kpm.host,
						 grailsApplication.config.kpm.port))
		http.request( GET) { req ->
			uri.path = grailsApplication.config.kpm.status_url
			//requestContentType = ContentType.URLENC
			//body = data
			uri.query = [questID: params['questID']]
		
			 response.success = { resp, json ->
				 if (json['completed']) {
//					 return [title: 'The network enrichment analysis has been sent to Keypathwayminer Web',
//						 text: String.format(' Click <a href="%s%s?runID=%s">here</a> to see the results on the KPM website.',
//								  grailsApplication.config.kpm.host,
//								  grailsApplication.config.kpm.run_result,
//								  params['runID']),
//						  finished: true]
					 redirect(url: String.format("%s%s?runID=%s", grailsApplication.config.kpm.host,
								  grailsApplication.config.kpm.run_result,
								  params['runID']))
				 } else {
					 return [title: 'The network enrichment analysis has been sent to Keypathwayminer Web',
						 text: '<p>It\'s still being processed.</p><p>This page should automatically refresh every 3 seconds. If it does not, please reload it manually.</p>',
						  finished: false]
				 }
			}
		
			 response.failure = { resp, json ->
				 print resp
				 print json
					 return [title: 'Your job could not be submitted to Keypathwayminer Web',
						 text: 'We encountered the following error:<br/>' + resp,
						  finished: false]
			}
		}
	}
	
	def set_keep_pattern() {
		TimeSeriesClusteringVisualization vis = TimeSeriesClusteringVisualization.get(params.id)
		Pattern_g p = Pattern_g.get(params.patternId)
		if (params.keep == "true")
			p.keep = true
		else
			p.keep = false
		p.save(flush:true);
		render(status: 200)
	}
}
