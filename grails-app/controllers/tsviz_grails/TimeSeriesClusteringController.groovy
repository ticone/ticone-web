package tsviz_grails

import static groovyx.net.http.Method.*
import grails.converters.JSON
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder

import java.awt.BasicStroke
import java.awt.Color

import org.jfree.chart.ChartFactory
import org.jfree.chart.JFreeChart
import org.jfree.chart.axis.NumberAxis
import org.jfree.chart.axis.ValueAxis
import org.jfree.chart.encoders.EncoderUtil
import org.jfree.chart.plot.PlotOrientation
import org.jfree.chart.plot.SeriesRenderingOrder
import org.jfree.chart.plot.XYPlot
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
import org.jfree.data.xy.XYDataset
import org.jfree.data.xy.XYSeries
import org.jfree.data.xy.XYSeriesCollection
import org.jfree.ui.RectangleInsets
import org.scribe.model.Token

import uk.co.desirableobjects.oauth.scribe.OauthService

class TimeSeriesClusteringController {
	
	OauthService oauthService;

    def index() { }
	
	def getPatternTimeSeriesObjectsAsJSON() {
		//cache validFor: 3600
		TimeSeriesClustering clustering = TimeSeriesClustering.get(params.id);
		Pattern_g pattern = null;
		if (params.pattern) {
			pattern = Pattern_g.get(params.pattern.toInteger())
		}
		withCacheHeaders {
			//etag {
			//	"${clustering.id}:${clustering.version}"
			//}
			delegate.lastModified {
				clustering.dateCreated ?: clustering.lastUpdated
			}
			generate {
				render getPatternTimeSeriesObjects(clustering, pattern) as JSON;
			}
		}
	}
	
	def getClusteringAsJSON() {
		TimeSeriesClustering clustering = TimeSeriesClustering.get(params.id);
		
		Map<String, Set<String>> res = new HashMap<String, Set<String>>();
		
		clustering.patterns.each { Pattern_g p ->
			res.put(p.name, new HashSet<String>());
		}
		
		clustering.objectPatternCoefficients.each { ObjectBelongsToPattern objPattern ->
			res.get(objPattern.pattern.name).add(objPattern.object.name);
		}
		
		render res as JSON
	}
	
	def getClusteringAsTSV() {
		TimeSeriesClustering clustering = TimeSeriesClustering.get(params.id);
		
		Map<String, Set<String>> res = new TreeMap<String, Set<String>>();
		clustering.patterns.each { Pattern_g p ->
			res.put(p.name, new HashSet<String>());
		}
		clustering.objectPatternCoefficients.each { ObjectBelongsToPattern objPattern ->
			res.get(objPattern.pattern.name).add(objPattern.object.name);
		}
		
		StringBuilder sb = new StringBuilder("")
		res.keySet().each { k ->
			sb.append(k);
			sb.append("\t")
			res.get(k).each { v ->
				sb.append(v);
				sb.append("\t");
			}
			sb.deleteCharAt(sb.length()-1);
			sb.append("\n");
		}
		
		
		response.setContentType("application/octet-stream")
		response.setHeader("Content-disposition", "filename=clustering.tsv")
		response.outputStream << sb.toString()
		return
	}
	
	def sendClusteringToTis2ma() {
		Token dimanaToken = session[oauthService.findSessionKeyForAccessToken('dimana')]
		println dimanaToken
		
		TimeSeriesClustering clustering = TimeSeriesClustering.get(params.id);
		Map<String, Set<String>> res = new HashMap<String, Set<String>>();
		clustering.patterns.each { Pattern_g p ->
			res.put(p.name, new HashSet<String>());
		}
		clustering.objectPatternCoefficients.each { ObjectBelongsToPattern objPattern ->
			res.get(objPattern.pattern.name).add(objPattern.object.name);
		}
		
		def http = new HTTPBuilder(String.format("%s:%s", 
						 grailsApplication.config.tis2ma.host,
						 grailsApplication.config.tis2ma.port))
		http.request( POST ) { req ->
			uri.path = '/import/clusteringFromJSON'
			requestContentType = ContentType.JSON
			uri.query = [access_token: dimanaToken.token]
			body = [clusteringAsJSON: res,
				objectType: 'geneId']
		
			 response.success = { resp, json ->
				 
				 render String.format('Import of clustering successful. '
					 	+ 'Click <a href="%s:%s%s">here</a> to do follow up analysis with tis2ma.',
						 grailsApplication.config.tis2ma.host,
						 grailsApplication.config.tis2ma.port,
						 json.redirect_path);
				
			}
		
			 response.failure = { resp, json ->
				 // TODO: doesnt work
				 println "BLA"
				 render "Connection failed"
			}
		}
	}
	
	static String[] colors = ["#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
                        "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
                        "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
                        "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
                        "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
                        "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
                        "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
                        "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",
                        "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
                        "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
                        "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
                        "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
                        "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
                        "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
                        "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
                        "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", "#012C58"]
	
	def getPatternTimeSeriesObjectsAsFreeChart() {
		//cache validFor: 3600
		TimeSeriesClustering clustering = TimeSeriesClustering.get(params.id);
		//Pattern_g pattern = clustering.patterns.find {it.number == params.pattern.toInteger()}
		Pattern_g pattern = Pattern_g.get(params.pattern.toInteger())
		boolean withAxes = params.containsKey("withAxes");
		
		withCacheHeaders {
			delegate.lastModified {
				clustering.dateCreated ?: clustering.lastUpdated
			}
			generate {
				Map<Integer, List<Double>> objectSignals = getPatternTimeSeriesObjects(clustering, pattern);
				
				final XYSeriesCollection dataset = new XYSeriesCollection();
				int count = 0;
				for (String s: objectSignals.keySet()) {
					final XYSeries series1 = new XYSeries(s);
					List<Double> signals = objectSignals.get(s);
					for (int x = 0; x < signals.size(); x++) {
						series1.add(x+1, signals.get(x));
					}
					dataset.addSeries(series1);
					// we limit the nuber of objects to 500
					count++;
					if (count >= 500)
						break;
				}
				
				String patternSeriesName = "Cluster " + pattern.number;
				
				final XYSeries patternSeries = new XYSeries(patternSeriesName);
				for (int x = 0; x < pattern.timeSeriesPattern.length; x++) {
					patternSeries.add(x+1, pattern.timeSeriesPattern[x]);
				}
				dataset.addSeries(patternSeries);
				
				final JFreeChart chart = createChart(dataset, colors[(int)(pattern.number%colors.length)], patternSeriesName, withAxes);
				
				def buffer = chart.createBufferedImage( 500, 500)
				def bytes = EncoderUtil.encode( buffer, 'png' )
				
				response.contentType = 'image/png'
				response.contentLength = bytes.length
				def out = response.outputStream
				out.write( bytes )
				out.flush()
				out.close()
			}
		}
	}
	
	static def getPatternTimeSeriesObjects(TimeSeriesClustering clustering, Pattern_g pattern) {
		Map<Integer, Map<String, List<Double>>> res = new TreeMap<Integer, Map<String, List<Double>>>();
		
		// add all patterns, independent of whether there are any objects assigned
		clustering.patterns.each { p ->
			if (!res.containsKey(p.number)) {
				res.put(p.number, new TreeMap<String, List<Double>>());
			}
		}
		def startTime = System.currentTimeMillis();
		ObjectBelongsToPattern.executeQuery("select op from ObjectBelongsToPattern op join op.pattern p join op.object o join o.samples s where op.clustering = ?", [clustering])
		//clustering.objectPatternCoefficients.each { ObjectBelongsToPattern objPat ->
		startTime = System.currentTimeMillis();
		
		def result;
		if (pattern != null) {
			int limit = 500*clustering.objects?.find {true}.samples?.find{true}.timepoints.size();
			result = ObjectBelongsToPattern.executeQuery(
				"select op.pattern.number,o.name,s.name,tp.timepoint,tp.timeSeriesSignal from " 
				+ "ObjectBelongsToPattern op join op.pattern p join op.object o join o.samples s join s.timepoints tp "
				+ " where op.clustering = ? and op.pattern = ? order by o.id, s.id, tp.timepoint asc", [clustering, pattern], [max: limit])
		} else {
			result = ObjectBelongsToPattern.executeQuery(
				"select op.pattern.number,o.name,s.name,tp.timepoint,tp.timeSeriesSignal from "
				+ "ObjectBelongsToPattern op join op.pattern p join op.object o join o.samples s join s.timepoints tp "
				+ " where op.clustering = ? order by tp.timepoint asc", [clustering])
		}
		
		result.each { item ->
			def patternNo = item[0]
			def objName = item[1]
			def sampleName = item[2]
			def tp = item[3]
			def signal = item[4]
			def objSample = objName + " (" + sampleName + ")"
			if (!res.containsKey(patternNo)) {
				res.put(patternNo, new TreeMap<String, List<Double>>());
			}
			if (!res.get(patternNo).containsKey(objSample)) {
				if (res.get(patternNo).keySet().size() > 500) {
					return;
				}
				res.get(patternNo).put(objSample, new ArrayList<Double>());
			}
			
			res.get(patternNo).get(objSample).add(signal)
			
		}
		startTime = System.currentTimeMillis();
		if (pattern != null) {
			def patternName = clustering.patterns.find {it.id == pattern.id}.number;
			Map<String, Double> patternMap;
			if (res.containsKey(patternName)) {
				patternMap = res.get(patternName);
			}
			else {
				
				patternMap = new TreeMap<String, Double>();
			}
			return patternMap
		} else {
			return res
		}
	}
	
	static def XYDataset createDataset() {
		Random r = new Random();
		final XYSeriesCollection dataset = new XYSeriesCollection();
		
		for (int i = 0; i < 1000; i++) {
			final XYSeries series1 = new XYSeries(i.toString());
			for (int x = 0; x < 10; x++) {
				series1.add(x+1, r.nextInt(10));
			}
			dataset.addSeries(series1);
		}
		
		return dataset;
		
	}
	
	/**
	 * Creates a chart.
	 *
	 * @param dataset  the data for the chart.
	 *
	 * @return a chart.
	 */
	static def JFreeChart createChart(final XYSeriesCollection dataset, final String color, final String patternSeriesName, boolean withAxes) {
		
		// create the chart...
		final JFreeChart chart = ChartFactory.createXYLineChart(
			"",      // chart title
			"",                      // x axis label
			"",                      // y axis label
			dataset,                  // data
			PlotOrientation.VERTICAL,
			false,                     // include legend
			true,                     // tooltips
			false                     // urls
		);

		chart.setBackgroundPaint(Color.white);
		chart.setBorderVisible(false);
		chart.setPadding(RectangleInsets.ZERO_INSETS);
		
		final XYPlot plot = chart.getXYPlot();
		if (!withAxes) {
			plot.setInsets(RectangleInsets.ZERO_INSETS);
			plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
			ValueAxis range = plot.getRangeAxis();
			range.setVisible(false);
			range = plot.getDomainAxis()
			range.setVisible(false);
		}
		plot.setOutlineVisible(false);
		plot.setSeriesRenderingOrder(SeriesRenderingOrder.FORWARD);
		plot.setBackgroundPaint(Color.white);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		
		final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setShapesVisible(false);
		//renderer.setPaint(hex2Rgb(color, 0.6));
		int patternSeriesIndex = dataset.getSeriesIndex(patternSeriesName)
		for (XYSeries series: dataset.getSeries()) {
			int currentSeriesIndex = dataset.getSeriesIndex(series.getKey());
			if (currentSeriesIndex != patternSeriesIndex) {
				renderer.setSeriesPaint(currentSeriesIndex, hex2Rgb(color, 0.6));
			} else {
				renderer.setSeriesPaint(currentSeriesIndex, Color.black);
				renderer.setSeriesStroke(patternSeriesIndex, new BasicStroke(3));
			}
		}
		
		plot.setRenderer(renderer);

		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		plot.getDomainAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
				
		return chart;
		
	}
	
	static def Color hex2Rgb(String colorStr, float alpha) {
		return new Color(
				Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
				Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
				Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) ,
				(int)(alpha*255));
	}
}
