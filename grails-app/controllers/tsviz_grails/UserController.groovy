package tsviz_grails

import org.apache.shiro.SecurityUtils

class UserController {
	
	def shiroSecurityService

    def index() { }
	
	def show() {
		User user = User.findByUsername(SecurityUtils.getSubject().getPrincipal())
		render(view: 'show', model: [user: user])
	}
	
	def change_password() {
		User user = User.findByUsername(SecurityUtils.getSubject().getPrincipal())
		if (params.currentpassword && params.password && params.password2) {
			// check that the user typed the correct password
			if (shiroSecurityService.encodePassword(params.currentpassword) == user.passwordHash) {
				// check if passwords match
				if (params.password == params.password2) {
					user.passwordHash = shiroSecurityService.encodePassword(params.password)
					if (user.validate()) {
						user.save(flush: true);
						flash.message = "Password changed successfully"
					} else {
						flash.message = "The password is not valid"
						user.discard();
						user = User.findByUsername(SecurityUtils.getSubject().getPrincipal())
					}
				} else {
				flash.message = "The passwords do not match"
			}
			} else {
				flash.message = "You misstyped your current password"
			}
		}
		render(view: 'change_password', model: [user: user])
	}
	
	def change_email() {
		User user = User.findByUsername(SecurityUtils.getSubject().getPrincipal())
		if (params.email) {
			user.email = params.email;
			if (user.validate()) {
				user.save(flush: true);
				flash.message = "E-Mail address changed successfully!"
			} else {
				flash.message = "The E-Mail address is not valid!"
				user.discard();
				user = User.findByUsername(SecurityUtils.getSubject().getPrincipal())
			}
		}
		render(view: 'change_email', model: [user: user])
	}
}
