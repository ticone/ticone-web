package tsviz_grails

import tsviz_Grails.IterationHasVisualization

class TutorialController {

	def index() {
	}

	def show_workflow() {
		String viewBaseName;
		Map<String, List<String>> versions;
		String product;
		switch(params["id"]) {
			case "1":
				def websiteVersionToWebVersion = [
					"1":["1.0.0"],
					"2":["1.0.1", "1.0.2", "1.0.3", "1.0.4", "1.0.5"]]
				product = "TiCoNE WEB";
				versions = websiteVersionToWebVersion;
				viewBaseName = 'show_demo_workflow_1';
				break
			case "2":
				def websiteVersionToCytoscapeAppVersion = [
					"1": [
						"1.1.44"
						],
					"2": [
						"1.2.54",
						"1.2.55",
						"1.2.56",
						"1.2.57",
						"1.2.58",
						"1.2.59",
						"1.2.60",
						"1.2.61",
						"1.2.62",
						"1.2.63",
						"1.2.64",
						"1.2.65",
                        "1.2.66",
                        "1.2.67",
                        "1.2.68",
                        "1.2.69",
                        "1.2.70",
                        "1.2.71"
						],
					"3": [
						"1.3.77",
						"1.3.79"
						]]
				product = "TiCoNE Cytoscape App";
				versions = websiteVersionToCytoscapeAppVersion;
				viewBaseName = 'show_cyto_app_workflow';
				break
			case "3":
				def websiteVersionToWebVersion = [
					"1":["1.0.0"],
					"2":["1.0.1", "1.0.2", "1.0.3", "1.0.4", "1.0.5"]]
				product = "TiCoNE WEB";
				versions = websiteVersionToWebVersion;
				viewBaseName = 'show_web_tutorial';
				break;
			case "4":
				def websiteVersionToCytoscapeAppVersion = [
					"1": [
						"1.2.54",
						"1.2.55",
						"1.2.56",
						"1.2.57",
						"1.2.58",
						"1.2.59",
						"1.2.60",
						"1.2.61",
						"1.2.62",
						"1.2.63",
						"1.2.64",
						"1.2.65",
                        "1.2.66",
                        "1.2.67",
                        "1.2.68",
                        "1.2.69",
                        "1.2.70",
                        "1.2.71"
					],
					"2": [
						"1.3.77",
						"1.3.79"
						]]
				product = "TiCoNE Cytoscape App";
				versions = websiteVersionToCytoscapeAppVersion;
				viewBaseName = 'show_cyto_app_workflow_cluster_connectivity';
				break;
			case "5":
				def websiteVersionToCytoscapeAppVersion = [
					"1": [
						"1.2.54",
						"1.2.55",
						"1.2.56",
						"1.2.57",
						"1.2.58",
						"1.2.59",
						"1.2.60",
						"1.2.61",
						"1.2.62",
						"1.2.63",
						"1.2.64",
						"1.2.65",
                        "1.2.66",
                        "1.2.67",
                        "1.2.68",
                        "1.2.69",
                        "1.2.70",
                        "1.2.71"
					],
					"2": [
						"1.3.77",
						"1.3.79"
						]]
				product = "TiCoNE Cytoscape App";
				versions = websiteVersionToCytoscapeAppVersion;
				viewBaseName = 'show_cyto_app_workflow_clustering_comparison';
				break;
			default:
				break
		}

		def versionsList = new ArrayList<String>(versions.keySet());

		def lastNextVer = [:]

		if (!("ver" in params) || !(versions.containsKey(params["ver"]))) {
			redirect(action: "show_workflow", id: params["id"], params: [ver: versionsList.last()])
			return;
		}
		else {
			int ind = versionsList.indexOf(params["ver"])
			
			if (ind > 0) {
				lastNextVer['lastVer'] = versionsList[ind-1]
			}
			if (ind < versions.size()-1) {
				lastNextVer['nextVer'] = versionsList[ind+1]
			}
		}

		lastNextVer['isLatest'] = params["ver"] == versionsList.last()

		def compatibleVersions = versions.get(params["ver"])
		
		// we summarize successive versions into a range
		def summarizedVersionsList = new ArrayList<String>();
		int indexFirstVersionOfRange = -1;
		for (int i = 0; i < compatibleVersions.size(); i++) {
			int[] version1 = parseVersionString(compatibleVersions.get(i));
			if (i < compatibleVersions.size()-1) {
				int[] version2 = parseVersionString(compatibleVersions.get(i+1));
				def isNext = isNextVersionOf(version1, version2);
				if (isNext) {
					// we already were in a range
					if (indexFirstVersionOfRange > -1 ) {
						// dont do anything
					}
					// we start a new range
					else {
						indexFirstVersionOfRange = i;
					}
					continue;
				}
			}
			// this is the end of a range
			if (indexFirstVersionOfRange > -1) {
				int[] firstVersionOfRange = parseVersionString(compatibleVersions.get(indexFirstVersionOfRange));
				String rangeString = getRangeForTwoVersions(firstVersionOfRange, version1);
				summarizedVersionsList.add(rangeString);
			}
			// we havent been in a range and we do not start a range
			else {
				summarizedVersionsList.add(compatibleVersions.get(i));
			}
			indexFirstVersionOfRange = -1;
		}
		

		lastNextVer['compatibleVersions'] = compatibleVersions
		
		lastNextVer['summarizedVersionsList'] = summarizedVersionsList

		lastNextVer['product'] = product

		render(view: viewBaseName + '_' + params["ver"], model: lastNextVer)
	}

	def parseVersionString(final String versionString) {
		String[] split = versionString.split("\\.");
		int[] numbers = new int[split.length];

		for (int i = 0; i < split.length; i++) {
			numbers[i] = Integer.valueOf(split[i]);
		}

		return numbers;
	}

	def isNextVersionOf(final int[] version1, final int[] version2) {
		boolean res = (version1[0] == version2[0]) && (version1[1] == version2[1]) && (version1[2] == version2[2]-1);
		return res;
	}
	
	def getRangeForTwoVersions(final int[] version1, final int[] version2) {
		return version1[0] + "." + version1[1] + "." + version1[2] + "-" + version2[0] + "." + version2[1] + "." + version2[2];
	}
}
