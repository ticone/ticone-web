package tsviz_grails

import static groovyx.net.http.Method.*
import grails.converters.JSON
import groovy.sql.Sql
import groovyx.net.http.AsyncHTTPBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder

import org.apache.shiro.SecurityUtils
import org.codehaus.groovy.grails.web.mapping.LinkGenerator

import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues
import dk.sdu.imada.ticone.io.Parser
import dk.sdu.imada.ticone.tsdata.TimeSeriesObject
import dk.sdu.imada.ticone.api.AbstractTimeSeriesPreprocessor
import dk.sdu.imada.ticone.api.Cluster

class GraphController {

	static def dataSourceUnproxied
	static def grailsApplication
	LinkGenerator grailsLinkGenerator

	def index() {
		render(view: 'index', model: [networks: accessibleNetworks()])
	}

	def create() {
		def user;
		if (SecurityUtils.getSubject().isAuthenticated()) {
			user = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
		}
		
		int popup = params.int('popup', 0)
		render(view: 'create', model: [user: user] + params)
	}
	
	def upload() {
		try {
			GraphFile gFile;
			println params
			// if we have a file uploaded
			if (params.graphFile) {
				byte[] fileBytes = request.getFile("graphFile").getBytes();
				
				gFile = new GraphFile(tmpFile: fileBytes);
				gFile.save(flush: true);
			}
			// if we have a graph file id passed (from async_upload)
			else {
				gFile = GraphFile.get(params.gFileId)
			}
			
			println "Creating graph instance from graph input"
			long start = System.currentTimeMillis();
			//Graph g = GraphController.loadGraphFromFile(graphFile, request.getParameter("name"));
			Graph g = GraphController.loadGraphBatchFromFile(gFile.tmpFile, request.getParameter("name"));
			println "time needed to insert graph:"
			println System.currentTimeMillis()-start;
			
			if (SecurityUtils.getSubject().isAuthenticated()) {
				def user = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
				g.user = user;
			} else {
				g.sessionId = session.getId();
			}
			
			if (params.published == 'on') {
				g.published = true;
			}
			g.save(flush: true);
			
			
			
			// if we have been passed a taskId, we inform the task that it is finished now
			if (params.taskId) {
				def url;
				if (params.popup == "1") {
					url = createLink(absolute: true, action: 'upload_finished');
				} else {
					url = createLink(absolute: true, action: 'index');
				}
				setTaskFinished(params.taskId, url, null);
			}
			if (params.popup == "1") {
				render(view: 'upload')
				return;
			}
			redirect(action: 'index')
		} catch (Exception e) {
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage());
			}
		}
	}
	
	def upload_finished() { }
	
	def async_upload() {
		// parameter validation
		if (!params.name) {
			flash.error = 'Please provide a name for your network'
			redirect(action: 'create', params: params)
			return
		}
		byte[] fileBytes = null;
		if (!request.getFile("graphFile").isEmpty()) {
			fileBytes = request.getFile("graphFile").getBytes();
		}
		GraphFile gFile = new GraphFile(tmpFile: fileBytes);
		if (!gFile.validate()) {
			if (gFile.errors.hasFieldErrors("tmpFile")) {
				flash.error = 'Please select a valid file'
			}
			redirect(action: 'create', params: params)
			return
		}
		gFile.save(flush: true);
		
		def p = [name: params.name, gFileId: gFile.id]
		if (params.published) {
			p['published'] = params.published
		}
		if (params.popup) {
			p['popup'] = params.popup
		}
		
		def url = createLink(controller: 'graph', action: 'upload')
		async_decorate_and_submit_task("Processing Your Network", url, false, p, request.cookies);
	}
	
	def async_decorate_and_submit_task(taskTitle, url, canCancel, p, cookies) {
		Task t = create_task(taskTitle, canCancel);
		
		def base = grailsLinkGenerator.serverBaseURL - grailsLinkGenerator.contextPath
		def curruser = null;
		if (SecurityUtils.getSubject().isAuthenticated()) {
			curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
		}
		
		def postParams;
		if (curruser != null) {
			postParams = [taskId: t.taskId, userId: curruser.id] + p
		} else {
			postParams = [taskId: t.taskId, userSessionId: session.getId()] + p
		}
		
		def http = new AsyncHTTPBuilder(poolSize: 4, uri: base)
		http.ignoreSSLIssues()
		http.request( POST ) { req ->
			uri.path = url
			requestContentType = ContentType.JSON
			uri.query = postParams
			headers['Cookie'] = cookies.collect {
				it.getName() + "=" + it.getValue()
			}.join('; ')
			body = []
		
			 response.success = { resp, json ->
				 println "SUCCESS"
			}
		
			 response.failure = { resp, json ->
				 println "FAILED"
				 render "Connection failed"
			}
		}
		redirect(controller: 'task', action: 'show', id: t.taskId)
	}
	
	def create_task(title, canCancel) {
		def curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
		def task_id = Task.create_task_id();
		Task t = new Task(taskId:task_id,
			title:title,
			user:curruser,
			finished:false,
			canCancel:canCancel,
			cancelled:false,
			sendMail:false)
		t.save(flush: true);
		return t;
	}
	
	def setTaskFinished(String taskId, String redirect_url, String statusMessage) {
		Task t = Task.findByTaskId(taskId)
		t.refresh();
		t.finished = true;
		t.redirect_url = redirect_url;
		if (statusMessage) {
			t.statusMessage = statusMessage;
		}
		t.save(flush: true);
		
		sendTaskMail(t);
	}
	
	def sendTaskMail(Task t) {
		if (t.sendMail && t.user) {
			sendMail {
				to(t.user.email)
				from "noreply@compbio.sdu.dk"
				subject "TiCoNE: " + t.title + " finished"
				html '<p>Your job has finished.</p><p>You can access the result here: <a href="' + t.redirect_url + '">' + t.redirect_url + '</a></p>'
			  }
		}
	}

	/*static def loadGraphFromFile(File f, String name) {
		GraphFile graphFile = new GraphFile(tmpFile: f);
		graphFile.save();
		
		Graph g = new Graph(file: graphFile, name: name, published: false);

		Map<String,Node> nodes = new HashMap<String,Node>();
		Map<String,Set<String>> edges = new HashMap<String,HashSet<String>>();
		def split;
		f.eachLine { line ->
			split = line.split("\t");
			// check if nodes are in graph
			Node n1;
			Node n2;
			if (!nodes.containsKey(split[0])) {
				// try to find a corresponding object
				n1 = new Node(name: split[0]);
				g.addToNodes(n1);
				nodes.put(split[0],n1);

			} else {
				n1 = nodes.get(split[0])
			}
			if (!nodes.containsKey(split[1])) {
				n2 = new Node(name: split[1]);
				g.addToNodes(n2);
				nodes.put(split[1],n2);
			} else {
				n2 = nodes.get(split[1])
			}

			// check if edge is in there (both directions)
			if (!(edges.containsKey(split[0]) && edges.get(split[0]).contains(split[1])) &&
			!(edges.containsKey(split[1]) && edges.get(split[1]).contains(split[0]))) {
				Edge e = new Edge(n1: n1, n2: n2);
				g.addToEdges(e);
				if (!edges.containsKey(split[0])) {
					edges.put(split[0],new HashSet<String>());
				}
				if (!edges.containsKey(split[1])) {
					edges.put(split[1],new HashSet<String>());
				}

				edges.get(split[0]).add(split[1]);
				edges.get(split[1]).add(split[0]);
			}
		}
		g.save(flush: true)
	}*/

	static def mapGraphAndDataSet(Graph g, TimeSeriesDataSet dataSet, boolean onlyCommon) {
		if (onlyCommon) {
			// only take those nodes / objects which are present in both
			Set<String> common = new HashSet<String>();
			common.addAll(g.nodes.collect {
				it.name
			});
			common.retainAll(dataSet.objects.collect {
				it.name
			});
		
			//StringBuilder sb = new StringBuilder();
			
			// we do not throw unmapped nodes out of the network
			//g.edges.each { edge ->
			//	String n1 = edge.n1.name;
			//	String n2 = edge.n2.name;
			//	if (common.contains(n1) && common.contains(n2)) {
			//		sb.append(n1);
			//		sb.append("\t");
			//		sb.append(n2);
			//		sb.append("\n");
			//	}
			//}
			
			//g = loadGraphBatchFromFile(sb.toString().getBytes(), g.name + " intersect")
		
			Scanner scanner = new Scanner(new ByteArrayInputStream(dataSet.file.tmpFile));
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] split = line.split("\t");
				// TODO: is this a good idea?
				String id = split[1];
				if (common.contains(id)) {
					os.write((line + "\n").getBytes());
				}
			}
			scanner.close();
			AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
			scanner = new Scanner(new ByteArrayInputStream(os.toByteArray()));
			List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
			scanner.close();
			
			calc.initializeTimeSeriesData(tsData);
			calc.calculatePatterns();
			
			TimeSeriesFile file = new TimeSeriesFile(tmpFile: os.toByteArray());
			file.save();
			
			println "before DS"
			dataSet = TimeSeriesDataSetController.parseDataSetFromInputBatch(file, dataSet.name + " Intersect", tsData);
			
			println "after DS"
		}
		
		g.nodes.each { node ->
			tsviz_grails.TimeSeriesObject obj = tsviz_grails.TimeSeriesObject.findByDataSetAndName(dataSet, node.name)
			node.object = obj
		}
		g.save(flush: true)
		
		return [g, dataSet]
	}

	static def loadGraphBatchFromFile(byte[] contents, String name) {
		
		Scanner scan = new Scanner(new ByteArrayInputStream(contents));

		def batchSize = 1000000;

		def addedNodes = new HashSet<String>();

		def nextNodeId = -1;
		def nodeIds = [];
		Map<String,Integer> nodeNameToId = new HashMap<String,Integer>();
		
		print "Writing graph nodes into database"
		long startTime = System.currentTimeMillis();
		//create an sql instance for direct inserts via groovy sql
		def sql = Sql.newInstance(dataSourceUnproxied);
		sql.withTransaction {
			// get maximal node id
			sql.eachRow('select max(id)+1 as id from node') { row ->
				nextNodeId =  row.id;
			}
			if (nextNodeId == null)
				nextNodeId = 1;

			// insert nodes
			try{
				sql.withBatch(batchSize, 'insert into node (id, version, name) values (?, ?, ?)') { stmt ->
					while (scan.hasNextLine()) {
						String line = scan.nextLine();
						def split = line.split("\t");
						// check if nodes are in graph
						if (!addedNodes.contains(split[0])) {
							addedNodes.add(split[0]);
							stmt.addBatch(nextNodeId, 1, split[0]);
							nodeNameToId.put(split[0], nextNodeId);
							nodeIds.add(nextNodeId);
							nextNodeId = nextNodeId+1;
						}
						if (!addedNodes.contains(split[1])) {
							addedNodes.add(split[1]);
							stmt.addBatch(nextNodeId, 1, split[1]);
							nodeNameToId.put(split[1], nextNodeId);
							nodeIds.add(nextNodeId);
							nextNodeId = nextNodeId+1;
						}
					}
				}
			} catch(java.sql.BatchUpdateException bue){
				return "Could not add nodes: ${bue.getMessage()}"
			} finally{
				sql.commit();
			}
		}
		println " ... done"
		println "time: " + (System.currentTimeMillis() - startTime).toString();
		
		def nextEdgeId = -1;
		def edgeIds = [];
		
		scan.close();
		scan = new Scanner(new ByteArrayInputStream(contents));
		
		print "Writing graph edges into database"
		startTime = System.currentTimeMillis();
		sql.withTransaction {
			// get maximal node id
			sql.eachRow('select max(id)+1 as id from edge') { row ->
				nextEdgeId =  row.id;
			}
			if (nextEdgeId == null)
				nextEdgeId = 1;

			//Map<String,Set<String>> edges = new HashMap<String,HashSet<String>>();

				//TODO: for now we assume, that we have only unique edges in file
			// insert edges
			try{
				sql.withBatch(batchSize, 'insert into edge (id, version, n1_id, n2_id) values (?, ?, ?, ?)') { stmt ->
					while (scan.hasNextLine()) {
						String line = scan.nextLine();
						def split = line.split("\t");
						// check if nodes are in graph
						//if (!(edges.containsKey(split[0]) && edges.get(split[0]).contains(split[1])) &&
						//!(edges.containsKey(split[1]) && edges.get(split[1]).contains(split[0]))) {
							//def n1 = Node.findByName(split[0]);
//							def n1 = Node.findById(nodeNameToId.get(split[0]));
							//def n2 = Node.findByName(split[1]);
//							def n2 = Node.findById(nodeNameToId.get(split[1]));
							
							stmt.addBatch(nextEdgeId, 1, nodeNameToId.get(split[0]), nodeNameToId.get(split[1]));
							edgeIds.add(nextEdgeId);
							nextEdgeId = nextEdgeId+1;

//							if (!edges.containsKey(split[0])) {
//								edges.put(split[0],new HashSet<String>());
//							}
//							if (!edges.containsKey(split[1])) {
//								edges.put(split[1],new HashSet<String>());
//							}

//							edges.get(split[0]).add(split[1]);
//							edges.get(split[1]).add(split[0]);
						//}
					}
				}
			} catch(java.sql.BatchUpdateException bue){
				return "Could not add edges: ${bue.getMessage()}"
			} finally{
				sql.commit();
			}
		}
		println " ... done"
		println "time: " + (System.currentTimeMillis() - startTime).toString();
		scan.close();
		GraphFile graphFile = new GraphFile(tmpFile: contents);
		graphFile.save();
		
		println NetworkGroup.list()
		Graph g = new Graph(file: graphFile, name: name, published: false, group: NetworkGroup.findByName('User network'));
		g.save();

		int graphId = g.id;
		
		print "Writing graph -> node associations into database"
		startTime = System.currentTimeMillis();
		sql.withTransaction {
			// insert graph_nodes
			try{
				sql.withBatch(batchSize, 'insert into graph_node (graph_nodes_id, node_id) values (?, ?)') { stmt ->
					nodeIds.each { nodeId ->
						stmt.addBatch(graphId, nodeId);
					}
				}
			} catch(java.sql.BatchUpdateException bue){
				return "Could not add graph_nodes: ${bue.getMessage()}"
			} finally{
				sql.commit();
			}
		}
		println " ... done"
		println "time: " + (System.currentTimeMillis() - startTime).toString();
		
		print "Writing graph -> edge associations into database"
		startTime = System.currentTimeMillis();
		sql.withTransaction {
			// insert graph_edges
			try{
				sql.withBatch(batchSize, 'insert into graph_edge (graph_edges_id, edge_id) values (?, ?)') { stmt ->
					edgeIds.each { edgeId ->
						stmt.addBatch(graphId, edgeId);
					}
				}
			} catch(java.sql.BatchUpdateException bue){
				return "Could not add graph_edges: ${bue.getMessage()}"
			} finally{
				sql.commit();
			}
		}
		println " ... done"
		println "time: " + (System.currentTimeMillis() - startTime).toString();

		//refresh graph because hibernate does not know about our changes
		g.refresh();
	}

	def nodesAsJSON() {
		Graph g = Graph.get(params.id)
		def nodes = []
		for (Node n: g.nodes) {
			def res = [id: n.name]
			for (String p: n.coefficients.keySet()) {
				res.put(p, n.coefficients.get(p).toDouble());
			}
			def data = [data: res]
			nodes.add(data);
		}
		def renderBla = [
			'nodes': nodes]
		render renderBla as JSON
	}

	def edgesAsJSON() {
		Graph g = Graph.get(params.id)
		def edges = []
		def i=1
		for (Edge n: g.edges) {
			def res = [id: "Edge"+i, source: n.n1.name, target: n.n2.name]
			def data = [data: res]
			edges.add(data);
			i=i+1
		}
		def renderBla = [
			'edges': edges]
		render renderBla as JSON
	}

	static def accessibleNetworks(session) {
		if (SecurityUtils.getSubject().isAuthenticated()) {
			def curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
			return Graph.where {(published == true) || (user == curruser)}.list()
		} else {
			if (session)
				return Graph.where {(published == true) || (sessionId == session.getId())}.list()
			else
				return Graph.where {(published == true)}.list()
		}
	}

	def nodesAndEdgesAsJSON() {
		TimeSeriesClusteringVisualization vis = TimeSeriesClusteringVisualization.get(params.id)
		ObjectBelongsToPattern[] coefficients = vis.clustering.objectPatternCoefficients;
		Graph g = vis.graph

		GraphLayout layout = vis.graphLayout;
		//NodeLocation[] nodeLocations = layout.locations;

		def nodes = []
		//for (NodeLocation loc: nodeLocations) {
		//def startTime = System.currentTimeMillis()
		//println Graph.executeQuery("select n.name, n.id, p.name, max(op.coefficient) from Graph g join g.nodes n join n.object o join o.objectPatterns op join op.pattern p where g = ? group by (o.id, p.id)", [g])
		//println System.currentTimeMillis()-startTime
		//for(Node n: g.nodes) {
		Graph.executeQuery("select n.name, n.id, p.name, max(op.coefficient) "
			+ "from Graph g join g.nodes n join n.object o join o.objectPatterns op join op.pattern p "
			+ "where g = ? group by o.id, p.id", [g]).each { item ->
			//Node n = loc.node;
			def res = [id: item[0], dbid: item[1]]
			//ObjectBelongsToPattern.findAllByClusteringAndObject(vis.clustering, n.object).each { ObjectBelongsToPattern nodePattern ->
			//	res.put(nodePattern.pattern.name, nodePattern.coefficient);
			//}
			res.put(item[2], item[3]);
			def data = [data: res]
			nodes.add(data);
		}
		def renderBlaNodes = [
			'nodes': nodes]

		def edges = []
		def i=1
		//for (Edge n: g.edges) {
		Graph.executeQuery("select e.n1.name, e.n2.name "
			+ "from Graph g join g.edges e join e.n1 n1 join e.n2 n2 "
			+ "where g = ?", [g]).each { item ->
			//def res = [id: "Edge"+i, source: n.n1.name, target: n.n2.name]
			def res = [id: "Edge"+i, source: item[0], target: item[1]]
			def data = [data: res]
			edges.add(data);
			i=i+1
		}
		def renderBlaEdges = [
			'edges': edges]
		def renderAll = ['nodes': nodes,'edges': edges]
		render renderAll as JSON
	}
	
	def importLatestBioGRIDFromWebsite() {
		// retrieve current version of BioGRID as string
		def http = new HTTPBuilder('http://webservice.thebiogrid.org')
		http.ignoreSSLIssues()
		http.request( GET ) { req ->
			uri.path = "/version/"
			requestContentType = ContentType.JSON
			uri.query = [accessKey: grailsApplication.config.biogrid.access_key]
		
			 response.success = { resp, reader ->
				 println "SUCCESS"
				 String version = reader.text
				 println version
				 def url = "http://thebiogrid.org/downloads/archives/Release%20Archive/BIOGRID-" + version + "/BIOGRID-ORGANISM-" + version + ".tab2.zip"
				 Graph.importBioGRIDFromZIPFile(version, url);
			}
		
			 response.failure = { resp, json ->
				 println "FAILED"
				 render "Connection failed"
			}
		}
	}
}

class PatternComparator implements Comparator<Cluster> {

	@Override
	public int compare(Cluster o1, Cluster o2) {
		return o1.toString().compareTo(o2.toString());
	}

}