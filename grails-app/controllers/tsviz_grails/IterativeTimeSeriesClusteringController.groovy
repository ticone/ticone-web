package tsviz_grails

import static groovyx.net.http.Method.*
import grails.converters.JSON
import groovyx.net.http.AsyncHTTPBuilder
import groovyx.net.http.ContentType

import java.nio.file.AccessDeniedException
import java.security.SecureRandom

import org.apache.shiro.SecurityUtils
import org.codehaus.groovy.grails.web.mapping.LinkGenerator
import org.scribe.model.Token

import tsviz_Grails.IterationHasVisualization
import uk.co.desirableobjects.oauth.scribe.OauthService
import dk.sdu.imada.ticone.api.AbstractTimeSeriesPreprocessor
import dk.sdu.imada.ticone.api.Cluster
import dk.sdu.imada.ticone.api.IAggregateCluster
import dk.sdu.imada.ticone.api.IClustering
import dk.sdu.imada.ticone.api.IDiscretizePrototype
import dk.sdu.imada.ticone.api.ISimilarity
import dk.sdu.imada.ticone.api.ITimeSeriesClusteringWithOverrepresentedPatterns
import dk.sdu.imada.ticone.api.PatternObjectMapping
import dk.sdu.imada.ticone.api.PatternObjectMapping.DELETE_METHOD
import dk.sdu.imada.ticone.clustering.BasicTimeSeriesClusteringWithOverrepresentedPatterns
import dk.sdu.imada.ticone.clustering.CLARAClustering
import dk.sdu.imada.ticone.clustering.PAMKClustering
import dk.sdu.imada.ticone.clustering.STEMClustering
import dk.sdu.imada.ticone.clustering.TransClustClustering
import dk.sdu.imada.ticone.clustering.refinement.AggregateClusterMean
import dk.sdu.imada.ticone.clustering.refinement.AggregateClusterMedian
import dk.sdu.imada.ticone.clustering.splitpattern.AbstractSplitCluster
import dk.sdu.imada.ticone.clustering.splitpattern.SplitClusterBasedOnTwoMostDissimilarObjects
import dk.sdu.imada.ticone.clustering.splitpattern.SplitClusterContainer
import dk.sdu.imada.ticone.clustering.splitpattern.SplitClusterWithClustering
import dk.sdu.imada.ticone.io.Parser
import dk.sdu.imada.ticone.permutation.PermutateDatasetRowwise
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues
import dk.sdu.imada.ticone.preprocessing.discretize.DiscretizePatternMinToMax
import dk.sdu.imada.ticone.similarity.NegativeEuclideanSimilarity
import dk.sdu.imada.ticone.similarity.PearsonCorrelation
import dk.sdu.imada.ticone.tsdata.TimeSeriesObject
import dk.sdu.imada.ticone.util.ExtractData
import dk.sdu.imada.ticone.util.PatternUtility
import dk.sdu.imada.ticone.util.StatsCalculation
import dk.sdu.imada.ticone.util.TimePointWeighting

class IterativeTimeSeriesClusteringController {

	OauthService oauthService;
	LinkGenerator grailsLinkGenerator

	def index() {
		redirect(action: 'create')
	}

	def create() {
		List<TimeSeriesDataSet> tsDataSets = TimeSeriesDataSetController.accessibleDataSets(session);
		List<Graph> graphs;
		
		if (SecurityUtils.getSubject().isAuthenticated()) {
			def curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
			
			graphs = Graph.where {(published == true) || (user == curruser)}.list()
		} else {
			graphs = Graph.where {published == true }.list()
		}
		List<SimilarityFunction> simFunctions = SimilarityFunction.list()
		println ClusteringMethod.list()
		List<ClusteringMethod> clusteringMethods = ClusteringMethod.where {available == true}.list(sort: "name")
		println clusteringMethods
		List<PatternRefinementFunction> refFunctions = PatternRefinementFunction.list(sort: "name")
		def user;
		return [
			tsDataSets: tsDataSets, 
			graphs: graphs, 
			simFunctions: simFunctions, 
			clusteringMethods: clusteringMethods, 
			refFunctions: refFunctions]
	}
	
	def async_upload() {
		println params
		// parameter validation
		if (!params.tsDataSet) {
			flash.error = 'Please select a data set'
			redirect(action: 'create', params: params)
			return
		}
		if (!params.numberPatterns || params.numberPatterns.toInteger() < 2) {
			flash.error = 'Please select a number of clusters (any number larger than 1)'
			redirect(action: 'create', params: params)
			return
		}
		if (!params.steps || params.steps.toInteger() < 1) {
			flash.error = 'Please select a number of discretization steps (any positive number larger than 0)'
			redirect(action: 'create', params: params)
			return
		}
		if (!params.randomSeed || params.randomSeed.toLong() < 0) {
			flash.error = 'Please select a random seed (any positive number, e.g.: 1)'
			redirect(action: 'create', params: params)
			return
		}
		if (!params.permutations || params.permutations.toInteger() < 0) {
			flash.error = 'Please select a number of permutations (can be 0 or positive)'
			redirect(action: 'create', params: params)
			return
		}
		if (!params.refinementPermutations || params.refinementPermutations.toInteger() < 0) {
			flash.error = 'Please select a number of later permutations (can be 0 or positive)'
			redirect(action: 'create', params: params)
			return
		}
		def url = createLink(controller: 'iterativeTimeSeriesClustering', action: 'upload')
		def p = [
			tsDataSet: params.tsDataSet,
			numberPatterns: params.numberPatterns,
			steps: params.steps,
			simFunction: params.simFunction,
			clusteringMethod: params.clusteringMethod,
			patternRefinementFunction: params.patternRefinementFunction,
			randomSeed: params.randomSeed,
			permutations: params.permutations,
			refinementPermutations: params.refinementPermutations];
		
		if (params.graph)
			p['graph'] = params.graph
		if (params.containsKey('objectIntersect'))
			p['objectIntersect'] = true
		async_decorate_and_submit_task("Initial Clustering", url, false, p, request.cookies);
	}

	def upload() {
		try {
			println "BLUUUU"
			println params
			TimeSeriesDataSet dataSet = TimeSeriesDataSet.get(request.getParameter("tsDataSet"));
	
			int numberPatterns = Integer.valueOf(request.getParameter("numberPatterns"))
			int steps = Integer.valueOf(request.getParameter("steps"))
			SimilarityFunction simFun = SimilarityFunction.findById(request.getParameter("simFunction"))
			ClusteringMethod clustMethod = ClusteringMethod.findById(request.getParameter("clusteringMethod"))
			PatternRefinementFunction patternRefFunction = PatternRefinementFunction.findById(request.getParameter("patternRefinementFunction"))
	
			long randomSeed =  request.getParameter("randomSeed").toLong();
	
			int permutations = request.getParameter("permutations").toInteger();
			
			int refinementPermutations = request.getParameter("refinementPermutations").toInteger();
	
			boolean hasGraph = params.graph != null;
	
			Graph g = null;
			GraphLayout graphLayout = null;
			
			int iterCount = 0;
			
			def curruser = null;
			if (SecurityUtils.getSubject().isAuthenticated()) {
				curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
			}
			// if it is a guest, use the sessionId
			def userSessionId = null;
			if (curruser == null) {
				userSessionId = session.getId();
			}
			println "intersect:"
			println params.objectIntersect
			if (hasGraph) {
				g = Graph.get(request.getParameter("graph"));
				
				(g, dataSet) = GraphController.mapGraphAndDataSet(g, dataSet, params.containsKey("objectIntersect"));
				println "graph #nodes:"
				println g.nodes.size()
				println "data set #objects:"
				println dataSet.objects.size()
				
				if (g.nodes.size() == 0 || dataSet.objects.size() == 0) {
					throw new IllegalArgumentException("The identifiers of your network and data set do not match.")
				}
				
				println "Creating graph layout"
				graphLayout = GraphLayoutController.createForGraph(g, curruser, userSessionId);
			}
			
			def clustering;
			
			def future = callAsync {
				// use library to find overrepresented patterns and cluster the dataset
				AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
				Scanner scanner = new Scanner(new ByteArrayInputStream(dataSet.file.tmpFile));
				List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
				scanner.close();
				calc.initializeTimeSeriesData(tsData);
				calc.calculatePatterns();
		
				IDiscretizePrototype discretizePattern = new DiscretizePatternMinToMax(calc.minValue, calc.maxValue, steps);
				ISimilarity similarityFunction;
				if (simFun.name == "Euclidian Distance")
					similarityFunction = new NegativeEuclideanSimilarity();
				else if (simFun.name == "Pearson Correlation")
					similarityFunction = new PearsonCorrelation();
		
				IAggregateCluster refinePattern;
				if (patternRefFunction.name == 'Mean')
					refinePattern = new AggregateClusterMean();
				else if (patternRefFunction.name == 'Median')
					refinePattern = new AggregateClusterMedian();
		
				IClustering transClust;
				if (clustMethod.name == "PAMK")
					transClust = new PAMKClustering(similarityFunction, new Random(randomSeed), refinePattern, discretizePattern);
				else if (clustMethod.name == "Transitivity Clustering")
					// TODO: make sim cutoff a parameter
					transClust = new TransClustClustering(discretizePattern, similarityFunction, 0.9, refinePattern);
				else if (clustMethod.name == "CLARA")
					transClust = new CLARAClustering(similarityFunction, refinePattern, discretizePattern, new Random(randomSeed));
				else if (clustMethod.name == "STEM")
					transClust = new STEMClustering(similarityFunction, discretizePattern, tsData.get(0).getOriginalTimeSeries().length);
				ITimeSeriesClusteringWithOverrepresentedPatterns cl = new BasicTimeSeriesClusteringWithOverrepresentedPatterns(calc, similarityFunction, new PermutateDatasetRowwise(), permutations,numberPatterns, discretizePattern, transClust, refinementPermutations);
		
				println "Finding overrrepresented patterns; i.e. clustering"
				PatternObjectMapping mapping = cl.doIteration();
				
				TimePointWeighting tpWeighting = new TimePointWeighting(tsData.get(0).getOriginalTimeSeries().length);
		
				// store the mapping in the database;
				println "Storing clustering in database"
				long start = System.currentTimeMillis();
				//def clustering = TimeSeriesClustering.createFromMapping(dataSet, mapping, Cluster.numberOfPatterns);
				clustering = TimeSeriesClustering.createFromMappingBatch(dataSet, tpWeighting, mapping, Cluster.numberOfPatterns);
				println "time needed to store mapping:"
				println System.currentTimeMillis()-start
			}
			future.get();
	
	
			TimeSeriesClusteringVisualization vis = new TimeSeriesClusteringVisualization(
					// may be null
					graph: g,
					// may be null
					graphLayout: graphLayout,
					clustering: clustering,
					patternColors: [
						'#FF0000',
						'#00FF00',
						'#0000FF',
						'#FFFF00',
						'#FF00FF',
						'#00FFFF',
						'#FFFFFF'
					])
			vis.save(flush: true);
			
			if (hasGraph && g.nodes.size() <= 500) {
				println "doing layout"
				vis.doForceLayout(curruser, userSessionId);
			}
	
			IterationHasVisualization itHasVis = new IterationHasVisualization(iteration: 1, visualization: vis, description:"Initial Iteration");
			itHasVis.save(flush: true);
			
			// generate unique ID for result
			SecureRandom random = new SecureRandom();
			String sessionID = new BigInteger(130, random).toString(32);
	
			IterativeTimeSeriesClustering it = new IterativeTimeSeriesClustering(
					graph: g,
					dataSet: dataSet,
					clusteringMethod: clustMethod,
					similarityFunction: simFun,
					discretizationSteps: steps,
					patternRefinementFunction: patternRefFunction,
					randomSeed: randomSeed,
					permutations: permutations,
					refinementPermutations: refinementPermutations,
					sessionId: sessionID,
					user: curruser,
					userSessionId: userSessionId);
			it.addToIterations(itHasVis);
			it.save(flush: true)
			
			
			// if we have been passed a taskId, we inform the task that it is finished now
			if (params.taskId) {
				def url = createLink(absolute: true, action: "show_cytoscape", id: it.sessionId, params: [iteration: itHasVis.id]);
				setTaskFinished(params.taskId, url, null);
			}
			
			redirect(action: "show_cytoscape", id: it.sessionId, params: [iteration: itHasVis.id])
		} catch (Exception e) {
			render(status: 403, text: e.getMessage());
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage());
			}
		}
	}
	
	def your() {
		def curruser = null;
		if (SecurityUtils.getSubject().isAuthenticated()) {
			curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
			def clusterings = IterativeTimeSeriesClustering.findAll("from IterativeTimeSeriesClustering as c where c.user = ? order by c.dateCreated DESC", [curruser]) 
			[clusterings: clusterings]
		}
	}
	
	def async_decorate_and_submit_task(taskTitle, url, canCancel, p, cookies) {
		Task t = create_task(taskTitle, canCancel);
		
		def base = grailsLinkGenerator.serverBaseURL - grailsLinkGenerator.contextPath
		def curruser = null;
		if (SecurityUtils.getSubject().isAuthenticated()) {
			curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
		}
		
		def postParams;
		if (curruser != null) {
			postParams = [taskId: t.taskId, userId: curruser.id] + p
		} else {
			postParams = [taskId: t.taskId, userSessionId: session.getId()] + p
		}
		
		println base
		def http = new AsyncHTTPBuilder(poolSize: 4, uri: base)
		http.ignoreSSLIssues()
		http.request( POST ) { req ->
			uri.path = url
			requestContentType = ContentType.JSON
			uri.query = postParams
			headers['Cookie'] = cookies.collect {
				it.getName() + "=" + it.getValue()
			}.join('; ')
			body = []
		
			 response.success = { resp, json ->
				 println "SUCCESS"
			}
		
			 response.failure = { resp, json ->
				 println "FAILED"
				 render "Connection failed"
			}
		}
		redirect(controller: 'task', action: 'show', id: t.taskId)
	}
	
	def async_do_iteration() {
		def url = createLink(controller: 'iterativeTimeSeriesClustering', action: 'do_iteration', id: params.id, params: [iteration: params.iteration])
		async_decorate_and_submit_task("Do Iteration", url, false, [:], request.cookies);
	}
	
	def do_iteration() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
			
			def future = callAsync {
				Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
				mapping = cl.doIteration();	
			}
			future.get();
			
			def itHasVis = createNewIteration(dataSet, tpWeighting, mapping, current_vis, itCl, "Do iteration");
			
			finishTaskAndRedirect(dataSet, mapping, itHasVis, itCl, params.taskId, null);
		} catch (Exception e) {
			render(status: 403, text: e.getMessage());
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage());
			}
		}
	}
	
	def get_user_and_session(userId, userSessionId) {
		if (userId == null) {
			if (SecurityUtils.getSubject().isAuthenticated() || SecurityUtils.getSubject().isRemembered()) {
				userId = User.findByUsername(SecurityUtils.getSubject().getPrincipal()).id;
			}
		}
		def user;
		//if (userId != null) {
//			user = User.get(userId)
		//}
		if (userId == null && userSessionId == null) {
			userSessionId = session.getId();
		}
		return [userId, userSessionId]
	}
	
	def create_task(title, canCancel) {
		def curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
		def task_id = Task.create_task_id();
		Task t = new Task(taskId:task_id,
			title:title,
			user:curruser,
			finished:false,
			canCancel:canCancel,
			cancelled:false,
			sendMail:false)
		t.save(flush: true);
		return t;
	}
	
	def async_do_iterations_until_convergence() {
		def url = createLink(controller: 'iterativeTimeSeriesClustering', action: 'do_iterations_until_convergence', id: params.id, params: [iteration: params.iteration])
		Task t = async_decorate_and_submit_task("Iterations Until Convergence", url, true, [:], request.cookies);
	}

	def do_iterations_until_convergence() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
			def taskId = params.taskId;
				
			IterationHasVisualization itHasVis;
		
			// do iterations until convergence
			double epsilon = 0.001;
			Iterator iterator = cl.getPatternObjectMapping().clusterSet().iterator();
			int currentNumberOfPatterns = countNumberOfElements(iterator);
			int oldNumberOfPatterns = -1;
	
			int maxIterations = 100;
			int iteration = 1;
	
			// do an iteration
			double oldRSS = StatsCalculation.calculateAllClusterRSS(tpWeighting, mapping);
			
			double currentRSS = Double.MAX_VALUE;
			
			while (iteration < maxIterations && (Math.abs(oldRSS - currentRSS) >= epsilon || oldNumberOfPatterns != currentNumberOfPatterns)) {
					
				if (taskId) {
					Task t = Task.findByTaskId(taskId)
					t.refresh();
					if (t.cancelled)
						break;
				}
				
				def future = callAsync {
					
					Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
					
					mapping = cl.doIteration();
					oldRSS = currentRSS;
					currentRSS = cl.getTotalRSS();
					iterator = cl.getPatternObjectMapping().clusterSet().iterator();
					oldNumberOfPatterns = currentNumberOfPatterns;
					currentNumberOfPatterns = countNumberOfElements(iterator);
					iteration++;
				}
				future.get()
			
				itHasVis = createNewIteration(dataSet, tpWeighting, mapping, current_vis, itCl, "Iterations until convergence");
				current_vis = itHasVis.visualization
			}
			
			finishTaskAndRedirect(dataSet, mapping, itHasVis, itCl, params.taskId, null);
		} catch (Exception e) {
			render(status: 403, text: e.getMessage());
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage());
			}
		}
	}
	
	def setTaskFinished(String taskId, String redirect_url, String statusMessage) {
		Task t = Task.findByTaskId(taskId)
		t.refresh();
		t.finished = true;
		t.redirect_url = redirect_url;
		if (statusMessage) {
			t.statusMessage = statusMessage;
		}
		t.save(flush: true);
		
		sendTaskMail(t);
	}
	
	def sendTaskMail(Task t) {
		if (t.sendMail && t.user) {
			sendMail {
				to(t.user.email)
				from "noreply@compbio.sdu.dk"
				subject "TiCoNE: " + t.title + " finished"
				html '<p>Your job has finished.</p><p>You can access the result here: <a href="' + t.redirect_url + '">' + t.redirect_url + '</a></p>'
			  }
		}
	}

	static def countNumberOfElements(Iterator iterator) {
		int sum = 0;
		while (iterator.hasNext()) {
			iterator.next();
			sum++;
		}
		return sum;
	}

	def delete_pattern() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
			Pattern_g patternToDelete = Pattern_g.get(params.pattern.toInteger())
	
			def future = callAsync {
				// find the correct Pattern object;
				Cluster pattern;
				for (Cluster p: mapping.clusterSet()) {
					if (Arrays.equals(p.prototype, patternToDelete.timeSeriesPattern)) {
						pattern = p;
						break;
					}
				}
				
				Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
				
				cl.deleteData(pattern, DELETE_METHOD.ONLY_PATTERN);
				mapping = cl.getPatternObjectMapping();
				
				Cluster.stopCounting()
				cl.totalRSS = StatsCalculation.calculateAllClusterRSS(tpWeighting, mapping);
				cl.calculatePValues2(mapping);
				Cluster.startCounting();
			}
			future.get()
	
	
			createNewIterationAndRedirect(dataSet, tpWeighting, mapping, current_vis, itCl, "Delete " + patternToDelete.name + " only");
		} catch (Exception e) {
			render(status: 403, text: e.getMessage());
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage());
			}
		}
	}

	def add_pattern() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
	
			Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
			
			double[] pattern_raw = JSON.parse(params.pattern)
			Cluster newPattern = new Cluster(pattern_raw);
			
			cl.addCluster(newPattern)
			mapping = cl.getPatternObjectMapping();
			
			Cluster.stopCounting()
			cl.totalRSS = StatsCalculation.calculateAllClusterRSS(tpWeighting, mapping);
			cl.calculatePValues2(mapping);
			Cluster.startCounting();
	
	
			createNewIterationAndRedirect(dataSet, tpWeighting, mapping, current_vis, itCl, "Added new pattern");
		} catch (AccessDeniedException e) {
			render(status: 403, text: e.getMessage());
		}
	}

	def merge_patterns() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
			// comma separated list of patterns;
			String[] patternsToMergeIds = params.patterns.split(",");
			
			List<Pattern_g> patternsToMerge;
	
			def future = callAsync {
				patternsToMerge = new ArrayList<Pattern_g>();
				for (String id: patternsToMergeIds)
					patternsToMerge.add(Pattern_g.get(id.toInteger()));
		
				// find the correct Pattern objects;
				List<Cluster> patterns = new ArrayList<Cluster>();
		
				for (Pattern_g pg: patternsToMerge) {
					for (Cluster p: mapping.clusterSet()) {
						if (Arrays.equals(p.prototype, pg.timeSeriesPattern)) {
							patterns.add(p);
							break;
						}
					}
				}
				
				Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
				
				cl.mergeClusters(patterns);
				mapping = cl.getPatternObjectMapping();
				
				Cluster.stopCounting()
				cl.totalRSS = StatsCalculation.calculateAllClusterRSS(tpWeighting, mapping);
				cl.calculatePValues2(mapping);
				Cluster.startCounting();
			}
			future.get()
	
			createNewIterationAndRedirect(dataSet, tpWeighting, mapping, current_vis, itCl, "Merge patterns " + patternsToMerge.collect {p -> p.number}.join(', '));
		} catch (Exception e) {
			render(status: 403, text: e.getMessage());
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage());
			}
		}
	}

	def createNewIteration(
			TimeSeriesDataSet dataSet,
			TimePointWeighting tpWeighting,
			PatternObjectMapping mapping,
			TimeSeriesClusteringVisualization current_vis,
			IterativeTimeSeriesClustering itCl,
			String iterationDescription) {

		// store the mapping in the database;
		println "Creating new clustering with patternnumber " + Cluster.numberOfPatterns
		
		long start = System.currentTimeMillis();
		//def clustering = TimeSeriesClustering.createFromMapping(dataSet, mapping, Cluster.numberOfPatterns);
		def clustering = TimeSeriesClustering.createFromMappingBatch(dataSet, tpWeighting, mapping, Cluster.numberOfPatterns);
		println "time needed to store mapping:"
		println System.currentTimeMillis()-start

		Graph g = current_vis.graph;
		GraphLayout graphLayout = current_vis.graphLayout;

		TimeSeriesClusteringVisualization vis = new TimeSeriesClusteringVisualization(
				graph: g,
				graphLayout: graphLayout,
				clustering: clustering,
				patternColors: current_vis.patternColors
				)
		vis.save();

		def currentIteration = IterationHasVisualization.executeQuery("select max(iteration)+1 as maxiter from IterationHasVisualization where visualization=" + current_vis.id)[0]

		// delete old previously older iterations
		itCl.iterations.findAll {it.iteration >= currentIteration}.each { it ->
			itCl.iterations.remove(it);
			it.delete()
		}

		IterationHasVisualization itHasVis = new IterationHasVisualization(iteration: currentIteration, visualization: vis,
		description: iterationDescription);
		itHasVis.save();

		itCl.addToIterations(itHasVis);
		itCl.save(flush: true)

		return itHasVis
	}

	def createNewIterationAndRedirect(
			TimeSeriesDataSet dataSet,
			TimePointWeighting tpWeighting,
			PatternObjectMapping mapping,
			TimeSeriesClusteringVisualization current_vis,
			IterativeTimeSeriesClustering itCl,
			String iterationDescription) {
		IterationHasVisualization itHasVis = createNewIteration(dataSet, tpWeighting, mapping, current_vis, itCl, iterationDescription);

		//redirect(action: "show_cytoscape", id: itCl.id, params: [iteration: itHasVis.id])
		redirectToIteration(itHasVis, itCl, [:])
	}

	def redirectToIteration(IterationHasVisualization itHasVis,
			IterativeTimeSeriesClustering itCl, LinkedHashMap parameters) {
		LinkedHashMap params = [iteration: itHasVis.id]
		params.putAll(parameters)
		redirect(action: "show_cytoscape", id: itCl.sessionId, params: params)
	}

	def delete_pattern_objects() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
			
			Pattern_g patternToDelete = Pattern_g.get(params.pattern.toInteger())
	
			def future = callAsync {
				// find the correct Pattern object;
				Cluster pattern;
				for (Cluster p: mapping.clusterSet()) {
					if (Arrays.equals(p.prototype, patternToDelete.timeSeriesPattern)) {
						pattern = p;
						break;
					}
				}
				
				Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
				
				cl.deleteData(pattern, DELETE_METHOD.ONLY_OBJECTS);
				mapping = cl.getPatternObjectMapping();
				
				Cluster.stopCounting()
				cl.totalRSS = StatsCalculation.calculateAllClusterRSS(tpWeighting, mapping);
				cl.calculatePValues2(mapping);
				Cluster.startCounting();
			}
			future.get()
	
			createNewIterationAndRedirect(dataSet, tpWeighting, mapping, current_vis, itCl, "Deleting objects of pattern");
		} catch (Exception e) {
			render(status: 403, text: e.getMessage());
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage());
			}
		}
	}
	
	def async_delete_least_similar_objects_pattern() {
		def url = createLink(controller: 'iterativeTimeSeriesClustering', action: 'delete_least_similar_objects_pattern', id: params.id, params: [percentage: params.percentage, pattern: params.pattern, iteration: params.iteration, type: params.type])
		Pattern_g patternToDelete = Pattern_g.get(params.pattern.toInteger())
		
		if (params.type == "percentage")
			async_decorate_and_submit_task("Delete " + params.percentage + "% least fitting objects from " + patternToDelete.name, url, false, [:], request.cookies);
		else if (params.type == "rss")
			async_decorate_and_submit_task("Delete object sets with average RSS > " + params.percentage + " from " + patternToDelete.name, url, false, [:], request.cookies);
		else if (params.type == "pearson")
			async_decorate_and_submit_task("Delete object sets with average Pearson < " + params.percentage + " from " + patternToDelete.name, url, false, [:], request.cookies);
		
	}

	def delete_least_similar_objects_pattern() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
			
			Pattern_g patternToDelete = Pattern_g.get(params.pattern.toInteger())
			double percentage = params.percentage.toDouble();
			String type = params.type;
	
			def future = callAsync {
				// find the correct Pattern object;
				Cluster pattern;
				for (Cluster p: mapping.clusterSet()) {
					if (Arrays.equals(p.prototype, patternToDelete.timeSeriesPattern)) {
						pattern = p;
						break;
					}
				}
				
				List<TimeSeriesObject> leastFitting;
				if (type == "percentage") {
					leastFitting = ExtractData.getLeastFittingDataFromPattern(mapping, pattern, (int)percentage, similarityFunction);
				} else if (type == "rss") {
					AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
					Scanner scanner = new Scanner(new ByteArrayInputStream(itCl.dataSet.file.tmpFile));
					List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
					scanner.close();
					calc.initializeTimeSeriesData(tsData);
					calc.calculatePatterns();
					
					similarityFunction = new NegativeEuclideanSimilarity();
					leastFitting = ExtractData.findObjectsFromPatternWithThreshold(pattern, mapping, percentage, similarityFunction)
				} else if (type == "pearson") {
					similarityFunction = new PearsonCorrelation();
					leastFitting = ExtractData.findObjectsFromPatternWithThreshold(pattern, mapping, percentage, similarityFunction)
				}
				
				Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
		
				mapping.deleteObjectsFromPattern(pattern, leastFitting);
				
				Cluster.stopCounting()
				cl.totalRSS = StatsCalculation.calculateAllClusterRSS(tpWeighting, mapping);
				cl.calculatePValues2(mapping);
				Cluster.startCounting();
			}
			future.get()
			
			String iterationName;
			switch (type) {
				case "percentage":
					iterationName = "Delete " + percentage + "% least fitting objects from " + patternToDelete.name;
					break;
				case "rss":
					iterationName = "Delete object sets with average RSS > " + params.percentage + " to prototype from " + patternToDelete.name;
					break;
				case "pearson":
					iterationName = "Delete object sets with average Pearson < " + params.percentage + " to prototype from " + patternToDelete.name;
					break;
			}
			
			
			def itHasVis = createNewIteration(dataSet, tpWeighting, mapping, current_vis, itCl, iterationName);
			
			finishTaskAndRedirect(dataSet, mapping, itHasVis, itCl, params.taskId, null);
		} catch (Exception e) {
			render(status: 403, text: e.getMessage());
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage());
			}
		}
	}
	
	def async_delete_least_conserved_objects() {
		def url = createLink(controller: 'iterativeTimeSeriesClustering', action: 'delete_least_conserved_objects', id: params.id, params: [percentage: params.percentage, iteration: params.iteration, type: params.type])
		if (params.type == "percentage")
			async_decorate_and_submit_task("Delete " + params.percentage + "% least conserved objects", url, false, [:], request.cookies);
		else if (params.type == "rss")
			async_decorate_and_submit_task("Delete object sets with average RSS > " + params.percentage, url, false, [:], request.cookies);
		else if (params.type == "pearson")
			async_decorate_and_submit_task("Delete object sets with average Pearson < " + params.percentage, url, false, [:], request.cookies);
	}

	def delete_least_conserved_objects() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
			double percentage = params.percentage.toDouble();
			String type = params.type
			
			
			def future = callAsync {
				List<TimeSeriesObject> datas = new ArrayList<TimeSeriesObject>();
				Iterator<String> objectIt = mapping.objectSet().iterator();
				while(objectIt.hasNext()) {
					datas.add(mapping.getTimeSeriesData(objectIt.next()));
				}
				
				List<TimeSeriesObject> leastFitting;
				if (type == "percentage") {
					// similarity function has been parsed according to parameter
					leastFitting = ExtractData.findLeastConservedObjectSets(datas, percentage, similarityFunction);
				} else if (type == "rss") {
					AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
					Scanner scanner = new Scanner(new ByteArrayInputStream(itCl.dataSet.file.tmpFile));
					List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
					scanner.close();
					calc.initializeTimeSeriesData(tsData);
					calc.calculatePatterns();
				
					similarityFunction = new NegativeEuclideanSimilarity();
					leastFitting = ExtractData.findLeastConservedObjectSetsWithThreshold(datas, percentage, similarityFunction)
				} else if (type == "pearson") {
					similarityFunction = new PearsonCorrelation();
					leastFitting = ExtractData.findLeastConservedObjectSetsWithThreshold(datas, percentage, similarityFunction)
				}
				
				Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
		
				Iterator<Cluster> patternIterator = mapping.clusterSet().iterator();
				while (patternIterator.hasNext()) {
					Cluster pattern = patternIterator.next();
					mapping.deleteObjectsFromPattern(pattern, leastFitting);
				}
				
				Cluster.stopCounting()
				cl.totalRSS = StatsCalculation.calculateAllClusterRSS(tpWeighting, mapping);
				cl.calculatePValues2(mapping);
				Cluster.startCounting();
			}
			future.get();
			
			
			String iterationName;
			switch (type) {
				case "percentage":
					iterationName = "Delete " + percentage + "% least conserved objects";
					break;
				case "rss":
					iterationName = "Delete object sets with average RSS > " + params.percentage + ' to each other';
					break;
				case "pearson":
					iterationName = "Delete object sets with average Pearson < " + params.percentage + ' to each other';
					break;
			}
			
			def itHasVis = createNewIteration(dataSet, tpWeighting, mapping, current_vis, itCl, iterationName);
			
			finishTaskAndRedirect(dataSet, mapping, itHasVis, itCl, params.taskId, null);
		} catch (Exception e) {
			render(status: 403, text: e.getMessage());
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage().substring(0, 250));
			}
		}
	}
	
	def finishTaskAndRedirect(dataSet, mapping, itHasVis, itCl, taskId, statusMessage) {
		if (taskId) {
			def url = createLink(absolute: true, action: 'show_cytoscape', id: itCl.sessionId, params: [iteration: itHasVis.id]);
			setTaskFinished(taskId, url, statusMessage);
		}
		redirectToIteration(itHasVis, itCl, [:])
	}

	def delete_pattern_and_objects() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
			
			Pattern_g patternToDelete = Pattern_g.get(params.pattern.toInteger())
	
			def future = callAsync {
				// find the correct Pattern object;
				Iterator<Cluster> patternIt = mapping.clusterSet().iterator();
				Cluster pattern;
				while (patternIt.hasNext()) {
					Cluster p = patternIt.next();
					if (Arrays.equals(p.prototype, patternToDelete.timeSeriesPattern)) {
						pattern = p;
						break;
					}
				}
				
				Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
				
				cl.deleteData(pattern, DELETE_METHOD.BOTH_PATTERN_AND_OBJECTS);
				mapping = cl.getPatternObjectMapping();
				
				Cluster.stopCounting()
				cl.totalRSS = StatsCalculation.calculateAllClusterRSS(tpWeighting, mapping);
				cl.calculatePValues2(mapping);
				Cluster.startCounting();
			}
			future.get()
	
			createNewIterationAndRedirect(dataSet, tpWeighting, mapping, current_vis, itCl, "Delete " + patternToDelete.name + " including objects");
		} catch (AccessDeniedException e) {
			render(status: 403, text: e.getMessage());
		}
	}
	
	def async_split_pattern_most_dissimilar_objects() {
		def url = createLink(controller: 'iterativeTimeSeriesClustering', action: 'split_pattern_most_dissimilar_objects', id: params.id, params: [pattern: params.pattern, iteration: params.iteration])
		Pattern_g patternToDelete = Pattern_g.get(params.pattern.toInteger())
		async_decorate_and_submit_task("Split " + patternToDelete.name + " by two most dissimilar objects", url, false, [:], request.cookies);
	}

	def split_pattern_most_dissimilar_objects() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
			
			Pattern_g patternToDelete = Pattern_g.get(params.pattern.toInteger())
	
			def future = callAsync {
				// find the correct Pattern object;
				Iterator<Cluster> patternIt = mapping.clusterSet().iterator();
				Cluster pattern;
				while (patternIt.hasNext()) {
					Cluster p = patternIt.next();
					if (Arrays.equals(p.prototype, patternToDelete.timeSeriesPattern)) {
						pattern = p;
						break;
					}
				}
		
				AbstractSplitCluster splitPattern = new SplitClusterBasedOnTwoMostDissimilarObjects(similarityFunction, discretizePattern, mapping);
				
				Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
				
				SplitClusterContainer splitContainer = cl.splitCluster(splitPattern, pattern);
				// TODO: do we also want a preview function? is in a way redundant, since we can always go an iteration back in history.
				mapping = splitPattern.applyNewClusters(splitContainer);
				
				Cluster.stopCounting()
				cl.totalRSS = StatsCalculation.calculateAllClusterRSS(tpWeighting, mapping);
				cl.calculatePValues2(mapping);
				Cluster.startCounting();
			}
			future.get();
	
			def itHasVis = createNewIteration(dataSet, tpWeighting, mapping, current_vis, itCl, "Split " + patternToDelete.name + " by two most dissimilar objects");
			finishTaskAndRedirect(dataSet, mapping, itHasVis, itCl, params.taskId, null);
		} catch (AccessDeniedException e) {
			render(status: 403, text: e.getMessage());
		}
	}
	
	def async_split_pattern_clustering() {
		def url = createLink(controller: 'iterativeTimeSeriesClustering', action: 'split_pattern_clustering', id: params.id, params: [numberClusters: params.numberClusters, pattern: params.pattern, iteration: params.iteration])
		Pattern_g patternToDelete = Pattern_g.get(params.pattern.toInteger())
		async_decorate_and_submit_task("Split " + patternToDelete.name + " into " + params.numberClusters + " clusters", url, false, [:], request.cookies);
	}

	def split_pattern_clustering() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
			Pattern_g patternToDelete = Pattern_g.get(params.pattern.toInteger())
			def numberClusters = params.numberClusters.toInteger()
	
			def future = callAsync {
				// find the correct Pattern object;
				Iterator<Cluster> patternIt = mapping.clusterSet().iterator();
				Cluster pattern;
				while (patternIt.hasNext()) {
					Cluster p = patternIt.next();
					if (Arrays.equals(p.prototype, patternToDelete.timeSeriesPattern)) {
						pattern = p;
						break;
					}
				}
				
				Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
		
				AbstractSplitCluster splitPattern = new SplitClusterWithClustering(mapping, numberClusters, clustMethod)
		
				SplitClusterContainer splitContainer = cl.splitCluster(splitPattern, pattern);
				// TODO: do we also want a preview function? is in a way redundant, since we can always go an iteration back in history.
				mapping = splitPattern.applyNewClusters(splitContainer);
				
				Cluster.stopCounting()
				cl.totalRSS = StatsCalculation.calculateAllClusterRSS(tpWeighting, mapping);
				cl.calculatePValues2(mapping);
				Cluster.startCounting()
			}
			future.get();
			
			
			def itHasVis = createNewIteration(dataSet, tpWeighting, mapping, current_vis, itCl, "Split " + patternToDelete.name + " into " + params.numberClusters + " clusters");
			finishTaskAndRedirect(dataSet, mapping, itHasVis, itCl, params.taskId, null);
		} catch (AccessDeniedException e) {
			render(status: 403, text: e.getMessage());
		}
	}

	def get_least_similar_objects() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization current_vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization

		double percentage = params.percentage.toDouble();

		// input files
		TimeSeriesDataSet dataSet = itCl.dataSet;
		AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
		Scanner scanner = new Scanner(new ByteArrayInputStream(itCl.dataSet.file.tmpFile));
		List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
		scanner.close();
		calc.initializeTimeSeriesData(tsData);
		calc.calculatePatterns();

		// restore PatternObjectMapping from database;
		PatternObjectMapping mapping = current_vis.clustering.toPatternObjectMapping(tsData);

		
		ISimilarity similarityFunction;
		List<TimeSeriesObject> leastFitting;
		if (params.type == "percentage") {
			if (itCl.similarityFunction.name == "Euclidian Distance")
				similarityFunction = new NegativeEuclideanSimilarity();
			else if (itCl.similarityFunction.name == "Pearson Correlation")
				similarityFunction = new PearsonCorrelation();
			leastFitting = ExtractData.getLeastFittingObjectsFromWholeDataset(mapping, (int)percentage, similarityFunction);
		} else if (params.type == "rss") {
			similarityFunction = new NegativeEuclideanSimilarity();
			leastFitting = ExtractData.findLeastFittingObjectsWithThreshold(mapping, percentage, similarityFunction)
		} else if (params.type == "pearson") {
			similarityFunction = new PearsonCorrelation();
			leastFitting = ExtractData.findLeastFittingObjectsWithThreshold(mapping, percentage, similarityFunction)
		}
		Map<String, Set<String>> res = new HashMap<String, Set<String>>();
		res.put('leastFittingObjects', leastFitting.collect { x -> x.name; });
		Map<String, double[]> objectTimeSeries = new HashMap<String, double[]>();
		leastFitting.each { x ->
			objectTimeSeries.put(x.name, x.exactPattern);
		};
		res.put('percentage',percentage);
		res.put('status', 200);
		res.put('objectTimeSeries', objectTimeSeries);
		render res as JSON
	}
	
	def get_object_pattern_similarity_histogram() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization current_vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization

		int numberBins = params.bins.toInteger();

		// input files
		TimeSeriesDataSet dataSet = itCl.dataSet;
		AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
		Scanner scanner = new Scanner(new ByteArrayInputStream(itCl.dataSet.file.tmpFile));
		List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
		scanner.close();
		calc.initializeTimeSeriesData(tsData);
		calc.calculatePatterns();

		// restore PatternObjectMapping from database;
		PatternObjectMapping mapping = current_vis.clustering.toPatternObjectMapping(tsData);
		
		Cluster pattern = null;
		if (params.pattern) {
			Pattern_g p_g = Pattern_g.get(params.pattern)
			
			// find the correct Pattern object;
			Iterator<Cluster> patternIt = mapping.clusterSet().iterator();
			while (patternIt.hasNext()) {
				Cluster p = patternIt.next();
				if (Arrays.equals(p.prototype, p_g.timeSeriesPattern)) {
					pattern = p;
					break;
				}
			}
		}
		
		ISimilarity similarityFunction;
		List<TimeSeriesObject> leastFitting;
		if (params.type == "rss") {
			similarityFunction = new NegativeEuclideanSimilarity();
		} else if (params.type == "pearson") {
			similarityFunction = new PearsonCorrelation();
		}
		org.apache.commons.lang3.tuple.Pair<double[], int[]> histogram;
		histogram = ExtractData.getSimilarityHistogram(mapping, pattern, numberBins, similarityFunction);
		Map<String, Set<String>> res = new HashMap<String, Set<String>>();
		res.put('bins', histogram.getLeft());
		res.put('counts', histogram.getRight());
		res.put('numberBins',numberBins);
		res.put('status', 200);
		render res as JSON
	}
	
	def _show_object_pattern_similarity_histogram() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization current_vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization
		
		int numberBins = params.bins.toInteger();
		
		Pattern_g p = null;
		if (params.pattern) {
			int pattern = params.pattern.toInteger();
			p = Pattern_g.get(pattern)
		}
		

		[clustering: itCl,
			type: params.type,
			iteration: params.iteration.toInteger(),
			bins: numberBins,
			pattern: p
		]
	}

	def get_least_conserved_objects() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization current_vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization
		
		double percentage = params.percentage.toDouble();

		// input files
		TimeSeriesDataSet dataSet = itCl.dataSet;
		AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
		Scanner scanner = new Scanner(new ByteArrayInputStream(itCl.dataSet.file.tmpFile));
		List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
		scanner.close();
		calc.initializeTimeSeriesData(tsData);
		calc.calculatePatterns();

		// restore PatternObjectMapping from database;
		PatternObjectMapping mapping = current_vis.clustering.toPatternObjectMapping(tsData);
		
		List<TimeSeriesObject> datas = new ArrayList<TimeSeriesObject>();
		Iterator<String> objectIt = mapping.objectSet().iterator();
		while(objectIt.hasNext()) {
			datas.add(mapping.getTimeSeriesData(objectIt.next()));
		}

		
		ISimilarity similarityFunction;
		List<TimeSeriesObject> leastFitting;
		if (params.type == "percentage") {
			if (itCl.similarityFunction.name == "Euclidian Distance")
				similarityFunction = new NegativeEuclideanSimilarity();
			else if (itCl.similarityFunction.name == "Pearson Correlation")
				similarityFunction = new PearsonCorrelation();
			leastFitting = ExtractData.findLeastConservedObjectSets(datas, (int)percentage, similarityFunction)
		} else if (params.type == "rss") {
			similarityFunction = new NegativeEuclideanSimilarity();
			leastFitting = ExtractData.findLeastConservedObjectSetsWithThreshold(datas, percentage, similarityFunction)
		} else if (params.type == "pearson") {
			similarityFunction = new PearsonCorrelation();
			leastFitting = ExtractData.findLeastConservedObjectSetsWithThreshold(datas, percentage, similarityFunction)
		}
		Map<String, Set<String>> res = new HashMap<String, Set<String>>();
		res.put('leastConservedObjects', leastFitting.collect { x -> x.name; });
		Map<String, double[]> objectTimeSeries = new HashMap<String, double[]>();
		leastFitting.each { x ->
			objectTimeSeries.put(x.name, x.exactPattern);
		};
		res.put('percentage',percentage);
		res.put('status', 200);
		res.put('objectTimeSeries', objectTimeSeries);
		render res as JSON
	}

	def _show_least_similar_objects() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization current_vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization
		
		[clustering: itCl,
			type: params.type,
			percentage: params.percentage,
			iteration: params.iteration.toInteger()
		]
	}

	def _show_least_conserved_objects() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization current_vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization

		[clustering: itCl,
			type: params.type,
			percentage: params.percentage,
			iteration: params.iteration.toInteger()
		]
	}
	
	def async_delete_least_similar_objects() {
		def url = createLink(controller: 'iterativeTimeSeriesClustering', action: 'delete_least_similar_objects', id: params.id, params: [percentage: params.percentage, iteration: params.iteration, type: params.type])
		if (params.type == "percentage")
			async_decorate_and_submit_task("Delete " + params.percentage + "% least fitting objects", url, false, [:], request.cookies);
		else if (params.type == "rss")
			async_decorate_and_submit_task("Delete object sets with average RSS > " + params.percentage, url, false, [:], request.cookies);
		else if (params.type == "pearson")
			async_decorate_and_submit_task("Delete object sets with average Pearson < " + params.percentage, url, false, [:], request.cookies);
	}

	def delete_least_similar_objects() {
		ITimeSeriesClusteringWithOverrepresentedPatterns cl;
		PatternObjectMapping mapping;
		TimeSeriesClusteringVisualization current_vis;
		IterativeTimeSeriesClustering itCl;
		TimeSeriesDataSet dataSet;
		ISimilarity similarityFunction;
		IDiscretizePrototype discretizePattern;
		IClustering clustMethod;
		TimePointWeighting tpWeighting;
		try {
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			(dataSet, cl, itCl, current_vis, mapping, similarityFunction, discretizePattern, clustMethod, tpWeighting) = IterativeTimeSeriesClustering.get_clustering_and_mapping(userId, userSessionId, params.id, params.iteration.toInteger())
			double percentage = params.percentage.toDouble();
			String type = params.type
	
			def future = callAsync {
				List<TimeSeriesObject> leastFitting;
				if (type == "percentage") {
					// similarity function has been parsed according to parameter
					leastFitting = ExtractData.getLeastFittingObjectsFromWholeDataset(mapping, (int)percentage, similarityFunction);
				} else if (type == "rss") {
					AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
					Scanner scanner = new Scanner(new ByteArrayInputStream(itCl.dataSet.file.tmpFile));
					List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
					scanner.close();
					calc.initializeTimeSeriesData(tsData);
					calc.calculatePatterns();
				
					similarityFunction = new NegativeEuclideanSimilarity();
					leastFitting = ExtractData.findLeastFittingObjectsWithThreshold(mapping, percentage, similarityFunction)
				} else if (type == "pearson") {
					similarityFunction = new PearsonCorrelation();
					leastFitting = ExtractData.findLeastFittingObjectsWithThreshold(mapping, percentage, similarityFunction)
				}
				
				Cluster.numberOfPatterns = current_vis.clustering.nextPatternNumber;
		
				Iterator<Cluster> patternIterator = mapping.clusterSet().iterator();
				while (patternIterator.hasNext()) {
					Cluster pattern = patternIterator.next();
					mapping.deleteObjectsFromPattern(pattern, leastFitting);
				}
				
				Cluster.stopCounting()
				cl.totalRSS = StatsCalculation.calculateAllClusterRSS(tpWeighting, mapping);
				cl.calculatePValues2(mapping);
				Cluster.startCounting();
			}
			future.get()
	
			String iterationName;
			switch (type) {
				case "percentage":
					iterationName = "Delete " + params.percentage + "% least fitting objects";
					break;
				case "rss":
					iterationName = "Delete object sets with average RSS > " + params.percentage + ' to prototype';
					break;
				case "pearson":
					iterationName = "Delete object sets with average Pearson < " + params.percentage + ' to prototype';
					break;
			}
			
			
			def itHasVis = createNewIteration(dataSet, tpWeighting, mapping, current_vis, itCl, iterationName);
			
			finishTaskAndRedirect(dataSet, mapping, itHasVis, itCl, params.taskId, null);
		} catch (AccessDeniedException e) {
			render(status: 403, text: e.getMessage());
		}
	}

	def show_vivagraph() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization

		List patternObjects = ObjectBelongsToPattern.findAllByPatternInList(vis.clustering.patterns).collect { it.object };
		List patternNodes = Node.findAllByObjectInList(patternObjects);

		[vis: vis,
			patterns: vis.clustering.patterns.sort { it.name},
			selectedPattern: params.int('pattern',0),
			layout: vis.graphLayout,
			patternNodes: patternNodes]
	}

	def show_cytoscape() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		
		def readOnlyMode = true;
		if (itCl.user != null) {
			if (SecurityUtils.getSubject().isAuthenticated() || SecurityUtils.getSubject().isRemembered()) {
				def curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
				if (curruser == itCl.user)
					readOnlyMode = false;
			}
		} else if (itCl.sessionId != null) {
		 	if (itCl.userSessionId == session.getId()) {
				readOnlyMode = false;
			}
		 }
		else {
			readOnlyMode = false;
		}
		
		TimeSeriesClusteringVisualization vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization

		Token dimanaToken = session[oauthService.findSessionKeyForAccessToken('dimana')]
		
		List<Pattern_g> patterns = vis.clustering.patterns.sort { it.name}
		
		List<double[]> p = new ArrayList<double[]>();
		patterns.each { p_g ->
			p.add(p_g.timeSeriesPattern);
		}
		
		Integer[] patternOrder = PatternUtility.getPatternSingleLinkage(p, new PearsonCorrelation());
		List<Pattern_g> patternInOrder = new ArrayList<Pattern_g>();
		patternOrder.each { po ->
			patternInOrder.add(patterns.get(po));
		}
		
		def layouts = GraphLayout.findAll("from GraphLayout as l where l.graph = ? and published = true and (user = ? or sessionId = ?)", [vis.graph, itCl.user, itCl.userSessionId]).sort {it.name};
		
		def objectIntersects = [[id: 1, name: 'Put on Negative List'], [id: 2, name: 'Put on Positive List']]
		
		[vis: vis,
			objects: vis.clustering.objects,
			patterns: patterns,
			patternOrder: patternInOrder,
			selectedPattern: params.int('pattern',0),
			layout: vis.graphLayout,
			itCl: itCl,
			dimanaToken: dimanaToken,
			convergence: params.containsKey("c"),
			discretizationSteps: itCl.discretizationSteps,
			clusteringMethod: itCl.clusteringMethod,
			similarityFunction: itCl.similarityFunction,
			patternRefinementFunction: itCl.patternRefinementFunction,
			randomSeed: itCl.randomSeed,
			permutations: itCl.permutations,
			layouts: layouts,
			readOnlyMode: readOnlyMode,
			objectIntersects: objectIntersects]
	}
	
	def redirect_to_cytoscape() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		//TimeSeriesClusteringVisualization vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization
		
		def first_iter = IterativeTimeSeriesClustering.createCriteria().get {
			eq("sessionId", params.id)
			iterations {
				projections {
					max "id"
				}	
			}
		} as Integer
	
		if (first_iter != null)
			redirect(action: 'show_cytoscape', id: params.id, params: [iteration: first_iter])
		else
			render(status: 404)
	}

	def getClusteringAsJSON() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization

		redirect(controller: "timeSeriesClustering", action: "getClusteringAsJSON", id: vis.clustering.id)
	}

	def sendClusteringToTis2ma() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization

		redirect(controller: "timeSeriesClustering", action: "sendClusteringToTis2ma", id: vis.clustering.id)
	}

	def dataSetsAsJSON() {
		def map = [:]
		
		if (params.inGroups) {
			TimeSeriesDataSetController.accessibleDataSets(session).each { ds ->
				if (!map.containsKey(ds.group.name))
					map[ds.group.name] = []
				map[ds.group.name].push([id: ds.id, name: ds.name])
			}
			
			map = map.sort { it.key.toLowerCase() }
			map.each { k, v ->
				map[k] = v.sort { it.name.toLowerCase() }
			}
		} else {
			TimeSeriesDataSetController.accessibleDataSets(session).each { ds ->
				map[ds.id] = ds.name
			}
			
			map = map.sort { it.value.toLowerCase() }
		}
		
		render map as JSON
	}
	
	def networksAsJSON() {
		def map = [:]
		
		if (params.inGroups) {
			GraphController.accessibleNetworks(session).each { ds ->
				if (!map.containsKey(ds.group.name))
					map[ds.group.name] = []
				map[ds.group.name].push([id: ds.id, name: ds.name])
			}
			map = map.sort { a, b -> a == "TiCoNE" ? -1 : (b == "TiCoNE" ? 1 : a.key.toLowerCase() <=> b.key.toLowerCase()) }
			//map = map.sort {it.key.toLowerCase()};
			//map = map.sort {a -> (a == "TiCoNE") ? "" : a.key.toLowerCase()};
			map.each { group, ds ->
				map[group] = ds.sort { it.name.toLowerCase() }
			}
		} else {
			GraphController.accessibleNetworks(session).each { ds ->
				map[ds.id] = ds.name
			}
			
			map = map.sort { it.value.toLowerCase() }
		}
		
		render map as JSON
	}


	def _show_least_similar_objects_pattern() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization current_vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization

		int pattern = params.pattern.toInteger();
		Pattern_g p = Pattern_g.get(pattern)

		[clustering: itCl,
			percentage: params.percentage,
			type: params.type,
			iteration: params.iteration.toInteger(),
			pattern: pattern,
			p: p
		]
	}

	def _show_pattern_overview() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization current_vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization

		[clustering: itCl,
			patterns: current_vis.clustering.patterns
		]
	}

	def get_pattern_least_similar_objects() {
		IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
		TimeSeriesClusteringVisualization current_vis = itCl.iterations.find { it.id == params.iteration.toInteger() }.visualization
		Pattern_g p_g = Pattern_g.get(params.pattern.toInteger())
		
		double percentage = params.percentage.toDouble();

		// input files
		TimeSeriesDataSet dataSet = itCl.dataSet;
		AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
		Scanner scanner = new Scanner(new ByteArrayInputStream(itCl.dataSet.file.tmpFile));
		List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
		scanner.close();
		calc.initializeTimeSeriesData(tsData);
		calc.calculatePatterns();
		
		// restore PatternObjectMapping from database;
		PatternObjectMapping mapping = current_vis.clustering.toPatternObjectMapping(tsData);

		// find the correct Pattern object;
		Iterator<Cluster> patternIt = mapping.clusterSet().iterator();
		Cluster pattern;
		while (patternIt.hasNext()) {
			Cluster p = patternIt.next();
			if (Arrays.equals(p.prototype, p_g.timeSeriesPattern)) {
				pattern = p;
				break;
			}
		}

		ISimilarity similarityFunction;
		List<TimeSeriesObject> leastFitting;
		if (params.type == "percentage") {
			if (itCl.similarityFunction.name == "Euclidian Distance")
				similarityFunction = new NegativeEuclideanSimilarity();
			else if (itCl.similarityFunction.name == "Pearson Correlation")
				similarityFunction = new PearsonCorrelation();
			leastFitting = ExtractData.getLeastFittingDataFromPattern(mapping, pattern, (int)percentage, similarityFunction);
		} else if (params.type == "rss") {
			similarityFunction = new NegativeEuclideanSimilarity();
			leastFitting = ExtractData.findObjectsFromPatternWithThreshold(pattern, mapping, percentage, similarityFunction)
		} else if (params.type == "pearson") {
			similarityFunction = new PearsonCorrelation();
			leastFitting = ExtractData.findObjectsFromPatternWithThreshold(pattern, mapping, percentage, similarityFunction)
		}
		
		
		Map<String, Set<String>> res = new HashMap<String, Set<String>>();
		res.put('leastFittingObjects', leastFitting.collect { x -> x.name; });
		Map<String, double[]> objectTimeSeries = new HashMap<String, double[]>();
		leastFitting.each { x ->
			objectTimeSeries.put(x.name, x.exactPattern);
		};
		res.put('percentage',percentage);
		res.put('status', 200);
		res.put('objectTimeSeries', objectTimeSeries);
		render res as JSON
	}
	
	def async_do_force_layout() {
		def url = createLink(controller: 'iterativeTimeSeriesClustering', action: 'do_force_layout', id: params.id, params: [iteration: params.iteration])
		async_decorate_and_submit_task("Do Force Layout", url, false, [:], request.cookies);
	}
	
	def do_force_layout() {
		try {
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
			IterationHasVisualization itHasVis = itCl.iterations.find { it.id == params.iteration.toInteger() }
			TimeSeriesClusteringVisualization vis = itHasVis.visualization
			vis.doForceLayout(userId, userSessionId);
			
			//redirect(action: 'show_cytoscape', id: params.id, params: [iteration: params.iteration])
			
			finishTaskAndRedirect(null, null, itHasVis, itCl, params.taskId, null);
		} catch (Exception e) {
			render(status: 403, text: e.getMessage());
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage());
			}
		}
	}
	
	def async_do_circle_layout() {
		def url = createLink(controller: 'iterativeTimeSeriesClustering', action: 'do_circle_layout', id: params.id, params: [iteration: params.iteration])
		async_decorate_and_submit_task("Do Circle Layout", url, false, [:], request.cookies);
	}
	
	def do_circle_layout() {
		try {
			def userId;
			def userSessionId;
			(userId, userSessionId) = get_user_and_session(params.userId, params.userSessionId)
			IterativeTimeSeriesClustering itCl = IterativeTimeSeriesClustering.findBySessionId(params.id)
			IterationHasVisualization itHasVis = itCl.iterations.find { it.id == params.iteration.toInteger() }
			TimeSeriesClusteringVisualization vis = itHasVis.visualization
			vis.doCircleLayout(userId, userSessionId);
			
			//redirect(action: 'show_cytoscape', id: params.id, params: [iteration: params.iteration])
			
			finishTaskAndRedirect(null, null, itHasVis, itCl, params.taskId, null);
		} catch (Exception e) {
			render(status: 403, text: e.getMessage());
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage());
			}
		}
	}
}
