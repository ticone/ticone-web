package tsviz_grails

import static groovyx.net.http.Method.*
import groovy.sql.Sql
import groovyx.net.http.AsyncHTTPBuilder
import groovyx.net.http.ContentType

import org.apache.shiro.SecurityUtils
import org.codehaus.groovy.grails.web.mapping.LinkGenerator

import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues
import dk.sdu.imada.ticone.preprocessing.filter.LeastConservedObjectSetsFilter
import dk.sdu.imada.ticone.preprocessing.filter.LeastVaryingObjectSetsFilter
import dk.sdu.imada.ticone.preprocessing.filter.LeastVaryingObjectSetsThresholdFilter
import dk.sdu.imada.ticone.io.Parser
import dk.sdu.imada.ticone.similarity.NegativeEuclideanSimilarity
import dk.sdu.imada.ticone.similarity.PearsonCorrelation
import dk.sdu.imada.ticone.tsdata.TimeSeriesObject
import dk.sdu.imada.ticone.variance.StandardVariance
import dk.sdu.imada.ticone.api.AbstractTimeSeriesPreprocessor
import dk.sdu.imada.ticone.api.IDataFilter
import dk.sdu.imada.ticone.api.ISimilarity

class TimeSeriesDataSetController {

	static def dataSourceUnproxied
	static def grailsApplication
	LinkGenerator grailsLinkGenerator

	def index() {
		render(view: 'index', model: [dataSets: accessibleDataSets(session)])
	}

	static def accessibleDataSets(session) {
		if (SecurityUtils.getSubject().isAuthenticated()) {
			def curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
			return TimeSeriesDataSet.where {(published == true) || (user == curruser)}.list()
		} else {
			if (session)
				return TimeSeriesDataSet.where {(published == true) || (sessionId == session.getId())}.list()
			else
				return TimeSeriesDataSet.where {(published == true)}.list()
		}
	}

	def create() {
		def user;
		if (SecurityUtils.getSubject().isAuthenticated())
			user = User.findByUsername(SecurityUtils.getSubject().getPrincipal());

		int popup = params.int('popup', 0)
		def p = [user: user] + params
		render(view: 'create', model: p)
	}
	
	def async_upload() {
		// parameter validation
		if (!params.name) {
			flash.error = 'Please provide a name for your data set'
			redirect(action: 'create', params: params)
			return
		}
		byte[] fileBytes = null;
		if (!request.getFile("tsFile").isEmpty()) {
			fileBytes = request.getFile("tsFile").getBytes();
		}
		TimeSeriesFile tsFile = new TimeSeriesFile(tmpFile: fileBytes);
		if (!tsFile.validate()) {
			if (tsFile.errors.hasFieldErrors("tmpFile")) {
				flash.error = 'Please select a valid file'
			}
			redirect(action: 'create', params: params)
			return
		}
		tsFile.save(flush: true);
		
		def p = [name: params.name, tsFileId: tsFile.id]
		if (params.published) {
			p['published'] = params.published
		}
		if (params.popup) {
			p['popup'] = params.popup
		}
		
		def url = createLink(controller: 'timeSeriesDataSet', action: 'upload')
		async_decorate_and_submit_task("Processing Time Series Data Set", url, false, p, request.cookies);
	}
	
	def async_decorate_and_submit_task(taskTitle, url, canCancel, p, cookies) {
		Task t = create_task(taskTitle, canCancel);
		
		def base = grailsLinkGenerator.serverBaseURL - grailsLinkGenerator.contextPath
		def curruser = null;
		if (SecurityUtils.getSubject().isAuthenticated()) {
			curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
		}
		
		def postParams;
		if (curruser != null) {
			postParams = [taskId: t.taskId, userId: curruser.id] + p
		} else {
			postParams = [taskId: t.taskId, userSessionId: session.getId()] + p
		}
		
		def http = new AsyncHTTPBuilder(poolSize: 4, uri: base)
		http.ignoreSSLIssues()
		println base
		http.request( POST ) { req ->
			uri.path = url
			requestContentType = ContentType.JSON
			uri.query = postParams
			headers['Cookie'] = cookies.collect {
				it.getName() + "=" + it.getValue()
			}.join('; ')
			body = []
		
			 response.success = { resp, json ->
				 println "SUCCESS"
			}
		
			 response.failure = { resp, json ->
				 println "FAILED"
				 render "Connection failed"
			}
		}
		redirect(controller: 'task', action: 'show', id: t.taskId)
	}
	
	def create_task(title, canCancel) {
		def curruser = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
		def task_id = Task.create_task_id();
		Task t = new Task(taskId:task_id,
			title:title,
			user:curruser,
			finished:false,
			canCancel:canCancel,
			cancelled:false,
			sendMail:false)
		t.save(flush: true);
		return t;
	}
	
	def setTaskFinished(String taskId, String redirect_url, String statusMessage) {
		Task t = Task.findByTaskId(taskId)
		t.refresh();
		t.finished = true;
		t.redirect_url = redirect_url;
		if (statusMessage) {
			t.statusMessage = statusMessage;
		}
		t.save(flush: true);
		
		sendTaskMail(t);
	}
	
	def sendTaskMail(Task t) {
		if (t.sendMail && t.user) {
			sendMail {
				to(t.user.email)
				from "noreply@compbio.sdu.dk"
				subject "TiCoNE: " + t.title + " finished"
				html '<p>Your job has finished.</p><p>You can access the result here: <a href="' + t.redirect_url + '">' + t.redirect_url + '</a></p>'
			  }
		}
	}

	def upload() {
		try {
			String name = request.getParameter("name")
			
			TimeSeriesFile tsFile;
			// if we have a file uploaded
			if (params.tsFile) {
				byte[] fileBytes = request.getFile("tsFile").getBytes();
				
				tsFile = new TimeSeriesFile(tmpFile: fileBytes);
				tsFile.save(flush: true);
			}
			// if we have a timeseriesfile id passed (from async_upload)
			else {
				tsFile = TimeSeriesFile.get(params.tsFileId)
			}
	
			println "Parsing time series data"
			// use library to normalize and preprocess input
			AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
			Scanner scanner = new Scanner(new ByteArrayInputStream(tsFile.tmpFile));
			List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
			scanner.close();
	
			calc.initializeTimeSeriesData(tsData);
			calc.calculatePatterns();
	
			println "Storing time series data in database"
			long start = System.currentTimeMillis();
			//TimeSeriesDataSet dataSet = TimeSeriesDataSetController.parseDataSetFromInput(timeSeriesFile.getAbsolutePath(), request.getParameter("name"), calc.getTimeSeriesDatas());
			TimeSeriesDataSet dataSet = TimeSeriesDataSetController.parseDataSetFromInputBatch(tsFile, request.getParameter("name"), calc.getTimeSeriesDatas());
			println "time needed to insert data set:"
			println System.currentTimeMillis()-start;
	
			if (SecurityUtils.getSubject().isAuthenticated()) {
				def user = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
				dataSet.user = user;
			} else {
				dataSet.sessionId = session.getId();
			}
	
			if (params.published == 'on') {
				dataSet.published = true;
			}
			dataSet.save(flush: true);
			
			
			// if we have been passed a taskId, we inform the task that it is finished now
			if (params.taskId) {
				def url;
				if (params.popup == "1") {
					url = createLink(absolute: true, action: 'upload_finished');
				} else {
					url = createLink(absolute: true, action: 'index');
				}
				setTaskFinished(params.taskId, url, null);
			}
			if (params.popup == "1") {
				render(view: 'upload_finished')
				return;
			}
			redirect(action: 'index')
		} catch (Exception e) {
			if (params.taskId) {
				setTaskFinished(params.taskId, null, e.getMessage());
			}
		}
	}
	
	def upload_finished() { }

	def filter() {
		println params
		if (!params.step) {
			def dataSets = accessibleDataSets(session)
			def dataFilters = TimeSeriesDataSetFilter.get_as_json();

			[dataFilters: dataFilters, dataSets: dataSets]
		} else if (params.step == "2") {
			switch (params.dataFilter) {
				case "0":
					List<SimilarityFunction> simFunctions = SimilarityFunction.list()
					println simFunctions
					render(view: 'filter_least_conserved_object_sets', model: [simFunctions: simFunctions])
					return;
				case "1":
					render(view: 'filter_least_varying_object_sets')
					return;
				default:
					break;
			}
		}
	}

	def do_filter() {
		def dataSet = TimeSeriesDataSet.get(params.tsDataSet)

		AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
		Scanner scanner = new Scanner(new ByteArrayInputStream(dataSet.file.tmpFile));
		List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
		scanner.close();
		calc.initializeTimeSeriesData(tsData);
		calc.calculatePatterns();

		switch (params.dataFilter) {
			case "0":
				ISimilarity similarityFunction;
				SimilarityFunction simFun = SimilarityFunction.get(params.simFunction)
				if (simFun.name == "Euclidian Distance")
					similarityFunction = new NegativeEuclideanSimilarity();
				else if (simFun.name == "Pearson Correlation")
					similarityFunction = new PearsonCorrelation();
				IDataFilter filter = new LeastConservedObjectSetsFilter(params.percent.toInteger(), similarityFunction);
				List<TimeSeriesObject> filteredData = filter.filterObjectSets(tsData);
				println filteredData

				ByteArrayOutputStream os = new ByteArrayOutputStream();
				Writer w = new OutputStreamWriter(os);
				Parser.writeObjectSetsToOutputStream(filteredData, w);
				w.close();
				
				TimeSeriesFile newTsFile = new TimeSeriesFile(tmpFile: os.toByteArray());
				newTsFile.save();

				// create a new data set from these time series datas
				TimeSeriesDataSet newDataSet = parseDataSetFromInputBatch(newTsFile, params.name, filteredData);
				
				if (SecurityUtils.getSubject().isAuthenticated()) {
					def user = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
					newDataSet.user = user;
				} else {
					newDataSet.sessionId = session.getId();
				}
		
				newDataSet.published = dataSet.published;
				dataSet.save(flush: true);
				
				println newDataSet
				
				break;
			// least varying
			case "1": 
				IDataFilter filter;
				if (params.type == "1")
					filter = new LeastVaryingObjectSetsFilter(params.percentage.toInteger(), new StandardVariance());
				else if (params.type == "2")
					filter = new LeastVaryingObjectSetsThresholdFilter(params.threshold.toDouble(), new StandardVariance());
				List<TimeSeriesObject> filteredData = filter.filterObjectSets(tsData);
				println tsData.size();
				println filteredData.size();

				ByteArrayOutputStream os = new ByteArrayOutputStream();
				Writer w = new OutputStreamWriter(os);
				Parser.writeObjectSetsToOutputStream(filteredData, w);
				w.close();
				
				TimeSeriesFile newTsFile = new TimeSeriesFile(tmpFile: os.toByteArray());
				newTsFile.save();

				// create a new data set from these time series datas
				TimeSeriesDataSet newDataSet = parseDataSetFromInputBatch(newTsFile, params.name, filteredData);
				
				if (SecurityUtils.getSubject().isAuthenticated()) {
					def user = User.findByUsername(SecurityUtils.getSubject().getPrincipal());
					newDataSet.user = user;
				} else {
					newDataSet.sessionId = session.getId();
				}
		
				newDataSet.published = dataSet.published;
				dataSet.save(flush: true);
				
				println newDataSet
				
				break;
			default:
				break;
		}
		if (params.popup) {
			render(view: 'filter_finished')
		} else {
			redirect(action: 'index')
		}
	}

	/*static def parseDataSetFromInput(String path, String name, List<TimeSeriesData> input) {
		TimeSeriesFile tsfile = new TimeSeriesFile(tmpFile: path);
		tsfile.save();

		TimeSeriesDataSet dataSet = new TimeSeriesDataSet(name:name, file:tsfile);
		dataSet.save();

		input.each { TimeSeriesData object ->
			List<String> sampleList = object.getSampleNameList();
			List<double[]> exactPatternList = object.getExactPatternList();
			int s;
			for (s = 0; s < sampleList.size(); s++) {
				String sample = sampleList.get(s);
				tsviz_grails.TimeSeriesObject obj = new tsviz_grails.TimeSeriesObject(dataSet: dataSet, name: object.name, sample: sample);

				double[] signal = exactPatternList.get(s);
				for (int i = 0; i < signal.size(); i++) {
					tsviz_grails.TimeSeriesObjectHasTimePoint tp = new tsviz_grails.TimeSeriesObjectHasTimePoint(object: obj, timeSeriesSignal: Double.valueOf(signal[i]), timepoint: i+1);
					obj.addToTimepoints(tp);
				}
				dataSet.addToObjects(obj);
			}
		}
		dataSet.save(flush: true);
		return dataSet;
	}*/

	static def parseDataSetFromInputBatch(TimeSeriesFile tsfile, String name, List<TimeSeriesObject> input) {
		
		//println contents
		println tsfile
		println tsfile.id

		TimeSeriesDataSet dataSet = new TimeSeriesDataSet(name:name, file:tsfile, group: DataSetGroup.findByName('User data set'));
		dataSet.save();

		def batchSize = 1000000;
		def nextObjectId = -1;
		def objectIds = [];

		print "Writing data set objects into database"
		long startTime = System.currentTimeMillis();
		def sql = Sql.newInstance(dataSourceUnproxied);
		// insert objects
		sql.withTransaction {
			// get maximal object id
			sql.eachRow('select max(id)+1 as id from time_series_object') { row ->
				nextObjectId =  row.id;
			}
			if (nextObjectId == null)
				nextObjectId = 1;

			try{
				//sql.withBatch(batchSize, 'insert into time_series_object (id, version, dataset_id, name, sample) values (?, ?, ?, ?, ?)') { stmt ->
				sql.withBatch(batchSize, 'insert into time_series_object (id, version, dataset_id, name) values (?, ?, ?, ?)') { stmt ->
					input.each { TimeSeriesObject object ->
						stmt.addBatch(nextObjectId, 1, dataSet.id, object.name);
						objectIds.add(nextObjectId);
						nextObjectId++;
//						println nextObjectId
					}
				}
			} catch(java.sql.BatchUpdateException bue){
				return "Could not add objects: ${bue.getMessage()}"
			} finally{
				sql.commit();
			}
		}
		println " ... done"
		println "time: " + (System.currentTimeMillis() - startTime).toString();
		
		def nextSampleId = -1;
		def sampleIds = [];
		def currObjectInd = 0;
		
		print "Writing object samples into database"
		startTime = System.currentTimeMillis();
		// insert objects
		sql.withTransaction {
			// get maximal sample id
			sql.eachRow('select max(id)+1 as id from time_series_object_sample') { row ->
				nextSampleId =  row.id;
			}
			if (nextSampleId == null)
				nextSampleId = 1;

			try{
				sql.withBatch(batchSize, 'insert into time_series_object_sample (id, version, name, object_id) values (?, ?, ?, ?)') { stmt ->
					input.each { TimeSeriesObject object ->
						int currentObjectId = objectIds[currObjectInd]
						List<String> sampleList = object.getSampleNameList();
						int s;
						for (s = 0; s < sampleList.size(); s++) {
							String sample = sampleList.get(s);
							stmt.addBatch(nextSampleId, 1, sample, currentObjectId);
							sampleIds.add(nextSampleId);
							nextSampleId++;
//							println nextSampleId
						}
						currObjectInd++;
					}
				}
			} catch(java.sql.BatchUpdateException bue){
				return "Could not add objects: ${bue.getMessage()}"
			} finally{
				sql.commit();
			}
		}
		println " ... done"
		println "time: " + (System.currentTimeMillis() - startTime).toString();

		def nextSignalId = -1;
		def signalIds = [];

		print "Writing object time-series into database"
		startTime = System.currentTimeMillis();
		// insert object signals
		sql.withTransaction {
			// get maximal signal id
			sql.eachRow('select max(id)+1 as id from time_series_object_sample_has_time_point') { row ->
				nextSignalId =  row.id;
			}
			if (nextSignalId == null)
				nextSignalId = 1;
			try{
				sql.withBatch(batchSize, 'insert into time_series_object_sample_has_time_point (id, version, sample_id, time_series_signal, timepoint) values (?, ?, ?, ?, ?)') { stmt ->
					int ind = 0;
					input.each { TimeSeriesObject object ->
						List<String> sampleList = object.getSampleNameList();
						List<double[]> exactPatternList = object.getPreprocessedTimeSeriesList();
						int s;
						for (s = 0; s < sampleList.size(); s++) {
							int sample_id = sampleIds[ind];
							String sample = sampleList.get(s);

							double[] signal = exactPatternList.get(s);
							for (int i = 0; i < signal.size(); i++) {
								stmt.addBatch(nextSignalId, 1, sample_id, Double.valueOf(signal[i]), i+1);
								objectIds.add(nextSignalId);
								nextSignalId++;
//								println nextSignalId
							}
							ind++;
						}
					}
				}
			} catch(java.sql.BatchUpdateException bue){
				return "Could not add signals: ${bue.getMessage()}"
			} finally{
				sql.commit();
			}
		}
		print " ... done"
		println "time: " + (System.currentTimeMillis() - startTime).toString();
		dataSet.refresh();
		return dataSet;
	}
}
