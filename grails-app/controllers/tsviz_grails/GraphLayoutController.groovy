package tsviz_grails

import grails.converters.JSON

class GraphLayoutController {

	def index() { }

	static def createForGraph(Graph g, curruser, userSessionId) {
		GraphLayout layout = new GraphLayout(graph: g, name: "Initial Layout", published: false, user: curruser, sessionId: userSessionId);
		layout.save();
		return layout;
	}

	def nodePositionsAsJSON() {
		GraphLayout layout = GraphLayout.get(params.id)
		def nodePositions = []
		GraphLayout.executeQuery("select n.name, loc.x, loc.y from GraphLayout l join l.locations loc join loc.node n where l = ?", [layout]).each { item ->
		//layout.locations.each { NodeLocation loc ->
			//nodePositions.add([id: loc.node.name, location: [x: loc.x, y: loc.y]])
			nodePositions.add([id: item[0], location: [x: item[1], y: item[2]]])
		}
		render nodePositions as JSON
	}
}
