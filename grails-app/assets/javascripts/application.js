// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require jquery
//= require cytoscape/cytoscape.js
//= require cytoscape/springy.js
//= require cytoscape/cytoscape-springy.js
//= require cytoscape/cytoscape-cose-bilkent.js
//= require vivagraph
//= require rainbowvis
//= require highcharts
// = require highcharts.exporting
// = require highcharts.boost.src
//= require highcharts.draggable-points
//= require jquery.jscrollpane
//= require jquery.mousewheel
//= require jquery-ui.min
//= require jquery.bpopup.min
//= require jquery.floatThead
//= require jquery.tablesorter.min
//= require jquery.tablesorter.widgets
//= require jquery.tablesorter.widget-staticRow
//= require jquery.resizableColumns.min
//= require bootstrap
//= require bootstrap-select
// = require_tree .
//= require_self

if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}

$(function() {
	$('.selectpicker').selectpicker({
	  //style: 'btn-info',
	  //size: 4
	});
	//$('.selectpicker').addClass('form-control').selectpicker('setStyle');
});