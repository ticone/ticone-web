<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
		<g:content tag="sidebarTitle">Getting Started</g:content>
		<g:content tag="sidebar">
		<p>On this website we provide you with all necessary documentation, tutorials as well as installation and usage instructions for our method.</p>
		
		<p>Please choose what you would like to do.</p>
		</g:content>
		<g:content tag="sidebarClass">col-lg-3</g:content>
		<g:content tag="title"><h1>What's new?</h1></g:content>
		
		<shiro:isNotLoggedIn>
		<script>
		$( document).ready(function() {
			$('#welcomeModal').modal({backdrop: 'static', keyboard: false});
			});
		</script>
		
		<div class="modal fade" id="welcomeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-header" style="vertical-align:bottom;">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <g:img height="48px" alt="${ grails.util.Holders.config.grails.project.groupId }" file="ticone_logo_3.png" style="vertical-align:bottom;"/>
		        <h2 class="modal-title" id="myModalLabel" style="display: inline-block;">
		        	<small>Time-Course Network Enricher</small></h2>
		      </div>
		      <div class="modal-body">
		      	<div class="container">
		      		<div class="row">
						<div class="col-sm-9">
							<!-- <h4>${ grails.util.Holders.config.grails.project.groupId }</h4> -->
							<div class="media">
							  <!-- <div class="media-left">
							    <a href="#">
							      <img 
							      	class="media-object" 
							      	src="http://www.indre-reisid.ee/wp-content/themes/envision/lib/images/default-placeholder.png" 
							      	alt="${ grails.util.Holders.config.grails.project.groupId } Logo"
							      	width="128"
							      	height="128">
							    </a>
							  </div>-->
							  <div class="media-body">
							    <p>Welcome to the Time Course Network Enricher. ${ grails.util.Holders.config.grails.project.groupId } provides an interactive and iterative approach to cluster multi-patient-sample time-series data.
							    This website serves as a starting point for ${ grails.util.Holders.config.grails.project.groupId }, 
								by providing tutorials, documentation, downloads as well as access to the ${ grails.util.Holders.config.grails.project.groupId } web application.</p>
							  </div>
							</div>
						</div>
					</div>
					<h2>Here for the first time?</h2>
					<div class="row">
						<div class="col-sm-9">
							<p>To get started, watch our screencasts or read through the tutorials.</p>
						</div> 
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="embed-responsive embed-responsive-4by3">
							  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/X32wiE7kSz4"></iframe>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="embed-responsive embed-responsive-4by3">
								<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-qf-JZIF3CM"></iframe>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="embed-responsive embed-responsive-4by3">
								<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/e077djD6IaE"></iframe>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top: 15px;">
						<div class="col-sm-9">
							<p>
								<a class="btn btn-default btn-lg" href="${createLink(controller: 'tutorial', action: 'index') }" role="button"><i class="glyphicon glyphicon-book"></i> Read Tutorials</a>
								<a class="btn btn-default btn-lg" href="${createLink(controller: 'citation', action: 'index') }" role="button"><i class="fa fa-quote-left"></i> Cite Us</a>
							</p>
						</div>
					</div>
					<h2>Get ${ grails.util.Holders.config.grails.project.groupId }</h2>
					<div class="row">
						<div class="col-sm-9">
							<p>
							 	<!-- <a class="btn btn-default btn-lg" href="#" role="button"><img src="http://www.cytoscape.org/images/logo/cy3logoOrange.svg" width="22px"/> Cytoscape App</a> -->
							 	<a id="btn-cytoscape" class="btn btn-default btn-lg" href="http://apps.cytoscape.org/apps/ticone" role="button">
							 		<asset:image src="cytoscape_logo_large_cropped.png" width="24px" height="24px"/> Use Cytoscape App
							 	</a>
							 	<a class="btn btn-default btn-lg" href="${ createLink(controller: 'iterativeTimeSeriesClustering', action: 'create') }" role="button"><i class="glyphicon glyphicon-globe"></i> Use ${ grails.util.Holders.config.grails.project.groupId } WEB</a>
							 	<!-- <a class="btn btn-default btn-lg" href="${ createLink(controller: 'download', action: 'index') }" role="button"><i class="glyphicon glyphicon-download"></i> Download Standalone</a> -->
							</p>
						</div>
					</div>
				</div>
		      </div>
		    </div>
		  </div>
		</div>
		</shiro:isNotLoggedIn>
		<h3>TiCoNE WEB Update <small>June 08th, 2017</small></h3>
		We updated TiCoNE WEB to version 1.0.4 (<g:link controller="changelog">list of changes</g:link>). 
		<h3>Screencast for new TiCoNE Cytoscape app version 1.3. <small>June 07th, 2017</small></h3>
		We added a screencast for TiCoNE Cytoscape App version 1.3.
		<h3>Updated tutorials for new TiCoNE Cytoscape app version 1.3. <small>May 30th, 2017</small></h3>
		We updated some of our tutorials for TiCoNE Cytoscape App version 1.3. Coming soon: Updated screencast.
		<h3>TiCoNE Cytoscape App 1.2 - Tutorials and Screencast <small>September 29th, 2016</small></h3>
		We published tutorials and screencasts for version 1.2. of the TiCoNE Cytoscape app.
		<h3>TiCoNE Cytoscape App 1.2 released <small>September 14th, 2016</small></h3>
		We released version 1.2 of the TiCoNE cytoscape app. Check out the help section for updated tutorials and screencasts.
		<h3>More Screencasts <small>April 5th, 2016</small></h3>
		We added a short screencast for the TiCoNE Cytoscape App to the <g:link controller="tutorial">help section</g:link>.
</body>
</html>