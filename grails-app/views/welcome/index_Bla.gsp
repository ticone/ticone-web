<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
		<g:content tag="sidebarTitle">Getting Started</g:content>
		<g:content tag="sidebar">
		<p>On this website we provide you with all necessary documentation, tutorials as well as installation and usage instructions for our method.</p>
		
		<p>Please choose what you would like to do.</p>
		</g:content>
		<g:content tag="title"><h1>${ grails.util.Holders.config.grails.project.groupId } - <small>Time-Course Network Enricher</small></h1></g:content>

		      		<div class="row">
						<div class="col-lg-8 col-md-8 col-xs-16 col-sm-8">
							<!-- <h4>${ grails.util.Holders.config.grails.project.groupId }</h4> -->
							<div class="media">
							  <div class="media-left">
							    <a href="#">
							      <img 
							      	class="media-object" 
							      	src="http://www.indre-reisid.ee/wp-content/themes/envision/lib/images/default-placeholder.png" 
							      	alt="${ grails.util.Holders.config.grails.project.groupId } Logo"
							      	width="128"
							      	height="128">
							    </a>
							  </div>
							  <div class="media-body">
							    <p>Welcome to the Time Course Network Enricher. ${ grails.util.Holders.config.grails.project.groupId } provides an interactive and iterative approach to cluster multi-patient-sample time-series data.</p>
							    <p>This website serves as a starting point for ${ grails.util.Holders.config.grails.project.groupId }, 
								by providing tutorials, documentation, downloads as well as access to the ${ grails.util.Holders.config.grails.project.groupId } web application.</p>
							  </div>
							</div>
						</div>
					</div>
					<h2>Here for the first time?</h2>
					<div class="row">
						<div class="col-lg-8 col-md-8 col-xs-16 col-sm-8">
							<p>To get started, watch our screencast or read through the tutorials.</p>
						</div> 
					</div>
					<div class="row">
						<div class="col-lg-4 col-md-4 col-xs-8 col-sm-4">
							<div class="btn-group-vertical" role="group" aria-label="...">
								<a class="btn btn-default btn-lg" href="#" role="button"><i class="glyphicon glyphicon-book"></i> Read Tutorials</a>
								<a class="btn btn-default btn-lg" href="#" role="button"><i class="glyphicon glyphicon-book"></i> Read Documentation</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-xs-6 col-sm-3">
							<div class="embed-responsive embed-responsive-4by3">
							  <iframe class="embed-responsive-item" src="http://www.youtube.com/embed/zpOULjyy-n8?rel=0"></iframe>
							</div>
						</div>
					</div>
					<h2>Use ${ grails.util.Holders.config.grails.project.groupId }</h2>
					<div class="row">
						<div class="col-lg-8 col-md-8 col-xs-16 col-sm-8">
							<p>
							 	<a class="btn btn-default btn-lg" href="#" role="button"><img src="http://www.cytoscape.org/images/logo/cy3logoOrange.svg" width="22px"/> Cytoscape App</a>
							 	<a class="btn btn-default btn-lg" href="#" role="button"><i class="glyphicon glyphicon-download"></i> Standalone</a>
							 	<a class="btn btn-default btn-lg" href="${ createLink(controller: 'iterativeTimeSeriesClustering', action: 'create') }" role="button"><i class="glyphicon glyphicon-globe"></i> Use Web Version</a>
							</p>
						</div>
					</div>
</body>
</html>