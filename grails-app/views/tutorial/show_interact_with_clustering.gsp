<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="title"><h1>Interacting with a TiCoNE clustering</h1></g:content>
	<g:content tag="sidebarTitle">Interacting with a TiCoNE clustering</g:content>
	<g:content tag="sidebar">
		<p>Quick jump:</p>
	<ul>
		<li><a href="#iteration">Perform another iteration</a></li>
		<li><a href="#convergence">Perform iterations until convergence</a></li>
		<li><a href="#split">Split a cluster</a></li>
		<li><a href="#deleteobjects">Delete objects from a cluster</a></li>
		<li><a href="#deleteonlycluster">Delete a pattern (but keep the objects)</a></li>
		<li><a href="#deletecluster">Delete a cluster including the objects</a></li>
		<li><a href="#leastfitting">Show Least Fitting Objects</a></li>
		<li><a href="#newclustersfromleastfitting">Form new clusters from the least fitting objects</a></li>
		<li><a href="#history">Go back to a previous iteration</a></li>
		<li><a href="#layout">Layout the graph</a></li>
	</ul>
	</g:content>
	
	  <p>Given a clustering on the TiCoNE website, you can interact with it and modify it in several ways.</p>

		<hr>
	
		<a id="iteration"></a><h3>Perform another iteration</h3>
		<p>...</p>

		<hr>
	
		<a id="convergence"></a><h3>Perform iterations until convergence</h3>
		<p>...</p>
	  
	  <hr>

		<a id="split"></a><h3>Split a cluster</h3>
		<p>...</p>

		<hr>
	
		<a id="deleteobjects"></a><h3>Delete objects from a cluster</h3>
		<p>...</p>

		<hr>
	
		<a id="deleteonlycluster"></a><h3>Delete a pattern (but keep the objects)</h3>
		<p>...</p>

		<hr>
	
		<a id="deletecluster"></a><h3>Delete a cluster including the objects</h3>
		<p>...</p>

		<hr>
	
		<a id="leastfitting"></a><h3>Show Least Fitting Objects</h3>
		<p>...</p>

		<hr>
	
		<a id="newclustersfromleastfitting"></a><h3>Form new clusters from the least fitting objects</h3>
		<p>...</p>

		<hr>
	
		<a id="history"></a><h3>Go back to a previous iteration</h3>
		<p>...</p>

		<hr>
	
		<a id="layout"></a><h3>Layout the graph</h3>
		<p>...</p>

</body>
</html>