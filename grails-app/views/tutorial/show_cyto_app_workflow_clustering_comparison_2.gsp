<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="tutorial" />
</head>
<body>
	<a id="#"></a>
	<g:content tag="title">TiCoNE Cytoscape App: Clustering Comparison Tutorial</g:content>
	
	<g:content tag="tutorialContent">
	
		<div id="import_datasets" class="tutorial_step">
			<h2>Step 1: Import Data sets</h2>
			<ol>
				<li>Download our two example data sets: <a href="${ assetPath(src: "qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5.txt")}">1</a> and 
				<a href="${ assetPath(src: "qual-patterns3-objects80-seed43-bg120-samples2-conftrue-tp5.txt")}">2</a></li>
				<li>For both of the files perform the following steps:</li>
				<li>
				Click on File &#8594; Import &#8594; Table.
				</li>
				<li>
				Select the dataset file
				</li>
				<li>
				Load it to unassigned tables, and give it a name if you want to easily recognise it:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="load_table.png"/>
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		
		<div id="cluster_dataset_1" class="tutorial_step">
		
			<h2>Step 2: Cluster data set 1</h2>
		
			<ol>
				<li>Use the values as shown on the picture:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="workflow4_input.png" />
					</div>
				</div>
				</li>
				<li>
				Click "Start" to start the clustering.
				</li>
				<li>The data set is clustered into the following 10 clusters:
				<div class="row">
					<div class="col-md-6">
						<asset:image src="workflow4_iteration1_clusters.png" />
					</div>
				</div>
				</li>
				<li>To improve the clustering, we perform iterations until convergence.</li>
				<li>
				We then end up at iteration 9 with the following clusters:
				<div class="row">
					<div class="col-md-6">
						<asset:image src="workflow4_iteration9_clusters.png" />
					</div>
				</div>
				</li>
				<li>We now merge the clusters 3 and 6, then 4 and 8, and then 9 and 10.</li>
				<li>
				We now have the following 7 clusters:
				<div class="row">
					<div class="col-md-6">
						<asset:image src="workflow4_iteration12_clusters.png" />
					</div>
				</div>
				</li>
				<li>Clusters 11, 12 and 13 are the clusters we generated in our data set. The remaining clusters are random artifacts.</li>
			</ol>
		</div>
		
		
		<div id="cluster_dataset_2" class="tutorial_step">
		
			<h2>Step 3: Cluster data set 2</h2>
		
			<ol>
				<li>Use the values as shown on the picture:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="workflow5_input_ds2.png" />
					</div>
				</div>
				</li>
				<li>
				Click "Start" to start the clustering.
				</li>
				<li>The data set is clustered into the following 10 clusters:
				<div class="row">
					<div class="col-md-6">
						<asset:image src="workflow5_iteration1_clusters.png" />
					</div>
				</div>
				</li>
				<li>To improve the clustering, we perform iterations until convergence.</li>
				<li>
				We then end up at iteration 4 with the following clusters:
				<div class="row">
					<div class="col-md-6">
						<asset:image src="workflow5_iteration4_clusters.png" />
					</div>
				</div>
				</li>
				<li>We now merge the clusters 4 and 7, then 2 and 6, and then 9 and 10.</li>
				<li>
				We now have the following 7 clusters:
				<div class="row">
					<div class="col-md-6">
						<asset:image src="workflow5_iteration7_clusters.png" />
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		
		
		
		<div id="setup_comparison" class="tutorial_step">
			<h2>Step 4: Configure and Start a Clustering Comparison</h2>
		
			<ol>
				<li>
				Click on the Comparison tab on the Cytoscape results panel:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="workflow5_open_clustering_comparison.png"/>
					</div>
				</div>
				</li>
				<li>
				Now, on the Cytoscape control panel the "New Comparison" tab is shown. Set it up as follows:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="workflow5_comparison_setup.png"/>
						<dl>
							<dt>
							Clustering 1
							</dt>
							<dd>
							Choose the first clustering to compare. In this tutorial this is the clustering of data set 1.
							</dd>
							<dt>
							Clustering 2
							</dt>
							<dd>
							Choose the second clustering to compare. In this tutorial this is the clustering of data set 2.
							</dd>
							<dt>
							Clustering 1 Iteration:
							</dt>
							<dd>
							The iteration of clustering 1 which to compare. In this tutorial choose the newest iteration.
							</dd>
							<dt>
							Clustering 2 Iteration:
							</dt>
							<dd>
							The iteration of clustering 2 which to compare. In this tutorial choose the newest iteration.
							</dd>
							<dt>
							Similarity Function:
							</dt>
							<dd>
							The similarity function to use when assessing the similarity of prototypes. In this tutorial choose "Euclidian Distance (Inverse)".
							</dd>
							<dt>
							Permutation for P-Value Calculation:
							</dt>
							<dd>
							The number of permutations to do when calculating p-values. In this tutorial leave this field at 1,000.
							</dd>
						</dl>
					</div>
				</div>
				</li>
				<li>
				Click on "Compare Clusterings".
				</li>
			</ol>
		</div>
		
		
		
		<div id="comparison_result" class="tutorial_step">
			<h2>Step 5: Cluster Connectivity Result</h2>
			<ol>
				<li>
				The result of a clustering comparison analysis is shown in the Comparison tab of the Cytoscape results panel.
				</li>
				<li>
				The table of a clustering comparison contains one row for each pair of clusters:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="workflow5_comparison_result.png"/>
						<dl>
							<dt>
							Cluster 1
							</dt>
							<dd>
							A cluster of clustering 1.
							</dd>
							<dt>
							Cluster 2
							</dt>
							<dd>
							A cluster of clustering 2.
							</dd>
							<dt>
							#Objs.
							</dt>
							<dd>
							The number of common objects of the two compared clusters. These are objects that are contained in both of the clusters.
							</dd>
							<dt>
							%Objs.
							</dt>
							<dd>
							The number of common objects scaled by the maximally possible number of common objects as a percentage: #CommonObjs/min(#Objs(Cluster1), #Objs(Cluster2)).
							</dd>
							<dt>
							Sim.
							</dt>
							<dd>
							The similarity of the prototypes of the two clusters.
							</dd>
							<dt>
							p
							</dt>
							<dd>
							An empirical p-value to observe at least this number of common objects for two clusters with at most this prototype similarity.
							</dd>
						</dl>
					</div>
				</div>
				</li>
				<li>
				Sorting the table increasingly by p-values shows us pairs of clusters at the top where we find more common objects than we expected.
				<div id="row">
					<div id="col-md-12">
						<asset:image src="workflow5_comparison_result_sorted.png"/>
					</div>
				</div>
				</li>
			</ol>
		</div>
</g:content>
</body>
</html>