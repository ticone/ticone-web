<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="title"><h1>Starting a TiCoNE clustering</h1></g:content>
	<g:content tag="sidebarTitle">Tutorials</g:content>
	<g:content tag="sidebar">TODO</g:content>
	
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h2 class="panel-title">1. Data Upload</h2>
	  </div>
	  
	  <div class="panel-body">
	  <p>Prior to the clustering you can either choose an existing time-series data set and graph, or upload your own ones.</p>
	  
	  <hr>

		<h3>Time-Series Data</h3>
		<p>Format should be ...</p>

		<hr>
	
		<h3>Graph</h3>
		<p>Format should be ... Graph is optional</p>
		</div>
	</div>
	
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h2 class="panel-title">2. Set parameters for clusterings</h2>
	  </div>
	  <div class="panel-body">
	  	You can influence the clustering procedure of TiCoNE by setting several parameters.
	  	
	  	<hr>
	  
		<h3>Choose Clustering Method</h3>
		<p>PAMK vs TransClust</p>
		
		<hr>
		
		<h3>Number of clusters</h3>
		<p>
		...
		</p>
		
		<hr>
		
		<h3>
			Similarity Measure
		</h3>
		<p>
		Pearson Correlation vs Euclidian Distance
		</p>
		
		<hr>
		
		<h3>
			Discretization Steps
		</h3>
		<p>
		...
		</p>
	  </div>
	</div>
	
	
	<div class="panel panel-default">
	  <div class="panel-heading">
	    <h2 class="panel-title">3. Hit "Start"</h2>
	  </div>
	  <div class="panel-body">
	  	<p>After you click on the start button, the following will happen:</p>
	  	
	  	<h3>1. Data Processing</h3>
	  	<p>First, TiCoNE will process your data according to your settings. This includes calculating pairwise similarities between all objects and can thus be quite time intensive.</p>

		<hr>
		
		<h3>2. Initial Clustering</h3>
		<p>Afterwards, it uses your selected clustering method to calculate an initial clustering.</p>

		<hr>
		
		<h3>3. Clustering visualization</h3>
		<p>You are then forwarded to the interactive visualization of the clustering. 
		If you have specified a graph, it will be shown on the right, whereas the patterns are shown on the left.
		Here you can interact with the clustering and apply operations such as splitting, merging or deleting clusters.
		These interactions are explained in more detail <g:link action="show" id="2">here</g:link>.</p>
	  </div>
	  </div>

</body>
</html>