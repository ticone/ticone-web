<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="tutorial" />
</head>
<body>
	<a id="#"></a>
	<g:content tag="title">TiCoNE Cytoscape App: Tutorial</g:content>
	
	<g:content tag="tutorialContent">
	
		<div id="installation" class="tutorial_step">
			<h2>Step 1: Installation</h2>
			<ol>
				<li>
				Download and install Cytoscape. Please make sure you have Java 1.6+ installed and configured.
				</li>
				<li>
				To install the TiCoNE plug-in with Cytoscape 3 and above, use the Cytoscape App Manager: "Apps -> App Manager -> Install Apps" and select TiCoNE.
				</li>
				<li>
				Once the plugin is installed, it should appear in the Apps menu as "TiCoNE".
				</li>
				<li>
				Further it should appear as "TiCoNE" under Control Panel as seen below:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="control_panel_1.2.53.png" />
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		
		<div id="import_network" class="tutorial_step">
			<h2>Step 2: Import network</h2>
			<ol>
				<li>Download our <a href="${ assetPath(src: "cytoscape_toy_network.txt")}">example network</a></li>
				<li>
				Click on File &#8594; Import &#8594; Network.
				</li>
				<li>
				Select our network file
				</li>
				<li>
				Choose "Source" and "Target" interaction, and do not transfer first lines as column names (unless your selected network file has column names on first line):
				<div class="row">
					<div class="col-md-12">
						<asset:image src="load_network.png"/>
					</div>
				</div>
				<li>
				Once the network is loaded, apply prefered layout to get a better overview of the network.
				</li>
				<li>
				The network will now be shown as below:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="network.png" />
					</div>
				</div>
				</li>
			</ol>
		</div>
		<div id="import_dataset" class="tutorial_step">
			<h2>Step 3: Import dataset</h2>
			<ol>
				<li>Download our <a href="${ assetPath(src: "cytoscape_toy_dataset.txt")}">example data set</a></li>
				<li>
				Click on File &#8594; Import &#8594; Table.
				</li>
				<li>
				Select our dataset file
				</li>
				<li>
				Load it to unassigned tables, and give it a name if you want to easily recognise it:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="load_table.png"/>
					</div>
				</div>
				</li>
				<li>
				Once the table is loaded, you can select it with the table selector:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="table_selector_1.2.53.png" />
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		<div id="setup_input" class="tutorial_step">
		
			<h2>Step 4: Setup the input</h2>
		
			<ol>
				<li>Use the values as shown on the picture:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="settings_1.2.53.png" />
						<dl>
							<dt>
							Initial clustering method
							</dt>
							<dd>
							The initial clustering method, is used in the first iteration of the clustering, to have a solution to work on.
							</dd>
							<dt>
							Discretization
							</dt>
							<dd>
							This setting is to setup the discretized values of the found patterns. You have to select how many steps you want from 0 to the maximum value of your input dataset (if te maximum values is above 0), and how many steps you want from 0 to the minimum values of your input dataset (If the minimum value is below 0).
							</dd>
							<dt>
							Data preprocessing
							</dt>
							<dd>
							This is for preprocessing of the data if you want any. Absolute values makes no preprocessing.
							</dd>
							<dt>
							Similarity Function
							</dt>
							<dd>
							There are two options for similarity functions; shape and magnitude. Bot similarity function will output a similarity in the range [0,1] where 1 is most similar, and 0 is least similar.
							</dd>
							<dt>
							Permutation Method
							</dt>
							<dd>
							The permutation method is used for statistics calculation. The function selected will be used when a random dataset (based on the input values) is generated.
							</dd>
						</dl>
					</div>
				</div>
				</li>
				<li>
				Click "Start" to start the clustering.
				</li>
			</ol>
		</div>
		
		
		
		<div id="clustering_visualization" class="tutorial_step">
			<h2>Step 5: View and analyze results</h2>
			<ol>
				<li>
				Now under the Clusters tab you can view your found patterns, and they will be visualized as below:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="graphs_1.2.53.png" />
					</div>
				</div>
				</li>
				<li>
				For each row, information about one pattern is displayed
				<ol type="1">
					<li>
					Pattern ID
					</li>
					<li>
					Number of objects assigned to the pattern
					</li>
					<li>
					The average pearson value for the assigned objects to the pattern
					</li>
					<li>
					The pearson p-value, based on the average pearson value
					</li>
					<li>
					The average Residual Sum of Squares (RSS) for the the assigned objects to the pattern
					</li>
					<li>
					The rss p-value, based on the average RSS
					</li>
					<li>
					KPM checkbox, specifying whether it should be used in Network Enrichment analysis
					</li>
					<li>
					Merge checkbox, specifying whether the pattern should be merged with other patterns
					</li>
					<li>
					Visualize checkbox, specifying whether the objects should be colored in the patterns color on the network
					</li>
					<li>
					A graph visualizing the pattern and the assigned objects
					</li>
				</ol>
				</li>
			</ol>
		</div>
		
		
		<div id="delete_action" class="tutorial_step">
			<h2>Step 6: Delete actions</h2>
			<ol>
				<li>
				You have multiple options for each pattern.
				</li>
				<li>
				You can delete data from a pattern:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="delete_pattern_options.png"/>
					</div>
				</div>
				</li>
				<li>
				As you can see on Pattern 6, there are some objects, that might not fit so well to the pattern. Thus you can click on delete below that pattern, and then choose "Least fitting objects":
				<div id="row">
					<div id="col-md-12">
						<asset:image src="pattern_least_fitting_objects.png"/>
					</div>
				</div>
				</li>
				<li>
				Here a graph will show the objects that will be removed from the pattern, and you can select how many percent you want to remove, and by which measure (shape or magnitude).
				</li>
				<li>
				Click delete objecst, will remove the objects from the pattern, and also from the following iterations and actions. (Thus, the deleted objects will be left out of the clustering from now on)
				</li>
			</ol>
		</div>
		
		
		<div id="split_pattern" class="tutorial_step">
			<h2>Step 7: Split pattern</h2>
			<ol>
				<li>
				You can split patterns, if you can see that one pattern may be able to be split in to 2 or more patterns.
				</li>
				<li>
				As you can see on Pattern 5, there may be 2 patterns hidden in that, so you can click on the split pattern button:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="splitpattern_least_similar.png"/>
					</div>
				</div>
				</li>
				<li>
				Now you get to choose how you want to split the pattern.
				</li>
				<li>
				Selecting "Based on least similar objects", the two least similar objects in the pattern will be candidates for new patterns, and all the other objects will now be assigned to the most similar of these two objects.
				</li>
				<li>
				Select Magnitude (Euclidean) as the similarity function, and you will get the following result:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="split_pattern_accept.png"/>
					</div>
				</div>
				</li>
				<li>
				You now have the option to save the new patterns (which will remove the old pattern), or discard the new patterns (which will keep the old pattern).
				</li>
				<li>
				By selecting save, the two new patterns will appear as shown below:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="splitted_pattern_action.png"/>
					</div>
				</div>
				</li>
				<li>
				As seen, the two new patterns are maked with green, where the old pattern is marked with red, indicating that the old pattern will be removed, and the two new patterns will be added, once pressing "Apply changes".
				</li>
			</ol>
		</div>
		
		
		<div id="suggest_patterns" class="tutorial_step">
			<h2>Step 7: Suggest patterns</h2>
			<ol>
				<li>
				Clicking "Filter objects" -> "Least fitting objects" in the Cluster Operations panel on the left, you will be pressented with a dialog, that shows the least fitting objects among all patterns:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="least_fitting_objects.png"/>
					</div>
				</div>
				</li>
				<li>
				You have the options, to either delete the least fitting objects, or suggest new patterns based on the objects. The suggestions will be based on a new clustering, using PAMK.
				</li>
				<li>
				The result from suggesting patterns will be pressented as below:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="suggested_patterns.png"/>
					</div>
				</div>
				</li>
				<li>
				You have the option to choose which patterns you want to keep.
				</li>
				<li>
				The patterns you mark to keep, will, after the save selected patterns have been pressed, show up in the pattern table. The objects will be removed from their previous patterns, and assigned to these new patterns. The objects on patterns you choose to not keep, will stay assigned to their original pattern.
				</li>
			</ol>
		</div>
		
		<div id="pattern_history" class="tutorial_step">
			<h2>Step 8: Cluster history</h2>
			<ol>
				<li>
				You can also preview all your previous steps, by clicking on "Show history". This will give the following window, where you can view previous states of you session:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="pattern_history.png"/>
					</div>
				</div>
				</li>
				<li>
				Clicking on apply history, will go back to the selected state, removing the trailing states from the session.
				</li>
			</ol>
		</div>
		
		
		<div id="iterations" class="tutorial_step">
			<h2>Step 9: Iterations</h2>
			<ol>
				<li>
				Besides applying manually actions on patterns, one can also do iterations of optimization.
				<li>
				One can run 1 iteration at a time, or run until convergence, by pressing either the "Do Iteration" or the "Until convergence" button on the Cluster Operations panel". This means that iterations will be run until no change occur in the assignment of objects to pattern (with an upper bound of 100).
				<li>
				After a run of "Until Convergence", the clusters should look like the following:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="lucky_iterations.png"/>
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		
		
		<div id="network_analysis" class="tutorial_step">
			<h2>Step 10: Network analysis</h2>
			<ol>
				<li>
				One can visualize the clusters on the network, by clicking the "Visualize" button.
				</li>
				<li>
				This will color the nodes in network in focus, according to which pattern they belong to (only the patterns selected for visualization, the others will be set to the default color), as shown below:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="visualized_network.png"/>
					</div>
				</div>
				</li>
				<li>
				Further network enrichment can be done with Key Pathway Miner, where a limited version is included in the app.
				</li>
				<li>
				Select the "KPM" checkboxes on the patterns you want to run network enrichment on, and then move to the Network Operations tab.
				</li>
				<li>
				Here one gets to select the number of exception nodes, the number of results one wants pressented, and whether unmapped nodes should be put on the positive or negative list:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="kpm_control_panel_1.2.53.png"/>
					</div>
				</div>
				<div id="row">
					<div id="col-md-12">
						<asset:image src="kpm_result_table_1.2.53.png"/>
						<dl>
							<dd>
							Here are the results of selecting Pattern 6, and running with the shown parameters.
							</dd>
							<dd>
							For each result, one get the ID of the result, the number of nodes appearing in the result, and the number of exception nodes in the result.
							</dd>
						</dl>
					</div>
				</div>
				</li>
				<li>
				One can highlight the results in the network, by selecting the results in the table.
				<div id="row">
					<div id="col-md-12">
						<asset:image src="kpm_selection_1.2.53.png"/>
					</div>
				</div>
				</li>
				<li>
				Further one can extract the union or core (intersection) of a selection of results, using the buttons. This will create a new network, with the corresponding nodes:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="kpm_subnetwork_1.2.53.png"/>
					</div>
				</div>
				</li>
			</ol>
		</div>
</g:content>
</body>
</html>