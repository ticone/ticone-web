<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="tutorial" />
</head>
<body>
	<a id="#"></a>
	<g:content tag="title">TiCoNE Cytoscape App: Cluster Connectivity Tutorial</g:content>
	
	<g:content tag="tutorialContent">
		<div id="import_network" class="tutorial_step">
			<h2>Step 1: Import network</h2>
			<ol>
				<li>Download our <a href="${ assetPath(src: "qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5-seed42-samePatternEdgeProb0.4.txt")}">example network</a></li>
				<li>
				Click on File &#8594; Import &#8594; Network.
				</li>
				<li>
				Select our network file
				</li>
				<li>
				Choose "Source" and "Target" interaction, and do not transfer first lines as column names (unless your selected network file has column names on first line):
				<div class="row">
					<div class="col-md-12">
						<asset:image src="load_network.png"/>
					</div>
				</div>
				<li>
				Once the network is loaded, apply prefered layout to get a better overview of the network.
				</li>
				<li>
				The network will now be shown as below:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="network.png" />
					</div>
				</div>
				</li>
			</ol>
		</div>
		<div id="import_dataset" class="tutorial_step">
			<h2>Step 2: Import dataset</h2>
			<ol>
				<li>Download our <a href="${ assetPath(src: "qual-patterns3-objects80-seed42-bg120-samples2-conftrue-tp5.txt")}">example data set</a></li>
				<li>
				Click on File &#8594; Import &#8594; Table.
				</li>
				<li>
				Select our dataset file
				</li>
				<li>
				Load it to unassigned tables, and give it a name if you want to easily recognise it:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="load_table.png"/>
					</div>
				</div>
				</li>
				<li>
				Once the table is loaded, you can select it with the table selector:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="table_selector.png" />
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		<div id="setup_input" class="tutorial_step">
		
			<h2>Step 3: Perform a clustering</h2>
		
			<ol>
				<li>Use the values as shown on the picture:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="workflow4_1.3.77_input.png" />
					</div>
				</div>
				</li>
				<li>
				Click "Start" to start the clustering.
				</li>
			</ol>
		</div>
		
		
		
		<div id="improve_clustering" class="tutorial_step">
			<h2>Step 4: Improve clustering</h2>
			<ol>
				<li>The data set is clustered into the following 10 clusters:
				<div class="row">
					<div class="col-md-6">
						<asset:image src="workflow4_1.3.77_iteration1_clusters.png" />
					</div>
				</div>
				</li>
				<li>To improve the clustering, we perform iterations until convergence.</li>
				<li>
				We then end up at iteration 5 with the following clusters:
				<div class="row">
					<div class="col-md-6">
						<asset:image src="workflow4_1.3.77_iteration5_clusters.png" />
					</div>
				</div>
				</li>
				<li>We now merge clusters 2, 3 and 5, then 6, 8 and 9, and then we perform iterations until convergence.</li>
				<li>
				We now have the following 6 clusters:
				<div class="row">
					<div class="col-md-6">
						<asset:image src="workflow4_1.3.77_iteration11_clusters.png" />
					</div>
				</div>
				</li>
				<li>Clusters 7, 13 and 14 are the clusters we generated in our data set. The remaining clusters are random artifacts.</li>
			</ol>
		</div>
		
		
		<div id="clusterPvalues" class="tutorial_step">
			<h2>Step 5: Cluster p-values</h2>
			<ol>
				<li>
				Next, we calculate p-values for our clusters:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="calculate_cluster_pvalues_1.3.77.png"/>
					</div>
				</div>
				</li>
				<li>
				After a short while, the cluster p-values are shown next to the cluster graphs (these values differ each time due to the random nature of empirical p-values):
				<div id="row">
					<div id="col-md-12">
						<asset:image src="workflow4_cluster_pvalues.png"/>
					</div>
				</div>
				Note: The p-values of our generated clusters are significant, while the p-values of random artifacts are non-significant.
				</li>
			</ol>
		</div>
		
		<div id="filter_clusters" class="tutorial_step">
			<h2>Step 6: Filter clusters</h2>
			<ol>
				<li>
				Now, we enable the cluster filter functionality under the "Cluster Operations" tab by setting the filter settings as follows and clicking "Apply Filter":
				<div id="row">
					<div id="col-md-12">
						<asset:image src="filterclusters_1.3.77.png"/>
					</div>
				</div>
				</li>
				<li>
				After clicking "Apply Filter", clusters that are filtered out are visualized with a shaded background and at the bottom of the cluster table: 
				<div id="row">
					<div id="col-md-12">
						<asset:image src="workflow4_clusters_filtered.png"/>
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		
		
		
		<div id="setup_cluster_connectivity" class="tutorial_step">
			<h2>Step 7: Configure and Start a Cluster Connectivity Analysis</h2>
			<ol>
				<li>
				Select all the significant clusters in the cluster table.
				</li>
				<li>
				Click on the Connectivity tab on the Cytoscape results panel.
				<div id="row">
					<div id="col-md-12">
						<asset:image src="workflow4_click_connectivity_tab.png"/>
					</div>
				</div>
				</li>
				<li>
				Now, on the Cytoscape control panel the "New Cluster Connectivity" tab is shown. Set it up as follows:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="workflow4_1.3.77_connectivity_setup.png"/>
						<dl>
							<dt>
							Clustering
							</dt>
							<dd>
							In the top you choose the clustering for which you want to analyze cluster connectivity. Here, we choose the clustering that you just performed.
							</dd>
							<dt>
							Clusters
							</dt>
							<dd>
							In the list you choose all the clusters for which you want to analyze their connectivity. In this tutorial, we choose the clusters that we generated in our toy data set (11, 12, 13).
							</dd>
							<dt>
							Network
							</dt>
							<dd>
							We also have to choose a network which defines how objects of the data set are connected. Choose the example network we imported previously.
							</dd>
							<dt>
							Type of connectivity analysis
							</dt>
							<dd>
							Depending on whether your network is directed or undirected you tick either of these options. TiCoNE implicitely calculates p-values for the cluster connectivities. Here we choose "Undirected" and as permutation function to create background distributions for the p-value calculation we choose "Shuffle Clusters (Cluster Degree Preserving)".
							</dd>
						</dl>
					</div>
				</div>
				</li>
				<li>
				Click on "Analyze Cluster Connectivity".
				</li>
			</ol>
		</div>
		
		
		<div id="connectivity_result" class="tutorial_step">
			<h2>Step 6: Cluster Connectivity Result</h2>
			<ol>
				<li>
				The result of a cluster connectivity analysis is shown in the Connectivity tab of the Cytoscape results panel.
				</li>
				<li>
				The table of a cluster connectivity contains one row for each pair of clusters:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="workflow4_1.3.77_connectivity_result.png"/>
						<dl>
							<dt>
							Cluster 1
							</dt>
							<dd>
							The first cluster of a cluster pair for which connectivity has been analyzed.
							</dd>
							<dt>
							Cluster 2
							</dt>
							<dd>
							The second cluster of a cluster pair for which connectivity has been analyzed.
							</dd>
							<dt>
							EE
							</dt>
							<dd>
							The expected number of edges between the cluster pair.
							</dd>
							<dt>
							E
							</dt>
							<dd>
							The actually present number of edges between the cluster pair.
							</dd>
							<dt>
							OR
							</dt>
							<dd>
							The log2 odds ratio of E and EE.
							</dd>
							<dt>
							p
							</dt>
							<dd>
							An empirical p-value to observe at least this number of edges between clusters with equal node degrees in random networks (after edge cross-over).
							</dd>
						</dl>
					</div>
				</div>
				</li>
			</ol>
		</div>
		
</g:content>
</body>
</html>