<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="preventLayoutResize">true</g:content>
	<g:content tag="sidebarPanelHide">false</g:content>
	<g:content tag="sidebarClass">col-lg-2</g:content>
	<g:content tag="sidebarTitle">Contents</g:content>
	<g:content tag="sidebar">
	<ul>
		<li><a href="#screencasts">Screencasts</a></li>
		<li><a href="#tutorials">Tutorials</a></li>
	</ul>
	</g:content>
	<g:content tag="mainClass">col-lg-9</g:content>
	<g:content tag="title"><h1>Help</h1></g:content>
	 
	<div class="container">
		<div class="row">
			<h2 id="screencasts">Screencasts</h2>
			<div class="col-sm-6">
				<h3>TiCoNE Cytoscape App 1.3 (Basic Introduction)</h3>
				<div class="embed-responsive embed-responsive-4by3">
				  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/X32wiE7kSz4"></iframe>
				</div>
			</div>
			<div class="col-sm-6">
				<h3>TiCoNE WEB</h3>
				<div class="embed-responsive embed-responsive-4by3">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/e077djD6IaE"></iframe>
				</div>
			</div>
			
			<h2><small>Archived Screencasts</small></h2>
			
					<div class="col-sm-3">
						<div class="embed-responsive embed-responsive-4by3">
						  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uR83FNFB6Fw"></iframe>
						</div>
						<h3><small>TiCoNE Cytoscape App 1.2 (Introduction - Short)</small></h3>
					</div>
					<div class="col-sm-3">
						<div class="embed-responsive embed-responsive-4by3">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-qf-JZIF3CM"></iframe>
						</div>
						<h3><small>TiCoNE Cytoscape App 1.2 (Cluster Connectivity & Clustering Comparison)</small></h3>
					</div>
			
					<div class="col-sm-3">
						<div class="embed-responsive embed-responsive-4by3">
						  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/YBqLJzzCSMI"></iframe>
						</div>
						<h3><small>TiCoNE Cytoscape App 1.1 (Short)</small></h3>
					</div>
					<div class="col-sm-3">
						<div class="embed-responsive embed-responsive-4by3">
						  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Tx44-DiYo-I"></iframe>
						</div>
						<h3><small>TiCoNE Cytoscape App 1.1 (Long)</small></h3>
					</div>
		</div>
		
		<div class="row">
			<h2 id="tutorials">Tutorials</h2>
			
			<ul>
				<li><g:link action="show_workflow" id="2">TiCoNE Cytoscape App: Tutorial</g:link></li>
				<li><g:link action="show_workflow" id="4">TiCoNE Cytoscape App: Cluster Connectivity Tutorial</g:link></li>
				<li><g:link action="show_workflow" id="3">TiCoNE WEB: Tutorial</g:link></li>
				<li><g:link action="show_workflow" id="1">TiCoNE WEB: Advanced Tutorial</g:link></li>
			</ul>
			<h2><small>Archived Tutorials</small></h2>
			
			<ul>
				<li><g:link action="show_workflow" id="5">TiCoNE Cytoscape App (v1.2): Clustering Comparison Tutorial</g:link></li>
			</ul>
		</div>
	</div>
	
	
</body>
</html>