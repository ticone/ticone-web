<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="tutorial" />
</head>
<body>
	<a id="#"></a>
	<g:content tag="title">TiCoNE WEB: Advanced Tutorial</g:content>

	<g:content tag="tutorialContent">
	<div id="download" class="tutorial_step">
	    <h2>1. Download example data</h2>
	  	<ol>
		  	<li>Download our example data:
		  	<div>
		  		<ul class="list-unstyled">
				<li><a href="${ assetPath(src: "workflow_1_time_series_data.tsv")}">Download Time-Series Data</a></li>
			 	<li><a href="${ assetPath(src: "workflow_1_network.txt")}">Download Graph Edge File</a></li>
			 	</ul>
			</div>
			</li>
		</ol>
	</div>
	
	<div id="web" class="tutorial_step">
	    <h2>2. Open the ${ grails.util.Holders.config.grails.project.groupId } website</h2>
  		<ol>
		  	<li>Browse to the ${ grails.util.Holders.config.grails.project.groupId } website:
		  		<div>
		  		<g:link controller="iterativeTimeSeriesClustering" action="create">${ createLink(absolute: true, controller: "iterativeTimeSeriesClustering", action: "create") }</g:link>
		  		</div>
		  	</li>
		  	<li>You will be presented with a page to upload/choose the time-series data and network for your clustering, and set some parameters:
		  		<div>
		  		<asset:image src="demo_workflow_1_upload_params.png" />
		  		</div>
		  	</li>
	  	</ol>
	</div>
	
	<div id="upload" class="tutorial_step">
	    <h2>3. Data Upload</h2>
		  	<ol>
			  	<li>Click on "Use your own time-series data ...". You are presented with this form:
			  	<div>
			  	<asset:image src="demo_workflow_1_upload_data.png" />
			  	</div>
			  	</li>
			  	<li>Give the data set a name you can remember (e.g. "Example Time-Series Data") and upload the example time-series data.
			  	<div>
			  	</div>
			  	</li>
			  	<li>Click on "Use your own network ...". You are presented with this form:
			  	<div>
			  	<asset:image src="demo_workflow_1_upload_graph.png" />
			  	</div>
			  	</li>
			  	<li>Give the network a name you can remember (e.g. "Example Network File") and upload the example network file.
			  	<div>
			  	</div>
			  	</li>
		  	</ol>
	</div>
	
	<div id="clust_params" class="tutorial_step">
	    <h2>4. Specify clustering parameters</h2>
		  	<ol>
			  	<li>Now back at the main form, focus on the lower part and configure the parameters as follows:
			  	<div>
			  	<asset:image src="demo_workflow_1_params.png" />
			  	<dl>
			  		<dt>Number of Clusters</dt>
			  		<dd>With the number of clusters parameters you can specify how many clusters you expect and should be returned by the initial clustering.</dd>
			  		<dt>Supplementary Clustering Method</dt>
			  		<dd>The supplementary clustering method is used to identify an initial clustering. Also, if you split a cluster later you can do so by clustering it into sub-cluster using this clustering method.</dd>
			  		<dt>Discretization Steps</dt>
			  		<dd>Our method discretizes your input and the possible patterns, to reduce the search-space and improve our method's performance. With the discretization steps you can specify, which values your input can be discretized to.</dd>
			  		<dt>Similarity Function</dt>
			  		<dd>The similarity function is needed to assess similarities of objects to each other, as well as of objects to patterns.</dd>
			  		<dt>Pattern Refinement Function</dt>
			  		<dd>This function is used to calculate the cluster patterns (a.k.a prototypes) from the assigned objects.</dd>
			  	</dl>
			  	</div>
			  	</li>
			  	<li>Submit your clustering job by pressing "submit"
			  	</li>
		  	</ol>
	</div>
	
	<div id="clust_vis" class="tutorial_step">
	    <h2>5. Clustering visualization</h2>
		  	<ol>
			  	<li>You are now presented with the clusters of the initial clustering. As specified earlier, the clustering consists of three clusters:
				  	<div>
				  	<asset:image src="demo_workflow_1_iteration_one.png" />
				  	</div>
			  	</li>
			  	<li>Let's focus on the left side for now. Here you see the clusters and their assigned objects. In each cluster the cluster prototype is highlighted in bold black. If you click on the Details link of a cluster, you see a larger visualization.
			  	</li>
		  	</ol>
	</div>
	
	<div id="iter1" class="tutorial_step">
	    <h2>6. Iteration 1</h2>
		  	<ol>
		  		<li>To get an overview of the initial clustering, we can click on the "Overview" button. We are then presented with a compressed view of all clusters:
				  	<div>
				  		<asset:image src="demo_workflow_1_cluster_overview_1.png" />
				  	</div>
				</li>
			  	<li>Each of the clusters has several prominent groups of similar objects; and also quite some objects behaving randomly and not similar to any of these groups. In the following, we want to separate the prominent groups and find the ones which are overrepresented.
			  	</li>
				<li>Let's have a closer look at the cluster 1. We can see that there are at least two, probably three groups of similarly behaving objects.
				  	<div>
					  	<div class="row">
						  	<div class="col-md-6">
				  				<asset:image src="demo_workflow_1_pattern_1.png" />
						  	</div>
					  	</div>
				  	</div>
			  	</li>
			  	<li>We split up the cluster 1 into three subclusters: 
			  	<div>
			  		<asset:image src="split_clustering_3.png" />
			  	</div>
			  	<div>
				  	<div class="row">
					  	<div class="col-md-4">
					  		<asset:image src="demo_workflow_1_pattern_4.png" />
					  	</div>
					  	<div class="col-md-4">
					  		<asset:image src="demo_workflow_1_pattern_5.png" />
					  	</div>
					  	<div class="col-md-4">
					  		<asset:image src="demo_workflow_1_pattern_6.png" />
					  	</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>We will keep cluster 5, as they it seems fairly clean apart from some outliers. The cluster prototype (in bold) agrees nicely with the majority of objects in the cluster:
			  	</li>
			  	<li>Cluster 4 seems not to contain well preserved groups and we thus will discard it from the clustering. For now however, we leave it in since some of its objects can maybe be reassigned to other clusters.
			  	</li>
			  	<li>Cluster 6 seems to contain at least two subclusters, which is why we are going to split it in the next iteration.
			  	</li>
		  	</ol>
		</div>
	
	<div id="iter2" class="tutorial_step">
	    <h2>7. Iteration 2</h2>
		  	<ol>
		  		<li>The overview now shows the "old" clusters 2 and 3, as well as the new clusters 4, 5 and 6: 
			  	<div>
			  		<asset:image src="demo_workflow_1_cluster_overview_2.png" />
			  	</div>
			  	</li>
			  	<li>Next, we are splitting up cluster 6 into two subclusters:
			  	<div>
				  	<div class="row">
					  	<div class="col-md-4">
					  		<asset:image src="demo_workflow_1_pattern_7.png " />
					  	</div>
					  	<div class="col-md-4">
					  		<asset:image src="demo_workflow_1_pattern_8.png" />
					  	</div>
				  	</div>
			  	</div> 
			  	</li>
			  	<li>Cluster 8 is very well conserved and we want to keep it.
			  	<li>We will discard cluster 7. For the same reason as above we will keep them in the clustering for now.
			  	</li>
		  	</ol>
		</div>
	
	<div id="iter2" class="tutorial_step">
	    <h2>8. Iteration 3</h2>
		  	<ol>
		  		<li>After iteration 1 (splitting cluster 1) and 2 (splitting cluster 6) we start out with the following clusters: 
			  	<div>
			  		<asset:image src="demo_workflow_1_cluster_overview_3.png" />
			  	</div>
			  	</li>
			  	<li>Also cluster 2 seems to contain at least three prominent subclusters. We therefore split it into 3 clusters:
			  	<div>
				  	<div class="row">
					  	<div class="col-md-4">
					  		<asset:image src="demo_workflow_1_pattern_9.png" />
					  	</div>
					  	<div class="col-md-4">
					  		<asset:image src="demo_workflow_1_pattern_10.png" />
					  	</div>
					  	<div class="col-md-4">
					  		<asset:image src="demo_workflow_1_pattern_11.png" />
					  	</div>
				  	</div>
			  	</div>
			  	</li>
		  	</ol>
	</div>
	
	<div id="iter3" class="tutorial_step">
	    <h2>9. Iteration 4</h2>
		  	<ol>
		  		<li>The overview: 
			  	<div>
			  		<asset:image src="demo_workflow_1_cluster_overview_4.png" />
			  	</div>
			  	</li>
			  	<li>We will deal with cluster 3 in the same way as with clusters 1 and 2 before:
			  	<div>
				  	<div class="row">
					  	<div class="col-md-6">
			  				<asset:image src="demo_workflow_1_pattern_3.png" />
			  			</div>
			  		</div>
			  	</div>
			  	</li>
			  	<li>Clusters 12, 13 and 14 emerge from splitting cluster 3:
			  	<div>
				  	<div class="row">
					  	<div class="col-md-4">
					  		<asset:image src="demo_workflow_1_pattern_12.png" />
					  	</div>
					  	<div class="col-md-4">
					  		<asset:image src="demo_workflow_1_pattern_13.png" />
					  	</div>
					  	<div class="col-md-4">
					  		<asset:image src="demo_workflow_1_pattern_14.png" />
					  	</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>Cluster 13 and 14 do not seem interesting. Cluster 12 may be after we split it up into two subclusters.
			  	</li>
		  	</ol>
	</div>
	
	<div id="iter4" class="tutorial_step">
	    <h2>10. Iteration 5</h2>
		  	<ol>
			  	<li>We split cluster 12 into two subclusters:
			  	<div>
				  	<div class="row">
					  	<div class="col-md-6">
			  				<asset:image src="demo_workflow_1_pattern_15.png" />
			  			</div>
					  	<div class="col-md-6">
			  				<asset:image src="demo_workflow_1_pattern_16.png" />
			  			</div>
			  		</div>
			  	</div>
			  	</li>
			  	<li>They do not contain many objects and the objects are not very well conserved. We therefore discard them as well.
			  	</li>
		  	</ol>
	</div>
	
	<div id="iter5" class="tutorial_step">
	    <h2>11. Iteration 6</h2>
		  	<ol>
			  	<li>The cluster overview:
			  	<div>
			  		<asset:image src="demo_workflow_1_cluster_overview_6.png" />
			  	</div>
			  	</li>
			  	<li>We now have split all clusters enough and want to use our method to optimize the clusters by iteratively reassigning objects to the most similar cluster prototype and then recalculating the prototypes. 
			  	<li>To do so, we click on the drop-down arrow of the "Do Iteration" button and choose "Until Convergence":
			  	<div>
			  		<asset:image src="do_iteration_until_convergence.png" />
			  	</div>
			  	</li>
			  	<li>The method will iteratively reassign objects to the closest prototype and then recalculate the prototypes based on the (possibly) changed cluster objects.</li>
			  	<li>It stops when the clustering does not change anymore, i.e. the clusterings of two steps are the same</li>
		  	</ol>
	</div>
	
	<div id="iter6_9" class="tutorial_step">
	    <h2>12. Iteration 7-10 - Optimization Step (1-4)</h2>
		  	<ol>
			  	<li>The clusters after 4 optimization steps:
			  	<div>
				  	<div class="row">
					  	<div class="col-md-6">
					  		<h5>Optimization Step 1</h5>
				  			<asset:image src="demo_workflow_1_cluster_overview_7.png" />
				  		</div>
					  	<div class="col-md-6">
					  		<h5>Optimization Step 2</h5>
				  			<asset:image src="demo_workflow_1_cluster_overview_8.png" />
				  		</div>
				  	</div>
				  	<div class="row">
					  	<div class="col-md-6">
					  		<h5>Optimization Step 3</h5>
				  			<asset:image src="demo_workflow_1_cluster_overview_9.png" />
				  		</div>
					  	<div class="col-md-6">
					  		<h5>Optimization Step 4</h5>
				  			<asset:image src="demo_workflow_1_cluster_overview_10.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>In summary, the clusters did not change dramatically. Especially not the clusters, which only contained one clean, enriched group of objects.</li>
			  	<li>All object which were similar to these were now reassigned, which is why we can assume that the other clusters do not contain interesting objects. We will now continue with deleting these clusters. 
		  	</ol>
	</div>
	
	<div id="iter10_16" class="tutorial_step">
	    <h2>13. Iterations 11-18 - Deleting uninteresting clusters</h2>
		  	<ol>
			  	<li>Now we delete the clusters we are not interested in (too much differences between the cluster objects or not enough objects), i.e. clusters 4, 7, 9, 11, 13, 14, 15 and 16.
			  	</li>
			  	<li>We do so by clicking on the Details button of each pattern, clicking on "Delete ..." and then "Delete Pattern & Objects"</li>
			  	<li>We are then left with clusters 5, 8 and 10:
			  	<div>
				  	<div class="row">
				  			<asset:image src="demo_workflow_1_cluster_overview_11.png" />
				  	</div>
			  	</div>
			  	</li>
			  	<li>They still contain some objects which do not perfectly follow the behaviour of the cluster.</li>
			  	<li>That is also the reason, why the prototype of cluster 8 is not following the prominent group very well.</li>
			  	</li>
		  	</ol>
	</div>
	
	<div id="iter17" class="tutorial_step">
	    <h2>14. Iteration 19 - Removing least similar objects</h2>
		  	<ol>
			  	<li>We use the "Show/Remove Least Similar Objects" to remove these non-fitting objects from the clusters:
			  	<div>
			  		<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_20_least_fitting_object.png" />
				  		</div>
				  	</div>
				</div>
				</li>
				<li>By clicking on "Delete", we delete these objects from the clustering. The resulting clusters look much cleaner: 
				<div>
			  	  	<div class="row">
				  			<asset:image src="demo_workflow_1_cluster_overview_12.png" />
				  	</div>
			  	</div>
			  	</li>
			  	<li>After deleting objects the prototypes are not recalculated. This is why the prototype of cluster 8 is not perfectly matching the objects. 
				</li>
		  	</ol>
	</div>
	
	<div id="iter18" class="tutorial_step">
	    <h2>15. Iteration 19 - The final clusters</h2>
		  	<ol>
			  	<li>By clicking on the "Do Iteration" button the objects are assigned to their closest cluster (here already given) and the prototypes recalculated afterwards.  
				</li>
				<li>Executing another iteration leads to the following (now final) clusters: 
				<div>
			  	  	<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_pattern_5_final.png" />
				  		</div>
			  	  	<div class="row">
				  	</div>
					  	<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_pattern_8_final.png" />
				  		</div>
			  	  	<div class="row">
				  	</div>
					  	<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_pattern_10_final.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
		  	</ol>
	</div>
	
	<div id="layout" class="tutorial_step">
	    <h2>15. Network Layout</h2>
		  	<ol>
			  	<li>So far we have only looked at the clustering and clusters.</li>
			  	<li>Now we use the network visualization on the right hand side of the window.</li>
			  	<li>At the moment the network looks something like this, because no layouting has been performed:
				<div>
			  	  	<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_graph_no_layout.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>We can layout the graph in two ways:</li>
			  	<ol>
			  		<li>A fast (but not very clear) circle layout. All nodes are placed on a circle.</li>
			  		<li>A slower force-directed layout, which places tightly connected node groups together. This layout will first perform the circle layout, to then run the force-layouter.</li>
			  	</ol>
			  	<li>We will now perform the force layout. The network will always look a little different, but somehow close to this:</li>
				<div>
			  	  	<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_graph_layout.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>We save the layout by clicking "Save Layout".</li>
		  	</ol>
	</div>
	
	<div id="net_clust_vis" class="tutorial_step">
	    <h2>16. Network Clustering Visualization</h2>
		  	<ol>
			  	<li>We can now visualize the clustering of each iteration on the network.</li>
			  	<li>We will start by checking out the clustering of iteration 1.</li>
			  	<li>To do so, we choose "1) Initial Iteration" in the iteration drop-down box:</li>
				<div>
			  	  	<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_choose_iteration_1.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>Whenever we now change to another iteration, the network layout will be used that we saved earlier.</li>
			  	<li>By default only the objects of the first cluster are highlighted on the network.</li>
			  	<li>We visualize all of them, by selecting the "Visualize All" radio button:</li>
			  	<div>
			  	  	<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_visualize_all.png" />
				  		</div>
				  	</div>
			  	</div>
				<div>
			  	  	<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_graph_iteration_1_all.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>The network visualization of the patterns is quite chaotic.</li>
		  	</ol>
	</div>
	
	<div id="net_clust_vis_2" class="tutorial_step">
	    <h2>17. Network Clustering Visualization #2</h2>
		  	<ol>
			  	<li>We will now demonstrate, how the network visualization changes over the iterations we have executed previously.
				<div>
			  	  	<div class="row">
					  	<div class="col-md-4">
					  		<h5>Iteration 1</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_1_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 2</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_2_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 3</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_3_all.png" />
				  		</div>
				  	</div>
			  	  	<div class="row">
					  	<div class="col-md-4">
					  		<h5>Iteration 4</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_4_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 5</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_5_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 6</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_6_all.png" />
				  		</div>
				  	</div>
			  	  	<div class="row">
					  	<div class="col-md-4">
					  		<h5>Iteration 7</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_7_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 8</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_8_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 9</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_9_all.png" />
				  		</div>
				  	</div>
			  	  	<div class="row">
					  	<div class="col-md-4">
					  		<h5>Iteration 10</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_10_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 11</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_11_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 12</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_12_all.png" />
				  		</div>
				  	</div>
			  	  	<div class="row">
					  	<div class="col-md-4">
					  		<h5>Iteration 13</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_13_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 14</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_14_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 15</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_15_all.png" />
				  		</div>
				  	</div>
			  	  	<div class="row">
					  	<div class="col-md-4">
					  		<h5>Iteration 16</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_16_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 17</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_17_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 18</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_18_all.png" />
				  		</div>
				  	</div>
			  	  	<div class="row">
					  	<div class="col-md-4">
					  		<h5>Iteration 19</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_19_all.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 20</h5>
				  			<asset:image src="demo_workflow_1_graph_iteration_20_all.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	
		  	</ol>
	</div>
	
	<div id="kpm" class="tutorial_step">
	    <h2>18. Network Enrichment with KeyPathwayMiner</h2>
		  	<ol>
		  		<li>The network after the final iteration looks as follows:
			  	  	<div class="row">
				  		<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_graph_iteration_20_all.png" />
				  		</div>
				  	</div>
			  	</li>
			  	<li>We can clearly identify a network enrichment of the clusters by eye inspection.</li>
			  	<li>However, we can also use Keypathwayminer to automate this task. Keypathwayminer (KPM) can be used to identify the maximal subgraphs only consisting of nodes we select (e.g. of one cluster) with some exception nodes (the k parameter).</li>
			  	<li>We will now submit our cluster 5 to KPM.</li>
			  	<li>First we mark the "KPM" checkbox of cluster 5:
			  	<div>
		  			<asset:image src="kpm_checkbox.png" />
				</div>
			  	</li>
			  	<li>Next we click on the "Network Enrichment" button:
			  	<div>
		  			<asset:image src="kpm_btn.png" />
				</div>
			  	</li>
			  	<li>A popup will show up in which you have to set parameters for KPM. Set them as follows:
			  	<div>
		  			<asset:image src="kpm_popup.png" />
		  			<dl>
		  				<dt>Number exception nodes (k)</dt>
		  				<dd>The number of nodes in the identified subgraph, that do not belong to the selected clusters (i.e. here cluster 5)</dd>
		  				<dt>Number pathways</dt>
		  				<dd>The maximal number of different subgraphs (of the same maximal size), that KPM should find.</dd>
		  			</dl>
				</div>
			  	</li>
			  	<li>The data is now sent to the KPM web service where it will be processed. A status message will tell you, as soon as the KPM calculation is finished. You can then continue to the KPM website and you will be presented with a list and a visualization of the identified subgraphs:
			  	<div>
		  			<asset:image src="kpm_results.png" />
				</div>
			  	</li>
		  	</ol>
	</div>
	</g:content>
</body>
</html>