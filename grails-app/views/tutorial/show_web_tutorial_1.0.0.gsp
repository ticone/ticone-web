<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="tutorial" />
</head>
<body>
	<a id="#"></a>
	<g:content tag="title">TiCoNE WEB: Tutorial</g:content>

	<g:content tag="tutorialContent">	
	<div id="download" class="tutorial_step">
	    <h2>1. Download example data</h2>
	  	<ol>
		  	<li>Download our example data:
		  	<div>
		  		<ul class="list-unstyled">
				<li><a href="${ assetPath(src: "workflow_1_time_series_data.tsv")}">Download Time-Series Data</a></li>
			 	<li><a href="${ assetPath(src: "workflow_1_network.txt")}">Download Graph Edge File</a></li>
			 	</ul>
			</div>
			</li>
		</ol>
	</div>
	
	<div id="web" class="tutorial_step">
	    <h2>2. Open the ${ grails.util.Holders.config.grails.project.groupId } website</h2>
  		<ol>
		  	<li>Browse to the ${ grails.util.Holders.config.grails.project.groupId } website:
		  		<div>
		  		<g:link controller="iterativeTimeSeriesClustering" action="create">${ createLink(absolute: true, controller: "iterativeTimeSeriesClustering", action: "create") }</g:link>
		  		</div>
		  	</li>
		  	<li>You will be presented with a page to upload/choose the time-series data and network for your clustering, and set some parameters:
		  		<div>
		  		<asset:image src="ticone_web_tutorial_upload_params.png" />
		  		</div>
		  	</li>
	  	</ol>
	</div>
	
	<div id="upload" class="tutorial_step">
	    <h2>3. Data Upload</h2>
		  	<ol>
			  	<li>Click on "Use your own time-series data ...". You are presented with this form:
			  	<div>
			  	<asset:image src="demo_workflow_1_upload_data.png" />
			  	</div>
			  	</li>
			  	<li>Give the data set a name you can remember (e.g. "Easy Time-Series Data") and upload the example time-series data.
			  	<div>
			  	</div>
			  	</li>
			  	<li>Click on "Use your own network ...". You are presented with this form:
			  	<div>
			  	<asset:image src="demo_workflow_1_upload_graph.png" />
			  	</div>
			  	</li>
			  	<li>Give the network a name you can remember (e.g. "Easy Network File") and upload the example network file.
			  	<div>
			  	</div>
			  	</li>
		  	</ol>
	</div>
	
	<div id="clust_params" class="tutorial_step">
	    <h2>4. Specify clustering parameters</h2>
		  	<ol>
			  	<li>Now back at the main form, focus on the lower part and configure the parameters as follows:
			  	<div>
			  	<asset:image src="ticone_web_tutorial_params.png" />
			  	<dl>
			  		<dt>Number of Clusters</dt>
			  		<dd>With the number of clusters parameters you can specify how many clusters you expect and should be returned by the initial clustering.</dd>
			  		<dt>Supplementary Clustering Method</dt>
			  		<dd>The supplementary clustering method is used to identify an initial clustering. Also, if you split a cluster later you can do so by clustering it into sub-cluster using this clustering method.</dd>
			  		<dt>Discretization Steps</dt>
			  		<dd>Our method discretizes your input and the possible patterns, to reduce the search-space and improve our method's performance. With the discretization steps you can specify, which values your input can be discretized to.</dd>
			  		<dt>Similarity Function</dt>
			  		<dd>The similarity function is needed to assess similarities of objects to each other, as well as of objects to patterns.</dd>
			  		<dt>Pattern Refinement Function</dt>
			  		<dd>This function is used to calculate the cluster patterns (a.k.a prototypes) from the assigned objects.</dd>
			  	</dl>
			  	</div>
			  	</li>
			  	<li>Submit your clustering job by pressing "submit"
			  	</li>
		  	</ol>
	</div>
	
	<div id="clust_vis" class="tutorial_step">
	    <h2>5. Clustering visualization</h2>
		  	<ol>
			  	<li>You are now presented with the clusters of the initial clustering. As specified earlier, the clustering consists of ten clusters:
				  	<div>
				  	<asset:image src="ticone_web_tutorial_iteration_one.png" />
				  	</div>
			  	</li>
			  	<li>Let's focus on the left side for now. Here you see the clusters and their assigned objects. In each cluster the cluster prototype is highlighted in bold black. If you click on the Details link of a cluster, you see a larger visualization.
			  	</li>
		  	</ol>
	</div>
	
	<div id="iter1" class="tutorial_step">
	    <h2>6. Iteration 1 - Initial Iteration & Clustering</h2>
		  	<ol>
		  		<li>To get an overview of the initial clustering, we can click on the "Overview" button. We are then presented with a compressed view of all clusters:
				  	<div>
				  		<asset:image src="ticone_web_tutorial_cluster_overview_1.png" />
				  	</div>
				</li>
			  	<li>In the following, we want to demonstrate the most important features of TiCoNE.</li>
			  	<li>Each of the clusters has at least one prominent group of similar objects; and also quite some objects behaving randomly and not similar to any of these groups.</li>
			  	<li>Furthermore, some of the clusters are very similar. For example, clusters 2, 4 and 10.
				  	<div>
					  	<div class="row">
						  	<div class="col-md-4">
						  		<asset:image src="ticone_web_tutorial_pattern_2.png" />
						  	</div>
						  	<div class="col-md-4">
						  		<asset:image src="ticone_web_tutorial_pattern_4.png" />
						  	</div>
						  	<div class="col-md-4">
						  		<asset:image src="ticone_web_tutorial_pattern_10.png" />
						  	</div>
					  	</div>
				  	</div>
			  	</li>
			  	<li>We are going to merge these clusters, by checking their "Merge" checkboxes and clickin on the "Merge Clusters" button.</li>
		  	</ol>
		</div>
	
	<div id="iter2" class="tutorial_step">
	    <h2>7. Iteration 2 - Split a Cluster Based on a Clustering into Subclusters</h2>
		  	<ol>
		  		<li>The overview now shows 8 clusters: 
			  	<div>
			  		<asset:image src="ticone_web_tutorial_cluster_overview_2.png" />
			  	</div>
			  	</li>
			  	<li>Next, we have a look at cluster 3: 
				  	<div>
				  		<asset:image src="ticone_web_tutorial_pattern_3.png" />
				  	</div>
			  	</li>
			  	<li>It seems that it contains 2 groups: One group with a peak at time point 1, and another group with a peak at time point 2.</li>
			  	<li>Therefore, we are going to split up cluster 2 by clustering it into 2 subclusters. We do so as follows:</li>
			  	<li>Open the Details popup for cluster 2</li>
			  	<li>Click on the "Split Cluster ..." button</li>
			  	<li>Click on "Split Based On Clustering ..."</li>
			  	<li>Choose 2 clusters and hit the button: 
				  	<div>
					  	<div class="row">
						  	<div class="col-md-12">
						  		<asset:image src="ticone_web_tutorial_split_clustering_2.png " />
						  	</div>
					  	</div>
				  	</div> 
			  	</li>
		  	</ol>
		</div>
	
	<div id="iter2" class="tutorial_step">
	    <h2>8. Iteration 3 - Split a Cluster Based on the Two Most Dissimilar Objects</h2>
		  	<ol>
		  		<li>After splitting cluster 2, we have the following clusters: 
			  	<div>
			  		<asset:image src="ticone_web_tutorial_cluster_overview_3.png" />
			  	</div>
			  	</li>
			  	<li>Next, we are going to split cluster 6: 
			  	<div>
			  		<asset:image src="ticone_web_tutorial_pattern_6.png" />
			  	</div>
			  	</li>
			  	<li>This time, we split the cluster based on the two most dissimilar objects of the cluster.
			  	</li>
			  	<li>We do so by opening the cluster 2 details popup</li>
			  	<li>Clicking on "Split Cluster ..."</li>
			  	<li>And choosing "Split Based On Two Most Dissimilar Objects"
			  	<div>
				  	<div class="row">
					  	<div class="col-md-12">
					  		<asset:image src="ticone_web_tutorial_split_cluster_choices.png" />
					  	</div>
				  	</div>
			  	</div></li>
		  	</ol>
	</div>
	
	<div id="iter3" class="tutorial_step">
	    <h2>9. Iteration 4 - Clean Up & Remove Least Similiar Objects</h2>
		  	<ol>
		  		<li>The overview: 
			  	<div>
			  		<asset:image src="ticone_web_tutorial_cluster_overview_4.png" />
			  	</div>
			  	</li>
			  	<li>Now we will clean up the whole data set a little by removing the objects that are least similar to their prototype.</li>
			  	<li>To that end, we click on the clean up button:
			  	<div>
				  	<div class="row">
					  	<div class="col-md-12">
			  				<asset:image src="ticone_web_tutorial_clean_up.png" />
			  			</div>
			  		</div>
			  	</div>
			  	</li>
			  	<li>And we will be presented with the following popup:
			  	<div>
				  	<div class="row">
					  	<div class="col-md-12">
			  				<asset:image src="ticone_web_tutorial_clean_up_popup.png" />
			  			</div>
			  		</div>
			  	</div>
			  	</li>
			  	<li>Here we will choose 20% and click on "Show Least Similar Object Sets" 
			  	<div>
				  	<div class="row">
					  	<div class="col-md-12">
			  				<asset:image src="ticone_web_tutorial_least_similar_20.png" />
			  			</div>
			  		</div>
			  	</div>
			  	</li>
			  	<li>We then click on "Delete"</li>
		  	</ol>
	</div>
	
	<div id="iter4" class="tutorial_step">
	    <h2>10. Iteration 5 - Deleting uninteresting clusters</h2>
		  	<ol>
			  	<li>After cleaning up we have the following clusters:
			  	<div>
				  	<div class="row">
					  	<div class="col-md-12">
			  				<asset:image src="ticone_web_tutorial_cluster_overview_5.png" />
			  			</div>
			  		</div>
			  	</div>
			  	</li>
			  	<li>Now we are going to delete cluster 12 including its objects.</li>
			  	<li>We do so, by opening the details popup of cluster 12</li>
			  	<li>Clicking on "Delete ..."</li>
			  	<li>And choosing "Delete Cluster & Objects":
			  	<div>
				  	<div class="row">
					  	<div class="col-md-12">
			  				<asset:image src="ticone_web_tutorial_delete_choices.png" />
			  			</div>
			  		</div>
			  	</div>
			  	</li>
		  	</ol>
	</div>
	
	<div id="iter5" class="tutorial_step">
	    <h2>11. Iteration 6</h2>
		  	<ol>
			  	<li>The cluster overview:
			  	<div>
			  		<asset:image src="ticone_web_tutorial_cluster_overview_6.png" />
			  	</div>
			  	</li>
			  	<li>We now want to use TiCoNE's iterative procedure to optimize the clusters by iteratively reassigning objects to the most similar cluster prototype and then recalculating the prototypes. 
			  	<li>To do so, we click on the drop-down arrow of the "Do Iteration" button and choose "Until Convergence":
			  	<div>
			  		<asset:image src="do_iteration_until_convergence.png" />
			  	</div>
			  	</li>
			  	<li>The method will iteratively reassign objects to the closest prototype and then recalculate the prototypes based on the (possibly) changed cluster objects.</li>
			  	<li>It stops when the clustering does not change anymore, i.e. the clusterings of two steps are the same</li>
		  	</ol>
	</div>
	
	<div id="iter6_9" class="tutorial_step">
	    <h2>12. Iteration 7-8 - Optimization Step (1-2)</h2>
		  	<ol>
			  	<li>The clusters after 2 optimization steps:
			  	<div>
				  	<div class="row">
					  	<div class="col-md-6">
					  		<h5>Optimization Step 1</h5>
				  			<asset:image src="ticone_web_tutorial_cluster_overview_7.png" />
				  		</div>
					  	<div class="col-md-6">
					  		<h5>Optimization Step 2</h5>
				  			<asset:image src="ticone_web_tutorial_cluster_overview_8.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>The clusters did not change at all. This is, because the cluster-object assignment was already very good before.</li>
			  	<li>Now we can be sure, that all objects are assigned to the most similar prototype, and the prototypes represent the groups of the data set well. 
		  	</ol>
	</div>
	
	<div id="layout" class="tutorial_step">
	    <h2>13. Network Layout</h2>
		  	<ol>
			  	<li>So far we have only looked at the clustering and clusters.</li>
			  	<li>Now we use the network visualization on the right hand side of the window.</li>
			  	<li>At the moment the network looks something like this, because no layouting has been performed:
				<div>
			  	  	<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_graph_no_layout.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>We can layout the graph in two ways:</li>
			  	<ol>
			  		<li>A fast (but not very clear) circle layout. All nodes are placed on a circle.</li>
			  		<li>A slower force-directed layout, which places tightly connected node groups together. This layout will first perform the circle layout, to then run the force-layouter.</li>
			  	</ol>
			  	<li>We will now perform the force layout. The network will always look a little different, but somehow close to this:</li>
				<div>
			  	  	<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="ticone_web_tutorial_graph_layout.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>We save the layout by clicking "Save Layout".</li>
		  	</ol>
	</div>
	
	<div id="net_clust_vis" class="tutorial_step">
	    <h2>14. Network Clustering Visualization</h2>
		  	<ol>
			  	<li>We can now visualize the clustering of each iteration on the network.</li>
			  	<li>We will start by checking out the clustering of iteration 1.</li>
			  	<li>To do so, we choose "1) Initial Iteration" in the iteration drop-down box:</li>
				<div>
			  	  	<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="ticone_web_tutorial_choose_iteration.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>Whenever we now change to another iteration, the network layout will be used that we saved earlier.</li>
			  	<li>By default only the objects of the first cluster are highlighted on the network.</li>
			  	<li>We visualize all of them, by selecting the "Visualize All" radio button:</li>
			  	<div>
			  	  	<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="demo_workflow_1_visualize_all.png" />
				  		</div>
				  	</div>
			  	</div>
				<div>
			  	  	<div class="row">
					  	<div class="col-md-12">
				  			<asset:image src="ticone_web_tutorial_graph_1.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	<li>Already here we can see some enrichment of the clusters in the network.</li>
		  	</ol>
	</div>
	
	<div id="net_clust_vis_2" class="tutorial_step">
	    <h2>15. Network Clustering Visualization #2</h2>
		  	<ol>
			  	<li>We will now demonstrate, how the network visualization changes over the iterations we have executed previously.
				<div>
			  	  	<div class="row">
					  	<div class="col-md-4">
					  		<h5>Iteration 1</h5>
				  			<asset:image src="ticone_web_tutorial_graph_1.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 2</h5>
				  			<asset:image src="ticone_web_tutorial_graph_2.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 3</h5>
				  			<asset:image src="ticone_web_tutorial_graph_3.png" />
				  		</div>
				  	</div>
			  	  	<div class="row">
					  	<div class="col-md-4">
					  		<h5>Iteration 4</h5>
				  			<asset:image src="ticone_web_tutorial_graph_4.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 5</h5>
				  			<asset:image src="ticone_web_tutorial_graph_5.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 6</h5>
				  			<asset:image src="ticone_web_tutorial_graph_6.png" />
				  		</div>
				  	</div>
			  	  	<div class="row">
					  	<div class="col-md-4">
					  		<h5>Iteration 7</h5>
				  			<asset:image src="ticone_web_tutorial_graph_7.png" />
				  		</div>
					  	<div class="col-md-4">
					  		<h5>Iteration 8</h5>
				  			<asset:image src="ticone_web_tutorial_graph_8.png" />
				  		</div>
				  	</div>
			  	</div>
			  	</li>
			  	
		  	</ol>
	</div>
	
	<div id="kpm" class="tutorial_step">
	    <h2>16. Network Enrichment with KeyPathwayMiner</h2>
		  	<ol>
		  		<li>The network after the final iteration looks as follows:
			  	  	<div class="row">
				  		<div class="col-md-12">
				  			<asset:image src="ticone_web_tutorial_graph_8.png" />
				  		</div>
				  	</div>
			  	</li>
			  	<li>We can clearly identify a network enrichment of the clusters by eye inspection.</li>
			  	<li>However, we can also use Keypathwayminer to automate this task. Keypathwayminer (KPM) can be used to identify the maximal subgraphs only consisting of nodes we select (e.g. of one cluster) with some exception nodes (the k parameter).</li>
			  	<li>We will now submit our cluster 11 to KPM.</li>
			  	<li>First we mark the "KPM" checkbox of cluster 11:
			  	<div>
		  			<asset:image src="ticone_web_tutorial_kpm_cluster_11.png" />
				</div>
			  	</li>
			  	<li>Next we click on the "Network Enrichment" button:
			  	<div>
		  			<asset:image src="kpm_btn.png" />
				</div>
			  	</li>
			  	<li>A popup will show up in which you have to set parameters for KPM. Set them as follows:
			  	<div>
		  			<asset:image src="kpm_popup.png" />
		  			<dl>
		  				<dt>Number exception nodes (k)</dt>
		  				<dd>The number of nodes in the identified subgraph, that do not belong to the selected clusters (i.e. here cluster 11)</dd>
		  				<dt>Number pathways</dt>
		  				<dd>The maximal number of different subgraphs (of the same maximal size), that KPM should find.</dd>
		  			</dl>
				</div>
			  	</li>
			  	<li>The data is now sent to the KPM web service where it will be processed. A status message will tell you, as soon as the KPM calculation is finished. You can then continue to the KPM website and you will be presented with a list and a visualization of the identified subgraphs:
			  	<div>
		  			<asset:image src="ticone_web_tutorial_kpm_results.png" />
				</div>
			  	</li>
			  	<li>We can visualize the union network of all subnetworks that KPM identified, by clicking on the "Union Network" button:
			  	<div>
		  			<asset:image src="ticone_web_tutorial_kpm_results_union.png" />
				</div>
			  	</li>
		  	</ol>
	</div>
	</g:content>
</body>
</html>