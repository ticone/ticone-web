<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="tutorial" />
</head>
<body>
	<a id="#"></a>
	<g:content tag="title">TiCoNE Cytoscape App: Tutorial</g:content>
	
	<g:content tag="tutorialContent">
	
		<div id="installation" class="tutorial_step">
			<h2>Step 1: Installation</h2>
			<ol>
				<li>
				Download and install Cytoscape. Please make sure you have Java 1.6+ installed and configured.
				</li>
				<li>
				To install the TiCoNE plug-in with Cytoscape 3 and above, use the Cytoscape App Manager: "Apps &#8594; App Manager &#8594; Install Apps" and select TiCoNE.
				</li>
				<li>
				Once the plugin is installed, it should appear in the Apps menu as "TiCoNE".
				</li>
				<li>
				Further it should appear as "TiCoNE" under Control Panel as seen below:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="control_panel_1.3.77.png" />
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		
		<div id="import_network" class="tutorial_step">
			<h2>Step 2: Import network</h2>
			<ol>
				<li>Download our <a href="${ assetPath(src: "cytoscape_toy_network.txt")}">example network</a></li>
				<li>
				Click on File &#8594; Import &#8594; Network.
				</li>
				<li>
				Select our network file
				</li>
				<li>
				Choose "Source" and "Target" interaction:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="load_network_cytoscape_3.4.png"/>
					</div>
				</div>
				<li>
				In the advanced options, do not use the first line of the input file as column names:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="load_network_cytoscape_3.4_advanced.png"/>
					</div>
				</div>
				<li>
				Once the network is loaded, Cytoscape should automatically apply the preferred layout to get a better representation of the network. If this is not done automatically, do it manually under "Layout &#8594; Apply Preferred Layout".
				</li>
				<li>
				The network should now be visualized similar to this:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="network_cytoscape_3.5.png" />
					</div>
				</div>
				</li>
			</ol>
		</div>
		<div id="import_dataset" class="tutorial_step">
			<h2>Step 3: Import dataset</h2>
			<ol>
				<li>Download our <a href="${ assetPath(src: "cytoscape_toy_dataset.txt")}">example data set</a></li>
				<li>
				Click on File &#8594; Import &#8594; Table.
				</li>
				<li>
				Select our dataset file
				</li>
				<li>
				Load it to unassigned tables, and give it a name such that you can easily identify it later:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="load_table_cytoscape_3.5.png"/>
					</div>
				</div>
				</li>
				<li>
				In the advanced options, do not use the first line of the input file as column names:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="load_table_cytoscape_3.5_advanced.png"/>
					</div>
				</div>
				</li>
				<li>
				Once the table is loaded, you can select it with the table selector:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="table_selector_1.3.77.png" />
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		<div id="setup_input" class="tutorial_step">
		
			<h2>Step 4: Setup the input</h2>
		
			<ol>
				<li>Use the values as shown on the following picture and leave the remaining settings at their defaults:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="settings_1.3.77.png" />
						<dl>
							<dt>
							Discretization
							</dt>
							<dd>
							This setting determines how cluster prototypes are discretized. You have to select how many discretization steps you want from 0 to the maximum value of your input dataset (if te maximum values is above 0), and how many discretization steps you want from 0 to the minimum values of your input dataset (If the minimum value is below 0).
							</dd>
							<dt>
							Initial clustering
							</dt>
							<dd>
							In the first iteration TiCoNE uses an existing clustering method to calculate the initial clustering. This clustering can then be refined in later iterations. Here you have to specify the number of clusters you want in the beginning.
							</dd>
						</dl>
					</div>
				</div>
				</li>
				<li>
				Click "Start" to start the clustering.
				</li>
			</ol>
		</div>
		
		
		
		<div id="clustering_visualization" class="tutorial_step">
			<h2>Step 5: View and analyze results</h2>
			<ol>
				<li>
				Now under the Clusters tab you can view your found clusters, and they will be visualized as below:
				<div class="row">
					<div class="col-md-12">
						<asset:image src="graphs_1.3.77.png" />
					</div>
				</div>
				</li>
				<li>
				Each row contains information about one cluster:
				<ol type="1">
					<li>
					A checkbox to select clusters when performing further operations, such as merging of clusters or network enrichment.
					</li>
					<li>
					Cluster ID
					</li>
					<li>
					Number of objects assigned to the cluster
					</li>
					<li>
					The average Pearson correlation of cluster objects and cluster prototype
					</li>
					<li>
					The average residual sum of squares (RSS) of cluster objects and cluster prototype
					</li>
					<li>
					A cluster p-value indicating the fit of of the objects to their cluster.
					</li>
					<li>
					A graph visualizing the cluster, its prototype and its objects.
					</li>
				</ol>
				</li>
				<li>
				Note: Since TiCoNE version 1.3.77 p-values are not automatically calculated anymore for each clustering due to performance reasons.
				</li>
			</ol>
		</div>
		
		
		
		<div id="iterations" class="tutorial_step">
			<h2>Step 6: Iterations</h2>
			<ol>
				<li>
				TiCoNE is based on an iterative optimization procedure to improve a clustering. One can run 1 optimization iteration at a time, or run iterations until convergence, by pressing either the "Do Iteration" or the "Until convergence" button on the Cluster Operations panel". The latter implies that iterations are performed until no change occur in the assignment of objects to clusters (with an upper bound of 100 iterations).
				</li>
				<li>
				Here, we perform iterations "Until Convergence". This will execute 9 more iterations:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="perform_iterations_1.3.77.png"/>
					</div>
				</div>
				</li>
				<li>
				Afterwards, the clusters should look as follows:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="lucky_iterations_1.3.77.png"/>
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		
		<div id="clusterPvalues" class="tutorial_step">
			<h2>Step 7: Cluster p-values</h2>
			<ol>
				<li>
				Next, we calculate p-values for our clusters:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="calculate_cluster_pvalues_1.3.77.png"/>
					</div>
				</div>
				</li>
				<li>
				After a short while, the cluster p-values are shown next to the cluster graphs (these values differ each time due to the random nature of empirical p-values):
				<div id="row">
					<div id="col-md-12">
						<asset:image src="lucky_iterations_1.3.77_with_pvals.png"/>
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		
		<div id="delete_action" class="tutorial_step">
			<h2>Step 8: Delete least-fitting objects</h2>
			<ol>
				<li>
				In order to clean up a clustering, TiCoNE provides several operations that can be applied to clusters.
				</li>
				<li>
				Here, we examplarily delete the least-fitting objects from cluster 7. To this end, we click on the "Delete" button under the graph of cluster 7: 
				<div id="row">
					<div id="col-md-12">
						<asset:image src="cluster_delete_1.3.77.png"/>
					</div>
				</div>
				</li>
				<li>
				A dialog will ask us, what we want to delete. Here we choose "Least fitting objects":
					<div id="row">
					<div id="col-md-12">
						<asset:image src="delete_pattern_options.png"/>
					</div>
				</div>
				</li>
				<li>
				Now, we are presented with the ten percent least-fitting objects of cluster 7:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="cluster_delete_leastfitting_1.3.77.png"/>
					</div>
				</div> 
				</li>
				<li>
				We leave the options at their defaults and click on "Delete objects". 
				</li>
				<li>
				The objects are now completely removed from cluster 7 and also the clustering. Thus, these objects will also not be reconsidered in later iterations.
				</li>
			</ol>
		</div>
		
		
		<div id="split_pattern" class="tutorial_step">
			<h2>Step 9: Split clusters</h2>
			<ol>
				<li>
				Clusters can be split into multiple new clusters. This makes sense if it is apparent that a cluster contains objects that follow multiple distinct time patterns.
				</li>
				<li>
				Here, we will examplarily split cluster 6 by clicking on its "Split" button:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="cluster_split_1.3.77.png"/>
					</div>
				</div>
				</li>
				<li>
				We are now presented with multiple ways of how to split a cluster: 
				<div id="row">
					<div id="col-md-12">
						<asset:image src="splitpattern_least_similar.png"/>
					</div>
				</div>
				</li>
				<li>
				Here, we select "Based on least similar objects". This implies, that the two least similar objects of the cluster will be the prototypes of two new clusters. The remaining objects of the current cluster will be assigned to the prototype they are more similar to.
				Furthermore, we choose Magnitude (Euclidean) as the similarity function.
				</li>
				<li>
				We are now presented with a dialog showing us the old cluster as well as the two new clusters:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="cluster_split_1.3.77_dialog.png"/>
					</div>
				</div>
				</li>
				<li>
				You now have the option to save the new clusters (which will remove the old cluster), or discard the new clusters (which will keep the old cluster).
				</li>
				<li>
				Here, we choose to save the two new clusters and they will be presented to us as shown below:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="cluster_split_1.3.77_accept.png"/>
					</div>
				</div>
				</li>
				<li>
				As seen, the two new clusters are maked with green, where the old cluster is marked with red, indicating that the old cluster will be removed, and the two new clusters will be added, when pressing "Apply changes".
				</li>
			</ol>
		</div>
		
		
		<div id="suggest_patterns" class="tutorial_step">
			<h2>Step 10: Suggest clusters</h2>
			<ol>
				<li>
				Clicking "Filter objects" -> "Least fitting objects" in the Cluster Operations panel on the left, you will be presented with a dialog, that shows the least fitting objects across all clusters:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="filterobjects_1.3.77.png"/>
					</div>
				</div>
				</li>
				<li>
				You have the options, to either delete the least fitting objects, or suggest new clusters based on a new clustering of these objects.
				</li>
				<li>
				The result from suggesting clusters will be presented as below:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="filterobjects_1.3.77_preview.png"/>
					</div>
				</div>
				</li>
				<li>
				You have the option to choose which of the new clusters you want to keep in your clustering when pressing "Save selected clusters".
				</li>
			</ol>
		</div>
		
		<div id="pattern_history" class="tutorial_step">
			<h2>Step 11: Cluster history</h2>
			<ol>
				<li>
				You can also review all your previous steps by clicking on "Show history":
				<div id="row">
					<div id="col-md-12">
						<asset:image src="cluster_history_1.3.77.png"/>
					</div>
				</div>
				</li>
				<li>
				Clicking on "Reset to this iteration" will revert your clustering to the selected state and removes all newer states from the session.
				</li>
			</ol>
		</div>
		
		
		<div id="filter_clusters" class="tutorial_step">
			<h2>Step 12: Filter clusters</h2>
			<ol>
				<li>
				Clusters can be filtered according to their properties. Here, we want to filter them based on cluster p-values.
				</li>
				<li>
				To this end, we first have to recalculate p-values for our current clusters as described previously.
				</li>
				<li>
				Now, we enable the cluster filter functionality under the "Cluster Operations" tab by setting the filter settings as follows and clicking "Apply Filter":
				<div id="row">
					<div id="col-md-12">
						<asset:image src="filterclusters_1.3.77.png"/>
					</div>
				</div>
				</li>
				<li>
				After clicking "Apply Filter", clusters that are filtered out are visualized with a shaded background and at the bottom of the cluster table: 
				<div id="row">
					<div id="col-md-12">
						<asset:image src="filterclusters_1.3.77_clustertable.png"/>
					</div>
				</div>
				</li>
			</ol>
		</div>
		
		
		
		
		<div id="network_analysis" class="tutorial_step">
			<h2>Step 13: Network analysis</h2>
			<ol>
				<li>
				One can visualize clusters on the network by changing to the "Network Operations" tab and clicking the "Colorize network" button.
				This will color the nodes of the current network according to their cluster.
				</li>
				<li>
				Here, we only want to visualize the significant clusters on the network. Thus, after applying the cluster filter (see previous step), we select all non-filtered clusters (or alternatively click on the header of the "Selected" column to select them all).
				</li>
				<li>
				We then click on "Colorize network" and are presented with a network such as the following:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="network_enrichment_1.3.77.png"/>
					</div>
				</div>
				</li>
				<li>
				Further, de novo network enrichment can be performed (a limited version of KeyPathwayMiner is included in TiCoNE).
				</li>
				<li>
				We leave the cluster selection as it is, i.e. we perform network enrichment only for the significant clusters.
				</li>
				<li>
				In the "Network Enrichment" panel we need to configure the number of exception nodes, the number of desired results (subnetworks), and whether unmapped nodes should be counted as exception nodes:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="kpm_control_panel_1.2.53.png"/>
					</div>
				</div>
				</li>
				<li>
				After clicking on "Perform", we are presented with the following subnetworks that are enriched in objects of the selected significant clusters:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="kpm_1.3.77.png"/>
						<dl>
							<dd>
							For each enriched subnetwork, we are presented with an ID, the number of objects contained in the subnetwork, and its number of exception nodes. Here, exception nodes are those that are not contained in our significant clusters.
							</dd>
						</dl>
					</div>
				</div>
				</li>
				<li>
				One can highlight the nodes contained in a enriched subnetwork, by selecting the subnetwork in the KPM results table:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="kpm_visualize_on_network.png"/>
					</div>
				</div>
				</li>
				<li>
				Further one can extract the union or core (intersection) of a selection of results, using the buttons. This will create a new network, with the corresponding nodes:
				<div id="row">
					<div id="col-md-12">
						<asset:image src="kpm_subnetwork_1.3.77.png"/>
					</div>
				</div>
				</li>
			</ol>
		</div>
</g:content>
</body>
</html>