<g:applyLayout name="main">
<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<a id="#"></a>
	<g:content tag="title">
		<h1>
			<g:pageProperty name="page.title" /> 
			<small>(<g:if test="${ isLatest }">Latest </g:if>Revision ${ params["ver"] })</small>
		</h1>
		<h2><small>Documentation for ${ product } version<g:if test="${ summarizedVersionsList.size() > 1 }">s</g:if> 
			<g:each in="${summarizedVersionsList}" var="it" status="x">
				${it }<g:if test="${ x < summarizedVersionsList.size()-1 }">, </g:if>
			</g:each>
		</small></h2>
	</g:content>
	<g:content tag="sidebarClass">col-lg-3</g:content>
	<g:content tag="mainClass">col-lg-9</g:content>
	<g:content tag="sidebarTitle">Tutorial Navigation</g:content>
	<g:content tag="sidebar">
		<div style="float: left;">
			<g:if test="${ lastVer }" >
				<g:link action="show_workflow" id="${ params["id"] }" params="[ver: lastVer]"><- (${ lastVer }) Previous Revision</g:link>
			</g:if>
		</div>
		<div style="float: right;">
			<g:if test="${ nextVer }" >
				<g:link action="show_workflow" id="${ params["id"] }" params="[ver: nextVer]">Newer Revision (${ nextVer }) -></g:link>
			</g:if>
		</div>
		<br/>
		<p>Quick jump:</p>
		<ul id="quicknav"></ul>
	</g:content>
	
	${ raw(pageProperty(name:'page.tutorialContent')) }
			  	
	<script type="text/javascript">
		$(document).ready(function() {
			// wrap headings and contents in bootstrap panels
			$('.tutorial_step h2').addClass('panel-title');
			$('.tutorial_step h2').wrap(function() {
				return '<div class="panel-heading"></div>'
			});
			$('.tutorial_step h2').append('<a style="float:right;" class="fa fa-chevron-up" href="#"></a>');
			$('.tutorial_step').addClass('panel panel-default');
			$('.tutorial_step > ol').wrap('<div class="panel-body">');
			$('.tutorial_step > .panel-body > ol').addClass('list-blockquotes');
			$('.tutorial_step > .panel-body > ol > li').addClass('li-blockquote');
			$('.tutorial_step > .panel-body > ol > li > div').addClass('list-blockquote');
			$('.tutorial_step img').addClass('img-responsive img-thumbnail');
			
			$('.panel-title').each(function(ind, bla) {
				var text = $(this).text();
				var id = $(this).parent().parent().attr("id");
				$("#quicknav").append('<li><a href="#' + id + '">' + text + '</a></li>');
			});
		});
	</script>

</body>
</html>
</g:applyLayout>