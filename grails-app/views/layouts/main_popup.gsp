<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="${ grails.util.Holders.config.grails.project.groupId } - Time Course Network Enricher"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<style>
		  .col-sm-height {
		        display: table-cell;
		        float: none !important;
		    }
			  .row-same-height {
			    display: table;
			    width: 100%;
			}
			
		.footer {
		  position: absolute;
		  bottom: 0;
		  width: 100%;
		  /* Set the fixed height of the footer here */
		  height: 60px;
		  background-color: #f5f5f5;
		}
		.container .text-muted {
		  margin: 10px 0;
		}
		
		html {
  position: relative;
  min-height: 100%;
}
					
		</style>
		<g:layoutHead/>
		<r:layoutResources/>
	</head>
	<body>

    <div class="container-fluid" style="padding-top: 20px;">
      <div class="row">
        <div class="col-md-4">
        	<div class="panel panel-default">
        		<div class="panel-heading"><g:pageProperty name="page.sidebarTitle" /></div>
  				<div class="panel-body">
					<g:pageProperty name="page.sidebar" />
				</div>
			</div>
        </div>
        <div class="col-md-8">
	      <div class="page-header">
	        <h1><g:pageProperty name="page.title" /></h1>
	      </div>
			<g:layoutBody/>
			<r:layoutResources/>
        </div>
      </div><!--/row-->
    </div> <!-- /container -->
	
	</body>
</html>
