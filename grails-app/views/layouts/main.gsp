<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="${ grails.util.Holders.config.grails.project.groupId } - Time Course Network Enricher"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" href="<g:assetPath src="ticone_logo_icon.png"/>" type="image/png" />
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<style>
		  .col-sm-height {
		        display: table-cell;
		        float: none !important;
		    }
			  .row-same-height {
			    display: table;
			    width: 100%;
			}
			
		.footer {
		  position: absolute;
		  bottom: 0px;
		  width: 100%;
		  /* Set the fixed height of the footer here */
		  height: 60px;
		  background-color: #f5f5f5;
		}
		.container .text-muted {
		  margin: 10px 0;
		}
		
		html {
		  position: relative;
		  min-height: 100%;
		}

		body {
		  /* Margin bottom by footer height */
		  //margin-bottom: 60px;
		  //overflow: hidden;
		}

					
		</style>
		<script type="text/javascript">
			function show_modal(id, title, text) {
				$('#' + id + ' .modal-title').html(' ' + title);
				$('#' + id + ' .modal-body').html('<asset:image src="ajax-loader.gif"/> ' + text);
				$('#' + id).modal('show');
			}

			function hide_modal(id) {
				$('#' + id).modal('hide');
			}
			
			function layoutLarger() {
				$(Highcharts.charts).each(function(i,chart){
					if (!(chart.renderTo.id.startsWith("highchartPopup"))) {
					    var height = chart.renderTo.clientHeight; 
					    chart.setSize(10, height);
					} 
				});

				if($('#sidebar_col').hasClass('col-lg-3')) {
					$('#sidebar_col').toggleClass('col-lg-3').toggleClass('col-lg-4');
					$('#main_col').toggleClass('col-lg-9').toggleClass('col-lg-8');
					$('#layoutSmallerBtn').show();
				} else if($('#sidebar_col').hasClass('col-lg-4')) {
					$('#sidebar_col').toggleClass('col-lg-4').toggleClass('col-lg-5');
					$('#main_col').toggleClass('col-lg-8').toggleClass('col-lg-7');
					//$('#layoutSmallerBtn').show();
				} else if($('#sidebar_col').hasClass('col-lg-5')) {
					$('#sidebar_col').toggleClass('col-lg-5').toggleClass('col-lg-6');
					$('#main_col').toggleClass('col-lg-7').toggleClass('col-lg-6');
					//$('#layoutLargerBtn').hide();
				} else if($('#sidebar_col').hasClass('col-lg-6')) {
					$('#sidebar_col').toggleClass('col-lg-6').toggleClass('col-lg-7');
					$('#main_col').toggleClass('col-lg-6').toggleClass('col-lg-5');
					$('#layoutLargerBtn').hide();
				}

				$(Highcharts.charts).each(function(i,chart){
					// do not resize the popup patterns
					if (!(chart.renderTo.id.startsWith("highchartPopup"))) {
					    var height = chart.renderTo.clientHeight; 
					    var width = chart.renderTo.clientWidth; 
					    chart.setSize(width, height); 
					}
				});
			}



			function layoutSmaller() {
				$(Highcharts.charts).each(function(i,chart){
					if (!(chart.renderTo.id.startsWith("highchartPopup"))) {
					    var height = chart.renderTo.clientHeight; 
					    chart.setSize(10, height);
					} 
				});

				if($('#sidebar_col').hasClass('col-lg-7')) {
					$('#sidebar_col').toggleClass('col-lg-7').toggleClass('col-lg-6');
					$('#main_col').toggleClass('col-lg-5').toggleClass('col-lg-6');
					$('#layoutLargerBtn').show();
				} else if($('#sidebar_col').hasClass('col-lg-6')) {
					$('#sidebar_col').toggleClass('col-lg-6').toggleClass('col-lg-5');
					$('#main_col').toggleClass('col-lg-6').toggleClass('col-lg-7');
					//$('#layoutLargerBtn').show();
				} else if($('#sidebar_col').hasClass('col-lg-5')) {
					$('#sidebar_col').toggleClass('col-lg-5').toggleClass('col-lg-4');
					$('#main_col').toggleClass('col-lg-7').toggleClass('col-lg-8');
					//$('#layoutSmallerBtn').hide();
				} else if($('#sidebar_col').hasClass('col-lg-4')) {
					$('#sidebar_col').toggleClass('col-lg-4').toggleClass('col-lg-3');
					$('#main_col').toggleClass('col-lg-8').toggleClass('col-lg-9');
					$('#layoutSmallerBtn').hide();
				}
				
				//$('#sidebar_col').toggleClass('col-lg-4').toggleClass('col-lg-8');
				//$('#main_col').toggleClass('col-lg-4').toggleClass('col-lg-8');
				//$('#icon-panel-toggle').toggleClass('fa-angle-double-right').toggleClass('fa-angle-double-left')

				$(Highcharts.charts).each(function(i,chart){
					// do not resize the popup patterns
					if (!(chart.renderTo.id.startsWith("highchartPopup"))) {
					    var height = chart.renderTo.clientHeight; 
					    var width = chart.renderTo.clientWidth; 
					    chart.setSize(width, height); 
					}
				});
			}

			$.ticone_queue = {
			    _timer: null,
			    _queue: [],
			    add: function(fn, context, time, parameters) {
			        var setTimer = function(time) {
			            $.ticone_queue._timer = setTimeout(function() {
			                time = $.ticone_queue.add();
			                if ($.ticone_queue._queue.length) {
			                    setTimer(time);
			                }
			            }, time || 2);
			        }

			        if (fn) {
			            $.ticone_queue._queue.push([fn, context, time, parameters]);
			            if ($.ticone_queue._queue.length == 1) {
			                setTimer(time);
			            }
			            return;
			        }

			        var next = $.ticone_queue._queue.shift();
			        if (!next) {
			            return 0;
			        }

			        next[0].apply(next[1] || window, next[3]);
			        return next[2];
			    },
			    clear: function() {
			        clearTimeout($.ticone_queue._timer);
			        $.ticone_queue._queue = [];
			    }
			};
		</script>
		<g:layoutHead/>
	</head>
	<body>
    <nav class="navbar navbar-default navbar" role="navigation">
    	<div class="container-fluid">
    		<div class="navbar-header">
    			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	          		<span class="sr-only">Toggle navigation</span>
		        	<span class="icon-bar"></span>
		        	<span class="icon-bar"></span>
		        	<span class="icon-bar"></span>
	      		</button>
	      		<a href="${ createLink(controller: 'welcome', action: 'index') }" style="padding: 6px;">
		        		<g:img height="48px" alt="${ grails.util.Holders.config.grails.project.groupId }" file="ticone_logo_3.png"/>
		        	</a>
			</div>
			<div class="navbar-collapse collapse">
		    	<ul class="nav navbar-nav ">
		        	<li class="divider-vertical"></li>
		        	
		        	
		        	<li><ul class="nav navbar-nav">
			            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><i class="fa fa-download" style="width:16px;"></i> Downloads <b class="caret"></b></a>
			            	<ul class="dropdown-menu">
			            		<!-- <li><g:link controller="download" action="index"><i class="fa fa-download" style="width:16px;"></i> Downloads</g:link></li>
			            		<li role="separator" class="divider"></li> -->
			              		<li><a href="${ createLink(controller: 'download', action: 'index', fragment: 'cyto') }"><asset:image src="cytoscape_logo_large_cropped.png" width="16"/> TiCoNE Cytoscape App</a></li>
			              		<!-- <li><a href="${ createLink(controller: 'download', action: 'index', fragment: 'stand') }">TiCoNE Standalone</a></li> -->
			              		<!-- <li><a href="${ createLink(controller: 'iterativeTimeSeriesClustering', action: 'create') }"><i class="fa fa-connectdevelop" style="width:16px;"></i> Use TiCoNE WEB</a></li> -->
							</ul>
						</li>
					</ul></li>
					
					<li><ul class="nav navbar-nav">
			            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><i class="fa fa-connectdevelop" style="width:16px;"></i> TiCoNE WEB <b class="caret"></b></a>
			            	<ul class="dropdown-menu">
			              		<li><a href="${ createLink(controller: 'iterativeTimeSeriesClustering', action: 'create') }"><i class="fa fa-connectdevelop" style="width:16px;"></i> Start a Clustering</a></li>
			              		<li role="separator" class="divider"></li>
			              		<li><a href="${ createLink(controller: 'timeSeriesDataSet', action: 'index') }">Data Sets</a></li>
			              		<li><a href="${ createLink(controller: 'graph', action: 'index') }">Networks</a></li>
							</ul>
						</li>
					</ul></li>
					
		        	<li><g:link controller="tutorial" action="index"><i class="fa fa-question-circle"></i> Help</g:link></li>
		        	<li><g:link controller="citation" action="index"><i class="fa fa-quote-left"></i> Citation</g:link></li>
		        	<li><a href="${createLink(controller: 'about', action: 'index') }"><i class="fa fa-user"></i> About</a></li>
				</ul>
				<shiro:isNotLoggedIn>
				<ul class="nav navbar-nav navbar-right">
  					<li class="memblogin"><a href="${ createLink(controller: 'signup', action: 'index') }"><u>Register an account</u></a></li>
				      <li class="dropdown">
				        <a class="dropdown-toggle" href="#" data-toggle="dropdown">Login</a>
				        <div class="dropdown-menu" style="padding:10px;">
				          <g:form class="form" controller="auth" action="signIn">
				          	<div class="form-group">
				            	<input name="username" id="username" class="form-control" type="text" placeholder="Username">
				            </div> 
				            <div class="form-group">
				            	<input name="password" id="password" class="form-control" type="password" placeholder="Password">
				            </div> 
				            <div class="form-group">
				            	<label for="rememberMe">Remember me</label>
				            	<g:checkBox name="rememberMe"/><br>
				            </div> 
				            <g:hiddenField name="targetUri" value="${request.forwardURI - request.contextPath}" />
				            <button type="submit" id="btnLogin" class="btn btn-primary">Login</button>
				          </g:form>
          				</div>  
          			</li>
				</ul>
				</shiro:isNotLoggedIn>
				<shiro:isLoggedIn>
				<ul class="nav navbar-nav navbar-right">
		            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"><i class="fa fa-user"></i> Logged in as <shiro:principal /> <b class="caret"></b></a>
		            	<ul class="dropdown-menu">
		              		<li><a href="${ createLink(controller: 'user', action: 'show')}"><i class="fa fa-user "></i> Your Profile</a></li>
		              		<li><a href="${ createLink(controller: 'iterativeTimeSeriesClustering', action: 'your')}"><i class="glyphicon glyphicon-stats "></i> Your Clusterings</a></li>
		              		<li><a href="${ createLink(controller: 'auth', action: 'signOut', params: [targetUri: request.forwardURI - request.contextPath]) }"><i class="fa fa-sign-out"></i> Log out</a></li>
						</ul>
					</li>
				</ul>
				</shiro:isLoggedIn>
			</div>
		</div>
	</nav>

    <div class="container-fluid">
      <div class="row">
      	<div class="col-lg-12">
      		<g:pageProperty name="page.topRow" />
      	</div>
      </div>
      <div class="row">
	      <g:if test="${!pageProperty(name:'page.sidebarHide').equals( 'true' )}">
	        <g:if test="${pageProperty(name:'page.hideGraph').equals("true")}">
	        	<div id="sidebar_col" class="col-lg-12">
	        </g:if>
	        <g:elseif test="${pageProperty(name:'page.sidebarClass') != ""}">
	        	<div id="sidebar_col" class="${ pageProperty(name:'page.sidebarClass') }">
	        </g:elseif>
	        <g:else>
	        	<div id="sidebar_col" class="col-lg-5">
	        </g:else>
	        	<g:if test="${!pageProperty(name:'page.hideGraph').equals("true") && !pageProperty(name: 'page.preventLayoutResize').equals("true")}">
		        	<button style="float: right;" class="btn btn-default" id="layoutLargerBtn" onclick="layoutLarger();" title="Expand the sidebar to the right">
		        		<i id="icon-panel-toggle-sm" class="fa fa-angle-double-right"></i>
		        	</button>
		        	<button style="float: right;" class="btn btn-default" id="layoutSmallerBtn" onclick="layoutSmaller();" title="Shrink the sidebar to the left">
		        		<i id="icon-panel-toggle-lg" class="fa fa-angle-double-left"></i>
		        	</button>
	        	</g:if>
	        	<g:if test="${pageProperty(name:'page.sidebarSimple').equals( 'true' )}">
	        		<g:pageProperty name="page.sidebar" />
				</g:if>
				<g:else>
					<g:if test="${!pageProperty(name:'page.sidebarPanelHide').equals( 'true' )}">
			        	<div class="panel panel-default"><!-- data-spy="affix" data-offset-top="60" data-offset-bottom="60"> --> 
			        		<div class="panel-heading"><g:pageProperty name="page.sidebarTitle" /></div>
			  				<div class="panel-body">
								<g:pageProperty name="page.sidebar" />
							</div>
						</div>
					</g:if>
				</g:else>
	        </div>
	        <g:if test="${ !pageProperty(name:'page.hideGraph').equals("true") }" >
		        <g:if test="${pageProperty(name:'page.mainClass') != ""}">
		        	<div id="main_col" class="${ pageProperty(name:'page.mainClass') }">
		        </g:if>
		        <g:else>
		        	<div id="main_col" class="col-lg-7">
		        </g:else>
		    </g:if>
        </g:if>
        <g:else>
	        <g:if test="${pageProperty(name:'page.mainClass') != ""}">
	        	<div id="main_col" class="${ pageProperty(name:'page.mainClass') }">
	        </g:if>
	        <g:else>
        		<div id="main_col" class="col-lg-12">
		    </g:else>
        </g:else>
        
       	<g:if test="${!pageProperty(name:'page.header').equals( '' )}">
       		<g:pageProperty name="page.header" />
       	</g:if>
       	<g:elseif test="${!pageProperty(name:'page.title').equals( '' )}">
	      <div class="page-header">
	        <g:pageProperty name="page.title" />
	      </div>
        </g:elseif>
		<g:layoutBody/>
		<r:layoutResources/>
        </div>
      </div><!--/row-->

    </div> <!-- /container -->
	
	<div style="height: 75px;">
	</div>
    <footer class="footer">
      <div class="container">
        <p class="text-muted">${ grails.util.Holders.config.grails.project.groupId } WEB version: <g:link controller="changelog" action="index" fragment="${grails.util.Holders.grailsApplication.metadata['app.version'].replaceAll('\\.','')}"><g:meta name="app.version"/></g:link> - Built with Grails <g:meta name="app.grails.version"/><br/>
        ${ grails.util.Holders.config.grails.project.groupId } Library version: <g:meta name="app.ticone_lib.version"/></p>
      </div>
    </footer>
	</body>
</html>
