<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="${ grails.util.Holders.config.grails.project.groupId } - Time Course Network Enricher"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" href="<g:assetPath src="ticone_logo_icon.png"/>" type="image/png" />
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<style>
		  .col-sm-height {
		        display: table-cell;
		        float: none !important;
		    }
			  .row-same-height {
			    display: table;
			    width: 100%;
			}
			
		.footer {
		  position: absolute;
		  bottom: 0;
		  width: 100%;
		  /* Set the fixed height of the footer here */
		  height: 60px;
		  background-color: #f5f5f5;
		}
		.container .text-muted {
		  margin: 10px 0;
		}
		
		html {
  position: relative;
  min-height: 100%;
}

		body {
		  /* Margin bottom by footer height */
		  margin-bottom: 60px;
		}
					
		</style>
		<g:layoutHead/>
		<r:layoutResources/>
	</head>
	<body>
    <div class="container">
        <div class="col-lg-12">
			<g:layoutBody/>
			<r:layoutResources/>
        </div>
      </div><!--/row-->

      <footer>
        <p></p>
      </footer>

    </div> <!-- /container -->
	

    <footer class="footer">
      <div class="container">
        <p class="text-muted">${ grails.util.Holders.config.grails.project.groupId } WEB version: <g:meta name="app.version"/> - Built with Grails <g:meta name="app.grails.version"/><br/>${ grails.util.Holders.config.grails.project.groupId } Library version: <g:meta name="app.ticone_lib.version"/></p>
      </div>
    </footer>
	</body>
</html>
