<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
<meta http-equiv="Refresh" content="3">
<script type="text/javascript">
	function submitMailForm() {
		$('#sendMailForm').submit();
	};

	<g:if test="${t.finished && t.redirect_url}">
		window.location = '${t.redirect_url}'	
	</g:if>
</script>
</head>
<body>
	<g:content tag="sidebarHide">true</g:content>
	<g:content tag="title">
	<h1>Task: '${t.title }'</h1>
	</g:content>
	<g:if test="${t.finished }">
	  <g:if test="${t.statusMessage }">
	  	<p>Your job finished with the following status message:</p>
	  	<p><strong>${t.statusMessage }</strong></p>
	  </g:if>
	  <g:else>
		 <p>Your job is finished.</p>
		 <p>You can access the results <a href="${ t.redirect_url }">here</a></p>
	  </g:else>
	</g:if>
	<g:else>
		<p>Your job has not finished yet.</p>
		<p>You have several options to be informed about your job's progress:</p>
		<ul>
			<li>You can refresh this page</li>
			<li>You can bookmark this page and come back later</li>
		<g:if test="${t.user && t.user.email }">
			<g:if test="${ t.sendMail }">
				<li>You will be notified by email to ${t.user.email }.</li>
			</g:if>
			<g:else>
				<g:form name="sendMailForm" action="update" params="[id: t.taskId ]"><li>We can send you an email: <g:checkBox name="sendMail" onclick="submitMailForm();" /></li></g:form>
			</g:else>
		</g:if>
		<g:if test="${t.canCancel }">
		<g:form name="cancelTaskForm" action="cancel" params="[id: t.taskId ]"><li>Is it taking too long? Stop your task as soon as possible (no guaranteed immediate stop): <g:submitButton name="cancelTask" value="Ask for Stop" onclick="cancelTask();" /></li></g:form>
		</g:if>
		</ul>
	</g:else>	
</body>
</html>