<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="sidebarTitle">
	Your Clusterings
	</g:content>
	<g:content tag="title">
	<h1>Your Clusterings</h1>
	</g:content>
	<g:content tag="sidebarClass">col-lg-3</g:content>
	<g:content tag="mainClass">col-lg-9</g:content>
	<g:content tag="sidebar">
	</g:content>
	<h2>Change your e-mail address:</h2>
	<g:link action="change_email">Click</g:link>
	
	<h2>Change your password: </h2>
	<g:link action="change_password">Click</g:link>
</body>
</html>