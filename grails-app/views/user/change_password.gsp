<%@page	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="sidebarHide">true</g:content>
	<g:content tag="title"><h1>Change your password</h1></g:content>
	
		<g:form action="change_password" class="form-horizontal">
			<div class="form-group">
		    <div class="col-sm-3"></div>
			<div class="col-sm-6">
				<g:if test="${flash.message}">
					   <div class="alert alert-info">${flash.message}</div>
				</g:if>
				
				<g:hasErrors bean="${user}">
				    <div class="alert alert-error">
				        <g:renderErrors bean="${user}" as="list"/>
				    </div>
				</g:hasErrors>
				</div>
			</div>
		<div class="form-group">
		    <label for="password" class="col-sm-3 control-label">Current Password:</label>
			<div class="col-sm-6">
				<g:passwordField name="currentpassword" value="" class="form-control"/>
			</div>
		</div>
		<div class="form-group">
		    <label for="password" class="col-sm-3 control-label">New Password:</label>
			<div class="col-sm-6">
				<g:passwordField name="password" value="" class="form-control"/>
			</div>
		</div>
		<div class="form-group">
		    <label for="password" class="col-sm-3 control-label">Confirm New Password:</label>
			<div class="col-sm-6">
				<g:passwordField name="password2" value="" class="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-3">
			</div>
			<div class="col-sm-6">
				<g:submitButton name="submit" class="btn btn-primary"/>
			</div>
		</div>
	</g:form>
</body>
</html>