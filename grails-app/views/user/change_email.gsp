<%@page	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="sidebarHide">true</g:content>
	<g:content tag="title"><h1>Change your e-mail address</h1></g:content>
	
		<g:form action="change_email" class="form-horizontal">
			<div class="form-group">
		    <div class="col-sm-3"></div>
			<div class="col-sm-6">
				<g:if test="${flash.message}">
					   <div class="alert alert-info">${flash.message}</div>
				</g:if>
				
				<g:hasErrors bean="${user}">
				    <div class="alert alert-error">
				        <g:renderErrors bean="${user}" as="list"/>
				    </div>
				</g:hasErrors>
				</div>
			</div>
		<div class="form-group">
		    <label for="email" class="col-sm-3 control-label">Current E-Mail:</label>
			<div class="col-sm-6">
				<label for="email" class="col-sm-6 control-label">${ user.email }</label>
			</div>
		</div>
		<div class="form-group">
		    <label for="email" class="col-sm-3 control-label">New E-Mail:</label>
			<div class="col-sm-6">
				<g:textField name="email" value="" class="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-3">
			</div>
			<div class="col-sm-6">
				<g:submitButton name="submit" class="btn btn-primary"/>
			</div>
		</div>
	</g:form>
</body>
</html>