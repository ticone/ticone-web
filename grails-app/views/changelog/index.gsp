<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="sidebarClass">col-lg-2</g:content>
	<g:content tag="mainClass">col-lg-5</g:content>
	<g:content tag="sidebarTitle">Version History</g:content>
	<g:content tag="sidebar">
		<p>Quick jump:</p>
	<ul>
		<li><a href="#106">1.0.6</a></li>
		<li><a href="#105">1.0.5</a></li>
		<li><a href="#104">1.0.4</a></li>
		<li><a href="#103">1.0.3</a></li>
		<li><a href="#102">1.0.2</a></li>
		<li><a href="#101">1.0.1</a></li>
	</ul>
	</g:content>
	<g:content tag="preventLayoutResize">true</g:content>
	<g:content tag="title">
		<h1>TiCoNE WEB - Version History</h1>
	</g:content>
	<a id="106"></a><h2>1.0.6 <small>(29th of October, 2018)</small></h2>
        
        <p><markdown:renderHtml>- Improved security of web application
        </markdown:renderHtml></p>
        
	<a id="105"></a><h2>1.0.5 <small>(09th of June, 2017)</small></h2>
	
	<p><markdown:renderHtml>- Bugfix: Network edges were not rendered properly - fixed by updating CytoscapeJS library dependency
	</markdown:renderHtml></p>
	
	
	<a id="104"></a><h2>1.0.4 <small>(08th of June, 2017)</small></h2>
	
	<p><markdown:renderHtml>- Bugfix: Keypathwayminer Web tasks are now executed properly again
- Bugfix: Networks were not rendered due to an error in the database configuration
	</markdown:renderHtml></p>
	
	
	
	<a id="103"></a><h2>1.0.3 <small>(30th of May, 2017)</small></h2>
	
	<p><markdown:renderHtml>- Updated tutorials for new TiCoNE Cytoscape app version 1.3.
	</markdown:renderHtml></p>
	
	
	
	<a id="102"></a><h2>1.0.2 <small>(27th of February, 2017)</small></h2>
	
	<p><markdown:renderHtml>- Removed Transitivity Clustering from available clustering methods for now.
	 - It was buggy and often led to an NullPointerException error, because it could not identify a clustering with the specified number of clusters.
	 - Please use one of the other clustering methods instead.
	 - We are looking into a possible solution in the future.
	</markdown:renderHtml></p>
	<a id="101"></a><h2>1.0.1</h2>
	<p><markdown:renderHtml>- Initial version of TiCoNE WEB.
	</markdown:renderHtml></p>
</body>
</html>
