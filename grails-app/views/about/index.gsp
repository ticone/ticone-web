<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="preventLayoutResize">true</g:content>
	<g:content tag="sidebarPanelHide">true</g:content>
	<g:content tag="sidebarClass">col-lg-2</g:content>
	<g:content tag="mainClass">col-lg-5</g:content>
	<g:content tag="title"><h1>About TiCoNE WEB</h1></g:content>
	
	<h2>Acknowledgements</h2>
	TiCoNE WEB uses <a href="http://js.cytoscape.org/">Cytoscape JS</a> for the network visualization and <a href="https://www.highcharts.com/">Highcharts</a> for the cluster visualizations.
	
	<h2>Contact us</h2>
	<p>If you want to contact us regarding TiCoNE, consider writing us in the following order:</p>
	<ul>
		<li><a href="mailto:wiwiec@imada.sdu.dk">Christian Wiwie</a></li>
		<li><a href="mailto:jan.baumbach@imada.sdu.dk">Jan Baumbach</a></li>
		<li><a href="mailto:roettger@imada.sdu.dk">Richard Röttger</a></li>
	</ul>
	<h2>Core Developers</h2>
	<p>If you want to report a bug or need assistance with a feature or have feature requests, please contact a core developer:</p>
	<ul>
		<li>Christian Wiwie: TiCoNE Core Library & TiCoNE WEB</li>
	</ul>
	<h2>Former Developers</h2>
	<ul>
		<li>Christian Nørskov: Helped developing early versions of the TiCoNE core library & Cytoscape App</li>
	</ul>
</body>
</html>