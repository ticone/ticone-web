<%@page import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
    <head>
        <title>An Example Page</title>
        <meta name="layout" content="main" />
        <r:script type="text/javascript">
		    function SubmitFrm(){
		        var patternNo = $('input:radio[name=myGroup]:checked').val();
		        window.location = "/tsviz_grails/timeSeriesClusteringVisualization/show/${params.id}/" + patternNo;
		    }
		    
		    
	        $(function(){ // on dom ready
	
				$('#cy').cytoscape({
				  container: document.getElementById('cy'),
				  
				  // this is an alternative that uses a bitmap during interaction
				  hideEdgesOnViewport: true,
				  textureOnViewport: true,
				  
				  // interpolate on high density displays instead of increasing resolution
				  pixelRatio: 1.0,
				  
				  style: cytoscape.stylesheet()
				    .selector('node')
				      .css({
				        'width': '60px',
				        'height': '60px',
				        'min-zoomed-font-size': 8,
				        'content': 'data(id)',
				        'background-color': 'mapData(${patterns[selectedPattern].name}, ${String.format("%.2f",vis.graph.minCoeff)}, ${String.format("%.2f",vis.graph.maxCoeff)}, white, ${vis.patternColors[selectedPattern]})',
				        'border-width': '2px'
				      })
				    .selector('edge')
				      .css({
				        'width': 4,
				        'opacity': 1
				      })
				    .selector(':selected')
				      .css({
				        'background-color': 'black',
				        'line-color': 'black',
				        'target-arrow-color': 'black',
				        'source-arrow-color': 'black',
				        'opacity': 1
				      })
				    .selector('.faded')
				      .css({
				        'opacity': 0.25,
				        'text-opacity': 0
				      }),
				  
				  elements: {
				    nodes: [
				    ], 
				
				    edges: [
				    ]
				  },
				  
				  layout: {
				    name: 'circle',
				    padding: 10
				  },
				  
				  ready: function(){
				    window.cy = this;
				    
				    $.ajax({
					  dataType: "json",
					  url: '/tsviz_grails/graph/nodesAndEdgesAsJSON/${vis.graph.id}',
					  success: function(data) {
							console.log("Loading graph data...");
						  	window.cy.load(data, function(e) {
						  	
								console.log("... done");
							  	
							  	var optionsCose = {
								  name: 'cose',
								
								  // Called on `layoutready`
								  ready               : function() {},
								
								  // Called on `layoutstop`
								  stop                : function() {},
								
								  // Whether to animate while running the layout
								  animate             : false,
								
								  // Number of iterations between consecutive screen positions update (0 -> only updated on the end)
								  refresh             : 0,
								  
								  // Whether to fit the network view after when done
								  fit                 : true, 
								
								  // Padding on fit
								  padding             : 30, 
								
								  // Constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
								  boundingBox         : undefined,
								
								  // Whether to randomize node positions on the beginning
								  randomize           : true,
								  
								  // Whether to use the JS console to print debug messages
								  debug               : false,
								
								  // Node repulsion (non overlapping) multiplier
								  nodeRepulsion       : 400000,
								  
								  // Node repulsion (overlapping) multiplier
								  nodeOverlap         : 10,
								  
								  // Ideal edge (non nested) length
								  idealEdgeLength     : 10,
								  
								  // Divisor to compute edge forces
								  edgeElasticity      : 10,
								  
								  // Nesting factor (multiplier) to compute ideal edge length for nested edges
								  nestingFactor       : 5, 
								  
								  // Gravity force (constant)
								  gravity             : 250, 
								  
								  // Maximum number of iterations to perform
								  numIter             : 100,
								  
								  // Initial temperature (maximum node displacement)
								  initialTemp         : 200,
								  
								  // Cooling factor (how the temperature is reduced between consecutive iterations
								  coolingFactor       : 0.95, 
								  
								  // Lower temperature threshold (below this point the layout will end)
								  minTemp             : 1.0
								};
								
								var optionsCola = {
								  name: 'cola',
								
								  animate: false, // whether to show the layout as it's running
								  refresh: 1, // number of ticks per frame; higher is faster but more jerky
								  maxSimulationTime: 4000, // max length in ms to run the layout
								  ungrabifyWhileSimulating: false, // so you can't drag nodes during layout
								  fit: true, // on every layout reposition of nodes, fit the viewport
								  padding: 30, // padding around the simulation
								  boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
								
								  // layout event callbacks
								  ready: function(){}, // on layoutready
								  stop: function(){}, // on layoutstop
								
								  // positioning options
								  randomize: true, // use random node positions at beginning of layout
								  avoidOverlap: true, // if true, prevents overlap of node bounding boxes
								  handleDisconnected: true, // if true, avoids disconnected components from overlapping
								  nodeSpacing: function( node ){ return 10; }, // extra spacing around nodes
								  flow: undefined, // use DAG/tree flow layout if specified, e.g. { axis: 'y', minSeparation: 30 }
								  alignment: undefined, // relative alignment constraints on nodes, e.g. function( node ){ return { x: 0, y: 1 } }
								
								  // different methods of specifying edge length
								  // each can be a constant numerical value or a function like `function( edge ){ return 2; }`
								  edgeLength: 200, // sets edge length directly in simulation
								  edgeSymDiffLength: undefined, // symmetric diff edge length in simulation
								  edgeJaccardLength: undefined, // jaccard edge length in simulation
								
								  // iterations of cola algorithm; uses default values on undefined
								  unconstrIter: undefined, // unconstrained initial layout iterations
								  userConstIter: undefined, // initial layout iterations with user-specified constraints
								  allConstIter: undefined, // initial layout iterations with all constraints including non-overlap
								
								  // infinite layout options
								  infinite: false // overrides all other options for a forces-all-the-time mode
								};
								
								var optionsArbor = {
								  name: 'arbor',
								
								  animate: true, // whether to show the layout as it's running
								  maxSimulationTime: 4000, // max length in ms to run the layout
								  fit: true, // on every layout reposition of nodes, fit the viewport
								  padding: 30, // padding around the simulation
								  boundingBox: undefined, // constrain layout bounds; { x1,4.9E-324 y1, x2, y2 } or { x1, y1, w, h }
								  ungrabifyWhileSimulating: false, // so you can't drag nodes during layout
								
								  // callbacks on layout events
								  ready: undefined, // callback on layoutready 
								  stop: undefined, // callback on layoutstop
								
								  // forces used by arbor (use arboupload?patternFile=&tsFile=&threshold=r default on undefined)4.9E-324
								  repulsion: undefined,
								  stiffness: undefined,
								  friction: undefined,
								  gravity: true,
								  fps: undefined,
								  precision: undefined,
								
								  // static numbers or functions that dynamically return what these
								  // values should be for each element
								  // e.g. nodeMass: function(n){ return n.data('weight') }
								  nodeMass: undefined, 
								  edgeLength: undefined,
								
								  stepSize: 0.1, // smoothing of arbor bounding box
								
								  // function that returns true if the system is stable to indicate
								  // that the layout can be stopped
								  stableEnergy: function( energy ){
								    var e = energy; 
								    return (e.max <= 0.5) || (e.mean <= 0.3);
								  },
								
								  // infinite layout options
								  infinite: false // overrides all other options for a forces-all-the-time mode
								};
								
								var optionsSpringy = {
								  name: 'springy',
								
								  animate: true, // whether to show the layout as it's running
								  maxSimulationTime: 4000, // max length in ms to run the layout
								  ungrabifyWhileSimulating: false, // so you can't drag nodes during layout
								  fit: true, // whether to fit the viewport to the graph
								  padding: 30, // padding on fit
								  boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
								  random: true, // whether to use random initial positions
								  infinite: false, // overrides all other options for a forces-all-the-time mode
								  ready: undefined, // callback on layoutready
								  stop: undefined, // callback on layoutstop
								
								  // springy forces
								  stiffness: 400,
								  repulsion: 400,
								  damping: 0.5
								};
								
								console.log("Starting layout");
								//window.cy.layout(optionsCola);
						  	});
					  }
					});
				  }
				});
			
			}); // on dom ready
        </r:script>
    </head>
    <body>
    	<% i=0 %>
		<g:each in="${patterns}">
			<g:if test="${ selectedPattern == i }">
				<g:radio name="myGroup" value="${ i}" onclick="SubmitFrm();" checked="true"/>${patterns[i].name}<br />
			</g:if>
			<g:else>
				<g:radio name="myGroup" value="${ i}" onclick="SubmitFrm();"/>${patterns[i].name}<br />
			</g:else>
			<% i=i+1 %>
		</g:each>
		<div style="position: relative; height: 700px; width: 100%;" id="cy"></div>
    </body>
</html>