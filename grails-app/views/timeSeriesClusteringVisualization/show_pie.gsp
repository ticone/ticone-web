<html>
    <head>
        <title>An Example Page</title>
        <meta name="layout" content="main" />
        <script type="text/javascript">
	        $(function(){ // on dom ready
	
				$('#cy').cytoscape({
				  container: document.getElementById('cy'),
				  
				  // this is an alternative that uses a bitmap during interaction
				  hideEdgesOnViewport: true,
				  textureOnViewport: true,
				  
				  // interpolate on high density displays instead of increasing resolution
				  pixelRatio: 1.0,
				  
				  // a motion blur effect that increases perceived performance for little or no cost
				  motionBlur: false,
				  style: cytoscape.stylesheet()
				    .selector('node')
				      .css({
				        'width': '60px',
				        'height': '60px',
				        'min-zoomed-font-size': 10,
				        'content': 'data(id)',
				        'pie-size': '80%',
				        <% i = 0 %>
				        <g:each in="${patterns}">
						'pie-${i+1}-background-color': '${vis.patternColors[i]}',
				        'pie-${i+1}-background-size': 'mapData(${it.name}, 0, 1, 0, 100)',
				        <% i = i + 1 %>
						</g:each>
				      })
				    .selector('edge')
				      .css({
				        'width': 4,
				        'opacity': 0.5
				      })
				    .selector(':selected')
				      .css({
				        'background-color': 'black',
				        'line-color': 'black',
				        'target-arrow-color': 'black',
				        'source-arrow-color': 'black',
				        'opacity': 1
				      })
				    .selector('.faded')
				      .css({
				        'opacity': 0.25,
				        'text-opacity': 0
				      }),
				  
				  elements: {
				    nodes: [
				    ], 
				
				    edges: [
				    ]
				  },
				  
				  layout: {
				    name: 'circle',
				    padding: 10
				  },
				  
				  ready: function(){
				    window.cy = this;
				    
				    $.ajax({
					  dataType: "json",
					  url: '/tsviz_grails/graph/nodesAndEdgesAsJSON/${vis.graph.id}',
					  success: function(data) {
							console.log("Loading graph data...");
						  	window.cy.load(data, function(e) {
						  	
								console.log("... done");
							  	
							  	var optionsCose = {
								  name: 'cose',
								
								  // Called on `layoutready`
								  ready               : function() {},
								
								  // Called on `layoutstop`
								  stop                : function() {},
								
								  // Whether to animate while running the layout
								  animate             : false,
								
								  // Number of iterations between consecutive screen positions update (0 -> only updated on the end)
								  refresh             : 0,
								  
								  // Whether to fit the network view after when done
								  fit                 : true, 
								
								  // Padding on fit
								  padding             : 30, 
								
								  // Constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
								  boundingBox         : undefined,
								
								  // Whether to randomize node positions on the beginning
								  randomize           : true,
								  
								  // Whether to use the JS console to print debug messages
								  debug               : false,
								
								  // Node repulsion (non overlapping) multiplier
								  nodeRepulsion       : 400000,
								  
								  // Node repulsion (overlapping) multiplier
								  nodeOverlap         : 10,
								  
								  // Ideal edge (non nested) length
								  idealEdgeLength     : 10,
								  
								  // Divisor to compute edge forces
								  edgeElasticity      : 10,
								  
								  // Nesting factor (multiplier) to compute ideal edge length for nested edges
								  nestingFactor       : 5, 
								  
								  // Gravity force (constant)
								  gravity             : 250, 
								  
								  // Maximum number of iterations to perform
								  numIter             : 100,
								  
								  // Initial temperature (maximum node displacement)
								  initialTemp         : 200,
								  
								  // Cooling factor (how the temperature is reduced between consecutive iterations
								  coolingFactor       : 0.95, 
								  
								  // Lower temperature threshold (below this point the layout will end)
								  minTemp             : 1.0
								};
								
								console.log("Starting layout");
								window.cy.layout(optionsCose);
						  	});
					  }
					});
				  }
				});
			
			}); // on dom ready
        </script>
    </head>
    <body>
		<div id="cy"></div>
    </body>
</html>