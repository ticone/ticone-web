<%@page import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
    <head>
        <title>An Example Page</title>
        <meta name="layout" content="main" />
    </head>
    <body>
    	<g:uploadForm action="upload" params="[patternFile: patternFile, tsFile: tsFile, threshold: threshold, graphFile: graphFile]">
    		<table>
	    	<tr><td>Pattern File:</td><td><g:field type="file" id="patternFile" name="patternFile"/></td></tr>
	    	<tr><td>Time-Series File:</td><td><g:field type="file" id="tsFile" name="tsFile"/></td></tr>
	    	<tr><td>Graph-Edge File:</td><td><g:field type="file" id="graphFile" name="graphFile"/></td></tr>
	    	<tr><td>Threshold:</td><td><g:field type="number" min="1" max="100" id="threshold" name="threshold"/></td></tr>
	    	</table>
	    	<g:submitButton name="submit"/>
	    </g:uploadForm>
    </body>
</html>