<%@page import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
    <head>
        <title>An Example Page</title>
        <meta name="layout" content="main" />
        <script type="text/javascript">
        	var graph;
        	var renderer;
        	var colors = [<g:each var="color" in="${vis.patternColors}">'${color}',</g:each>];
        	var patterns = [<g:each var="p" in="${patterns}">'${p.name}',</g:each>];
        	// create color gradient
        	var rainbow = new Rainbow();
        	rainbow.setNumberRange(0, 100);
        	
        	var rendererPaused = false;
        	
        	function ToggleRender() {
        		if(!rendererPaused)
					renderer.pause();
				else
					renderer.resume();
				rendererPaused = !rendererPaused
        	};
        	
		    function SubmitFrm(){
				rainbow.setSpectrum('white', colors[$('input[type="radio"]:checked').val()]);
				graph.forEachNode(function(node) {
					var bla = patterns[$('input[type="radio"]:checked').val()];
					if (bla in node.data) {
						graph.addNode(node.id, $.extend(node.data, {color: '#' + rainbow.colourAt(node.data[bla]), size: 10}));
					} else {
						var node = graph.addNode(node.id, $.extend(node.data, {color: '#FFFFFF', size: 5}));
					}
				});
				if(rendererPaused) {
					renderer.rerender();
				}
		    };
		    
	        $(function(){ // on dom ready
			    
				var graphGenerator = Viva.Graph.graph();
				graph = Viva.Graph.graph();
				
				
				<g:each var="node" in="${vis.graph.nodes}">
					graph.addNode('${node.id}', {label: '${node.name}', color: '#FFFFFF',<g:each var="pattern" in="${node.coefficients}">'${pattern.key}' : ${(int)((Double.valueOf(pattern.value)-vis.graph.minCoeff)/(vis.graph.maxCoeff-vis.graph.minCoeff)*100)},</g:each>});
				</g:each>

				<g:each var="edge" in="${vis.graph.edges}">
					//if (typeof graph.getNode('${edge.n1.id}') != 'undefined' & typeof graph.getNode('${edge.n2.id}') != 'undefined')
						graph.addLink('${edge.n1.id}','${edge.n2.id}');
				</g:each>
				
				var layout = Viva.Graph.Layout.forceDirected(graph, {
				    springLength : 10,
				    springCoeff : 0.0005,
				    dragCoeff : 0.02,
				    gravity : -5
				});
				
				var graphics = Viva.Graph.View.webglGraphics();
				
				var renderer = Viva.Graph.View.renderer(graph,
				    {
				        graphics : graphics
				    });
				renderer.run();
				    
				SubmitFrm();
				
				renderer = Viva.Graph.View.renderer(graph, {
				    layout : layout,
				    container: document.getElementById('viva'),
                    graphics   : graphics
				});
				renderer.run();
			}); // on dom ready
        </script>

        <style type="text/css" media="screen">
            body, html, svg { width: 100%; height: 100%; overflow: hidden; }
        </style>
    </head>
    <body>
    	<% i=0 %>
		<g:each in="${patterns}">
			<g:if test="${ selectedPattern == i }">
				<g:radio name="myGroup" value="${ i}" onclick="SubmitFrm();" checked="true"/>${patterns[i].name}<br />
			</g:if>
			<g:else>
				<g:radio name="myGroup" value="${ i}" onclick="SubmitFrm();"/>${patterns[i].name}<br />
			</g:else>
			<% i=i+1 %>
		</g:each>
		<g:submitButton name="toggleRender" onclick="ToggleRender();" />
		<div style="position: relative; height: 700px; width: 100%;" id="viva"></div>
    </body>
</html>