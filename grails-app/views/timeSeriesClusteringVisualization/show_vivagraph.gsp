<%@page import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
    <head>
        <title>An Example Page</title>
        <meta name="layout" content="main" />
        <script type="text/javascript">
        	var completeGraph;
        	var graph;
        	var renderer;
        	var layout;
        	var graphics;
        	//var colors = [<g:each var="color" in="${vis.patternColors}">'${color}',</g:each>];
        	var patterns = [<g:each var="p" in="${patterns}">'${p.name}',</g:each>];
        	var colors = ['#FF0000', '#00FF00', '#0000FF', '#FFFF00', '#FF00FF', '#A52A2A', '#00FFFF'];
        	
        	// get a list of nodes that follow any pattern most strongly
        	var patternNodes = [<g:each var="node" in="${vis.graph.nodes}"><g:if test="${node.coefficients.size() > 0}">'${node.id}',</g:if></g:each>];
        	var visibleNodes;
			
        	// create color gradients
        	var rainbows = [];
        	for (var c = 0; c < colors.length; c++) {
	        	var rainbow = new Rainbow();
	        	rainbow.setNumberRange(0, 100);
				rainbow.setSpectrum('white', colors[c]);
				
				rainbows.push(rainbow);
			};
        	
        	var rendererPaused = false;
        	
        	function ToggleRender() {
        		if(!rendererPaused)
					renderer.pause();
				else
					renderer.resume();
				rendererPaused = !rendererPaused
        	};
        	
		    function SubmitFrm(){
				visibleNodes = getVisibleNodes(completeGraph,$('#neighborhood').val());
				visibleNodeIds = [];
				for (var n = 0; n < visibleNodes.length; n++) {
					visibleNodeIds.push(visibleNodes[n].id);
				}
				
				// remove nodes not visible anymore
				graph.forEachNode(function(node) {
					if ($.inArray(node.id,visibleNodeIds) == -1) {
						graph.removeNode(node.id);
					}
				});
				// add the nodes which are visible
				for (var n = 0; n < visibleNodes.length; n++) {
					var node = visibleNodes[n];
					var data = node.data;
					
					if ($('input[type="radio"]:checked').val() > -1) {
						var bla = patterns[$('input[type="radio"]:checked').val()];
						var gradient = rainbows[$('input[type="radio"]:checked').val()];
					} else {
						// find pattern with maximum similarity
						var maxPattern = -1;
						var maxSim = -1;
						for (var p = 0; p < patterns.length; p++) {
							var newSim = data[patterns[p]];
							if (newSim > maxSim) {
								maxSim = data[patterns[p]];
								maxPattern = p;	
							};
						};
						
						var bla = patterns[maxPattern];
						var gradient = rainbows[maxPattern];
					}
				
					if (bla in data) {
						graph.addNode(node.id,$.extend(node.data,{color: '#' + gradient.colourAt(data[bla])}));
					} else {
						graph.addNode(node.id,$.extend(node.data,{color: '#FFFFFF'}));
					}
				}

				<g:each var="edge" in="${vis.graph.edges}">
					if (typeof graph.getNode('${edge.n1.id}') != 'undefined' & typeof graph.getNode('${edge.n2.id}') != 'undefined')
						if (!(graph.hasLink('${edge.n1.id}','${edge.n2.id}'))) {
							graph.addLink('${edge.n1.id}','${edge.n2.id}');
						}
				</g:each>

				//if(rendererPaused) {
				//	renderer.rerender();
				//}
		    };

		    function saveLayout() {

				var locationHash = {};

				graph.forEachNode(function(node) {
					var pos = layout.getNodePosition(node.id);
					locationHash[node.id.toString()] = [pos.x, pos.y];
				});
				//locationHash['transform'] = graphics.getSvgRoot().childNodes[0].getAttribute('transform').replace('matrix(','').replace(')','')
				locationHash['transform'] = graphics.getSvgRoot().childNodes[0].getAttribute('transform')
			    
			    $.ajax({
					type: 'POST',
				    url: '${createLink(controller: 'timeSeriesClusteringVisualization', action: 'save_layout', id: vis.id)}',
				    data: JSON.stringify(locationHash),
				    contentType: 'application/json',
				    dataType: 'json'});
			};
		    
		    function getVisibleNodes(graph,neighborhood) {
		    	//console.log(neighborhood);
		    	var visibleNodes = [];
		    	
		    	for (var n = 0; n < patternNodes.length; n++) {
		    		visibleNodes.push(graph.getNode(patternNodes[n]));
		    	}
		    	
		    	var result = getNodesWithinNeighborhood(graph, visibleNodes, neighborhood);
		    	
		    	return result;
		    };
		    
		    function getNodesWithinNeighborhood(graph, seedNodes, neighborhood) {
		    	var neighbors = seedNodes;
		    	// visit each node only once
		    	var visitedNodes = [];
		    	
		    	for (i = 0; i < neighborhood; i++) {
		    		var newNeighbors = [];
		    		
		    		//console.log(neighbors);
			    	for (var n = 0; n  < neighbors.length; n++) {
			    		var node = neighbors[n];
			    		if ($.inArray(node,visitedNodes) == -1) {
			    			// iterate over neighbors of nodes
			    			newNeighbors = $.merge(newNeighbors, getNeighbors(graph, node));
			    			visitedNodes.push(node);
			    		}
			    	};
			    	
			    	neighbors = $.merge(neighbors, newNeighbors);
			    }
			    return neighbors;
		    };
		    
			// adapted from vivagraph.js
			function getNeighbors(graph, node) {
				var neighbors = [];
				if (!node.links) {
					return neighbors;
				}
				//var maxNeighbors = Math.min(node.links.length, 2);
				var maxNeighbors = node.links.length;
				for (var i = 0; i < maxNeighbors; ++i) {
					var link = node.links[i];
					var otherBody = link.fromId !== node.id ? graph.getNode(link.fromId) : graph.getNode(link.toId);
					if (otherBody && $.inArray(otherBody,neighbors) == -1) {
						neighbors.push(otherBody);
					}
				}
				
				return neighbors;
			}
		    
	        $(function(){ // on dom ready
	                	
		        	for (var p = 0; p < patterns.length; p++) {
		        		//console.log($('#colorBox' + p));
		        		$('#colorBox' + p).css("background-color", colors[p]);
		        	};
		        	
		        	var bla = patterns[$('input[type="radio"]:checked').val()];
		        	completeGraph = Viva.Graph.graph();
	        	
			    <g:each var="node" in="${vis.graph.nodes}">
					<g:if test="${true}" test2="${node.coefficients.size() > 0}">
					// ${node.coefficients}
						var data = {label: '${node.name}', color: '#FFFFFF',<g:each var="pattern" in="${node.coefficients}">'${pattern.key}' : ${(int)((Double.valueOf(pattern.value)-vis.graph.minCoeff)/(vis.graph.maxCoeff-vis.graph.minCoeff)*100)},</g:each>};
						
						if (bla in data) {
							data = $.extend(data, {size: 10});
						} else {
							data = $.extend(data, {size: 5});
						}
						
						completeGraph.addNode('${node.id}', data);
					</g:if>
				</g:each>

				<g:each var="edge" in="${vis.graph.edges}">
					if (typeof completeGraph.getNode('${edge.n1.id}') != 'undefined' & typeof completeGraph.getNode('${edge.n2.id}') != 'undefined')
						completeGraph.addLink('${edge.n1.id}','${edge.n2.id}');
				</g:each>
			    
				graph = Viva.Graph.graph();
				    
				SubmitFrm();
				
				layout = Viva.Graph.Layout.forceDirected(graph, {
				    springLength : 10,
				    springCoeff : 0.00005,
				    dragCoeff : 0.02,
				    gravity : -5,
				    stableThreshold: graph.getNodesCount()
				});

				// init node positions with stored positions from layout
				<g:each var="nodePos" in="${layout.locations}">
				layout.setNodePosition(${nodePos.node.id}, ${nodePos.x}, ${nodePos.y});
				</g:each>
                graphics = Viva.Graph.View.svgGraphics();
				graphics.node(function(node) {
				       // The function is called every time renderer needs a ui to display node
				       var circle = Viva.Graph.svg('circle')
				             .attr('r', node.data.size)
				             .attr('fill', node.data.color)
				             .attr('stroke', '#000000')
				             .attr('stroke-width', '2px');
				             
						var text = Viva.Graph.svg('text')
							.attr('style', 'fill: #000000;')
							.attr('opacity','1')
						    .text(node.data.label);
						var ui = Viva.Graph.svg("g");
						ui.append(circle);
						ui.append(text);
						return ui;
				    })
				    .placeNode(function(nodeUI, pos){
				        // Shift image to let links go to the center:
						var circle = nodeUI.childNodes[0];
						var radius = Number(circle.attr('r'));
						var text = nodeUI.childNodes[1];

						circle.attr('cx', pos.x).attr('cy', pos.y);
						text.attr('x', pos.x + radius + 5).attr('y', pos.y);
				    });
				
				
				renderer = Viva.Graph.View.renderer(graph, {
				    layout : layout,
				    container: document.getElementById('viva'),
                    graphics   : graphics
				});
				renderer.run();

				graphics.getSvgRoot().childNodes[0].setAttribute('transform','${layout.transformMatrix}');
			}); // on dom ready
        </script>

        <style type="text/css" media="screen">
            body, html, svg { width: 100%; height: 100%; overflow: hidden; }
        </style>
    </head>
    <body>
    	<g:content tag="sidebar">
    	<g:if test="${ selectedPattern == i }">
    		<g:radio name="myGroup" value="-1" checked="true" onclick="SubmitFrm();"/>All Patterns
    	</g:if>
    	<g:else>
    		<g:radio name="myGroup" value="-1" onclick="SubmitFrm();"/>All Patterns
    	</g:else>
    	<% i=0 %>
		<g:each in="${patterns}">
			<g:if test="${ selectedPattern == i }">
				<g:radio name="myGroup" value="${ i}" onclick="SubmitFrm();" checked="true"/>${patterns[i].name}<div id="colorBox${i }" style="width:10px;height:10px; background-color: #000000;"></div>
			</g:if>
			<g:else>
				<g:radio name="myGroup" value="${ i}" onclick="SubmitFrm();"/>${patterns[i].name}<div id="colorBox${i }" style="width:10px;height:10px; background-color: #000000;"></div>
			</g:else>
			<g:javascript>
		      var series${i } = [];
		      <% j=0 %>
		      <g:each in="${patterns[i].timeSeriesPattern}">
		      	series${i }.push([${j }, ${it}]);
		      	<% j=j+1 %>
		      </g:each>
		      
		      var data${i } = [series${i }];
		
		      var options${i } = {
		          lines: { show: true },
		          points: { show: true },
		          //color: 'rgb(0, 0, 0)'
		        };
		    </g:javascript>
			<% i=i+1 %>
		</g:each>
		
		<g:textField name="neighborhood" value="1"/>
		<g:submitButton name="neighborhoodBtn" value="Update Neighborhood" onclick="SubmitFrm();" />
		<br/>
		<g:submitButton name="toggleRender" value="Toggle Layouting" onclick="ToggleRender();" />
		<br/>
		<g:submitButton name="saveLayout" value="Save Graph Layout" onclick="saveLayout();" />
		</g:content>
		<div style="position: relative; height: 100%; width: 90%; float:right; z-index: 1;" id="viva"></div>
    </body>
</html>