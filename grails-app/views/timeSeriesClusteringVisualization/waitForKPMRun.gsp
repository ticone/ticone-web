<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
<g:if test="${!finished }" >
<meta http-equiv="Refresh" content="3">
</g:if>
</head>
<body>
	<g:content tag="sidebarHide">true</g:content>
	<g:content tag="title"><h1>${title }</h1></g:content>
	<h2>${raw(text) }</h2>
</body>
</html>