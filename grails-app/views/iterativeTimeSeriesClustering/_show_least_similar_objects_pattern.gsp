<script type="text/javascript">
	$(function(){ // on dom ready
		loadHighcharts();
	}); // on dom ready

      function loadHighcharts() {
          $.ajax({
		 	dataType: "json",
		 	url: '${request.contextPath}/iterativeTimeSeriesClustering/get_pattern_least_similar_objects_as_JSON/${ clustering.sessionId }/${ iteration }/${ pattern }/${ percentage }/${ type}',
		 	success: function(allData) {

			 	window.allData = allData;
		 		
		 		// create highchart
				var chart_popup = new Highcharts.Chart({
			        chart: {
			            renderTo: 'patternLeastSimilarObjectsHighchartPopup',
			            type: 'line',
			            animation: false
			        },
			        credits:{enabled:false},
			        title: {
				        <g:if test="${type == "percentage"}">
			            	text: 'Least Similar Object Sets (' + ${params.percentage} + '%) of ${ p.name }',
			            </g:if>
			            <g:elseif test="${type == "rss"}">
			            	text: 'Least Similar Object Sets (RSS > ' + ${params.percentage} + ') of ${ p.name }',
			            </g:elseif>
			            <g:elseif test="${type == "pearson"}">
			            	text: 'Least Similar Object Sets (Pearson < ' + ${params.percentage} + ') of ${ p.name }',
			            </g:elseif>
			            x: -20 //center
			        },
			        yAxis: {
			            title: {
			                text: ''
			            },
			            plotLines: [{
			                value: 0,
			                width: 1
			            }],
			            //min: 0,
			            //max: 1
			        },
			        legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'top',
			            x: -10,
			            y: 100,
			            borderWidth: 0
			        },
			        series: [
			        ],
			        plotOptions: {
			        	line: {
			        		marker: {
			        			enabled: false
			        		}
			        	},
			        	series: {
			            	animation: false
			        	}
			        }
			    });
			    
		 		console.log("Adding object time series to highcharts");
		 		$.each(allData.objectTimeSeries, function(object, timeSeries) {
	 				chart_popup.addSeries({
	 					name: object,
	 					data: timeSeries,
	 					lineWidth: 1,
	 					color: 'rgba(0, 0, 0, 0.1)'
	 				},
	 				false);
			 		
					 console.log("...done");
				 });
				 chart_popup.redraw();
		 	}
		});
	};

	function deletePatternLeastSimilarObjects() {
    	$.ticone_queue.add(function() {
    		$('#patternLeastSimilarObjectsOuterPopup').bPopup().close();
    		$('.pattern' + ${params.pattern}).bPopup().close();
			show_modal('loading_modal', 'Deleting Least Similar Objects Of Cluster', 'Deleting ${params.percentage}% least similar objects of cluster ' + patternInternalIdToNumber[${params.pattern}] + ' ...');
		}, this);
		window.location.href = "${request.contextPath}/iterativeTimeSeriesClustering/async_delete_least_similar_objects_pattern/${params.id}/${params.iteration}/${params.pattern}/${params.percentage}/${type}";
	}
</script>

<div>
	<div id="patternLeastSimilarObjectsHighchartPopup"></div>
    	<g:submitButton name="deletePatternLeastSimilarObjects" value="Delete" onclick="deletePatternLeastSimilarObjects();"/><br />
</div>
