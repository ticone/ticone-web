<script type="text/javascript">
	$(function(){ // on dom ready
		loadHighcharts();
	}); // on dom ready

      function loadHighcharts() {
          $.ajax({
		 	dataType: "json",
		 	<g:if test="${pattern}">
		 	url: '${request.contextPath}/iterativeTimeSeriesClustering/get_object_pattern_similarity_histogram/${ clustering.sessionId }/${ iteration }/${ bins }/${type}?pattern=${pattern.id}',
		 	</g:if>
		 	<g:else>
		 	url: '${request.contextPath}/iterativeTimeSeriesClustering/get_object_pattern_similarity_histogram/${ clustering.sessionId }/${ iteration }/${ bins }/${type}',
		 	</g:else>
		 	success: function(allData) {

			 	window.allData = allData;

		 		// create highchart
				var chart_popup = new Highcharts.Chart({
			        chart: {
			            renderTo: 'objectPatternSimilarityHistogram',
			            type: 'column',
			            animation: false
			        },
			        credits:{enabled:false},
			        title: {
			            <g:if test="${type == "rss"}">
			            	text: 'Object Pattern Similarity Histogram (RSS)',
			            </g:if>
			            <g:elseif test="${type == "pearson"}">
			            	text: 'Object Pattern Similarity Histogram (Pearson Correlation)',
			            </g:elseif>
			            x: -20 //center
			        },
			        xAxis: {
				        categories: $.map($(allData).attr("bins"), function(val, i) {return (Math.round(val*1000)/1000).toString()})
				    },
			        yAxis: {
			            title: {
			                text: ''
			            }
			        },
			        series: [{
				        data: $(allData).attr("counts")
			        }],
			        plotOptions: {
			        	series: {
			            	animation: false
			        	},
				        column: {
				            groupPadding: 0,
				            pointPadding: 0,
				            borderWidth: 0
				        }
			        }
			    });

				 chart_popup.redraw();
		 	}
		});
	};
</script>

<div>
	<div id="objectPatternSimilarityHistogram"></div>
</div>
