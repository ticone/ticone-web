<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="sidebarTitle">
	Your Clusterings
	</g:content>
	<g:content tag="title">
	<h1>Your Clusterings</h1>
	</g:content>
	<g:content tag="sidebarClass">col-lg-3</g:content>
	<g:content tag="mainClass">col-lg-9</g:content>
	<g:content tag="sidebar">
	<p>Here you see a list of all clusterings you have performed so far.</p>
	</g:content>
	
	<table class="table">
		<thead>
			<tr>
			<th>ID</th>
			<th>Data Set</th>
			<th>Network</th>
			<th>Clustering Method</th>
			<th>Similarity Function</th>
			<th>Prototype Refinement Function</th>
			<th>Discretization Steps</th>
			<th>Date</th>
			</tr>
		</thead>
	<g:each in="${clusterings}" var="${clustering}">
		<tr>
			<td><g:link action="show_cytoscape" id="${clustering.sessionId}" >${clustering.sessionId }</g:link></td>
			<td>${clustering.dataSet.name }</td>
			<td>${clustering.graph != null ? clustering.graph.name : "---" }</td>
			<td>${clustering.clusteringMethod.name }</td>
			<td>${clustering.similarityFunction.name }</td>
			<td>${clustering.patternRefinementFunction.name }</td>
			<td>${clustering.discretizationSteps }</td>
			<td><g:formatDate date="${clustering.dateCreated }" /></td>
		</tr>
	</g:each>
	</table>
</body>
</html>