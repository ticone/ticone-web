<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
<script>
function PopupCenter(url, title) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    
	var w = width / 5 * 4;
	var h = height / 5 * 4;

    var left = ((width / 4) - (w / 5)) + dualScreenLeft;
    var top = ((height / 4) - (h / 5)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }

    return newWindow;
}

function showDataSetsPopup(url, title) {
	var newWindow = PopupCenter(url, title);

    newWindow.onload = function() {
    	newWindow.onunload =  function () {
        	//window.onfocus = function() {
	    	//	loadDataSets();
        	//};
        	$(window).one("focus", loadDataSets);
        };
    }
}

function showNetworksPopup(url, title) {
	var newWindow = PopupCenter(url, title);

    newWindow.onload = function() {
        
    	newWindow.onunload =  function () {
        	//window.onfocus = function() {
	    	//	loadNetworks();
        	//};
    		$(window).one("focus", loadNetworks);
        };
    }
}

function showFilterDataSetPopup() {
	var url = '${createLink(controller: 'timeSeriesDataSet', action: 'filter', id: '%s', params: [popup: 1])}'
	url = url.replace('%s',$("#tsDataSet").val());
	var title = 'Filter an Existing Data Set';
	console.log(url);
	console.log($("#tsDataSet").val());
	var newWindow = PopupCenter(url, title);

    newWindow.onload = function() {
        
    	newWindow.onunload =  function () {
        	//window.onfocus = function() {
	    	//	loadDataSets();
        	//};
        	$(window).one("focus", loadDataSets);
        };
    }
}

function loadDataSets() {
	$('#tsDataSet').find('option').remove();
    $.ajax({url: "${createLink(controller: 'iterativeTimeSeriesClustering', action: 'dataSetsAsJSON', params: [inGroups: true])}",
    	    success: function(data, textStatus, jqXHR) {
        	    var count = 0;
    	    	$.each(data, function(k, v) {
    	    		$("#tsDataSet").append($('<optgroup id="optGroup' + count + '"></optgroup>').attr("label", k));
            	    $.each(v, function(i, m) {
        	    		$("#optGroup" + count).append($('<option></option>').attr("value", m.id).text(m.name));
    	            	if (k == "TiCoNE")
        	    			$('#tsDataSet').val(m.id);
            		});
            		count++;
    	    	});
    	    	if (count > 0) {
        	    	$('#btnFilterDataSet').prop('disabled', false);
        	    }
        	    $('.selectpicker').selectpicker('refresh');
        	 }
    });
}


function loadNetworks() {
	$('#graph').find('option').remove();
    $('#graph').append($('<option></option>').attr("value", "").text("Do not use a network"));
    $.ajax({url: "${createLink(controller: 'iterativeTimeSeriesClustering', action: 'networksAsJSON', params: [inGroups: true])}",
    	    success: function(data, textStatus, jqXHR) {
        	    var count = 0;
    	    	$.each(data, function(group, datasets) {
    	    		$("#graph").append($('<optgroup id="optGroupGraph' + count + '"></optgroup>').attr("label", group));
	        	    $.each(datasets, function(i, m) {
	        	    	$("#optGroupGraph" + count).append($('<option></option>').attr("value", m.id).text(m.name));
		            	if (group == "TiCoNE")
	    	    			$('#graph').val(m.id);
	            	});
	            	count++;
    	    	});
        	    $('.selectpicker').selectpicker('refresh');
        	 }
    });
}

$(document).ready(function() {
	//$('form').submit(function(event) {
		//show_modal('modal', 'Initial Clustering', 'Initial Clustering is being performed ...');
	//});
	loadDataSets();
	loadNetworks();
});

</script>
</head>
<body>
	<g:content tag="sidebarTitle">
	Initialize a Clustering
	</g:content>
	<g:content tag="title">
	<h1>Initialize a Clustering</h1>
	</g:content>
	<g:content tag="sidebarClass">col-lg-3</g:content>
	<g:content tag="mainClass">col-lg-9</g:content>
	<g:content tag="sidebar">
	<p>Here you can prepare a time-series clustering by uploading a time-series file and an (optional) graph-edge file. Additionally, you need to specify the desired clusters.</p>
	<dl>
	  <dt>Time-Series File</dt><dd>Explain explain explain</dd>
	  <dt>Graph-Edge File</dt><dd>Explain explain explain</dd>
	  <dt>Number of Clusters</dt><dd>Explain explain explain</dd>
	</dl> 
	</g:content>
	
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static"  data-keyboard="false">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel"></h4>
		      </div> <!-- /modal-header -->
		      <div class="modal-body">
		      <asset:image src="ajax-loader.gif"/> 
		      </div> <!-- /modal-body -->
		  </div> <!-- /modal-content -->
		</div> <!-- /modal-dialog -->
	</div> <!-- /modal -->
	
	<g:if test="${flash.error}">
	  <div class="alert alert-danger" style="display: block">${flash.error}</div>
	</g:if>
	<g:uploadForm action="async_upload" class="form-horizontal">
		<div class="panel panel-default">
		  <div class="panel-heading">Inputs</div>
			  <div class="panel-body">
			    <div class="form-group">
					<label for="tsDataSet" class="col-sm-3 control-label">Time-Series Data Set</label>
					<div class="col-sm-6">
						<div class="input-group">
							<select id="tsDataSet" class="form-control selectpicker" name="tsDataSet">
							</select>
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" title="Refresh data sets" onclick="loadDataSets();">
									<i class="fa fa-refresh"></i>
								</button>
								<button id="btnFilterDataSet" type="button" class="btn btn-default" title="Filter selected data set" 
									onclick="showFilterDataSetPopup();"
									disabled>
									<i class="fa fa-filter"></i>
								</button>
							</span>
						</div>
					</div>
					<div class="col-sm-3">
						<a class="form-control-static"
							href="#" 
							onclick="showDataSetsPopup('${createLink(controller: 'timeSeriesDataSet', action: 'create', params: [popup: 1])}','Upload your Time-Series Data Set');">Use your own time-series data ...</a>
					</div>
				</div>
				<div class="form-group">
					<label for="graph" class="col-sm-3 control-label">Network</label>
					<div class="col-sm-6">
						<div class="input-group">
							<select id="graph" class="form-control selectpicker" name="graph">
							</select>
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" title="Refresh networks" onclick="loadNetworks();">
									<i class="fa fa-refresh"></i>
								</button>
							</span>
						</div>
					</div>
					<div class="col-sm-3">
						<a class="form-control-static"
							href="#" 
							onclick="showNetworksPopup('${createLink(controller: 'graph', action: 'create', params: [popup: 1])}','Upload your network');">Use your own network ...</a>
					</div>
				</div>
		  	</div>
		</div>
		
		<div class="panel panel-default">
		  <div class="panel-heading">Parameters</div>
			  <div class="panel-body">
					<div class="form-group">
						<label for="numberPatterns" class="col-sm-3 control-label">Number of Clusters</label>
						<div class="col-sm-6">
							<g:field type="number" min="2" max="1000" value="10"
									id="numberPatterns" name="numberPatterns" class="form-control" />
						</div>
					</div>
		  	</div>
		</div>
		
		<div class="panel panel-default">
		  <div class="panel-heading">Advanced Parameters</div>
			  <div class="panel-body">
					<div class="form-group">
						<label for="clusteringMethod" class="col-sm-3 control-label">Clustering Method</label>
						<div class="col-sm-6">
							<g:select name="clusteringMethod" from="${clusteringMethods }" optionKey="id" optionValue="name" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="steps" class="col-sm-3 control-label">Discretization Steps</label>
						<div class="col-sm-6">
							<g:field type="number" min="1" max="100" value="10"
									id="steps" name="steps" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="simFunction" class="col-sm-3 control-label">Similarity Function</label>
						<div class="col-sm-6">
							<g:select name="simFunction" from="${simFunctions }" optionKey="id" optionValue="name" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="objectIntersect" class="col-sm-3 control-label">Only keep objects present in the network</label>
						<div class="col-sm-6">
							<g:field type="checkbox" id="objectIntersect" name="objectIntersect" class="form-control" checked="true" />
						</div>
					</div>
					<div class="form-group">
						<label for="patternRefinementFunction" class="col-sm-3 control-label">Pattern Refinement Function</label>
						<div class="col-sm-6">
							<g:select name="patternRefinementFunction" from="${refFunctions }" optionKey="id" optionValue="name" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="randomSeed" class="col-sm-3 control-label">Random Seed</label>
						<div class="col-sm-6">
							<g:field type="number" value="${ System.currentTimeMillis() }" id="randomSeed" name="randomSeed" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="permutations" class="col-sm-3 control-label">Number Permutations for p-value calculation of initial clustering</label>
						<div class="col-sm-6">
							<g:field type="number" value="0" id="permutations" name="permutations" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="refinementPermutations" class="col-sm-3 control-label">Number Permutations for p-value calculation of later iterations</label>
						<div class="col-sm-6">
							<g:field type="number" value="100" id="refinementPermutations" name="refinementPermutations" class="form-control" />
						</div>
					</div>
				</div>
			</div>
		<g:submitButton name="submit" />
	</g:uploadForm>
</body>
</html>