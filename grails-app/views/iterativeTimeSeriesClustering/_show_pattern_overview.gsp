<script type="text/javascript">
	$(function(){ // on dom ready
		loadHighcharts();
	}); // on dom ready
    function loadHighcharts() {
        $.ajax({
		 	dataType: "json",
		 	url: '${request.contextPath}/timeSeriesClustering/getPatternTimeSeriesObjectsAsJSON/${ clustering.sessionId }',
		 	success: function(allData) {
		 		$.each(allData, function(pattern, data) {
		 			var patternNo = pattern;
			 		console.log("Adding object time series to highcharts");
			 		console.log(patternNo);
			 		var color = colors[parseInt(patternNo)%colors.length];
			 		
			 		var series = window['series' + patternNo];
			 		
			 		// create highchart
					var chart = new Highcharts.Chart({
				        chart: {
				            renderTo: 'highchartOverview' + patternNo,
				            type: 'line',
				            marginRight: 0,
				            marginBottom: 0,
				            animation: false,
				            reflow: true
				        },
				        credits:{enabled:false},
				        title: {
				            text: '',
				            x: -20 //center
				        },
				        yAxis: {
				        	maxPadding: 0,
						   lineWidth: 0,
						   minorGridLineWidth: 0,
						   minorTickLength: 0,
						   tickLength: 0,
				            title: {
				                text: ''
				            },
				            labels: {
				            	enabled: false
				            },
				            plotLines: [{
				                value: 0,
				                width: 1,
				                color: color
				            }],
				            //min: 0,
				            //max: 1
				        },
				        xAxis: {
						   lineWidth: 0,
						   minorGridLineWidth: 0,
						   minorTickLength: 0,
						   tickLength: 0,
				            labels: {
				            	enabled: false
				            }
				        },
				        legend: {
				            layout: 'vertical',
				            align: 'right',
				            verticalAlign: 'top',
				            x: -10,
				            y: 100,
				            borderWidth: 0
				        },
				        series: [
				        ],
				        plotOptions: {
				        	line: {
				        		marker: {
				        			enabled: false
				        		}
				        	},
				        	series: {
				            	animation: false
				        	}
				        }
				    });
		 		
		 			$.each(data, function(object, timeseries) {
		 				chart.addSeries({
		 					name: object,
		 					data: timeseries,
		 					lineWidth: 1,
		 					color: color
		 				},
		 				false);
		 			});
			 		
				    // series for the patterns
	              	chart.addSeries({
	              		name: "Pattern " + pattern,
	              		data: series,
	              		lineWidth: 2,
	              		color: '#000'
	              	},
	              	true);
					 console.log("...done");
		 	}) // end each
		 } // end success
	}); // end ajax
	}
</script>

<div class="container">
	<div class="row">
		<g:each var="p" in="${patterns}">
			<div class="col-md-3">
			    <div class="thumbnail">
			      <div class="highchartPattern" id="highchartOverview${p.number}" style="width: 200px; height: 100px;"></div>
			      <div class="caption">
			        <h3>Thumbnail label</h3>
			        <p>...</p>
			        <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
			      </div>
			    </div>
		  </div>
		</g:each>
	</div>
</div>
