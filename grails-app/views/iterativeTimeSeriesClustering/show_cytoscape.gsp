<%@page	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />

<g:javascript>
        	var completeGraph;
        	var graph;
        	var renderer;
        	var layout;
        	var graphics;
        	var patterns = [<g:each var="p" in="${patternOrder}">'${p.name}',</g:each>];
        	var patternIndexToId = {<g:each status="i" var="p" in="${patterns}">${ i }: ${ p.number },</g:each>}
        	var patternInternalIdToNumber = {<g:each status="i" var="p" in="${patterns}">${ p.id }: ${ p.number },</g:each>}
        	var patternOrder = [<g:each var="p" in="${patternOrder}">'${p.id}',</g:each>];
			var selectedPatternName;
        	var colors = ["#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
                        "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
                        "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
                        "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
                        "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
                        "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
                        "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
                        "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",
                        "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
                        "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
                        "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
                        "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
                        "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
                        "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
                        "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
                        "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", "#012C58"];
        	
        	// get a list of nodes that follow any pattern most strongly
        	var cyStylesheet;

        	function weightHexColors(color1, color2, weight) {
		        var color11 = parseInt(color1.substring(1,3), 16)*weight;
		        var color12 = parseInt(color1.substring(3,5), 16)*weight;
		        var color13 = parseInt(color1.substring(5,7), 16)*weight;

		        var color21 = parseInt(color2.substring(1,3), 16)*(1-weight);
		        var color22 = parseInt(color2.substring(3,5), 16)*(1-weight);
		        var color23 = parseInt(color2.substring(5,7), 16)*(1-weight);
		        
		        var newColor1 = Math.round(color11+color21).toString(16);
		        while (newColor1.length < 2) { newColor1 = '0' + newColor1; }
		        var newColor2 = Math.round(color12+color22).toString(16);
		        while (newColor2.length < 2) { newColor2 = '0' + newColor2; }
		        var newColor3 = Math.round(color13+color23).toString(16);
		        while (newColor3.length < 2) { newColor3 = '0' + newColor3; }
		        
		        return '#' + newColor1 + newColor2 + newColor3;
          }
          
          function hexColorToRgbString(hexColor, alpha) {
          	if (hexColor.startsWith('#')) {
          		hexColor = hexColor.substring(1);
          	}
          	var r = parseInt(hexColor.substring(0,2),16);
          	var g = parseInt(hexColor.substring(2,4),16);
          	var b = parseInt(hexColor.substring(4,6),16);
          	var res = 'rgba(' + r + ',' + g + ',' + b + ',' + alpha + ')';
          	return res;
          }
          
          <g:if test="${!readOnlyMode }">
           <!-- begin write mode -->
          function setKeepPattern(pattern) {
          		var keep = $('#keepPattern' + pattern).prop("checked");
          		$.ajax({
					type: 'POST',
				    url: '${createLink(controller: 'timeSeriesClusteringVisualization', action: 'set_keep_pattern', id: vis.id)}' + '/' + pattern + '/' + keep,
				    data: '',
				    contentType: 'application/json',
				    dataType: 'json'});
          }
			
		    function saveLayout(name) {
				var locationHash = {};
				
				window.cyto.nodes().forEach(function(node) {
					var pos = node.position();
					locationHash[node.data('dbid')] = [pos.x, pos.y];
				});
				//locationHash['transform'] = graphics.getSvgRoot().childNodes[0].getAttribute('transform')
				locationHash['transform'] = [0,0,0,0];
				
				console.log(locationHash);
			    
			    $.ajax({
					type: 'POST',
				    url: '${createLink(controller: 'timeSeriesClusteringVisualization', action: 'save_layout', id: vis.id)}' + '/' + name,
				    data: JSON.stringify(locationHash),
				    contentType: 'application/json',
				    success: function(data, textStatus, jqXHR ) {
		    			$('#layout_name_popup .modal-body').text('The layout has been saved.');
		    			$('#layout_name_popup').modal('show');
		    			}
				    });
			}
			
			function changeGraphLayout(graphLayout) {
				loadGraphNodePositions(graphLayout);
				
				// save this as the future layout
				$.ajax({
	 			 	dataType: "json",
	 			 	url: '${createLink(controller: 'timeSeriesClusteringVisualization', action: 'change_layout', id: vis.id)}' + '/' + graphLayout,
	 			 	success: function(data) {
	 			 		console.log("Setting node positions");
	 			 		$.each(data, function(index, value) {
	 			 			window.cyto.getElementById(value["id"]).position(value["location"]);
	 			 		});
	 					 console.log("..done");
	 					 // fit to first ten nodes; otherwise it will take forever
	 					 //window.cyto.fit(window.cyto.nodes().slice(0,10));
	 					 window.cyto.fit(window.cyto.nodes());
	 					 console.log("viewport fitted to nodes");
	 			 	}
	 			 })
			}
			
			function mergeSelectedPatterns() {
				var checked = $('.checkbox-merge:checked');
				if (checked.length < 2)
					return;
				var patternIds = $.map(checked, function(val, i) {
					return $(val).attr("pattern");
				});
				var patterns = patternIds.join(',');
				
				console.log("Merging patterns " + patterns);
				
            	$.ticone_queue.add(function() {
					show_modal('loading_modal', 'Merging Clusters', 'Merging clusters ' + $.map(patternIds, function(val, i) {return patternInternalIdToNumber[val]}).join(',') + ' ...');
				}, this);
				
				window.location = "${request.contextPath}/iterativeTimeSeriesClustering/merge_patterns/${params.id}/${params.iteration}/" + patterns;
			}

			function showAddClusterPopup() {
				$('#addNewPatternPopup').modal('show');
			}
			
			function addNewPattern() {
				var chart = $('#addNewPatternHighchart').highcharts();
				//$('#addNewPatternPopup').modal('hide');
				
				var pattern = $.map(chart.series[0].data, function(p, ind) {return p.y});
				var pattern_str = JSON.stringify(pattern);
				
            	$.ticone_queue.add(function() {
					show_modal('loading_modal', 'Add New Cluster', 'Adding new cluster with predefined prototype ...');
				}, this);
				
				$('<input>').attr({
				    type: 'hidden',
				    id: 'pattern',
				    name: 'pattern',
				    value: pattern_str
				}).appendTo('#submitNewPattern');
				
				$('#submitNewPattern').submit();
			}

            function showSplitPopup(pattern) {
            	$('#splitPatternPopup' + pattern).bPopup(
                    	//{
                    //contentContainer:'.content',
                    //loadUrl: 'test.html' //Uses jQuery.load()
                //}
                );
            }

            function showSplitClusteringPopup(pattern) {
            	$('#splitPatternClusteringPopup' + pattern).bPopup(
                    	//{
                    //contentContainer:'.content',
                    //loadUrl: 'test.html' //Uses jQuery.load()
                //}
                );
            }

            function showDeletePopup(pattern) {
            	$('#deletePatternPopup' + pattern).bPopup(
                    	//{
                    //contentContainer:'.content',
                    //loadUrl: 'test.html' //Uses jQuery.load()
                //}
                );
            }

            function showCleanUpPopup(pattern) {
            	$('#cleanUpObjectsOuterPopupPattern' + pattern).bPopup();
                return false;
            }

            function showCleanUpObjectsPopup() {
            	$('#cleanUpObjectsOuterPopup').modal('show');
                return false;
            }

            function showLeastSimilarObjectsPopup() {
            	var type = $('input[name=cleanUpType]:checked').val();
            	console.log(type);
            	switch (type) {
	            	case '1':
	            		$('#leastSimilarObjectsOuterPopup').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_least_similar_objects/${params.id}/${params.iteration}/' + $('#leastFittingObjectsPercentage').val() + '/percentage'
		                });
		                break;
	            	case '2':
	            		$('#leastSimilarObjectsOuterPopup').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_least_similar_objects/${params.id}/${params.iteration}/' + $('#leastFittingObjectsRSSThreshold').val() + '/rss'
		                });
		                break;
	            	case '3':
	            		$('#leastSimilarObjectsOuterPopup').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_least_similar_objects/${params.id}/${params.iteration}/' + $('#leastFittingObjectsPearsonThreshold').val() + '/pearson'
		                });
		                break;
	            }
                return false;
            }
            
            function showLeastSimilarObjectsPopupPattern(pattern) {
            	var type = $('input[name=cleanUpTypePattern' + pattern + ']:checked').val();
            	console.log(type);
            	switch (type) {
	            	case '1':
	            		$('#patternLeastSimilarObjectsOuterPopup').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_least_similar_objects_pattern/${params.id}/${params.iteration}/' + pattern + '/' + $('#leastFittingObjectsPercentagePattern' + pattern).val() + '/percentage'
		                });
		                break;
	            	case '2':
	            		$('#patternLeastSimilarObjectsOuterPopup').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_least_similar_objects_pattern/${params.id}/${params.iteration}/' + pattern + '/' + $('#leastFittingObjectsRSSThresholdPattern' + pattern).val() + '/rss'
		                });
		                break;
	            	case '3':
	            		$('#patternLeastSimilarObjectsOuterPopup').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_least_similar_objects_pattern/${params.id}/${params.iteration}/' + pattern + '/' + $('#leastFittingObjectsPearsonThresholdPattern' + pattern).val() + '/pearson'
		                });
		                break;
	            }
            }
            
            function showObjectPatternSimilarityHistogramPopup(type) {
            	switch (type) {
	            	case 'rss':
	            		$('#objectPatternSimilarityHistogramPopup').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_object_pattern_similarity_histogram/${params.id}/${params.iteration}/100/rss'
		                });
		                break;
	            	case 'pearson':
	            		$('#objectPatternSimilarityHistogramPopup').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_object_pattern_similarity_histogram/${params.id}/${params.iteration}/100/pearson'
		                });
		                break;
	            }
                return false;
            }
            
            function showObjectPatternSimilarityHistogramPopupPattern(type, pattern) {
            	switch (type) {
	            	case 'rss':
	            		$('#objectPatternSimilarityHistogramPopupPattern').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_object_pattern_similarity_histogram/${params.id}/${params.iteration}/100/rss?pattern=' + pattern
		                });
		                break;
	            	case 'pearson':
	            		$('#objectPatternSimilarityHistogramPopupPattern').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_object_pattern_similarity_histogram/${params.id}/${params.iteration}/100/pearson?pattern=' + pattern
		                });
		                break;
	            }
                return false;
            }
            
            
            function showLeastConservedObjectsPopup(pattern) {
            	var type = $('input[name=cleanUpType]:checked').val();
            	console.log(type);
            	switch (type) {
	            	case '1':
	            		$('#leastConservedObjectsOuterPopup').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_least_conserved_objects/${params.id}/${params.iteration}/' + $('#leastFittingObjectsPercentage').val() + '/percentage'
		                });
		                break;
	            	case '2':
	            		$('#leastConservedObjectsOuterPopup').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_least_conserved_objects/${params.id}/${params.iteration}/' + $('#leastFittingObjectsRSSThreshold').val() + '/rss'
		                });
		                break;
	            	case '3':
	            		$('#leastConservedObjectsOuterPopup').bPopup({
		                    loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_least_conserved_objects/${params.id}/${params.iteration}/' + $('#leastFittingObjectsPearsonThreshold').val() + '/pearson'
		                });
		                break;
	            }
                return false;
            }

            function deletePattern(pattern) {
            	$.ticone_queue.add(function() {
	           		$('.pattern' + pattern).each(function(i) {
	           			$(this).bPopup().close();
	           		});
					show_modal('loading_modal', 'Deleting Cluster', 'Deleting cluster ' + patternInternalIdToNumber[pattern] + ' excluding objects ...');
				}, this);
		        window.location = "${request.contextPath}/iterativeTimeSeriesClustering/delete_pattern/${params.id}/${params.iteration}/" + pattern;
            }
            
            function deleteObjects(pattern) {
            	$.ticone_queue.add(function() {
	           		$('.pattern' + pattern).each(function(i) {
	           			$(this).bPopup().close();
	           		});
					show_modal('loading_modal', 'Deleting Cluster Objects Only', 'Deleting objects of cluster ' + patternInternalIdToNumber[pattern] + ' ...');
				}, this);
		        window.location = "${request.contextPath}/iterativeTimeSeriesClustering/delete_pattern_objects/${params.id}/${params.iteration}/" + pattern;
            }

            function deletePatternAndObject(pattern) {
            	$.ticone_queue.add(function() {
	           		$('.pattern' + pattern).each(function(i) {
	           			$(this).bPopup().close();
	           		});
					show_modal('loading_modal', 'Deleting Cluster Including Objects', 'Deleting cluster ' + patternInternalIdToNumber[pattern] + ' including objects ...');
				}, this);
            	window.location = "${request.contextPath}/iterativeTimeSeriesClustering/delete_pattern_and_objects/${params.id}/${params.iteration}/" + pattern;
            }

            function splitPatternClustering(pattern) {
                var numberClusters = $('#splitNumberClusters' + pattern).val();
            	$.ticone_queue.add(function() {
	           		$('.pattern' + pattern).each(function(i) {
	           			$(this).bPopup().close();
	           		});
					show_modal('loading_modal', 'Splitting Cluster', 'Splitting cluster ' + patternInternalIdToNumber[pattern] + ' by clustering into ' + numberClusters + ' subclusters ...');
				}, this);
            	window.location = "${request.contextPath}/iterativeTimeSeriesClustering/async_split_pattern_clustering/${params.id}/${params.iteration}/" + pattern + "/" + numberClusters;
            }

            function splitPatternMostDissimilarObjects(pattern) {
                console.log("Splitting pattern " + pattern);
            	$.ticone_queue.add(function() {
	           		$('.pattern' + pattern).each(function(i) {
	           			$(this).bPopup().close();
	           		});
					show_modal('loading_modal', 'Splitting Cluster', 'Splitting Cluster ' + patternInternalIdToNumber[pattern] + ' by two most dissimilar objects ...');
				}, this);
				window.location = "${request.contextPath}/iterativeTimeSeriesClustering/async_split_pattern_most_dissimilar_objects/${params.id}/${params.iteration}/" + pattern;
            }

            function doIteration() {
            	$.ticone_queue.add(function() {
					show_modal('loading_modal', 'Performing Iteration', 'Iteration in progress ...');
				}, this);
            	window.location = "${request.contextPath}/iterativeTimeSeriesClustering/async_do_iteration/${params.id}/${params.iteration}";
            }

            function doIterationsUntilConvergence() {
            	$.ticone_queue.add(function() {
						show_modal('loading_modal', 'Performing Iterations Until Convergence', 'Iterations in progress ...');
					}, this);
            	window.location = "${request.contextPath}/iterativeTimeSeriesClustering/async_do_iterations_until_convergence/${params.id}/${params.iteration}";
            }
            

			<g:if test="${vis.graphLayout && vis.graphLayout.graph.nodes.size() <= 500}">

			function applyCircleLayout() {
				var options = {
						  name: 'circle',

						  fit: true, // whether to fit the viewport to the graph
						  padding: 30, // the padding on fit
						  boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
						  avoidOverlap: true, // prevents node overlap, may overflow boundingBox and radius if not enough space
						  radius: undefined, // the radius of the circle
						  startAngle: 3/2 * Math.PI, // where nodes start in radians
						  sweep: undefined, // how many radians should be between the first and last node (defaults to full circle)
						  clockwise: true, // whether the layout should go clockwise (true) or counterclockwise/anticlockwise (false)
						  sort: undefined, // a sorting function to order the nodes; e.g. function(a, b){ return a.data('weight') - b.data('weight') }
						  animate: false, // whether to transition the node positions
						  animationDuration: 500, // duration of animation in ms if enabled
						  animationEasing: undefined, // easing of animation if enabled
						  stop: undefined, // callback on layoutstop,
							ready: function() {
								$('#startedLayouting').hide();
								$('#finishedLayouting').show();
								$('#finishedLayouting').fadeOut({duration: 4000});
							}
						};

	        	$('#finishedLayouting').hide();
				$('#startedLayouting').show();
				window.cyto.layout(options);
			}
			
			function applyForceLayout() {
				applyCircleLayout();
				var options = {
					name: 'cose-bilkent' ,
					animate: false,
					idealEdgeLength: 60,
					edgeElasticity: 0.01,
					gravity: 0.0000001,
					nodeRepulsion: 8000,
					ready: function() {
						$('#startedLayouting').hide();
						$('#finishedLayouting').show();
						$('#finishedLayouting').fadeOut({duration: 4000});
					}
				};
	        	$('#finishedLayouting').hide();
				$('#startedLayouting').show();
				window.cyto.layout(options);
			}
			</g:if>
			

	
    function addNewClusterHighchart() {
 		// create highchart
		var chart = new Highcharts.Chart({
	        chart: {
	            renderTo: 'addNewPatternHighchart',
	            type: 'line',
	            marginRight: 0,
	            marginBottom: 0,
	            animation: false,
	            reflow: true
	        },
	        credits:{enabled:true},
	        title: {
	            text: '',
	            x: -20 //center
	        },
	        yAxis: {
	        	maxPadding: 1.0,
	        	minPadding: 1.0,
	            title: {
	                text: ''
	            },
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#000'
	            }],
	            //min: 0,
	            //max: 1
	        },
	        legend: {
	            enabled: false
	        },
	        series: [
	        ],
	        plotOptions: {
	        	series: {
	            	animation: false,
	            	draggableY: true,
	            	cursor: 'ns-resize'
	        	}
	        }
	    });
	    
	    var series = [];
	    for (var i=0; i < ${patterns[0].timeSeriesPattern.length}; i++) {
	    	series.push(0.0);
	    }
	
	    // series for the patterns
            	chart.addSeries({
            		name: "New Cluster",
            		data: series,
            		lineWidth: 2,
            		color: '#000'
            	},
            	true);
	}
			
           <!-- /end write mode -->
          </g:if>
			
			function loadGraphNodePositions(graphLayout) {
				// manually set the positions of the nodes from previous layoutting
	 			 $.ajax({
	 			 	dataType: "json",
	 			 	async: false,
	 			 	url: '${request.contextPath}/graphLayout/nodePositionsAsJSON/' + graphLayout,
	 			 	success: function(data) {
	 			 		console.log("Setting node positions");
	 			 		$.each(data, function(index, value) {
	 			 			window.cyto.getElementById(value["id"]).position(value["location"]);
	 			 		});
	 					 console.log("..done");
	 					 // fit to first ten nodes; otherwise it will take forever
	 					 //window.cyto.fit(window.cyto.nodes().slice(0,10));
	 					 window.cyto.fit(window.cyto.nodes());
	 					 console.log("viewport fitted to nodes");
	 			 	}
	 			 })
			}
			
			function showKPMPopup() {
				var checked = $('.checkbox-kpm:checked');
				if (checked.length < 1)
					return;
				var patterns = $.map(checked, function(val, i) {
					return $(val).attr("pattern");
				}).join(',');
            	$('#kpmOuterPopup').modal('show');
			}

			function kpmSelectedPatterns() {
				var checked = $('input[type="checkbox"]:checked');
				if (checked.length < 1)
					return;
				var patterns = $.map(checked, function(val, i) {
					return $(val).attr("pattern");
				}).join(',');
				
				var k = $('#kpm_k').val();
				var n = $('#kpm_n').val();
				var nodesNotPresent = $('#kpm_objectsNotPresent').val();
				console.log(nodesNotPresent);
				
				window.open("${request.contextPath}/timeSeriesClusteringVisualization/submitClusteringToKPMWeb/${ vis.id }/" + k + '/' + n + '/' + nodesNotPresent + '/' + patterns);
			}

        	function updateSelectedPattern() {
            	$.ticone_queue.add(function() {
					show_modal('loading_modal', 'Updating Network Visualization', 'Coloring network nodes ...');
				}, this);
        	
            	$.ticone_queue.add(function() {
	            	if ($('input:radio[name=myGroup]:checked').val() >= 0) {
	            		selectedPatternName = patterns[$('input:radio[name=myGroup]:checked').val()];
	            		selectedPatternColor = colors[patternIndexToId[$('input:radio[name=myGroup]:checked').val()]];
	            	}
	            	else
	            		selectedPatternName = "All";
	            	console.log("Pattern selected: " + selectedPatternName);
	
	            	console.log("Updating style ...");
	            	// 'background-color': 'mapData('+selectedPatternName+', ${String.format("%.2f",0.0)}, ${String.format("%.2f",vis.clustering.maxCoeff)}, white, ' + selectedPatternColor + ')',
	            	cyStylesheet = cytoscape.stylesheet()
				    .selector('node')
				      .css({
				        'width': function(ele) {
				          // if the node is similar to any of the patterns, we make it bigger;
						 	for (var i = 0; i < patterns.length; i++) {
						 		var p = patterns[i];
						 		if (ele.data().hasOwnProperty(p))
						 			return '60px';
						 	}
					        return '30px';
				      	 },
				        'height': function(ele) {
				          // if the node is similar to any of the patterns, we make it bigger;
						 	for (var i = 0; i < patterns.length; i++) {
						 		var p = patterns[i];
						 		if (ele.data().hasOwnProperty(p))
						 			return '60px';
						 	}
					        return '30px';
				      	 },
				      	'font-size': function(ele) {
				          // if the node is similar to any of the patterns, we make it bigger;
						 	for (var i = 0; i < patterns.length; i++) {
						 		var p = patterns[i];
						 		if (ele.data().hasOwnProperty(p))
						 			return '12';
						 	}
					        return '6';
				      	 },
				        'min-zoomed-font-size': 4,
				        'content': 'data(id)',
				        'background-color': function(ele) {
					        var patternColor = selectedPatternColor;
					        var patternName = selectedPatternName;
					        
					        var patternColorMinWeight = ${vis.clustering.minCoeff};
					        var patternColorMaxWeight = ${vis.clustering.maxCoeff};
					        var coeff;
					        if (patternName == "All") {
						    	// we color the node according to that pattern, to which it has the largest similarity
						    	var maxPattern;
						    	var maxSim;
								for (var i = 0; i < patterns.length; i++) {
									var p = patterns[i];
									if (ele.data().hasOwnProperty(p)) {
							        	var coeff = parseFloat(ele.data()[p])
							        	if (undefined == maxSim || coeff > maxSim) {
								        	maxSim = coeff;
								        	maxPattern = i;
								       }
							       }
								}
								if (undefined != maxSim) {
									patternColor = colors[patternIndexToId[maxPattern]];
									patternName = patterns[maxPattern];
								}
							 }
	
					        if (ele.data().hasOwnProperty(patternName)) {
					        	coeff = parseFloat(ele.data()[patternName])
					        }
					        else
					        	coeff = patternColorMinWeight;
					        	
					        var patternColorWeight = Math.max(0,
					        	Math.min(1, 
					        		coeff-patternColorMinWeight)/(patternColorMaxWeight-patternColorMinWeight));
					        var hexStr = weightHexColors(patternColor, '#FFFFFF', patternColorWeight);
					        return hexStr;
					        },
				        'border-width': '2px',
				        'text-valign': 'center',
				        'visibility': function(ele) {
				        		return 'visible'
					        }
				      })
				    .selector('edge')
				      .css({
				        'width': 4,
				        'opacity': 1,
				        'line-color': '#666666',
				        'visibility': function(ele) {
			        		return 'visible'
				        }
				      })
				    .selector(':selected')
				      .css({
				        'background-color': 'black',
				        'line-color': 'black',
				        'target-arrow-color': 'black',
				        'source-arrow-color': 'black',
				        'opacity': 1
				      })
				    .selector('.faded')
				      .css({
				        'opacity': 0.25,
				        'text-opacity': 0
				      });
	
	            	
	            	if (typeof window.cyto != 'undefined') {
	  			      
	            		window.cyto.style(cyStylesheet);
	            	}
				}, this);
            	
            	$.ticone_queue.add(function() {
					hide_modal('loading_modal');
				}, this);
            }

            function showPatternPopup(pattern) {
            	$('#patternPopup' + pattern).bPopup(
                    	//{
                    //contentContainer:'.content',
                    //loadUrl: 'test.html' //Uses jQuery.load()
                //}
                );
            }
            
            function showPatternOverview() {
            	$('#patternOverviewPopup').modal('show');
            }

            function changeIteration() {
                var chosenIteration = $('select[name=iteration] option:selected').val();
                
            	$.ticone_queue.add(function() {
					show_modal('loading_modal', 'Loading Iteration', 'Changing Iteration ...');
				}, this);
                
            	window.location = "${request.contextPath}/iterativeTimeSeriesClustering/show_cytoscape/${params.id}/" + chosenIteration;
            }
        
		    function SubmitFrm(){
		        var patternNo = $('input:radio[name=myGroup]:checked').val();
		        window.location = "${request.contextPath}/iterativeTimeSeriesClustering/show_cytoscape/${params.id}/" + patternNo;
		    }
		    
			function adjustmodal() {
			    var height = $(window).height() - 155; //value corresponding to the modal heading + footer
			    $(".modal-scroll").css({"height":height,"overflow-y":"auto"});
			}

	        $(function(){ // on dom ready
	        	$('[data-toggle="tooltip"]').tooltip({
	        		delay: {
	        			show: 500
	        		},
	        		container: 'body'
	        	});
	        	
				$(document).ready(adjustmodal);
				$(window).resize(adjustmodal);
	        	
				$.ticone_queue.add(function() {
					show_modal('loading_modal', 'Loading', 'Loading Clustering, Network and Creating Visualizations ...');
				}, this);
	        	
	        	$.ticone_queue.add(updateSelectedPattern, this);
	        	for (var p = 0; p < patterns.length; p++) {
	        		$('#colorBox' + p).css("background-color", colors[p]);
	        	};
	        	
	        	loadHighcharts();
	        	
	        	<g:if test="${vis.graphLayout && vis.graphLayout.graph.nodes.size() <= 500}">
	        	$.ticone_queue.add(renderCytoscape, this);
	        	</g:if>
	        	
	     		<g:if test="${!readOnlyMode }">
	           <!-- begin write mode -->
	        	$('#startedLayouting').hide();
	        	$('#finishedLayouting').hide();
	        	
	        	<g:if test="${ convergence }">
	     			$.ticone_queue.add(doIterationsUntilConvergence, this);
	     		</g:if>
	     		
				$('#addNewPatternPopup').modal({
                show: false
               });
				$('#addNewPatternPopup').one('shown.bs.modal', function() {
		        	addNewClusterHighchart();
				});
				
				$('#saveLayoutBtn').click(function() {
		    		var name = $('#layout_name').val();
		    		if (name == '') {
		    			$('#layout_name_popup .modal-body').text('Please provide a name for the layout.');
		    			$('#layout_name_popup').modal('show');
		    		} else {
						saveLayout(name);
					}
				});
				
				$('.checkbox-merge').click(function() {
					var checked = $('.checkbox-merge:checked');
					if (checked.length < 2)
						$('#mergePatternBtn').prop('disabled', true);
					else
						$('#mergePatternBtn').prop('disabled', false);
				});
	           <!-- /end write mode -->
	        	</g:if>
	     		
	     		$(function() {
			    	$( ".resizable" ).resizable();
			    });
			    
			    $("#pattern_table").tablesorter({
			    	theme : 'bootstrap',
			    	widgets : [ "uitheme", "staticRow" ],
			    	headerTemplate : '{content} {icon}',
			    	emptyTo: 'top',
			    	sortList: [[4,0]],
				    headers: {
				      // disable sorting of the first & second column - before we would have to had made two entries
				      // note that "first-name" is a class on the span INSIDE the first column th cell
				      '.do-not-sort' : {
				        // disable it by setting the property sorter to false
				        sorter: false
				      }
				    }
			    });
			  
			  //$("#pattern_table").floatThead({
			  //	zIndex: 1
			  //});
			  
			  //$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			  //    $('#pattern_table').floatThead('reflow');
			  //})
			  
			  $('#patternOverviewPopup').modal({
                   loadUrl: '${request.contextPath}/iterativeTimeSeriesClustering/show_pattern_overview/${params.id}/${params.iteration}',
                   show: false
               });
               $('#kpmOuterPopup').modal({
                show: false
               });
				$('#patternOverviewPopup').one('shown.bs.modal', function() {
					$.ticone_queue.add(function() {
						show_modal('loading_modal', 'Loading Overview', 'Loading Clusters and Creating Visualizations ...');
					}, this);
					loadOverviewHighcharts();
		 			
					$.ticone_queue.add(function() {
						hide_modal('loading_modal');
					}, this);
				});
				
				
				$('.checkbox-kpm').click(function() {
					var checked = $('.checkbox-kpm:checked');
					if (checked.length < 1)
						$('#kpmBtn').prop('disabled', true);
					else
						$('#kpmBtn').prop('disabled', false);
				});
				
				$.ticone_queue.add(function() {
					hide_modal('loading_modal');
				}, this);
	
			}); // on dom ready
			
			
			
			<g:if test="${vis.graphLayout && vis.graphLayout.graph.nodes.size() <= 500}">
			function renderCytoscape() {
				var cy = cytoscape({
					  container: document.getElementById('cy'),
					  
					  // this is an alternative that uses a bitmap during interaction
					  hideEdgesOnViewport: false,
					  textureOnViewport: false,
					  
					  // interpolate on high density displays instead of increasing resolution
					  pixelRatio: 1.0,
					  
					  style: cyStylesheet,
					  
					  elements: {
					    nodes: [
					    ], 
					
					    edges: [
					    ]
					  },
					  
					  layout: {
					    name: 'preset'
					  },
					  
					  ready: function(){
					    window.cyto = this;
					    
					    $.ajax({
						  dataType: "json",
						  async: false,
						  url: '${request.contextPath}/graph/nodesAndEdgesAsJSON/${vis.id}',
						  success: function(data) {
						 		console.log("Loading graph data...");
			 			 		window.cyto.batch(function() {
							 	  	window.cyto.add(data);
							 		console.log("... done");
							 			
							 		loadGraphNodePositions(${vis.graphLayout.id});
							 			
							 		console.log(window.cyto.edges().size());
			 			 		});
						   }
						 });
					  }
				});
			}
			</g:if>
			
	var patternHighchartOpacity = 0.3;
			
	function doLoadOverviewHighchart(pattern, data) {
		var patternNo = pattern;
 		console.log("Adding object time series to highcharts");
 		console.log(patternNo);
 		var color = colors[parseInt(patternNo)%colors.length];
 		
 		var series = window['series' + patternNo];

 		var series_arr = [];
		$.each(data, function(object, timeseries) {
			series_arr.push({
				name: object,
				data: timeseries,
				lineWidth: 1,
				color: hexColorToRgbString(color,patternHighchartOpacity)
			});
		});
 		
	    // series for the patterns
        series_arr.push({
        	name: "Cluster " + pattern,
        	data: series,
        	lineWidth: 2,
        	color: '#000'
        });
        
        console.log('#highchartOverview' + patternNo);
        console.log($('#highchartOverview' + patternNo).length);
        
        if ($('#highchartOverview' + patternNo).length > 0) {
	 		
	 		// create highchart
			var chart = new Highcharts.Chart({
		        chart: {
		            renderTo: 'highchartOverview' + patternNo,
		            type: 'line',
		            marginRight: 0,
		            marginBottom: 0,
		            animation: false,
		            reflow: true
		        },
		        credits:{enabled:true},
		        title: {
		            text: '',
		            x: -20 //center
		        },
		        yAxis: {
		        	maxPadding: 0,
				   lineWidth: 0,
				   minorGridLineWidth: 0,
				   minorTickLength: 0,
				   tickLength: 0,
		            title: {
		                text: ''
		            },
		            labels: {
		            	enabled: false
		            },
		            plotLines: [{
		                value: 0,
		                width: 1,
		                color: hexColorToRgbString(color, patternHighchartOpacity)
		            }],
		            //min: 0,
		            //max: 1
		        },
		        xAxis: {
				   lineWidth: 0,
				   minorGridLineWidth: 0,
				   minorTickLength: 0,
				   tickLength: 0,
		            labels: {
		            	enabled: false
		            }
		        },
		        legend: {
		            enabled: false
		        },
		        series: series_arr,
		        plotOptions: {
		        	line: {
		        		marker: {
		        			enabled: false
		        		}
		        	},
		        	series: {
		            	animation: false
		        	}
		        }
		    });
		}
	}
			
    function loadOverviewHighcharts() {
       	<g:each var="p" in="${patterns}">
        	 $.ajax({
			 	dataType: "json",
     					async: false,
			 	url: '${request.contextPath}/timeSeriesClustering/getPatternTimeSeriesObjectsAsJSON/${vis.clustering.id}/${p.id}',
			 	success: function(allData) {
			 		$.ticone_queue.add(doLoadOverviewHighchart, this, 0, ["${p.number}", allData]);
			 	}
			 });
       	</g:each>
        //$.ajax({
		// 	dataType: "json",
		// 	async: false,
		// 	url: '${request.contextPath}/timeSeriesClustering/getPatternTimeSeriesObjectsAsJSON/${ vis.clustering.id }',
		// 	success: function(allData) {
		// 		$.each(allData, function(pattern, data) {
		// 			$.ticone_queue.add(doLoadOverviewHighchart, this, 0, [pattern, data]);
		// 		}) // end each
		// 	} // end success
		//}); // end ajax
	}
	
        </g:javascript>
<style type="text/css" media="screen">
body, html, svg {
	width: 100%;
	height: 100%;
}
</style>
</head>
<body>
	<g:if test="${ vis.graph == null || vis.graph.nodes.size() > 500 }" >
		<g:content tag="hideGraph">true</g:content>
	</g:if>
	<g:content tag="sidebarSimple">true</g:content>
	<g:content tag="sidebarTitle">
	Clustering Visualization
	</g:content>
	<g:content tag="topRow">
		<g:if test="${vis.graphLayout && vis.graphLayout.graph.nodes.size() > 500}">
			<div class="alert alert-info alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    <p><i class="fa fa-lg fa-connectdevelop"></i> The network is too large to be visualized. Please reduce your network to at most 500 nodes.</p>
			</div>
		</g:if>
		<g:if test="${readOnlyMode }">
			<div class="alert alert-info">
			    <p><i class="fa fa-lg fa-eye"></i> You are viewing this clustering in read-only mode.</p>
			</div>
		</g:if>
	</g:content>
	<g:content tag="header">

	
	<div class="modal" id="loading_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static"  data-keyboard="false">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel"></h4>
		      </div> <!-- /modal-header -->
		      <div class="modal-body">
		      </div> <!-- /modal-body -->
		  </div> <!-- /modal-content -->
		</div> <!-- /modal-dialog -->
	</div> <!-- /modal -->
	
					<form class="form-inline">
						<div class="row">
							<g:if test="${layouts.size() > 0 && vis.graph.nodes.size() <= 500}">
							<div class="form-group">
								<div class="input-group">
								  <span class="input-group-addon">Change Layout</span>
								  <g:select name="layout_select"
								          from="${layouts}"
								          value="${vis.graphLayout.id }"
								          optionKey="id"
								          optionValue="name"
								          onchange="changeGraphLayout(\$('#layout_select').val());" 
								          class="form-control"
								          style="max-width: 150px;"  
								          id="layout_select"
								           data-toggle="tooltip" data-placement="bottom" title="Change the layout of your network to one of the layouts you have performed before."/>
								</div>
							</div>
							</g:if>
							
							<g:if test="${!readOnlyMode }">
								<g:if test="${vis.graphLayout && vis.graphLayout.graph.nodes.size() <= 500}">
								<div class="btn-group" data-toggle="tooltip" data-placement="left" title="Perform a network layouting of your network. The layouting is performed on the server and may take a while to finish. It will then be available in the layout dropdown list to the left. You can continue working on your clustering and network and change the layout later when it has finished.">
								  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    <i class="fa fa-connectdevelop"></i> Perform Layout <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu">
								    <li>
									    <!-- <a href="#" onclick="applyCircleLayout();">Circle Layout</a> -->
									    <a href="${createLink(action: 'async_do_circle_layout', id: params.id, params: [iteration: params.iteration]) }">Circle Layout</a>
								    </li>
								    <li>
									    <!-- <a href="#" onclick="applyForceLayout();">Force Layout</a>-->
									    <a href="${createLink(action: 'async_do_force_layout', id: params.id, params: [iteration: params.iteration]) }">Force Layout</a>
								    </li>
								  </ul>
								</div>
								<div class="input-group">
							      <span class="input-group-btn">
							        <button id="saveLayoutBtn" type="button" name="saveLayout" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Save the current layout of the network under a name to be able to load it again later.">
										<i class="fa fa-save"></i> Save Layout
									</button>
							      </span>
							      <g:textField name="layout_name" class="form-control" placeholder="Name..." style="max-width: 200px;"/>
							    </div><!-- /input-group -->
							    </g:if>
						    </g:if>
						</div> <!-- /row -->
				    </form>
	</g:content>
	<g:content tag="sidebar">
		<ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#clustering" aria-controls="clustering" role="tab" data-toggle="tab">Clustering</a></li>
		    <li role="presentation"><a href="#runParameters" aria-controls="runParameters" role="tab" data-toggle="tab">Parameters</a></li>
		    <li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Services</a></li>
		  </ul>
  
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane" id="runParameters">
			<p>For this clustering you used the following settings:</p>
			<dl>
			  <dt>Data set name:</dt><dd>${ itCl.dataSet.name }</dd>
			  <g:if test="${ vis.graph != null && vis.graph.nodes.size() <= 500 }">
			  <dt>Network name:</dt><dd>${ vis.graph.name }</dd>
			  </g:if>
			  <dt>Number of discretization steps:</dt><dd>${ discretizationSteps }</dd>
			  <dt>Supplementary clustering method:</dt><dd>${ clusteringMethod.name }</dd>
			  <dt>Similarity Function:</dt><dd>${ similarityFunction.name }</dd>
			  <dt>Pattern Refinement Function:</dt><dd>${ patternRefinementFunction.name }</dd>
			  <dt>Random Seed:</dt><dd>${ randomSeed }</dd>
			  <dt>Number of Permutation for p-value calculations:</dt><dd>${ permutations }</dd>
			</dl>
		</div>
		<div role="tabpanel" class="tab-pane" id="services">
			<g:if test="${dimanaToken == null}">
			<oauth:connect provider="dimana">Connect to DiMAna</oauth:connect>
			</g:if>
			<g:else>
			<g:submitButton name="sendClusteringDiMAna" value="Use in diMAna"
				onclick="sendClusteringDiMAna();" />
			</g:else>
		</div>
  		<div role="tabpanel" class="tab-pane active" id="clustering">
			<div class="form-inline"
					style="margin-bottom: 15px;">
				<div class="form-group" data-toggle="tooltip" data-placement="top" title="The iteration history; change between clusterings of the iterations you have performed so far.">
						<div class="input-group">
						  <span class="input-group-addon" id="iteration"><i class="fa fa-exchange"></i></span>
						  <g:select name="iteration"
						          from="${itCl.iterations.sort { it.iteration }
								  }"
						          value="${params.iteration}"
						          optionKey="id"
						          optionValue="${{ it.iteration + ") " + it.description }}"
						          onchange="changeIteration();" 
						          class="form-control"
						          style="max-width: 200px;"  
						          id="iteration"/>
						</div>
				</div>
				
				<g:if test="${!readOnlyMode }">
				<!-- Split button -->
				<div class="btn-group">
				  <button type="button" class="btn btn-primary" onclick="doIteration();" data-toggle="tooltip" data-placement="top" title="Perform exactly one iteration; i.e. refine the cluster prototypes and reassign the objects to the most similar prototype."><i class="fa fa-play"></i> Do Iteration</button>
				  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <span class="caret"></span>
				    <span class="sr-only">Toggle Dropdown</span>
				  </button>
				  <ul class="dropdown-menu">
				    <!-- <li><a href="#" onclick="doIteration();">One Iteration</a></li> -->
				    <li><a href="#" onclick="doIterationsUntilConvergence();" data-toggle="tooltip" data-placement="bottom" title="Perform iterations until the clustering does not change anymore (at most 100).">Until Convergence</a></li>
				  </ul>
				</div>
				</g:if>
				<div style="float: right;" class="btn-group">
				  <button title="Export Clusters" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-download"></i> <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
  					<li class="dropdown-header">Export Clusters</li>
				    <li><a href="${createLink(controller: 'timeSeriesClustering', action: 'getClusteringAsTSV', id: vis.clustering.id) }"><i class="fa fa-table"></i> As TSV file</a></li>
				  </ul>
				</div>
				<button type="button" style="float: right;" name="showPatternOverview" title="Overview" class="btn btn-default" onclick="showPatternOverview();">
				<i class="fa fa-line-chart"></i>
				</button>
			</div>
	        
	        <div class="panel panel-default" style="height: 450px; overflow: scroll;">
	    		<!-- <div class="panel-heading">Patterns</div> -->
  				<!-- <div class="panel-body" style="height: 450px; overflow: scroll;"></div> -->
  				<!-- <div class="panel-body"></div> -->
			
				<table id="pattern_table" class="table table-condensed">
					<thead>
						<tr>
							<th data-toggle="tooltip" data-placement="top" title="The ID of the cluster is unique throughout the whole cluster process. A cluster will keep its ID, unless it is split up." class="rotate"><div style="display: inline-block;"><span>Cluster ID</span></div></th>
							<th data-toggle="tooltip" data-placement="top" data-html="true" title="The number of objects contained in the cluster." class="rotate hidden-xs"><div style="display: inline-block;"><span># Objects</span></div></th>
							<th data-toggle="tooltip" data-placement="top" title="The average Residual Sum of Squares (RSS) of the objects of this cluster to the cluster prototype; it is a measure of how well the objects fit to the cluster. The lower, the better. The RSS of one object to the cluster prototype is defined as the sum of squared differences of the object signal to the prototype signal over all timepoints." class="rotate hidden-xs"><div style="display: inline-block;"><span>RSS</span></div></th>
							<th data-toggle="tooltip" data-placement="top" title="The average Pearson Correlation of the objects of this cluster with the cluster prototype; it is a measure of how well the objects fit to the cluster trend. The larger, the better. The Pearson Correlation of one object and the cluster prototype is defined over their respective signals over all timepoints." class="rotate hidden-xs"><div style="display: inline-block;"><span>Pearson</span></div></th>
							<th data-toggle="tooltip" data-placement="top" title="The p-value is a statistical measure indicating the probability, that one observes a cluster of this size (number of objects) with this good a fit by chance. The accuracy of the p-value depends on the number of random iterations you chose in the options." class="rotate hidden-xs"><div style="display: inline-block;"><span>P-value</span></div></th>
							<th data-toggle="tooltip" data-placement="top" title="Choose which clusters should be visualized on the network." class="rotate"><div style="display: inline-block;"><span class="do-not-sort">Visualization</span></div></th>
							<g:if test="${!readOnlyMode }">
							<th data-toggle="tooltip" data-placement="top" title="Choose the clusters you want to merge. After you checked the corresponding checkboxes click the Merge Clusters button below." class="rotate"><div style="display: inline-block;"><span class="do-not-sort">Merge</span></div></th>
							<th data-toggle="tooltip" data-placement="top" title="Choose which clusters (cluster prototypes) should be forced to be kept. Cluster prototypes of keep clusters will not be changed during iterations and after assigned objects changed." class="rotate"><div style="display: inline-block;"><span class="do-not-sort">Keep</span></div></th>
							</g:if>
							<th data-toggle="tooltip" data-placement="top" title="Choose which clusters should be sent to Keypathwayminer Web (KPM) for network enrichment analysis. After you checked the corresponding checkboxes click the Network Enrichment button below." class="rotate"><div style="display: inline-block;"><span class="do-not-sort">KPM</span></div></th>
							<th data-toggle="tooltip" data-placement="top" title="A graphical representation of the cluster, its prototype and its assigned objects." class="rotate col-md-5"><div style="display: inline-block;"><span>Graph</span></div></th>
						</tr>
					</thead>
					<tr class="static">
						<td>All (${ vis.clustering.patterns.size() })</td>
					    <td data-toggle="tooltip" data-placement="top" data-html="true" title="The total number of objects in the data set (graph) after mapping" class="hidden-xs">
					    	${vis.clustering.dataSet.objects.size()}
					    	
							<g:if test="${ vis.graph != null}" >
								(${vis.graph.nodes.size()})
							</g:if>
					    </td>
					    <td class="hidden-xs"></td>
					    <td class="hidden-xs"></td>
					    <td class="hidden-xs"></td>
						<td>
							<g:if test="${ selectedPattern == i }">
								<g:radio name="myGroup" value="-1" checked="true"
									onclick="updateSelectedPattern();" />
					    	</g:if>
							<g:else>
								<g:radio name="myGroup" value="-1" onclick="updateSelectedPattern();" />
					    	</g:else>
					    </td>
					    <g:if test="${!readOnlyMode }">
					    <td></td>
					    <td></td>
					    </g:if>
					    <td></td>
						<td></td>
				    </tr>
					<% i=0 %>
					<g:each var="p" in="${patternOrder}">
					<tr>
						<td>
							${p.name} (<g:link url="javascript:showPatternPopup(${ p.id });">Details</g:link>) 
						</td>
						<td class="hidden-xs">
							${p.get_number_of_objects(vis.clustering)}
						</td>
						<td class="hidden-xs">
							<g:formatNumber number="${p.rss}" type="number" minFractionDigits="3" maxFractionDigits="3" />
						</td>
						<td class="hidden-xs">
							<g:formatNumber number="${p.pearson}" type="number" minFractionDigits="3" maxFractionDigits="3" />
						</td>
						<td class="hidden-xs">
						<g:if test="${ similarityFunction.name == "Euclidian Distance" }">
							<g:if test="${ p.rsspvalue > -1 }">
								<g:formatNumber number="${p.rsspvalue}" type="number" minFractionDigits="3" maxFractionDigits="3" />
							</g:if>
							<g:else>
								--
							</g:else>
						</g:if>
						<g:else>
							<g:if test="${ p.pearsonpvalue > -1 }">
								<g:formatNumber number="${p.pearsonpvalue}" type="number" minFractionDigits="3" maxFractionDigits="3" />
							</g:if>
							<g:else>
								--
							</g:else>
						</g:else>
						</td>
						<td>
							<g:if test="${ selectedPattern == i }">
								<g:radio name="myGroup" value="${ i}"
									onclick="updateSelectedPattern();" checked="true" />
								<!-- <div id="colorBox${i}"
									style="width: 10px; height: 10px; background-color: #000000; display: inline-block;"></div> -->
							</g:if>
							<g:else>
								<g:radio name="myGroup" value="${ i}"
									onclick="updateSelectedPattern();" />
								<!-- <div id="colorBox${i }"
									style="width: 10px; height: 10px; background-color: #000000; display: inline-block;"></div> -->
							</g:else>
						</td>
						<g:if test="${!readOnlyMode }">
						<td>
							<g:checkBox class="checkbox-merge" name="mergePattern${p.id}" value="${false}" pattern="${p.id}" />
						</td>
						<td>
							<g:checkBox class="checkbox-keep" name="keepPattern${p.id}" value="${p.keep}" pattern="${p.id}" onclick="setKeepPattern(${ p.id});" />
						</td>
						</g:if>
						<td>
							<g:checkBox class="checkbox-kpm" name="kpmPattern${p.id}" value="${false}" pattern="${p.number}" />
						</td>
						<td>
							<div class="hidden">${ i }</div>
						<g:if test="${ p.get_number_of_objects(vis.clustering) < 200 }">
							<div class="highchartPattern" id="highchart${p.number}" style="width:100%;margin: 0 auto; height: 100px;"></div>
						</g:if>
						<g:else>
							<img style="width: 100%; height: 100px;" src="${createLink(controller: 'timeSeriesClustering', action: 'getPatternTimeSeriesObjectsAsFreeChart', id: vis.clustering.id) + "/" + p.id}"/>
						</g:else>
						</td>
						<g:javascript>
					      var series${p.number } = [];
					      <% j=0 %>
							<g:each in="${p.timeSeriesPattern}">
					      	series${p.number }.push([${j }, ${it}]);
					      	<% j=j+1 %>
							</g:each>
						</g:javascript>
						<div class="popup pattern${ p.id }" id="patternPopup${ p.id }">
							<h2>Details of ${ p.name }</h2>
							Number of objects: ${ p.get_number_of_objects(vis.clustering) }<br />
							RSS: ${p.rss }<br />
							<g:if test="${ p.get_number_of_objects(vis.clustering) < 200 }">
								<div id="highchartPopup${ p.number }"></div>
							</g:if>
							<g:else>
								<div><img style="width: 500px;" src="${createLink(controller: 'timeSeriesClustering', action: 'getPatternTimeSeriesObjectsAsFreeChart', id: vis.clustering.id) + "/" + p.id + "?withAxes"}"/>
								</div>
							</g:else>
							<g:if test="${!readOnlyMode }">
							<g:submitButton name="splitPattern${ p.id }" value="Split Cluster ..." onclick="showSplitPopup(${ p.id })" />
							<g:submitButton name="deletePattern${ p.id }" value="Delete ..." onclick="showDeletePopup(${ p.id })" />
							
						    <button type="button" name="showPatternLeastSimilarObjects" title="Show Least Similar Objects of Cluster (in %)" onclick="showCleanUpPopup(${ p.id });">
						    	<i class="fa fa-magic"></i>
						    </button>
						    </g:if>
							
						</div>
						<g:if test="${!readOnlyMode }">
						<div class="popup pattern${ p.id }" id="splitPatternPopup${ p.id }" style="display:none;">
							<h3>How would you like to split the pattern?</h3>
							<g:submitButton name="splitDissimilarObjects${ p.id }" value="Split Based On Two Most Dissimilar Objects" onclick="splitPatternMostDissimilarObjects(${ p.id });" />
							<g:submitButton name="splitClustering${ p.id }" value="Split Based On Clustering ..." onclick="showSplitClusteringPopup(${ p.id });" />
						</div>
						<div class="popup pattern${ p.id }" id="splitPatternClusteringPopup${ p.id }" style="display:none;">
							<g:field type="number" name="splitNumberClusters${ p.id }" min="2" value="2"/>
							<g:submitButton name="submitSplitClustering${ p.id }" value="Split Based On Clustering" onclick="splitPatternClustering(${ p.id });" />
						</div>
						<div class="popup pattern${ p.id }" id="deletePatternPopup${ p.id }" style="display:none;">
							<h3>What would you like to delete?</h3>
							<g:submitButton name="deletePattern${ p.id }" value="Delete Cluster" onclick="deletePattern(${ p.id });" />
							<g:submitButton name="deleteObjectsOnly${ p.id }" value="Delete Objects" onclick="deleteObjects(${ p.id });" />
							<g:submitButton name="deletePatternAndObjects${ p.id }" value="Delete Cluster & Objects" onclick="deletePatternAndObject(${ p.id });" />
						</div>
						
						<div class="popup pattern${ p.id }" id="cleanUpObjectsOuterPopupPattern${ p.id }" style="display:none;">
				      		<p>
				      		   <button type="button" id="showObjectPatternRSSHistogramPattern" title="Show RSS Histogram for Cluster ${ p.number }" class="btn btn-default" onclick="showObjectPatternSimilarityHistogramPopupPattern('rss', ${ p.id });">
						      	<i class="fa fa-bar-chart"> Show RSS Histogram</i>
						       </button>
						       <button type="button" id="showObjectPatternPearsonHistogramPattern" title="Show Pearson Histogram for Cluster ${ p.number }" class="btn btn-default" onclick="showObjectPatternSimilarityHistogramPopupPattern('pearson', ${ p.id });">
						      	<i class="fa fa-bar-chart"> Show Pearson Histogram</i>
						       </button>   
						    </p>
			                <p>
			                <p>
					      	<input type="radio" name="cleanUpTypePattern${ p.id }" value="1" checked="checked" />
					      	<label for="leastFittingObjectsPercentagePattern${ p.id }">Percentage of objects</label>
			      			  <g:textField name="leastFittingObjectsPercentagePattern${ p.id }" value="10" class="form-control" style="max-width: 60px; display: inline-block;"/>
					      	</p>
					      	<p>
					      	<input type="radio" name="cleanUpTypePattern${ p.id }" value="2" />
					      	<label for="leastFittingObjectsRSSThresholdPattern${ p.id }">RSS threshold for objects</label>
			      			  <g:textField name="leastFittingObjectsRSSThresholdPattern${ p.id }" value="1.0" class="form-control" style="max-width: 60px; display: inline-block;"/>
					      	</p>
					      	<p>
					      	<input type="radio" name="cleanUpTypePattern${ p.id }" value="3" />
					      	<label for="leastFittingObjectsPearsonThresholdPattern${ p.id }">Pearson Correlation threshold for objects</label>
			      			  <g:textField name="leastFittingObjectsPearsonThresholdPattern${ p.id }" value="0.75" class="form-control" style="max-width: 60px; display: inline-block;"/>
					      	</p>
					      	<button type="button" id="showLeastSimilarObjectsPattern${ p.id }" title="Show Least Similar Objects" class="btn btn-default" onclick="showLeastSimilarObjectsPopupPattern(${ p.id });"/>
					        	<i class="fa fa-magic"> Show Least Similar Object Sets</i>
					        </button>
						</div>
						
						</g:if>
						<% i=i+1 %>
						</td></tr>
					</g:each>
					<div class="modal fade" id="kpmOuterPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="myModalLabel">Network Enrichment (Keypathwayminer)</h4>
						      </div> <!-- /modal-header -->
						      <div class="modal-body">
						      	<div class="container-fluid">
						      		<div class="row">
										<div class="form-group">
											<label for="steps" class="col-sm-4 control-label">Number exception nodes (k)</label>
											<div class="col-sm-8">
												<g:field type="number" min="0" value="0"
														id="kpm_k" name="kpm_k" class="form-control" />
											</div>
										</div>
									</div> <!-- /row -->
						      		<div class="row">
										<div class="form-group">
											<label for="steps" class="col-sm-4 control-label">Number pathways</label>
											<div class="col-sm-8">
												<g:field type="number" min="1" max="100" value="10"
														id="kpm_n" name="kpm_n" class="form-control" />
											</div>
										</div>
									</div> <!-- /row -->
						      		<div class="row">
										<div class="form-group">
											<label for="objectIntersect" class="col-sm-4 control-label">Nodes not in data set</label>
											<div class="col-sm-8">
												<g:select id="kpm_objectsNotPresent" name="objectIntersect" from="${objectIntersects }" optionKey="id" optionValue="name" class="form-control" />
											</div>
										</div>
									</div> <!-- /row -->
						        </div> <!-- /container-fluid -->
						      </div> <!-- /modal-body -->
					        <div class="modal-footer">
					          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="kpmSelectedPatterns();">Start And Open Status Window</button>
					        </div> <!-- /modal-footer -->
						  </div> <!-- /modal-content -->
						</div> <!-- /modal-dialog -->
					</div> <!-- /modal -->
					
					<g:if test="${!readOnlyMode }">
					<div class="popup" id="leastSimilarObjectsOuterPopup" style="display:none;"></div>
					<div class="popup" id="leastConservedObjectsOuterPopup" style="display:none;"></div>
					<div class="popup" id="objectPatternSimilarityHistogramPopup" style="display:none;"></div>
					<div class="popup" id="objectPatternSimilarityHistogramPopupPattern" style="display:none;"></div>
					
					<div class="modal fade" id="cleanUpObjectsOuterPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="myModalLabel">Clean Up Objects</h4>
						      </div> <!-- /modal-header -->
						      <div class="modal-body">
						      		<p>
						      		   <button type="button" id="showObjectPatternRSSHistogram" title="Show RSS Histogram" class="btn btn-default" onclick="showObjectPatternSimilarityHistogramPopup('rss');">
								      	<i class="fa fa-bar-chart"> Show RSS Histogram</i>
								       </button>
								       <button type="button" id="showObjectPatternPearsonHistogram" title="Show Pearson Histogram" class="btn btn-default" onclick="showObjectPatternSimilarityHistogramPopup('pearson');">
								      	<i class="fa fa-bar-chart"> Show Pearson Histogram</i>
								       </button>   
								    </p>
					                <p>
							      	<input type="radio" name="cleanUpType" value="1" checked="checked" />
							      	<label for="leastFittingObjectsPercentage">Percentage of objects</label>
					      			  <g:textField name="leastFittingObjectsPercentage" value="10" class="form-control" style="max-width: 60px; display: inline-block;"/>
							      	</p>
							      	<p>
							      	<input type="radio" name="cleanUpType" value="2" />
							      	  <label for="leastFittingObjectsRSSThreshold">RSS threshold for objects</label>
					      			  <g:textField name="leastFittingObjectsRSSThreshold" value="1.0" class="form-control" style="max-width: 60px; display: inline-block;"/>
							      	</p>
							      	<p>
							      	<input type="radio" name="cleanUpType" value="3" />
							      	  <label for="leastFittingObjectsPearsonThreshold">Pearson Correlation threshold for objects</label>
					      			  <g:textField name="leastFittingObjectsPearsonThreshold" value="0.75" class="form-control" style="max-width: 60px; display: inline-block;"/>
							      	</p>
							      	<button type="button" id="showLeastSimilarObjects" title="Show Least Similar Objects" class="btn btn-default" onclick="showLeastSimilarObjectsPopup();">
							        	<i class="fa fa-magic"> Show Least Similar Object Sets</i>
							        </button>
							        <button type="button" id="showLeastConservedObjects" class="btn btn-default" onclick="showLeastConservedObjectsPopup();"
							        	<g:if test="${ objects?.find {true}.samples.size() < 2 }">
							        		title="Show Least Conserved Objects. This operation is only applicable if the data set has more than one sample." disabled
							        	</g:if>
							        	<g:else>
							        		title="Show Least Conserved Objects"
							        	</g:else>
							        >
							        	<i class="fa fa-magic"> Show Least Conserved Object Sets</i>
							        </button>
						      </div> <!-- /modal-body -->
						  </div> <!-- /modal-content -->
						</div> <!-- /modal-dialog -->
					</div> <!-- /modal -->
					
					<div class="modal fade" id="layout_name_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="myModalLabel">Save Layout</h4>
						      </div> <!-- /modal-header -->
						      <div class="modal-body">
						      </div> <!-- /modal-body -->
						  </div> <!-- /modal-content -->
						</div> <!-- /modal-dialog -->
					</div> <!-- /modal -->
					
					<div class="popup" id="patternLeastSimilarObjectsOuterPopup" style="display:none;"></div>
					</g:if>
					
					<div class="modal fade" id="patternOverviewPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog modal-lg" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title" id="myModalLabel">Cluster Overview</h4>
					      </div>
					      <div class="modal-body modal-scroll">
					      	<div class="container-fluid">
								<g:each status="i" var="p" in="${patternOrder}">
									<g:if test="${i % 4 == 0 }" >
									<div class="row">
									</g:if>
									<div class="col-md-3">
										<h5>Cluster ${ p.number }  (<g:link url="javascript:showPatternPopup(${ p.id });">Details</g:link>)</h5>
										<g:if test="${ p.get_number_of_objects(vis.clustering) < 200 }">
											<div id="highchartOverview${p.number}" style="height: 150px;"></div>
										</g:if>
										<g:else>
											<img style="height: 150px; width: 150px;" src="${createLink(controller: 'timeSeriesClustering', action: 'getPatternTimeSeriesObjectsAsFreeChart', id: vis.clustering.id) + "/" + p.id}"/>
										</g:else>
								    </div>
								    <g:if test="${ i % 4 == 3 }" >
								    </div>
									</g:if>
								</g:each>
							</div>
					      </div>
					    </div>
					  </div>
					</div>
					</div>
					
					<g:if test="${!readOnlyMode }">
					<div class="modal fade" id="addNewPatternPopup" tabindex="-1" role="dialog" aria-labelledby="addClusterLabel">
					  <div class="modal-dialog modal-lg" role="document">
					    <div class="modal-content">
					      <form id="submitNewPattern" enctype="application/json" action="${createLink(action: 'add_pattern', id: params.id, params: [iteration: params.iteration]) }" method="POST">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" id="addClusterLabel">Add a Cluster with Predefined Prototype</h4>
						      </div>
						      <div class="modal-body">
						      	<div id="addNewPatternHighchart">
								</div>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						        <button type="button" class="btn btn-primary" onclick="addNewPattern();">Save changes</button>
						      </div>
						  </form>
					    </div>
					  </div>
					</div>
					</div>
					
					</g:if>
					
					
					
					
					<script type="text/javascript">
						function doLoadHighchart(pattern, data) {
				 			var patternNo = pattern;
					 		console.log("Adding object time series to highcharts");
					 		console.log(patternNo);
					 		var color = colors[parseInt(patternNo)%colors.length];
					 		
					 		var series = window['series' + patternNo];

					 		var series_arr = [];
					 		
				 			$.each(data, function(object, timeseries) {
				 				series_arr[series_arr.length] = {
				 					name: object,
				 					data: timeseries,
				 					lineWidth: 1,
				 					color: hexColorToRgbString(color, patternHighchartOpacity)
				 				};
				 			});
				 			
						    // series for the patterns
			              	series_arr[series_arr.length] = {
			              		name: "Cluster " + pattern,
			              		data: series,
			              		lineWidth: 2,
			              		color: '#000'
			              	};
			              	if($('#highchart' + patternNo).length > 0) {
			              	
						 		// create highchart
								var chart = new Highcharts.Chart({
							        chart: {
							            renderTo: 'highchart' + patternNo,
							            type: 'line',
							            marginRight: 0,
							            marginBottom: 0,
							            animation: false
							        },
							        credits:{enabled:true},
							        title: {
							            text: '',
							            x: -20 //center
							        },
							        yAxis: {
							        	maxPadding: 0,
									   lineWidth: 0,
									   minorGridLineWidth: 0,
									   minorTickLength: 0,
									   tickLength: 0,
							            title: {
							                text: ''
							            },
							            labels: {
							            	enabled: false
							            },
							            plotLines: [{
							                value: 0,
							                width: 1,
							                color: hexColorToRgbString(color, patternHighchartOpacity)
							            }],
							            //min: 0,
							            //max: 1
							        },
							        xAxis: {
									   lineWidth: 0,
									   minorGridLineWidth: 0,
									   minorTickLength: 0,
									   tickLength: 0,
							            labels: {
							            	enabled: false
							            }
							        },
							        legend: {
							            layout: 'vertical',
							            align: 'right',
							            verticalAlign: 'top',
							            x: -10,
							            y: 100,
							            borderWidth: 0
							        },
							        series: series_arr,
							        plotOptions: {
							        	line: {
							        		marker: {
							        			enabled: false
							        		}
							        	},
							        	series: {
							            	animation: false
							        	}
							        }
							    });
							
							}
							
							if($('#highchartPopup' + patternNo).length > 0) {
							 
								var chart_popup = new Highcharts.Chart({
							        chart: {
							            renderTo: 'highchartPopup' + patternNo,
							            type: 'line',
							            marginRight: 130,
							            marginBottom: 25,
							            animation: false
							        },
							        credits:{enabled:true},
							        title: {
							            text: '',
							            x: -20 //center
							        },
							        yAxis: {
							        	maxPadding: 0,
							            title: {
							                text: ''
							            },
							            plotLines: [{
							                value: 0,
							                width: 1,
							                color: hexColorToRgbString(color, patternHighchartOpacity)
							            }],
							            //min: 0,
							            //max: 1
							        },
							        legend: {
							            layout: 'vertical',
							            align: 'right',
							            verticalAlign: 'top',
							            x: -10,
							            y: 100,
							            borderWidth: 0
							        },
							        series: series_arr,
							        plotOptions: {
							        	line: {
							        		marker: {
							        			enabled: false
							        		}
							        	},
							        	series: {
							            	animation: false
							        	}
							        }
							    });
							 }
							 
						}

						 
				        function loadHighcharts() {
				        	<g:each var="p" in="${patterns}">
					        	 $.ajax({
								 	dataType: "json",
		        					async: false,
								 	url: '${request.contextPath}/timeSeriesClustering/getPatternTimeSeriesObjectsAsJSON/${vis.clustering.id}/${p.id}',
								 	success: function(allData) {
								 		$.ticone_queue.add(doLoadHighchart, this, 0, ["${p.number}", allData]);
								 	}
								 });
				        	</g:each>
				             //$.ajax({
							 //	dataType: "json",
	        				//	async: false,
							// 	url: '${request.contextPath}/timeSeriesClustering/getPatternTimeSeriesObjectsAsJSON/${vis.clustering.id}',
							// 	success: function(allData) {
							// 		$.each(allData, function(pattern, data) {
							// 			$.ticone_queue.add(doLoadHighchart, this, 0, [pattern, data]);
							// 		}); <!-- / end each -->
							// 	}
							 //});
						 };
						 
						 function sendClusteringDiMAna() {			 	
							var win = window.open(
								'${request.contextPath}/timeSeriesClustering/sendClusteringToTis2ma/${vis.clustering.id}', 
								'_blank');
							win.focus();
						 };
					</script>
			  
  				</table>
			  </div>
			<form class="form-inline">
				<div class="form-group">
					<g:if test="${!readOnlyMode }">
			        <div class="input-group">
				      <span class="input-group-btn">
				        <button type="button" id="showCleanUpObjects" title="Clean Up Objects" class="btn btn-default" onclick="showCleanUpObjectsPopup();"/>
				        	<i class="fa fa-magic"></i>
				        </button>
				      </span>
				    </div><!-- /input-group -->
				    
					<button id="addClusterBtn" title="Add Cluster" type="button" class="btn btn-default" onclick="showAddClusterPopup();">
				    	<i class="fa fa-plus"></i>
				  	</button>
				  	<button id="mergePatternBtn" type="button" class="btn btn-default" disabled="disabled" onclick="mergeSelectedPatterns();" data-toggle="tooltip" data-placement="bottom" title="Merge the clusters selected above (at least two) into one cluster.">
				    	<i class="fa fa-compress"></i>
				  	</button>
				  	</g:if>
				  	<g:if test="${ vis.graph != null}" >
						<button id="kpmBtn" type="button" class="btn btn-default" disabled="disabled" onclick="showKPMPopup();" data-toggle="tooltip" data-placement="bottom" title="Perform a network enrichment of the clusters checked above using Keypathwayminer Web.">
					    	<i></i> Network Enrichment
					  	</button>
				  	</g:if>
				  	
				</div>
			</form>
		</div>
  	</div>
						
	</g:content>
	<g:if test="${!readOnlyMode }">
	<div class="alert alert-info" id="startedLayouting">
	    <p><asset:image src="ajax-loader.gif"/> Graph layouting in progress ...</p>
	</div>
	<div class="alert alert-success alert-dismissable" id="finishedLayouting">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	    <p>Graph layouting finished.</p>
	</div>
	</g:if>
	<g:if test="${ vis.graph != null && vis.graph.nodes.size() <= 500 }" >
	<div style="position: relative; height: 550px; width: 100%; float: right; z-index: 1;" id="cy">
		<!--<g:submitButton name="renderGraph" value="Render Graph" onclick="renderCytoscape();" />-->
		</div>
	</g:if>
	<r:layoutResources />
</body>
</html>