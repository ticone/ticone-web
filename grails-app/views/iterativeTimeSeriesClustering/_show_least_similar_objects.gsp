<script type="text/javascript">
	$(function(){ // on dom ready
		loadHighcharts();
	}); // on dom ready

      function loadHighcharts() {
          $.ajax({
		 	dataType: "json",
		 	url: '${request.contextPath}/iterativeTimeSeriesClustering/get_least_similar_objects_as_JSON/${ clustering.sessionId }/${ iteration }/${ percentage }/${type}',
		 	success: function(allData) {

			 	window.allData = allData;
		 		
		 		// create highchart
				var chart_popup = new Highcharts.Chart({
			        chart: {
			            renderTo: 'leastSimilarObjectsHighchartPopup',
			            type: 'line',
			            marginRight: 0,
			            marginBottom: 0,
			            animation: false
			        },
			        credits:{enabled:false},
			        title: {
				        <g:if test="${type == "percentage"}">
			            	text: 'Least Similar Object Sets (' + ${params.percentage} + '%)',
			            </g:if>
			            <g:elseif test="${type == "rss"}">
			            	text: 'Least Similar Object Sets (RSS > ' + ${params.percentage} + ')',
			            </g:elseif>
			            <g:elseif test="${type == "pearson"}">
			            	text: 'Least Similar Object Sets (Pearson < ' + ${params.percentage} + ')',
			            </g:elseif>
			            x: -20 //center
			        },
			        yAxis: {
			            title: {
			                text: ''
			            },
			            plotLines: [{
			                value: 0,
			                width: 1
			            }],
			            //min: 0,
			            //max: 1
			        },
			        legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'top',
			            x: -10,
			            y: 100,
			            borderWidth: 0
			        },
			        series: [
			        ],
			        plotOptions: {
			        	line: {
			        		marker: {
			        			enabled: false
			        		}
			        	},
			        	series: {
			            	animation: false
			        	}
			        }
			    });
			    
		 		console.log("Adding object time series to highcharts");
		 		$.each(allData.objectTimeSeries, function(object, timeSeries) {
	 				chart_popup.addSeries({
	 					name: object,
	 					data: timeSeries,
	 					lineWidth: 1,
	 					color: 'rgba(0, 0, 0, 0.1)'
	 				},
	 				false);
			 		
					 console.log("...done");
				 });
				 chart_popup.redraw();
		 	}
		});
	};

	function deleteLeastSimilarObjects() {
    	$.ticone_queue.add(function() {
    		$('#leastSimilarObjectsOuterPopup').bPopup().close();
    		<g:if test="${type == "percentage"}">
			show_modal('loading_modal', 'Deleting Least Similar Object Sets', 'Deleting ' + ${params.percentage} + '% least similar objects ...');
			</g:if>
			<g:if test="${type == "rss"}">
			show_modal('loading_modal', 'Deleting Least Similar Object Sets', 'Deleting object sets with average RSS > ' + ${params.percentage});
			</g:if>
			<g:if test="${type == "pearson"}">
			show_modal('loading_modal', 'Deleting Least Similar Object Sets', 'Deleting object sets with average Pearson Correlation < ' + ${params.percentage});
			</g:if>
		}, this);
		window.location.href = "${request.contextPath}/iterativeTimeSeriesClustering/async_delete_least_similar_objects/${params.id}/${params.iteration}/${params.percentage}/${type}";
	}
</script>

<div>
	<div id="leastSimilarObjectsHighchartPopup"></div>
    	<g:submitButton name="deleteLeastSimilarObjects" value="Delete" onclick="deleteLeastSimilarObjects();"/><br />
</div>
