<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="preventLayoutResize">true</g:content>
	<g:content tag="sidebarPanelHide">true</g:content>
	<g:content tag="sidebarClass">col-lg-2</g:content>
	<g:content tag="mainClass">col-lg-5</g:content>
	<g:content tag="title"><h1>Citing TiCoNE</h1></g:content>
	
	<p>We are currently preparing TiCoNE for publication. Please stay tuned.</p>

%{--
	<h4>Supplementary Material</h4>
	<p>Here we provide supplementary data and material used in our recent publication. This includes the following items:</p>
	<ul>
		<li>The synthetically generated <a href="${ assetPath(src: "paper1_synthetic_ts_data.tsv")}">time-series data</a> and <a href="${ assetPath(src: "paper1_synthetic_network.tsv")}">network</a></li>
		<li>The <a href="${ assetPath(src: "paper1_cy_session.cys")}">Cytoscape session</a> containing all results presented in the manuscript</li>
	</ul>
--}%
</body>
</html>
