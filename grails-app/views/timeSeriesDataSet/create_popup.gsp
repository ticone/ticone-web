<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main_popup" />
<script>
$(document).ready(function() {
	<g:if test="${ user == null }">
	$('#published').click(function() {
		$('#login_to_not_publish').modal('show');
		$('#published').prop('checked', true);
	});
	</g:if>
});

</script>
</head>
<body>
	<g:content tag="title">
		Upload Time-Series Data Set
	</g:content>
	<g:content tag="sidebarTitle">
	TODO
	</g:content>
	<g:content tag="sidebar">
	TODO
	</g:content>
	<div class="modal fade" id="login_to_not_publish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Private Data Set</h4>
		      </div> <!-- /modal-header -->
		      <div class="modal-body">
		      	You have to login to be able to keep your data set private.
		      </div> <!-- /modal-body -->
		  </div> <!-- /modal-content -->
		</div> <!-- /modal-dialog -->
	</div> <!-- /modal -->
	<g:uploadForm action="upload" params="[popup:1]" class="form-horizontal">
		<div class="form-group">
			<label for="name" class="col-sm-4 control-label">Name for Time-Series Data Set</label>
			<div class="col-sm-8">
				<g:field type="text" id="name" name="name" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label for="tsFile" class="col-sm-4 control-label">Time-Series File</label>
			<div class="col-sm-8">
				<g:field type="file" id="tsFile" name="tsFile" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label for="published" class="col-sm-4 control-label">Make data set public?</label>
			<div class="col-sm-8">
				<g:checkBox id="published" name="published" value="on" class="form-control" /> 
			</div>
		</div>
		<g:submitButton name="submit" class="btn btn-primary" />
	</g:uploadForm>
</body>
</html>