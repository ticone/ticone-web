<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="sidebarTitle">
	TODO
	</g:content>
	<g:content tag="title">
	<h1>Remove Least Varying Object Sets</h1>
	</g:content>
	<g:content tag="sidebarClass">col-lg-3</g:content>
	<g:content tag="mainClass">col-lg-9</g:content>
	<g:content tag="sidebar">
	TODO
	</g:content>
	
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static"  data-keyboard="false">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel"></h4>
		      </div> <!-- /modal-header -->
		      <div class="modal-body">
		      <asset:image src="ajax-loader.gif"/> 
		      </div> <!-- /modal-body -->
		  </div> <!-- /modal-content -->
		</div> <!-- /modal-dialog -->
	</div> <!-- /modal -->
	
	<g:uploadForm action="do_filter" class="form-horizontal">
		<g:if test="${params.popup }">
			<g:hiddenField name="popup" value="1"/>
		</g:if>
		<div class="panel panel-default">
		  <div class="panel-heading">Filter Parameters</div>
			  <div class="panel-body">
			  	<p>
		      	<input type="radio" name="type" value="1" checked="checked" />
		      	<label for="percentage">Percentage of objects</label>
      			  <g:textField name="percentage" value="10" class="form-control" style="max-width: 60px; display: inline-block;"/>
		      	</p>
		      	<p>
		      	<input type="radio" name="type" value="2" />
		      	<label for="threshold">Standard variance threshold</label>
      			  <g:textField name="threshold" value="1.0" class="form-control" style="max-width: 60px; display: inline-block;"/>
		      	</p>
				<div class="form-group">
					<div class="col-sm-6">
						<g:hiddenField name="name" value="${ params.name }" />
						<g:hiddenField name="dataFilter" value="${ params.dataFilter }" />
						<g:hiddenField name="tsDataSet" value="${ params.tsDataSet }" />
					</div>
				</div>
		<g:submitButton name="Submit" />
	</g:uploadForm>
</body>
</html>