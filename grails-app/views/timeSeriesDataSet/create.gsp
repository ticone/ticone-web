	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<g:if test="${params.popup }">
<meta name="layout" content="main_popup" />
</g:if>
<g:else>
<meta name="layout" content="main" />
</g:else>
<script>
$(document).ready(function() {
	<g:if test="${ user == null }">
	$('#published').click(function() {
		$('#login_to_not_publish').modal('show');
		$('#published').prop('checked', false);
	});
	</g:if>
});

</script>
</head>
<body>
	<g:content tag="title">
		<h1>Upload Time-Series Data Set</h1>
	</g:content>
	<g:content tag="sidebarTitle">
	Upload your own Data Set
	</g:content>
	<g:content tag="sidebar">
	<p>Your time-series data set file should be a <b>tab-separated file</b> with the following format:</p>
	<table class="table table-condensed table-bordered tablesorter-bootstrap">
		<thead>
			<tr>
				<th class="rotate"><div style="display: inline-block;"><span>Sample ID</span></div></th>
				<th class="rotate"><div style="display: inline-block;"><span>Object ID</span></div></th>
				<th class="rotate"><div style="display: inline-block;"><span title="Signal at time point 1">Signal TP 1</span></div></th>
				<th class="rotate"><div style="display: inline-block;"><span title="Signal at time point 2">Signal TP 2</span></div></th>
				<th>...</th>
				<th class="rotate"><div style="display: inline-block;"><span title="Signal at time point T">Signal TP T</span></div></th>
			</tr>
		</thead>
		<tr><td>sample1</td><td>gene 1</td><td>1.0</td><td>2.0</td><td>...</td><td>0.4</td></tr>
		<tr><td>sample1</td><td>gene 2</td><td>13.5</td><td>201.0</td><td>...</td><td>5.0</td></tr>
		<tr><td>...</td><td>...</td><td>...</td><td>...</td><td>...</td><td>...</td></tr>
		<tr><td>sampleX</td><td>gene N-1</td><td>1.0</td><td>2.0</td><td>...</td><td>0.4</td></tr>
		<tr><td>sampleX</td><td>gene N</td><td>3.0</td><td>15.0</td><td>...</td><td>7.1</td></tr>
	</table>
	</g:content>
	<g:if test="${!params.popup }">
	<g:content tag="sidebarClass">col-lg-3</g:content>
	</g:if>
	<div class="modal fade" id="login_to_not_publish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Private Data Set</h4>
		      </div> <!-- /modal-header -->
		      <div class="modal-body">
		      	You have to login to set your data set public.
		      </div> <!-- /modal-body -->
		  </div> <!-- /modal-content -->
		</div> <!-- /modal-dialog -->
	</div> <!-- /modal -->
	<g:if test="${flash.error}">
	  <div class="alert alert-danger" style="display: block">${flash.error}</div>
	</g:if>
	<g:uploadForm action="async_upload" class="form-horizontal">
		<g:if test="${params.popup }">
			<g:hiddenField name="popup" value="1"/>
		</g:if>
		<div class="form-group">
			<label for="name" class="col-sm-4 control-label">Name for Time-Series Data Set</label>
			<div class="col-sm-8">
				<g:field type="text" id="name" name="name" value="${ name }" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label for="tsFile" class="col-sm-4 control-label">Time-Series File</label>
			<div class="col-sm-8">
				<g:field type="file" id="tsFile" name="tsFile" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label for="published" class="col-sm-4 control-label">Make data set public?</label>
			<div class="col-sm-8">
				<g:checkBox id="published" value="${published }" name="published" class="form-control" /> 
			</div>
		</div>
		<g:submitButton name="submit" class="btn btn-primary" />
	</g:uploadForm>
</body>
</html>