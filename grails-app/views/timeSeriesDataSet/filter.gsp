<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<g:if test="${params.popup }">
<meta name="layout" content="main_popup" />
</g:if>
<g:else>
<meta name="layout" content="main" />
</g:else>
</head>
<body>
	<g:content tag="sidebarTitle">
	Filter a Data Set
	</g:content>
	<g:content tag="title">
	<h1>Filter a Data Set</h1>
	</g:content>
	<g:if test="${!params.popup }">
		<g:content tag="sidebarClass">col-lg-3</g:content>
		<g:content tag="mainClass">col-lg-9</g:content>
	</g:if>
	<g:content tag="sidebar">
	Filters can be used to remove unwanted objects from your data set, for example objects that do not change much or are not consistent across replicates. 
	</g:content>
	
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static"  data-keyboard="false">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel"></h4>
		      </div> <!-- /modal-header -->
		      <div class="modal-body">
		      <asset:image src="ajax-loader.gif"/> 
		      </div> <!-- /modal-body -->
		  </div> <!-- /modal-content -->
		</div> <!-- /modal-dialog -->
	</div> <!-- /modal -->
	
	<g:uploadForm action="filter" params="[step: 2]" class="form-horizontal">
		<g:if test="${params.popup }">
			<g:hiddenField name="popup" value="1"/>
		</g:if>
		<div class="panel panel-default">
		  <div class="panel-heading">Inputs</div>
			  <div class="panel-body">
			    <div class="form-group">
					<label for="tsDataSet" class="col-sm-3 control-label">Time-Series Data Set</label>
					<div class="col-sm-6">
						<g:select from="${dataSets }" optionKey="id" optionValue="name" value="${params.id }" class="form-control" name="tsDataSet" />
					</div>
				</div>
		  	</div>
		</div>
		
		<div class="panel panel-default">
		  <div class="panel-heading">Parameters</div>
			  <div class="panel-body">
					<div class="form-group">
						<label for="name" class="col-sm-3 control-label">Name for Filtered Data Set</label>
						<div class="col-sm-6">
							<g:field type="text" id="name" name="name" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="dataFilter" class="col-sm-3 control-label">Data Filter</label>
						<div class="col-sm-6">
							<g:select name="dataFilter" from="${dataFilters }" optionKey="id" optionValue="name" class="form-control" />
						</div>
					</div>
		  	</div>
		</div>
		
		<!-- <div class="panel panel-default">
		  <div class="panel-heading">Filter Parameters</div>
			  <div class="panel-body">
					<div class="form-group">
						<label for="clusteringMethod" class="col-sm-3 control-label">Supplementary Clustering Method</label>
						<div class="col-sm-6">
							<g:select name="clusteringMethod" from="${clusteringMethods }" optionKey="id" optionValue="name" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="steps" class="col-sm-3 control-label">Discretization Steps</label>
						<div class="col-sm-6">
							<g:field type="number" min="1" max="100" value="10"
									id="steps" name="steps" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="simFunction" class="col-sm-3 control-label">Similarity Function</label>
						<div class="col-sm-6">
							<g:select name="simFunction" from="${simFunctions }" optionKey="id" optionValue="name" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="patternRefinementFunction" class="col-sm-3 control-label">Pattern Refinement Function</label>
						<div class="col-sm-6">
							<g:select name="patternRefinementFunction" from="${refFunctions }" optionKey="id" optionValue="name" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="randomSeed" class="col-sm-3 control-label">Random Seed</label>
						<div class="col-sm-6">
							<g:field type="number" value="1" id="randomSeed" name="randomSeed" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="permutations" class="col-sm-3 control-label">Number Permutations for p-value calculation</label>
						<div class="col-sm-6">
							<g:field type="number" value="1000" id="permutations" name="permutations" class="form-control" />
						</div>
					</div>
				</div>
			</div> -->
		<g:submitButton name="next" />
	</g:uploadForm>
</body>
</html>