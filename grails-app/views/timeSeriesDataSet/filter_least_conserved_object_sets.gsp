<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
<script>
$(document).ready(function() {
	$('form').submit(function(event) {
		show_modal('modal', 'Filter Data Set', 'Filtering of data set in progress ...');
	});
});

</script>
</head>
<body>
	<g:content tag="sidebarTitle">
	TODO
	</g:content>
	<g:content tag="title">
	<h1>Remove Least Conserved Object Sets</h1>
	</g:content>
	<g:content tag="sidebarClass">col-lg-3</g:content>
	<g:content tag="mainClass">col-lg-9</g:content>
	<g:content tag="sidebar">
	TODO
	</g:content>
	
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static"  data-keyboard="false">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel"></h4>
		      </div> <!-- /modal-header -->
		      <div class="modal-body">
		      <asset:image src="ajax-loader.gif"/> 
		      </div> <!-- /modal-body -->
		  </div> <!-- /modal-content -->
		</div> <!-- /modal-dialog -->
	</div> <!-- /modal -->
	
	<g:uploadForm action="do_filter" class="form-horizontal">
		<g:if test="${params.popup }">
			<g:hiddenField name="popup" value="1"/>
		</g:if>
		<div class="panel panel-default">
		  <div class="panel-heading">Filter Parameters</div>
			  <div class="panel-body">
					<div class="form-group">
						<label for="percent" class="col-sm-3 control-label">Percent of Objects to Remove</label>
						<div class="col-sm-6">
							<g:field type="number" min="1" max="100" value="10"
									id="percent" name="percent" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="simFunction" class="col-sm-3 control-label">Similarity Function</label>
						<div class="col-sm-6">
							<g:select name="simFunction" from="${simFunctions }" optionKey="id" optionValue="name" class="form-control" />
							<g:hiddenField name="name" value="${ params.name }" />
							<g:hiddenField name="dataFilter" value="${ params.dataFilter }" />
							<g:hiddenField name="tsDataSet" value="${ params.tsDataSet }" />
						</div>
					</div>
				</div>
			</div>
		<g:submitButton name="Submit" />
	</g:uploadForm>
</body>
</html>