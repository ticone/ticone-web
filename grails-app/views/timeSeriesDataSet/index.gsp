<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="sidebarTitle">
	Data Sets
	</g:content>
	<g:content tag="title">
	<h1>Data Sets</h1>
	</g:content>
	<g:content tag="sidebarClass">col-lg-3</g:content>
	<g:content tag="mainClass">col-lg-9</g:content>
	<g:content tag="sidebar">
	<p>Here you see a list of all data sets you have access to.</p>
	</g:content>
	
	<g:link action="create">Upload a new data set here</g:link>
	<h2>Available Data Sets</h2>
	
	<table class="table">
		<thead>
			<tr>
			<th>Name</th>
			<th># Objects</th>
			<th># Samples</th>
			<th># Time Points</th>
			<th>Date</th>
			<th></th>
			</tr>
		</thead>
	<g:each in="${dataSets}" var="${ds}">
		<tr>
			<td>${ds.name }</td>
			<td>${ds.objects.size() }</td>
			<td>${ds.objects?.find {true}.samples.size()}</td>
			<td>${ds.objects?.find {true}.samples?.find { true }.timepoints.size()}</td>
			<td><g:formatDate date="${ds.dateCreated }" /></td>
			<td><g:link action="filter" id="${ds.id}" >Filter this data set</g:link></td>
		</tr>
	</g:each>
	</table>
</body>
</html>