<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta name="layout" content="${layout ?: 'main'}">
    </head>
    <body data-page="blog/index">
        <g:content tag="title">News</g:content>
        <g:content tag="sidebarClass">col-lg-3</g:content>
	    <g:content tag="mainClass">col-lg-9</g:content>
        <g:each var="post" in="${posts}">
            <g:render template="/blog/post" model="[post: post]" />
        </g:each>
    </body> 
</html>