<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="preventLayoutResize">true</g:content>
	<g:content tag="sidebarPanelHide">true</g:content>
	<g:content tag="sidebarClass">col-lg-2</g:content>
	<g:content tag="mainClass">col-lg-5</g:content>
	<g:content tag="title">
		<h1>Download TiCoNE</h1>
	</g:content>
	<p class="lead">Apart from this web service, TiCoNE is available as a Cytoscape App and a standalone tool.</p>
	<h2 id="cyto">Cytoscape App</h2>
	<p>Download the Cytoscape App from the Cytoscape Store:</p><a href="http://apps.cytoscape.org/apps/ticone"><img src="http://apps.cytoscape.org/static/common/img/logo.png"/></a>
	<!-- 
	<h2>Standalone Tool</h2>
	<p>Download the Standalone version of TiCoNE from <a href="#">here</a></p>
	 -->
</body>
</html>