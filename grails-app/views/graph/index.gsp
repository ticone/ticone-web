<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<meta name="layout" content="main" />
</head>
<body>
	<g:content tag="sidebarTitle">
	Networks
	</g:content>
	<g:content tag="title">
	<h1>Networks</h1>
	</g:content>
	<g:content tag="sidebarClass">col-lg-3</g:content>
	<g:content tag="mainClass">col-lg-9</g:content>
	<g:content tag="sidebar">
	<p>Here you see a list of all networks you have access to.</p>
	</g:content>
	<g:link action="create">Upload a new network here</g:link>
	<h2>Available Networks</h2>
	<table class="table">
		<thead>
			<tr>
			<th>Name</th>
			<th>Date</th>
			</tr>
		</thead>
	<g:each in="${networks}" var="${net}">
		<tr>
			<td>${net.name }</td>
			<td><g:formatDate date="${net.dateCreated }" /></td>
		</tr>
	</g:each>
	</table>
</body>
</html>