<%@page
	import="org.springframework.web.servlet.tags.form.SelectedValueComparator"%>
<html>
<head>
<g:if test="${params.popup }">
<meta name="layout" content="main_popup" />
</g:if>
<g:else>
<meta name="layout" content="main" />
</g:else>
<script>
$(document).ready(function() {
	$("form").submit(function() {
	  var $form = $(this);
	  var formData = new FormData($(this)[0]);
	  // submit form
	  $.post($form.attr('action'), formData);
	  // return
	  return false;
	});

	<g:if test="${ user == null }">
	$('#published').click(function() {
		$('#login_to_not_publish').modal('show');
		$('#published').prop('checked', false);
	});
	</g:if>
});

</script>
</head>
<body>
	<g:content tag="title">
		<h1>Upload Network</h1>
	</g:content>
	<g:content tag="sidebarTitle">
	Upload your own network
	</g:content>
	<g:content tag="sidebar">
	<p>Your network file should be a <b>tab-separated file</b> of the following format:</p>
	<table class="table table-condensed table-bordered tablesorter-bootstrap">
		<thead>
			<tr>
				<th class="rotate"><div style="display: inline-block;"><span>Object ID 1</span></div></th>
				<th class="rotate"><div style="display: inline-block;"><span>Object ID 2</span></div></th>
			</tr>
		</thead>
		<tr><td>object 1</td><td>object 2</td></tr>
		<tr><td>object 1</td><td>object 4</td></tr>
		<tr><td>...</td><td>...</td></tr>
		<tr><td>object N</td><td>object 7</td></tr>
		<tr><td>object N</td><td>object 13</td></tr>
	</table>
	<ul>
	<li>And each line describes an edge (e.g. interaction) in the network of one object and another.</li>
	</ul>
	</g:content>
	<g:if test="${!params.popup }">
	<g:content tag="sidebarClass">col-lg-3</g:content>
	</g:if>
	<div class="modal fade" id="login_to_not_publish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Private Network</h4>
		      </div> <!-- /modal-header -->
		      <div class="modal-body">
		      	You have to login to set your network public.
		      </div> <!-- /modal-body -->
		  </div> <!-- /modal-content -->
		</div> <!-- /modal-dialog -->
	</div> <!-- /modal -->
	<g:if test="${flash.error}">
	  <div class="alert alert-danger" style="display: block">${flash.error}</div>
	</g:if>
	<g:uploadForm action="async_upload" class="form-horizontal">
		<g:if test="${params.popup }">
			<g:hiddenField name="popup" value="1"/>
		</g:if>
		<div class="form-group">
			<label for="name" class="col-sm-4 control-label">Name for Network</label>
			<div class="col-sm-8">
				<g:field type="text" id="name" name="name" value="${name }" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label for="graphFile" class="col-sm-4 control-label">Network File</label>
			<div class="col-sm-8">
				<g:field type="file" id="graphFile" name="graphFile" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label for="published" class="col-sm-4 control-label">Make data set public?</label>
			<div class="col-sm-8">
				<g:checkBox id="published" value="${published }" name="published" class="form-control" /> 
			</div>
		</div>
		<g:submitButton name="submit" class="btn btn-primary" />
	</g:uploadForm>
</body>
</html>