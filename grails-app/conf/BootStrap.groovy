import groovy.sql.Sql

import java.nio.file.Files

import tsviz_grails.ClusteringMethod
import tsviz_grails.DataSetGroup
import tsviz_grails.Graph
import tsviz_grails.GraphController
import tsviz_grails.GraphFile
import tsviz_grails.NetworkGroup
import tsviz_grails.ObjectBelongsToPattern
import tsviz_grails.PatternRefinementFunction
import tsviz_grails.Role
import tsviz_grails.SimilarityFunction
import tsviz_grails.TimeSeriesDataSet
import tsviz_grails.TimeSeriesDataSetController
import tsviz_grails.TimeSeriesFile
import dk.sdu.imada.ticone.api.AbstractTimeSeriesPreprocessor
import dk.sdu.imada.ticone.io.Parser
import dk.sdu.imada.ticone.preprocessing.calculation.AbsoluteValues
import dk.sdu.imada.ticone.tsdata.TimeSeriesObject


class BootStrap {
	
	static def grailsResourceLocator

	static def dataSourceUnproxied

    def init = { servletContext ->
		def clustMethod = ClusteringMethod.findByName('PAMK')
		if (!clustMethod) {
			new ClusteringMethod(name: 'PAMK', available: true).save()
		}
		clustMethod = ClusteringMethod.findByName('Transitivity Clustering')
		if (!clustMethod) {
			new ClusteringMethod(name: 'Transitivity Clustering', available: true).save()
		} else {
			// remove it for now, as it is buggy
			clustMethod.available = false;
		}
		
		clustMethod = ClusteringMethod.findByName('CLARA')
		if (!clustMethod) {
			new ClusteringMethod(name: 'CLARA', available: true).save()
		}
		clustMethod = ClusteringMethod.findByName('STEM')
		if (!clustMethod) {
			new ClusteringMethod(name: 'STEM', available: true).save()
		}
		
		
		def simFun = SimilarityFunction.findByName('Euclidian Distance')
		if (!simFun) {
			new SimilarityFunction(name: 'Euclidian Distance').save()
		}
		simFun = SimilarityFunction.findByName('Pearson Correlation')
		if (!simFun) {
			new SimilarityFunction(name: 'Pearson Correlation').save()
		}
		
		def refFun = PatternRefinementFunction.findByName('Mean')
		if (!refFun) {
			new PatternRefinementFunction(name: 'Mean').save()
		}
		refFun = PatternRefinementFunction.findByName('Median')
		if (!refFun) {
			new PatternRefinementFunction(name: 'Median').save()
		}
		
		// create roles
		def role = Role.findByName('User')
		if (!role) {
			new Role(name: 'User').save()
		}
		role = Role.findByName('Admin')
		if (!role) {
			new Role(name: 'Admin').save()
		}
		
		// create dataset groups
		def group = DataSetGroup.findByName('User data set')
		if (!group) {
			new DataSetGroup(name: 'User data set').save()
		}
		group = DataSetGroup.findByName('TiCoNE')
		if (!group) {
			group = new DataSetGroup(name: 'TiCoNE')
			group.save()
		}
		
		// insert example data set
		TimeSeriesDataSet exampleDataSet = TimeSeriesDataSet.findByName('Example Time-Series Data')
		if (!exampleDataSet) {
			println "insert example data set"
			File f = grailsResourceLocator.findResourceForURI('workflow_1_time_series_data.tsv').file
			// use library to normalize and preprocess input
			AbstractTimeSeriesPreprocessor calc = new AbsoluteValues();
			Scanner scanner = new Scanner(f);
			List<TimeSeriesObject> tsData = Parser.parseObjectSets(scanner);
			scanner.close();
	
			calc.initializeTimeSeriesData(tsData);
			calc.calculatePatterns();
			
			byte[] fileBytes = Files.readAllBytes(f.toPath());
			TimeSeriesFile tsFile = new TimeSeriesFile(tmpFile: fileBytes);
			tsFile.save();
			
			TimeSeriesDataSet dataSet = TimeSeriesDataSetController.parseDataSetFromInputBatch(
				tsFile, 
				"Example Time-Series Data", 
				tsData);

			dataSet.published = true;
			dataSet.group = group;
			dataSet.save(flush: true);
		}
		
		// create network groups
		group = NetworkGroup.findByName('User network')
		if (!group) {
			new NetworkGroup(name: 'User network').save()
		}
		group = NetworkGroup.findByName('BioGRID')
		if (!group) {
			new NetworkGroup(name: 'BioGRID').save()
		}
		group = NetworkGroup.findByName('I2D')
		if (!group) {
			new NetworkGroup(name: 'I2D').save()
		}
		group = NetworkGroup.findByName('TiCoNE')
		if (!group) {
			group = new NetworkGroup(name: 'TiCoNE')
			group.save()
		}
		
		// insert example network
		Graph exampleGraph = Graph.findByName('Example Network File')
		if (!exampleGraph) {
			println "insert example network"
			File f = grailsResourceLocator.findResourceForURI('workflow_1_network.txt').file
			
			byte[] fileBytes = Files.readAllBytes(f.toPath());
			
			GraphFile gFile = new GraphFile(tmpFile: fileBytes);
			gFile.save(flush: true);
			
			println "Creating graph instance from graph input"
			long start = System.currentTimeMillis();
			Graph g = GraphController.loadGraphBatchFromFile(gFile.tmpFile, 'Example Network File');
			
			g.published = true;
			g.group = group;
			g.save(flush: true);
		}

		// create custom index with "special" order
		if (!ObjectBelongsToPattern.count()) { // new database
			createIndexes()
		}
		
    }
    def destroy = {
    }

    private void createIndexes() {
        Sql sql = Sql.newInstance(dataSourceUnproxied)
        sql.execute("create unique index Idx4 on object_belongs_to_pattern(clustering_id,pattern_id,object_id);")
    }
}
