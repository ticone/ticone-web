import java.util.concurrent.Executors

// Place your Spring DSL code here
beans = {
	//dimanaApi20(tsviz_grails.DimanaApi20) {
	//	grailsApplication = ref('grailsApplication')
	//	// or use 'autowire'
	//}

	executorService( grails.plugin.executor.PersistenceContextExecutorWrapper ) { bean->
		bean.destroyMethod = 'destroy'
		persistenceInterceptor = ref("persistenceInterceptor")
		// make a config variable
		executor = Executors.newFixedThreadPool(10)
	}
}