class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
		
		"/timeSeriesClusteringVisualization/show/$id/$pattern"(controller: "timeSeriesClusteringVisualization", action: "show")
		"/timeSeriesClusteringVisualization/show_vivagraph/$id/$pattern"(controller: "timeSeriesClusteringVisualization", action: "show_vivagraph")
		"/timeSeriesClusteringVisualization/save_layout/$id/$name"(controller: "timeSeriesClusteringVisualization", action: "save_layout")
		"/timeSeriesClusteringVisualization/change_layout/$id/$layoutId"(controller: "timeSeriesClusteringVisualization", action: "change_layout")
		"/timeSeriesClusteringVisualization/set_keep_pattern/$id/$patternId/$keep"(controller: "timeSeriesClusteringVisualization", action: "set_keep_pattern")
		"/resource/$path**"(controller: 'script', action: 'parse')
		
		"/iterativeTimeSeriesClustering/show_cytoscape/$id"(controller: "iterativeTimeSeriesClustering", action: "redirect_to_cytoscape")
		"/iterativeTimeSeriesClustering/show_cytoscape/$id/$iteration/$pattern?"(controller: "iterativeTimeSeriesClustering", action: "show_cytoscape")
		"/iterativeTimeSeriesClustering/show_vivagraph/$id/$iteration"(controller: "iterativeTimeSeriesClustering", action: "show_vivagraph")
		"/iterativeTimeSeriesClustering/do_iteration/$id/$iteration"(controller: "iterativeTimeSeriesClustering", action: "do_iteration")
		"/iterativeTimeSeriesClustering/do_iterations_until_convergence/$id/$iteration"(controller: "iterativeTimeSeriesClustering", action: "do_iterations_until_convergence")
		"/iterativeTimeSeriesClustering/add_pattern/$id/$iteration"(controller: "iterativeTimeSeriesClustering", action: "add_pattern")
		"/iterativeTimeSeriesClustering/delete_pattern/$id/$iteration/$pattern"(controller: "iterativeTimeSeriesClustering", action: "delete_pattern")
		"/iterativeTimeSeriesClustering/delete_pattern_objects/$id/$iteration/$pattern"(controller: "iterativeTimeSeriesClustering", action: "delete_pattern_objects")
		"/iterativeTimeSeriesClustering/delete_pattern_and_objects/$id/$iteration/$pattern"(controller: "iterativeTimeSeriesClustering", action: "delete_pattern_and_objects")
		"/iterativeTimeSeriesClustering/split_pattern_most_dissimilar_objects/$id/$iteration/$pattern"(controller: "iterativeTimeSeriesClustering", action: "split_pattern_most_dissimilar_objects")
		"/iterativeTimeSeriesClustering/split_pattern_clustering/$id/$iteration/$pattern/$numberClusters"(controller: "iterativeTimeSeriesClustering", action: "split_pattern_clustering")
		"/iterativeTimeSeriesClustering/merge_patterns/$id/$iteration/$patterns"(controller: "iterativeTimeSeriesClustering", action: "merge_patterns")
		
		"/iterativeTimeSeriesClustering/show_least_similar_objects/$id/$iteration/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "_show_least_similar_objects")
		"/iterativeTimeSeriesClustering/delete_least_similar_objects/$id/$iteration/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "delete_least_similar_objects")
		
		"/iterativeTimeSeriesClustering/show_object_pattern_similarity_histogram/$id/$iteration/$bins/$type"(controller: "iterativeTimeSeriesClustering", action: "_show_object_pattern_similarity_histogram")
		"/iterativeTimeSeriesClustering/get_object_pattern_similarity_histogram/$id/$iteration/$bins/$type"(controller: "iterativeTimeSeriesClustering", action: "get_object_pattern_similarity_histogram")
		
		"/iterativeTimeSeriesClustering/show_least_similar_objects_pattern/$id/$iteration/$pattern/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "_show_least_similar_objects_pattern")
		"/iterativeTimeSeriesClustering/delete_least_similar_objects_pattern/$id/$iteration/$pattern/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "delete_least_similar_objects_pattern")
		
		"/iterativeTimeSeriesClustering/show_least_conserved_objects/$id/$iteration/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "_show_least_conserved_objects")
		"/iterativeTimeSeriesClustering/delete_least_conserved_objects/$id/$iteration/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "delete_least_conserved_objects")
		
		"/iterativeTimeSeriesClustering/show_pattern_overview/$id/$iteration"(controller: "iterativeTimeSeriesClustering", action: "_show_pattern_overview")
		
		// layouts
		"/iterativeTimeSeriesClustering/do_force_layout/$id/$iteration"(controller: "iterativeTimeSeriesClustering", action: "do_force_layout")
		"/iterativeTimeSeriesClustering/do_circle_layout/$id/$iteration"(controller: "iterativeTimeSeriesClustering", action: "do_circle_layout")
		
		
		// async tasks
		"/iterativeTimeSeriesClustering/async_do_iteration/$id/$iteration"(controller: "iterativeTimeSeriesClustering", action: "async_do_iteration")
		"/iterativeTimeSeriesClustering/async_do_iterations_until_convergence/$id/$iteration"(controller: "iterativeTimeSeriesClustering", action: "async_do_iterations_until_convergence")
		"/iterativeTimeSeriesClustering/async_delete_least_similar_objects/$id/$iteration/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "async_delete_least_similar_objects")
		"/iterativeTimeSeriesClustering/async_delete_least_similar_objects_pattern/$id/$iteration/$pattern/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "async_delete_least_similar_objects_pattern")
		"/iterativeTimeSeriesClustering/async_delete_least_conserved_objects/$id/$iteration/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "async_delete_least_conserved_objects")
		"/iterativeTimeSeriesClustering/async_split_pattern_most_dissimilar_objects/$id/$iteration/$pattern"(controller: "iterativeTimeSeriesClustering", action: "async_split_pattern_most_dissimilar_objects")
		"/iterativeTimeSeriesClustering/async_split_pattern_clustering/$id/$iteration/$pattern/$numberClusters"(controller: "iterativeTimeSeriesClustering", action: "async_split_pattern_clustering")
		
		
		
		// task status
		"/task/show/$id"(controller: "task", action: "show")
		
		
		// user
		"/user/show/$id"(controller: "user", action: "show")
		"/user/change_password/$id"(controller: "user", action: "change_password")
		"/user/change_email/$id"(controller: "user", action: "change_email")
		
		
		//"/tutorial/download_workflow_ts_data/$id"(controller: "tutorial", action: "download_workflow_ts_data")
		"/tutorial/show_workflow/$id"(controller: "tutorial", action: "show_workflow")
		"/tutorial/show_workflow/$id/$ver"(controller: "tutorial", action: "show_workflow")
		
		// JSON
		"/iterativeTimeSeriesClustering/get_least_similar_objects_as_JSON/$id/$iteration/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "get_least_similar_objects")
		"/iterativeTimeSeriesClustering/get_least_conserved_objects_as_JSON/$id/$iteration/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "get_least_conserved_objects")
		"/iterativeTimeSeriesClustering/get_pattern_least_similar_objects_as_JSON/$id/$iteration/$pattern/$percentage/$type"(controller: "iterativeTimeSeriesClustering", action: "get_pattern_least_similar_objects")
		"/iterativeTimeSeriesClustering/getClusteringAsJSON/$id/$iteration"(controller: "iterativeTimeSeriesClustering", action: "getClusteringAsJSON")
		"/timeSeriesClustering/getPatternTimeSeriesObjectsAsJSON/$id/$pattern"(controller: "timeSeriesClustering", action: "getPatternTimeSeriesObjectsAsJSON")
		"/timeSeriesClustering/getClusteringAsJSON/$id"(controller: "timeSeriesClustering", action: "getClusteringAsJSON")
		"/timeSeriesClusteringVisualization/submitClusteringToKPMWeb/$id/$k/$n/$nodesNotPresent/$pattern_id"(controller: "timeSeriesClusteringVisualization", action: "submitClusteringToKPMWeb")
		"/timeSeriesClusteringVisualization/waitForKPMRun"(controller: "timeSeriesClusteringVisualization", action: "waitForKPMRun")
		
		// FreeChart
		"/timeSeriesClustering/getPatternTimeSeriesObjectsAsFreeChart/$id/$pattern"(controller: "timeSeriesClustering", action: "getPatternTimeSeriesObjectsAsFreeChart")
		
		"/iterativeTimeSeriesClustering/sendClusteringToTis2ma/$id"(controller: "iterativeTimeSeriesClustering", action: "sendClusteringToTis2ma")
		"/timeSeriesClustering/sendClusteringToTis2ma/$id"(controller: "timeSeriesClustering", action: "sendClusteringToTis2ma")
		
        "/"(controller: 'welcome', action: "index")
        "500"(view:'/error')
	}
}
