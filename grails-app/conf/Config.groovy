
// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination

// The ACCEPT header will not be used for content negotiation for user agents containing the following strings (defaults to the 4 major rendering engines)
grails.mime.disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
grails.mime.types = [ // the first one is the default format
    all:           '*/*', // 'all' maps to '*' or the first available format in withFormat
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    hal:           ['application/hal+json','application/hal+xml'],
    xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// Legacy setting for codec used to encode data with ${}
grails.views.default.codec = "html"

// The default scope for controllers. May be prototype, session or singleton.
// If unspecified, controllers are prototype scoped.
grails.controllers.defaultScope = 'singleton'

// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'html' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        // filteringCodecForContentType.'text/html' = 'html'
    }
}


grails.converters.encoding = "UTF-8"
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

// configure passing transaction's read-only attribute to Hibernate session, queries and criterias
// set "singleSession = false" OSIV mode in hibernate configuration after enabling
grails.hibernate.pass.readonly = false
// configure passing read-only to OSIV session by default, requires "singleSession = false" OSIV mode
grails.hibernate.osiv.readonly = false

environments {
    development {
        grails.logging.jul.usebridge = true
        //grails.serverURL = "http://localhost:8080/ticone"
    }
    production {
        grails.logging.jul.usebridge = false
		
		grails.serverURL = "https://ticone.compbio.sdu.dk"
    }
}

// log4j configuration
log4j.main = {
    // Example of changing the log pattern for the default console appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}

    error  'org.codehaus.groovy.grails.web.servlet',        // controllers
           'org.codehaus.groovy.grails.web.pages',          // GSP
           'org.codehaus.groovy.grails.web.sitemesh',       // layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping',        // URL mapping
           'org.codehaus.groovy.grails.commons',            // core / classloading
           'org.codehaus.groovy.grails.plugins',            // plugins
           'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'
}

// tsviz_grails specific settings
environments {
	development {
		// TODO: rename to dimana
		tis2ma {
			host = "http://localhost"
			port = 8000
		}
		
		grails.assets.minifyJs = false
		
		authorizationUri = 'http://localhost:8000/o/authorize/?client_id=%s&response_type=code'
		tokenAccessPoint = 'http://localhost:8000/o/token/'
		
		oauth {
			providers {
				dimana {
					api = tsviz_grails.DimanaApi20
					key = 'm46YxPKQpk4jDPNo7OoyR3ofU7aQWaiOJ7jxNKfR'
					secret = 'ijWXxoFn4TSSnD5AhGVh0HgDPgiE4UUIC9ykRHaPjsRBi1Er35NxbzCMhb5V6LQv2yvzyu10U2qx2FrRKP0zCXzcLOwgff5vMddxeFbBARelLB0LBbacdBtXDWJ2AXNN'
					//successUri = '/dimana/authorize/success'
					callback = "http://localhost:8080/TiCoNE/oauth/dimana/callback/"
					successUri = '/your/success/page'
					failureUri = '/your/failure/page'
				}
			}
			debug = true
		}
	}
	test {
		// TODO: rename to dimana
		tis2ma {
			host = "http://dimana.compbio.sdu.dk"
			port = 80
		}
		
		//grails.assets.minifyJs = true
		grails.assets.minifyJs = false
		
		authorizationUri = 'http://dimana.compbio.sdu.dk/o/authorize/?client_id=%s&response_type=code'
		tokenAccessPoint = 'http://dimana.compbio.sdu.dk/o/token/'
		
		oauth {
			providers {
				dimana {
					api = tsviz_grails.DimanaApi20
					key = 'm46YxPKQpk4jDPNo7OoyR3ofU7aQWaiOJ7jxNKfR'
					secret = 'ijWXxoFn4TSSnD5AhGVh0HgDPgiE4UUIC9ykRHaPjsRBi1Er35NxbzCMhb5V6LQv2yvzyu10U2qx2FrRKP0zCXzcLOwgff5vMddxeFbBARelLB0LBbacdBtXDWJ2AXNN'
					//successUri = '/dimana/authorize/success'
					callback = "http://tomcat.compbio.sdu.dk/tsviz_grails-0.3.2/oauth/dimana/callback/"
					successUri = '/your/success/page'
					failureUri = '/your/failure/page'
				}
			}
		}
	}
	production {
		// TODO: rename to dimana
		tis2ma {
			host = "http://dimana.compbio.sdu.dk"
			port = 80
		}
		
		//grails.assets.minifyJs = true
		grails.assets.minifyJs = false
		
		authorizationUri = 'http://dimana.compbio.sdu.dk/o/authorize/?client_id=%s&response_type=code'
		tokenAccessPoint = 'http://dimana.compbio.sdu.dk/o/token/'
		
		oauth {
			providers {
				dimana {
					api = tsviz_grails.DimanaApi20
					key = 'm46YxPKQpk4jDPNo7OoyR3ofU7aQWaiOJ7jxNKfR'
					secret = 'ijWXxoFn4TSSnD5AhGVh0HgDPgiE4UUIC9ykRHaPjsRBi1Er35NxbzCMhb5V6LQv2yvzyu10U2qx2FrRKP0zCXzcLOwgff5vMddxeFbBARelLB0LBbacdBtXDWJ2AXNN'
					//successUri = '/dimana/authorize/success'
					callback = "https://ticone.compbio.sdu.dk/oauth/dimana/callback/"
					successUri = '/your/success/page'
					failureUri = '/your/failure/page'
				}
			}
		}
	}
}
application.name = TiCoNE
grails.plugin.seed.autoSeed=true


kpm {
	host = "https://keypathwayminer.compbio.sdu.dk"
	port = 443
	submit_url = '/keypathwayminer/requests/submit'
	submit_async_url = '/keypathwayminer/requests/submitAsync'
	get_networks_url = '/keypathwayminer/rest/availableNetworks'
	status_url = '/keypathwayminer/requests/runStatus'
	run_result = '/keypathwayminer/results/seeResults'
}

biogrid {
	access_key = "c38feae304bfe294a3591845484cb900"
}

// minifyJS breaks blobs of cytoscape.js cose-bilkent layout
grails.assets.minifyJs = false
//grails.app.context = "/ticone"
security.shiro.authc.required = false

grails {
	mail {
	  host = "localhost"
	  port = 25
	}
 }


grails {
	jesque {
		workers {
			ticoneWorkerPool {
				workers = 5 //defaults to 1
				queueNames = 'ticoneQueue' //or a list
				//jobTypes = [(DoIterationJob.simpleName):DoIterationJob]
			}
		}
	}
}

spud {
	blog {
		blogEnabled = false
		newsEnabled = true
		blogMapping = '/blog'
		newsMapping = '/news'
		newsLayout = 'main' //Base Layout to use
		blogLayout = 'main' //Base Layout to use
		blogName = 'Spud Blog' //Used for rss and atom feed meta
		blogDescription = 'Spud blog Description' //Used for rss and atom feed meta
		newsName = 'TiCoNE News' //Used for rss and atom feed meta
		newsDescription = 'TiCoNE News' //Used for rss and atom feed meta
	}
}

xssSanitizer.enabled = true 
